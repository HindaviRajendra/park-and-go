package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class TransactionReq {

    /**
     * order_id : 1
     */

    @SerializedName("order_id")
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
