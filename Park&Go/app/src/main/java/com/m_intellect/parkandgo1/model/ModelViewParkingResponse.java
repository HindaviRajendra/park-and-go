package com.m_intellect.parkandgo1.model;


public class ModelViewParkingResponse {

    private static ModelViewParkingResponse modelViewParkingResponse;

    public SetterViewParkingResponse setterViewParkingResponse;

    private ModelViewParkingResponse() {

    }

    public static ModelViewParkingResponse getInstance() {
        if (modelViewParkingResponse == null) {
            modelViewParkingResponse = new ModelViewParkingResponse();
        }
        return modelViewParkingResponse;
    }

    public SetterViewParkingResponse getSetterViewParkingResponse() {
        return setterViewParkingResponse;
    }

    public void setSetterViewParkingResponse(SetterViewParkingResponse setterViewParkingResponse) {
        this.setterViewParkingResponse = setterViewParkingResponse;
    }
}
