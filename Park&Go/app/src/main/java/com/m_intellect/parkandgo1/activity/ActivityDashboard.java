package com.m_intellect.parkandgo1.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterParkingList;
import com.m_intellect.parkandgo1.model.SingletonContractorParkingList;
import com.m_intellect.parkandgo1.model.ViewparkClass;
import com.m_intellect.parkandgo1.model.Viewparkresponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.COLL_TOTAL;

public class ActivityDashboard extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public String TAG = getClass().getSimpleName();
    TextView total;
    List<Viewparkresponse.DataBean> list = new ArrayList<>();
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences preferences;
    private ScrollView mainLayout;
    private TextView title1TV, addressTV, vacantTV, viewDetailsTV, Total_collection,
            totalSpaceTV;
    private TextView generalSeatsTV;
    private TextView reservedSeatsTV;
    private TextView gzerotooneTV;
    private TextView gtwotothreeTV;
    private TextView gthreetosixTV;
    private TextView gsixtotwelveTV;
    private TextView rzerotooneTV;
    private TextView rtwotothreeTV;
    private TextView rthreetosixTV;
    private TextView rsixtotwelveTV;
    private RecyclerView parkingListRV;
    private AdapterParkingList adapterParkingList;
    private FloatingActionButton fab;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String total_coll;
    private int totol = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initToolbar();
        initView();

        setupDrawerContent(navigationView);

        if (Utility.isNetworkAvailable(this)) {
            getParkingList();
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }

    }

    private void getParkingList() {
        SingletonContractorParkingList.getInstance().reset();
        new WebServiceGet(this, new HandlerParkingListResponse(), Constants.VIEW_MOBILE_PARK + preferences.getInt(Constants.USER_ID, 0)).execute();
        Log.e(TAG, "URL   ::::   " + Constants.VIEW_MOBILE_PARK + preferences.getInt(Constants.USER_ID, 0));

    }

    private void initView() {
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        mainLayout = (ScrollView) findViewById(R.id.mainLayout);
        mainLayout.setVisibility(View.GONE);

        title1TV = (TextView) findViewById(R.id.title1TV);
        addressTV = (TextView) findViewById(R.id.addressTV);
        totalSpaceTV = (TextView) findViewById(R.id.totalSpaceTV);
        vacantTV = (TextView) findViewById(R.id.vacantTV);
        viewDetailsTV = (TextView) findViewById(R.id.viewDetailsTV);

        generalSeatsTV = (TextView) findViewById(R.id.generalSeatsTV);
        reservedSeatsTV = (TextView) findViewById(R.id.reservedSeatsTV);

        gzerotooneTV = (TextView) findViewById(R.id.gzerotooneTV);
        gtwotothreeTV = (TextView) findViewById(R.id.gtwotothreeTV);
        gthreetosixTV = (TextView) findViewById(R.id.gthreetosixTV);
        gsixtotwelveTV = (TextView) findViewById(R.id.gsixtotwelveTV);

        rzerotooneTV = (TextView) findViewById(R.id.rzerotooneTV);
        rtwotothreeTV = (TextView) findViewById(R.id.rtwotothreeTV);
        rthreetosixTV = (TextView) findViewById(R.id.rthreetosixTV);
        rsixtotwelveTV = (TextView) findViewById(R.id.rsixtotwelveTV);

        parkingListRV = (RecyclerView) findViewById(R.id.parkingListRV);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout dashboardLayout = (RelativeLayout) toolbar.findViewById(R.id.dashboardLayout);
        dashboardLayout.setVisibility(View.VISIBLE);

        ImageView drawerDashboardIV = (ImageView) toolbar.findViewById(R.id.drawerDashboardIV);
        drawerDashboardIV.setOnClickListener(this);

        TextView titleTV = (TextView) toolbar.findViewById(R.id.txt_text);
        titleTV.setText("My Parking lot");

        Total_collection = (TextView) toolbar.findViewById(R.id.tv_collection);
        Total_collection.setText("₹ " + COLL_TOTAL);
    }

    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.findViewById(R.id.my_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityDashboard.this, MyProfile.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

        navigationView.findViewById(R.id.dashboardTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
            }
        });

        navigationView.findViewById(R.id.logoutTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.ISLOGIN, false);
                editor.apply();

                Intent intent = new Intent(ActivityDashboard.this, ActivityMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                drawerLayout.closeDrawers();
            }
        });
    }

    @Override
    public void onRefresh() {
        if (Utility.isNetworkAvailable(this)) {
            swipeRefreshLayout.setRefreshing(true);
            getParkingList();
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            if (Utility.isNetworkAvailable(this)) {
                getParkingList();
            } else {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        }
    }    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerDashboardIV:
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
                break;

            case R.id.fab:
                Intent intent = new Intent(this, ActivityNewParking.class);
                //intent.putExtra(Constants.FROM_EDIT, false);
                startActivityForResult(intent, 111);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private class HandlerParkingListResponse extends Handler {
        int sum = 0;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            if (response != null) {
                ViewparkClass viewparkClass = new Gson().fromJson(response, ViewparkClass.class);
                if (viewparkClass.getStatus() == 200) {
                    swipeRefreshLayout.setRefreshing(false);
                    adapterParkingList = new AdapterParkingList(ActivityDashboard.this, viewparkClass.getModelParkingResponseList(), true);
                    for (int i = 0; i < viewparkClass.getModelParkingResponseList().size(); i++) {
                        if (viewparkClass.getModelParkingResponseList().get(i).getTotal_collection() != null) {
                            total_coll = viewparkClass.getModelParkingResponseList().get(i).getTotal_collection();
                            sum += Integer.parseInt(total_coll);
                            Total_collection.setText("₹ " + sum);
                        } else {
                            totol = 0;
                        }
                    }
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityDashboard.this);
                    parkingListRV.setLayoutManager(mLayoutManager);
                    parkingListRV.setItemAnimator(new DefaultItemAnimator());
                    parkingListRV.setAdapter(adapterParkingList);
                    Total_collection.setText("");
                    Log.e(TAG, "SUM :::::::" + sum);
                    Total_collection.setText("");
                    Total_collection.setText("₹ " + sum);
                    adapterParkingList.notifyDataSetChanged();

                } else {
                    Toast.makeText(ActivityDashboard.this, viewparkClass.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }




}
