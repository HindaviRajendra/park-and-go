package com.m_intellect.parkandgo1.dialogfragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityDashboard;
import com.m_intellect.parkandgo1.activity.ActivityLogin;
import com.m_intellect.parkandgo1.activity.ActivityMain;
import com.m_intellect.parkandgo1.model.ModelLogin;
import com.m_intellect.parkandgo1.model.ModelLoginResponse;
import com.m_intellect.parkandgo1.model.ModelValidateOTP;
import com.m_intellect.parkandgo1.model.ModelValidateOTPResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

/**
 * Created by sanket on 1/14/2018.
 */

public class DialogFragmentOTP extends DialogFragment {


    private int userID;
    private EditText otpET;
    public String TAG = getClass().getSimpleName();
    SharedPreferences preferences;
    String Mobno, Password, Customer;
    Double latitude, longitude;

    public DialogFragmentOTP() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_dialog_fragment_otp, container, false);
        preferences = getActivity().getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        Constants.GCM_ID = preferences.getString("GCMID", "");


        if (getArguments() != null) {
            userID = getArguments().getInt(Constants.USER_ID, 0);
            Mobno = getArguments().getString("mobno", "");
            Password = getArguments().getString("pass", "");
            latitude = getArguments().getDouble("lat", 0.0);
            longitude = getArguments().getDouble("lang", 0.0);
            Customer = getArguments().getString("cust", "");
        }

        otpET = (EditText) view.findViewById(R.id.otpET);
        getDialog().setCanceledOnTouchOutside(false);

        view.findViewById(R.id.okTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otpET.getText().toString().length() == 0) {
                    otpET.setError(getString(R.string.error_otp));
                } else {
                    otpET.setError(null);
                    validateOTP();
                }
            }
        });


        view.findViewById(R.id.resendTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTP();
            }
        });
        return view;
    }

    private void resendOTP() {
        new WebServiceGet(getActivity(), new HandlerResendOTPResponse(), Constants.RESEND_OTP_URL + userID).execute();
        Log.e(TAG, "OTP ::: " + Constants.RESEND_OTP_URL + userID);

    }

    private class HandlerResendOTPResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelValidateOTPResponse modelValidateOTPResponse = new Gson().fromJson(response, ModelValidateOTPResponse.class);
                Toast.makeText(getActivity(), modelValidateOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void validateOTP() {

        ModelValidateOTP modelValidateOTP = new ModelValidateOTP();
        modelValidateOTP.setId(userID);
        modelValidateOTP.setAppId(Constants.APP_ID);
        modelValidateOTP.setOtp(otpET.getText().toString());
        modelValidateOTP.setMailNoti(1);

        Gson gson = new Gson();
        String payload = gson.toJson(modelValidateOTP);
        Log.e("test", "payload: " + payload);
        if (Customer.equals("Customer")) {
            new WebServicePost(getActivity(), new HandlerValidateOTPResponse(), Constants.VALIDATE_OTP_URL1, payload).execute();
        } else {
            new WebServicePost(getActivity(), new HandlerValidateOTPResponse(), Constants.VALIDATE_OTP_URL, payload).execute();
        }

        Log.e("test", "payload: " + Constants.VALIDATE_OTP_URL1);
    }

    private class HandlerValidateOTPResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelValidateOTPResponse modelValidateOTPResponse = new Gson().fromJson(response, ModelValidateOTPResponse.class);
                if (modelValidateOTPResponse.getStatus() == 200) {
                    if (modelValidateOTPResponse.getData() != null) {
                        if (modelValidateOTPResponse.getData().get(0).getValidateotp() == 1) {
                            if (!Customer.equals("Customer")) {
                                Intent intent1 = new Intent(getActivity(), ActivityLogin.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                                Toast.makeText(getActivity(), "Your request has been submitted successfully ,within 24Hrs our team will contact you then you can login", Toast.LENGTH_LONG).show();
                            } else {
                                login();
                                Constants.Cancelstatus = true;
                            }
                        } else {
                            Toast.makeText(getContext(), "Please check Your OTP", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "Please check Your OTP", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void login() {
        String Str_token = FirebaseInstanceId.getInstance().getToken();
        ModelLogin modelLogin = new ModelLogin();
        modelLogin.setMobileNo(Mobno);
        modelLogin.setPassword(Password);
        modelLogin.setAppId(Constants.APP_ID);
        modelLogin.setLatitude(String.valueOf(latitude));
        modelLogin.setLongitude(String.valueOf(longitude));
        modelLogin.setFcm_id(Str_token);

        Gson gson = new Gson();
        String payload = gson.toJson(modelLogin);
        Log.e("test", "payload: " + payload);
        Log.e("URL ", "payload: " + Constants.LOGIN_URL);
        new WebServicePost(getContext(), new HandlerLoginResponse(), Constants.LOGIN_URL, payload).execute();

    }

    private class HandlerLoginResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelLoginResponse modelLoginResponse = new Gson().fromJson(response, ModelLoginResponse.class);
                if (modelLoginResponse.getStatus() == 200) {
                    if (modelLoginResponse.getModelLoginDataResponseList().size() > 0) {
                        // getDialog().dismiss();
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(Constants.ISLOGIN, true);
                        editor.putInt(Constants.USER_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getUserId());
                        editor.putString(Constants.USER_TYPE, modelLoginResponse.getModelLoginDataResponseList().get(0).getUsertype());
                        editor.putString(Constants.EMAIL_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getEmail());
                        editor.putString(Constants.MOBILE_NO, modelLoginResponse.getModelLoginDataResponseList().get(0).getMobileNo());
                        editor.putString("VH_NO", modelLoginResponse.getModelLoginDataResponseList().get(0).getVehicle_no());
                        if (modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() != null)
                            editor.putInt(Constants.PARKING_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId());
                        editor.apply();

                        if (modelLoginResponse.getModelLoginDataResponseList().get(0).getUsertype().equalsIgnoreCase("customer")) {
                            Intent intent = new Intent(getActivity(), ActivityMain.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            if (modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() == null ||
                                    modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() == 0) {
                                //getDialog().dismiss();
                                Toast.makeText(getActivity(), "Your request has been submitted successfully ,within 24Hrs our team will contact you then you can login", Toast.LENGTH_LONG).show();
                                Intent intent1 = new Intent(getActivity(), ActivityLogin.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);

                            } else {
                                Intent intent1 = new Intent(getActivity(), ActivityDashboard.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "Invalid email id or password ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // getDialog().dismiss();
                    //  Toast.makeText(getActivity(), "your request has been submitted successfully ,within 24 Hrs our team will contact you then you can login", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }


}