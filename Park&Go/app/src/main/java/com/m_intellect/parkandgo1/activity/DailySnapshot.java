package com.m_intellect.parkandgo1.activity;


import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.DailysnapAdapter;
import com.m_intellect.parkandgo1.model.ViewCollectionClass;
import com.m_intellect.parkandgo1.model.ViewTotalClass;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;

public class DailySnapshot extends AppCompatActivity implements View.OnClickListener {


    private int parkid;
    TextView tv_total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.snapshot);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        Bundle b = getIntent().getExtras();
        if (b != null) {
            parkid = b.getInt("parkID");
        }

        initToolbar();
        tv_total = (TextView) findViewById(R.id.sum);
        viewcollection();
        viewtotal();

    }

    private void viewtotal() {
        new WebServiceGet(this, new HandlerdateListResponse1(), Constants.VIEW_TOTAL_COLL + parkid).execute();
        Log.e("TAG", "URL>>>" + Constants.VIEW_TOTAL_COLL + parkid);
    }

    private class HandlerdateListResponse1 extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            ProgressDialog dialog = new ProgressDialog(DailySnapshot.this);
            dialog.setCancelable(false);
            dialog.setTitle("Please Wait");
            dialog.show();

            String response = (String) msg.obj;
            Log.d("Response>>>", "response: " + response);

            if (response != null) {
                ViewTotalClass viewdatesconfigClass = new Gson().fromJson(response, ViewTotalClass.class);

                if (viewdatesconfigClass.getStatus() == 200) {
                    try {
                        dialog.cancel();
                        tv_total.setText("Total :" + viewdatesconfigClass.getModelAttendantList().get(0).getTotal());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(DailySnapshot.this, "DailySnapshot are not Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);

        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("DailySnapShot");


        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

        ImageView backArrowIV1 = (ImageView) toolbar.findViewById(R.id.searchIV);
        backArrowIV1.setVisibility(View.GONE);

    }

    private void viewcollection() {
        new WebServiceGet(this, new HandlerdateListResponse(), Constants.VIEW_SEVENDAY_COLL + parkid).execute();
        Log.e("TAG", "URL>>>" + Constants.VIEW_SEVENDAY_COLL + parkid);
    }

    private class HandlerdateListResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("Response>>>", "response: " + response);

            if (response != null) {
                ViewCollectionClass viewdatesconfigClass = new Gson().fromJson(response, ViewCollectionClass.class);

                if (viewdatesconfigClass.getStatus() == 200) {
                    try {
                        RecyclerView rview = (RecyclerView) findViewById(R.id.parkingListRV);
                        rview.setHasFixedSize(true);
                        rview.setLayoutManager(new LinearLayoutManager(DailySnapshot.this));
                        rview.setAdapter(new DailysnapAdapter(viewdatesconfigClass.getModelAttendantList(), getApplicationContext()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backArrowIV:
                onBackPressed();
                break;
        }
    }
}
