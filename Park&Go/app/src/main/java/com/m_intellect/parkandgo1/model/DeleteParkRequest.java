package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

public class DeleteParkRequest {

    /**
     * app_id : 29
     * parking_id : 164
     */

    @SerializedName("app_id")
    private int appId;
    @SerializedName("parking_id")
    private int parkingId;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }
}
