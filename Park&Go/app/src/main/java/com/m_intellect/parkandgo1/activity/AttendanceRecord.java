package com.m_intellect.parkandgo1.activity;

import android.app.DatePickerDialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.RecordsAdapter;
import com.m_intellect.parkandgo1.model.SetterRecordListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Admin on 12-03-2018.
 */

public class AttendanceRecord extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyeclView;
    private RecordsAdapter recordsAdapter;
    private TextView vehicleText, inText, outText, amountText, titleTV;
    private SetterRecordListResponse setterRecordListResponse;
    private int parkID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.att_record);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Bundle b=getIntent().getExtras();
        if (b != null) {
            parkID = getIntent().getExtras().getInt("parkID");
        }
        initToolbar();
        initView();
    }

    private void fetchData(String date) {
        Log.d("test", "in fetchData");
        if (Utility.isNetworkAvailable(this)) {
            getList(date);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getList(String date) {
        date = Utility.formatDateFromOnetoAnother(date, "dd/MM/yyyy", "yyyy-MM-dd");
        String url = Constants.VIEW_ATTENDANCE_RECORD + "/" + date + "/" + parkID;
        Log.e("test", "url: " + url);
        new WebServiceGet(this, new HandlerGetRecordList(), url).execute();
    }

    private class HandlerGetRecordList extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {

                if (response != null) {
                    setterRecordListResponse = new Gson().fromJson(response, SetterRecordListResponse.class);

                    if (setterRecordListResponse.getSuccess()) {

                        if (setterRecordListResponse.getData() != null) {
                            recyeclView.setVisibility(View.VISIBLE);
                            recordsAdapter = new RecordsAdapter(AttendanceRecord.this, setterRecordListResponse.getData());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AttendanceRecord.this);
                            recyeclView.setLayoutManager(mLayoutManager);
                            recyeclView.setItemAnimator(new DefaultItemAnimator());
                            recyeclView.setAdapter(recordsAdapter);
                        } else {
                            recyeclView.setVisibility(View.GONE);
                            Toast.makeText(AttendanceRecord.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        recyeclView.setVisibility(View.GONE);
                        Toast.makeText(AttendanceRecord.this, setterRecordListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    recyeclView.setVisibility(View.GONE);
                    Toast.makeText(AttendanceRecord.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }//handler

    private void initView() {

        recyeclView = (RecyclerView) findViewById(R.id.recyeclView);
        vehicleText = (TextView) findViewById(R.id.vehicleText);
        inText = (TextView) findViewById(R.id.inText);
        outText = (TextView) findViewById(R.id.outText);
        amountText = (TextView) findViewById(R.id.amountText);

//        vehicleText.setTypeface(Utility.getTypeFace(this));
//        inText.setTypeface(Utility.getTypeFace(this));
//        outText.setTypeface(Utility.getTypeFace(this));
//        amountText.setTypeface(Utility.getTypeFace(this));


    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        //LinearLayout filterLayout = (LinearLayout) toolbar.findViewById(R.id.filterLayout);


        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setImageResource(R.drawable.backarrow);
        ImageView search = (ImageView) toolbar.findViewById(R.id.searchIV);
        search.setVisibility(View.GONE);

        backArrowIV.setOnClickListener(this);
        titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
        String date = df3.format(c.getTime());
        //  titleTV.setTypeface(Utility.getTypeFace(this));
        titleTV.setText("Records On " + date);

//        ImageView logoutImage = (ImageView) toolbar.findViewById(R.id.logoutImage);
//        logoutImage.setImageResource(R.drawable.ic_calender_22dp);

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(year, month, dayOfMonth);
                titleTV.setText("Records On " + (dateFormatter.format(calendar1.getTime())));
                fetchData(dateFormatter.format(calendar1.getTime()));
            }
        }, year, month, day);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//
//        logoutImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                datePickerDialog.show();
//            }
//        });

        fetchData(date);


    }

//    private void changeDate() {
//        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//        Calendar calendar = Calendar.getInstance();
//        int year = calendar.get(Calendar.YEAR);
//        int month = calendar.get(Calendar.MONTH);
//        int day = calendar.get(Calendar.DAY_OF_MONTH);
//
//        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                Calendar calendar1 = Calendar.getInstance();
//                calendar1.set(year, month, dayOfMonth);
//                titleTV.setText("Records On "+(dateFormatter.format(calendar1.getTime())));
//            }
//        }, year, month, day);
//        datePickerDialog.setTitle("Select Date");
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backArrowIV:
                onBackPressed();
                break;
            default:
                break;
        }
    }

}
