package com.m_intellect.parkandgo1.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.dialogfragment.DialogFragmentOTP;
import com.m_intellect.parkandgo1.model.DeleteuserReq;
import com.m_intellect.parkandgo1.model.ModelSignUp;
import com.m_intellect.parkandgo1.model.ModelSignUpResponse;
import com.m_intellect.parkandgo1.model.ViewversionResponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivitySignUp extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnDismissListener {

    public String VehicleNo, locationName;
    SharedPreferences preferences;
    private EditText nameET;
    private EditText emailET;
    private EditText mobileNoET;
    private EditText vehicleNameET;
    private EditText vehicleNoET;
    private EditText cityET;
    private EditText stateET;
    private EditText passwordET;
    private EditText edit_1, edit_2, edit_3, edit_4, edit_5, edit_6, edit_7, edit_8, edit_9, edit_10, edit_addr;
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private String latitude1;
    private String longitude1;
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        Constants.GCM_ID = preferences.getString("GCMID", "");
        if (getIntent().getExtras() != null) {
            latitude = getIntent().getDoubleExtra(Constants.LATITUDE, 0.0);
            longitude = getIntent().getDoubleExtra(Constants.LONGITUDE, 0.0);
        }
        initToolbar();
        initView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }

    private void initView() {

        nameET = (EditText) findViewById(R.id.nameET);
        mobileNoET = (EditText) findViewById(R.id.mobileNoET);
        vehicleNameET = (EditText) findViewById(R.id.vehicleNameET);
        cityET = (EditText) findViewById(R.id.cityET);
        stateET = (EditText) findViewById(R.id.stateET);
        passwordET = (EditText) findViewById(R.id.passwordET);

        TextView signUpTV = (TextView) findViewById(R.id.signUpTV);
        signUpTV.setOnClickListener(this);

        edit_1 = (EditText) findViewById(R.id.pin_one_edittext);
        edit_2 = (EditText) findViewById(R.id.pin_two_edittext);
        edit_3 = (EditText) findViewById(R.id.pin_three_edittext);
        edit_4 = (EditText) findViewById(R.id.pin_four_edittext);
        edit_5 = (EditText) findViewById(R.id.pin_five_edittext);
        edit_6 = (EditText) findViewById(R.id.pin_six_edittext);
        edit_7 = (EditText) findViewById(R.id.pin_sev_edittext);
        edit_8 = (EditText) findViewById(R.id.pin_eight_edittext);
        edit_9 = (EditText) findViewById(R.id.pin_nine_edittext);
        edit_10 = (EditText) findViewById(R.id.pin_ten_edittext);
        edit_addr = (EditText) findViewById(R.id.editaddr);
        edit_addr.setOnClickListener(this);

        addTextListner();

    }

    private void addTextListner() {

        edit_1.requestFocus();
        edit_1.setCursorVisible(true);
        edit_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_1.getText().toString().length() == 1) {
                    edit_2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_2.getText().toString().length() == 1) {
                    edit_3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_2.getText().toString().length() == 0) {
                    edit_1.requestFocus();
                }
            }
        });

        edit_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_3.getText().toString().length() == 1) {
                    edit_4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_3.getText().toString().length() == 0) {
                    edit_2.requestFocus();
                }
            }
        });


        edit_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_4.getText().toString().length() == 1) {
                    edit_5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_4.getText().toString().length() == 0) {
                    edit_3.requestFocus();
                }
            }
        });


        edit_5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_5.getText().toString().length() == 1) {
                    edit_6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_5.getText().toString().length() == 0) {
                    edit_4.requestFocus();
                }
            }
        });


        edit_6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_6.getText().toString().length() == 1) {
                    edit_7.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_6.getText().toString().length() == 0) {
                    edit_5.requestFocus();
                }
            }
        });


        edit_7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_7.getText().toString().length() == 1) {
                    edit_8.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_7.getText().toString().length() == 0) {
                    edit_6.requestFocus();
                }
            }
        });


        edit_8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_8.getText().toString().length() == 1) {
                    edit_9.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_8.getText().toString().length() == 0) {
                    edit_7.requestFocus();
                }
            }
        });

        edit_9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_9.getText().toString().length() == 1) {
                    edit_10.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_9.getText().toString().length() == 0) {
                    edit_8.requestFocus();
                }
            }
        });

        edit_10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_10.getText().toString().length() == 0) {
                    edit_9.requestFocus();
                }
            }
        });


    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Signup");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;
            case R.id.signUpTV:
                if (Utility.isNetworkAvailable(this)) {
                    if (isValid()) {
                        VehicleNo = edit_1.getText().toString() + "" + edit_2.getText().toString() + "-" + edit_3.getText().toString() + "" + edit_4.getText().toString() + "-" +
                                edit_5.getText().toString() + "" + edit_6.getText().toString() + "-" + edit_7.getText().toString() + "" + edit_8.getText().toString() + edit_9.getText().toString() + "" + edit_10.getText().toString();
                        signUp();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.editaddr:
                if (edit_addr.getText().length() == 0) {
                    Intent intent = new Intent(this, ActivityAddMapLocation.class);
                    overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                    startActivityForResult(intent, 111);
                }

                break;
            default:
                break;
        }
    }

    private boolean isValid() {

        if (nameET.getText().toString().length() == 0) {
            nameET.setError(getString(R.string.error_name));
        } else {
            nameET.setError(null);
        }

        if (mobileNoET.getText().toString().length() == 0) {
            mobileNoET.setError(getString(R.string.error_mobile_no));
        } else {
            mobileNoET.setError(null);
        }
        if (vehicleNameET.getText().toString().length() == 0) {
            vehicleNameET.setError(getString(R.string.error_vehicle_name));
        } else {
            vehicleNameET.setError(null);
        }
        if (cityET.getText().toString().length() == 0) {
            cityET.setError(getString(R.string.error_city));
        } else {
            cityET.setError(null);
        }
        if (stateET.getText().toString().length() == 0) {
            stateET.setError(getString(R.string.error_state));
        } else {
            stateET.setError(null);
        }
        if (passwordET.getText().toString().length() == 0) {
            passwordET.setError(getString(R.string.error_password));
        } else {
            passwordET.setError(null);
        }
        if (edit_addr.getText().toString().length() == 0) {
            edit_addr.setError("Please Enter Address");
        } else {
            edit_addr.setError(null);
        }


        if (edit_1.getText().toString().length() == 0 && edit_2.getText().toString().length() == 0 && edit_3.getText().toString().length() == 0 &&
                edit_4.getText().toString().length() == 0 && edit_5.getText().toString().length() == 0 && edit_6.getText().toString().length() == 0 && edit_7.getText().toString().length() == 0
                && edit_8.getText().toString().length() == 0 && edit_9.getText().toString().length() == 0 && edit_10.getText().toString().length() == 0) {
            Toast.makeText(ActivitySignUp.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
            edit_1.setError("");
            edit_2.setError("");
            edit_3.setError("");
            edit_4.setError("");
            edit_5.setError("");
            edit_6.setError("");
            edit_7.setError("");
            edit_8.setError("");
            edit_9.setError("");
            edit_10.setError("");
        } else {
            edit_1.setError(null);
            edit_2.setError(null);
            edit_3.setError(null);
            edit_4.setError(null);
            edit_5.setError(null);
            edit_6.setError(null);
            edit_7.setError(null);
            edit_8.setError(null);
            edit_9.setError(null);
            edit_10.setError(null);

        }

        return nameET.getError() == null &&
                mobileNoET.getError() == null && vehicleNameET.getError() == null &&
                vehicleNameET.getError() == null && cityET.getError() == null &&

                edit_1.getError() == null && edit_2.getError() == null &&
                edit_3.getError() == null && edit_4.getError() == null &&
                edit_5.getError() == null && edit_6.getError() == null &&
                edit_7.getError() == null && edit_8.getError() == null &&
                edit_9.getError() == null && edit_10.getError() == null &&

                stateET.getError() == null && passwordET.getError() == null && edit_addr.getError() == null;
    }

    private void signUp() {

        ModelSignUp modelSignUp = new ModelSignUp();
        modelSignUp.setId(0);
        modelSignUp.setAppId(Constants.APP_ID);
        modelSignUp.setName(nameET.getText().toString());
        modelSignUp.setEmail("");
        modelSignUp.setMobileNo(mobileNoET.getText().toString());
        modelSignUp.setVehicleName(vehicleNameET.getText().toString());
        modelSignUp.setVehicleNo(VehicleNo);
        modelSignUp.setCity(cityET.getText().toString());
        modelSignUp.setState(stateET.getText().toString());
        modelSignUp.setPassword(passwordET.getText().toString());
        modelSignUp.setLatitude(latitude);
        modelSignUp.setLongitude(longitude);
        modelSignUp.setActiveStatus(0);
        modelSignUp.setAddress(edit_addr.getText().toString());
        String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        modelSignUp.setDeviceId(deviceId);
        modelSignUp.setUserType("customer");

        Gson gson = new Gson();
        String payload = gson.toJson(modelSignUp);
        Log.e("test", "payload: " + payload);
        Log.e("test", "payload: " + Constants.SIGNUP_URL);

        new WebServicePost(this, new HandlerSignUpResponse(), Constants.SIGNUP_URL, payload).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {

            if (data != null) {
                latitude1 = data.getStringExtra(Constants.LATITUDE);
                longitude1 = data.getStringExtra(Constants.LONGITUDE);
                locationName = data.getStringExtra(Constants.LOCATION_NAME);
                if (latitude1 != null && !latitude1.isEmpty() && longitude1 != null && !longitude1.isEmpty()) {

                }
                edit_addr.setText(locationName);
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

//        if (!Constants.Cancelstatus){
//            Log.e("TAG","Dialog true");
//        }else {
//            Log.e("TAG","Dialog dismiss");
//        }

        deleteuser();

    }

    private void deleteuser() {
        DeleteuserReq deletebookingReq = new DeleteuserReq();
        deletebookingReq.setAppId(29);
        deletebookingReq.setMobileNo(mobileNoET.getText().toString());
        Gson gson = new Gson();
        String payload = gson.toJson(deletebookingReq);
        Log.e("test", "payload: " + payload);
        new WebServicePost(ActivitySignUp.this, new Handlerupdate(), Constants.DELETE_USER, payload).execute();
        Log.e(TAG, "URL :: " + Constants.DELETE_USER);


    }

    private class HandlerSignUpResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelSignUpResponse modelSignUpResponse = new Gson().fromJson(response, ModelSignUpResponse.class);
                try {
                    if (modelSignUpResponse.getId() > 0) {
                        if (modelSignUpResponse.getStatus() == 200) {

                            if (modelSignUpResponse.getId() == -1) {
                                Toast.makeText(ActivitySignUp.this, modelSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                DialogFragmentOTP dialogFragmentOTP = new DialogFragmentOTP();
                                Bundle bundle = new Bundle();
                                bundle.putInt(Constants.USER_ID, modelSignUpResponse.getId());
                                bundle.putString("mobno", mobileNoET.getText().toString());
                                bundle.putString("pass", passwordET.getText().toString());
                                bundle.putDouble("lat", latitude);
                                bundle.putDouble("lang", longitude);
                                bundle.putString("cust", "Customer");
                                dialogFragmentOTP.setArguments(bundle);
                                dialogFragmentOTP.show(getSupportFragmentManager(), "OTP");
                            }
                        }

                    } else {
                        Toast.makeText(ActivitySignUp.this, modelSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                //Toast.makeText(ActivitySignUp.this, modelSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class Handlerupdate extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response: " + response);

            if (response != null) {
                ViewversionResponse viewversionResponse = new Gson().fromJson(response, ViewversionResponse.class);
                if (viewversionResponse.getStatus() == 200) {
                    Log.e(TAG, "Success on delete user");
                }
            }
        }
    }
}
