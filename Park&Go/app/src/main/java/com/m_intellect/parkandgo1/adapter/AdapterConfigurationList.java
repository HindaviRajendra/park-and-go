package com.m_intellect.parkandgo1.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityConfigurationList;
import com.m_intellect.parkandgo1.activity.ActivityParkingConfiguration;
import com.m_intellect.parkandgo1.model.ModelConfiguration;
import com.m_intellect.parkandgo1.model.SingletonConfigurationDetail;
import com.m_intellect.parkandgo1.utils.Constants;

import java.util.List;

/**
 * Created by sanket on 1/13/2018.
 */

public class AdapterConfigurationList extends RecyclerView.Adapter<AdapterConfigurationList.ViewHolder> {

    private Context context;
    public List<ModelConfiguration> modelConfigurationList;

    public AdapterConfigurationList(Context context, List<ModelConfiguration> modelConfigurationList) {

        this.context = context;
        this.modelConfigurationList = modelConfigurationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_configuration, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final ModelConfiguration modelConfiguration = modelConfigurationList.get(position);

        holder.titleTV.setText(modelConfiguration.getPlaceName());

        holder.addressTV.setText(modelConfiguration.getPlaceLandmark());
        holder.viewDetailsTV.setTag(modelConfiguration);
        holder.viewDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SingletonConfigurationDetail.getInstance().setModelConfiguration(modelConfiguration);
                Intent intent = new Intent(context, ActivityParkingConfiguration.class);
                intent.putExtra(Constants.FROM_EDIT, true);
                intent.putExtra("Total",modelConfiguration.getTotalspace());
                Log.e("","TOATL BUNDLE SEND>>"+modelConfiguration.getTotalspace());
                context.startActivity(intent);
                ((ActivityConfigurationList) context).startActivityForResult(intent, 111);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelConfigurationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTV, dateTV, addressTV, viewDetailsTV;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            viewDetailsTV = (TextView) itemView.findViewById(R.id.viewDetailsTV);
        }
    }

}
