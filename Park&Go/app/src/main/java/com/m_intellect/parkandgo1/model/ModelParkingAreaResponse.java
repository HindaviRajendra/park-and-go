package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 1/2/2018.
 */

public class ModelParkingAreaResponse {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":10,"bus":10,"car":10,"truck":10}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":5,"bus":5,"car":5,"truck":5}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":30,"bus":10,"car":50,"truck":10}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":2,"bus":2,"car":2,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":1,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":20,"bus":5,"car":30,"truck":5}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":1,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":1,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":1,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":20,"bus":5,"car":30,"truck":5}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":2,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]},{"parking_id":212,"app_id":29,"coordinates":{"x":19.1327663967808,"y":72.8734822198749},"place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","pincode":400093,"mobile_no":"9757425645","email":"saurabh.t@m-intellect.in","general_fare":null,"reserved_fare":null,"parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":2,"vacant":2,"fate_dates":null,"car_status":1,"bike_status":1,"bus_status":1,"truck_status":1,"general":[{"bike":0,"bus":0,"car":2,"truck":0}],"reserved":[{"bike":0,"bus":0,"car":0,"truck":0}],"regular":[{"bike":0,"bus":0,"car":0,"truck":0}],"event":[{"bike":0,"bus":0,"car":0,"truck":0}],"premium":[{"bike":0,"bus":0,"car":0,"truck":0}]}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_id : 212
         * app_id : 29
         * coordinates : {"x":19.1327663967808,"y":72.8734822198749}
         * place_name : Mahakali Caves
         * place_landmark : WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India
         * city : Mumbai
         * state : Maharashtra
         * pincode : 400093
         * mobile_no : 9757425645
         * email : saurabh.t@m-intellect.in
         * general_fare : null
         * reserved_fare : null
         * parking_type : null
         * start_date : null
         * end_date : null
         * start_time : null
         * end_time : null
         * total_space : 2
         * vacant : 2
         * fate_dates : null
         * car_status : 1
         * bike_status : 1
         * bus_status : 1
         * truck_status : 1
         * general : [{"bike":10,"bus":10,"car":10,"truck":10}]
         * reserved : [{"bike":0,"bus":0,"car":0,"truck":0}]
         * regular : [{"bike":0,"bus":0,"car":0,"truck":0}]
         * event : [{"bike":0,"bus":0,"car":0,"truck":0}]
         * premium : [{"bike":0,"bus":0,"car":0,"truck":0}]
         */

        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("coordinates")
        private CoordinatesBean coordinates;
        @SerializedName("place_name")
        private String placeName;
        @SerializedName("place_landmark")
        private String placeLandmark;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("pincode")
        private int pincode;
        @SerializedName("mobile_no")
        private String mobileNo;
        @SerializedName("email")
        private String email;
        @SerializedName("general_fare")
        private Object generalFare;
        @SerializedName("reserved_fare")
        private Object reservedFare;
        @SerializedName("parking_type")
        private Object parkingType;
        @SerializedName("start_date")
        private Object startDate;
        @SerializedName("end_date")
        private Object endDate;
        @SerializedName("start_time")
        private Object startTime;
        @SerializedName("end_time")
        private Object endTime;
        @SerializedName("total_space")
        private int totalSpace;
        @SerializedName("vacant")
        private int vacant;
        @SerializedName("fate_dates")
        private Object fateDates;
        @SerializedName("car_status")
        private int carStatus;
        @SerializedName("bike_status")
        private int bikeStatus;
        @SerializedName("bus_status")
        private int busStatus;
        @SerializedName("truck_status")
        private int truckStatus;
        @SerializedName("general")
        private List<GeneralBean> general;
        @SerializedName("reserved")
        private List<ReservedBean> reserved;
        @SerializedName("regular")
        private List<RegularBean> regular;
        @SerializedName("event")
        private List<EventBean> event;
        @SerializedName("premium")
        private List<PremiumBean> premium;
        private boolean isChecked;

        private boolean isRadioChecked;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public boolean isRadioChecked() {
            return isRadioChecked;
        }

        public void setRadioChecked(boolean radioChecked) {
            isRadioChecked = radioChecked;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public CoordinatesBean getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(CoordinatesBean coordinates) {
            this.coordinates = coordinates;
        }

        public String getPlaceName() {
            return placeName;
        }

        public void setPlaceName(String placeName) {
            this.placeName = placeName;
        }

        public String getPlaceLandmark() {
            return placeLandmark;
        }

        public void setPlaceLandmark(String placeLandmark) {
            this.placeLandmark = placeLandmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getGeneralFare() {
            return generalFare;
        }

        public void setGeneralFare(Object generalFare) {
            this.generalFare = generalFare;
        }

        public Object getReservedFare() {
            return reservedFare;
        }

        public void setReservedFare(Object reservedFare) {
            this.reservedFare = reservedFare;
        }

        public Object getParkingType() {
            return parkingType;
        }

        public void setParkingType(Object parkingType) {
            this.parkingType = parkingType;
        }

        public Object getStartDate() {
            return startDate;
        }

        public void setStartDate(Object startDate) {
            this.startDate = startDate;
        }

        public Object getEndDate() {
            return endDate;
        }

        public void setEndDate(Object endDate) {
            this.endDate = endDate;
        }

        public Object getStartTime() {
            return startTime;
        }

        public void setStartTime(Object startTime) {
            this.startTime = startTime;
        }

        public Object getEndTime() {
            return endTime;
        }

        public void setEndTime(Object endTime) {
            this.endTime = endTime;
        }

        public int getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(int totalSpace) {
            this.totalSpace = totalSpace;
        }

        public int getVacant() {
            return vacant;
        }

        public void setVacant(int vacant) {
            this.vacant = vacant;
        }

        public Object getFateDates() {
            return fateDates;
        }

        public void setFateDates(Object fateDates) {
            this.fateDates = fateDates;
        }

        public int getCarStatus() {
            return carStatus;
        }

        public void setCarStatus(int carStatus) {
            this.carStatus = carStatus;
        }

        public int getBikeStatus() {
            return bikeStatus;
        }

        public void setBikeStatus(int bikeStatus) {
            this.bikeStatus = bikeStatus;
        }

        public int getBusStatus() {
            return busStatus;
        }

        public void setBusStatus(int busStatus) {
            this.busStatus = busStatus;
        }

        public int getTruckStatus() {
            return truckStatus;
        }

        public void setTruckStatus(int truckStatus) {
            this.truckStatus = truckStatus;
        }

        public List<GeneralBean> getGeneral() {
            return general;
        }

        public void setGeneral(List<GeneralBean> general) {
            this.general = general;
        }

        public List<ReservedBean> getReserved() {
            return reserved;
        }

        public void setReserved(List<ReservedBean> reserved) {
            this.reserved = reserved;
        }

        public List<RegularBean> getRegular() {
            return regular;
        }

        public void setRegular(List<RegularBean> regular) {
            this.regular = regular;
        }

        public List<EventBean> getEvent() {
            return event;
        }

        public void setEvent(List<EventBean> event) {
            this.event = event;
        }

        public List<PremiumBean> getPremium() {
            return premium;
        }

        public void setPremium(List<PremiumBean> premium) {
            this.premium = premium;
        }

        public static class CoordinatesBean {
            /**
             * x : 19.1327663967808
             * y : 72.8734822198749
             */

            @SerializedName("x")
            private double x;
            @SerializedName("y")
            private double y;

            public double getX() {
                return x;
            }

            public void setX(double x) {
                this.x = x;
            }

            public double getY() {
                return y;
            }

            public void setY(double y) {
                this.y = y;
            }
        }

        public static class GeneralBean {
            /**
             * bike : 10
             * bus : 10
             * car : 10
             * truck : 10
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class ReservedBean {
            /**
             * bike : 0
             * bus : 0
             * car : 0
             * truck : 0
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class RegularBean {
            /**
             * bike : 0
             * bus : 0
             * car : 0
             * truck : 0
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class EventBean {
            /**
             * bike : 0
             * bus : 0
             * car : 0
             * truck : 0
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class PremiumBean {
            /**
             * bike : 0
             * bus : 0
             * car : 0
             * truck : 0
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }
    }
}

