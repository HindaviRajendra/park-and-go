package com.m_intellect.parkandgo1.activity;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelAddAttendant;
import com.m_intellect.parkandgo1.model.ModelAddAttendantResponse;
import com.m_intellect.parkandgo1.model.SingletonAttendantDetail;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityAddAttendant extends AppCompatActivity implements View.OnClickListener {

    private EditText nameET;
    private EditText mobileET;
    private EditText passwordET;
    private SharedPreferences preferences;
    private boolean fromEdit;
    private int id = 0;
    private String title = "Add Attendant";
    private String TAG = getClass().getSimpleName();
    private int ParkingID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendant);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Bundle b = getIntent().getExtras();


        if (getIntent() != null) {
            fromEdit = getIntent().getBooleanExtra(Constants.FROM_EDIT, false);
            if (b != null) {
                ParkingID = b.getInt("parkID", 0);
                Log.e(TAG, "PARKID>>" + ParkingID);
            }


        }

        initToolbar();
        initView();

        if (fromEdit) {
            nameET.setText(SingletonAttendantDetail.getInstance().getModelAttendant().getName());
            Log.e(TAG, "NAME>>>>" + SingletonAttendantDetail.getInstance().getModelAttendant().getName());
            mobileET.setText(SingletonAttendantDetail.getInstance().getModelAttendant().getMobileNo());
            passwordET.setText(SingletonAttendantDetail.getInstance().getModelAttendant().getPassword());
            id = SingletonAttendantDetail.getInstance().getModelAttendant().getId();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        if (fromEdit)
            title = "Edit Attendant";
        titleTV.setText(title);
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    private void initView() {

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        nameET = (EditText) findViewById(R.id.nameET);
        mobileET = (EditText) findViewById(R.id.mobileET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        TextView submitTV = (TextView) findViewById(R.id.submitTV);
        submitTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
            case R.id.submitTV:
                if (Utility.isNetworkAvailable(this)) {
                    if (isValid()) {
                        addAttendant();
                    }
                } else
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    private boolean isValid() {

        if (nameET.getText().toString().length() == 0) {
            nameET.setError(getString(R.string.error_name));
        } else {
            nameET.setError(null);
        }
        if (mobileET.getText().toString().length() < 10) {
            mobileET.setError(getString(R.string.error_mobile_no));
        } else {
            mobileET.setError(null);
        }
        if (passwordET.getText().toString().length() == 0) {
            passwordET.setError(getString(R.string.error_password));
        } else {
            passwordET.setError(null);
        }

        return nameET.getError() == null && mobileET.getError() == null && passwordET.getError() == null;
    }

    private void addAttendant() {

        ModelAddAttendant modelAddAttendant = new ModelAddAttendant();
        modelAddAttendant.setId(id);
        modelAddAttendant.setAppId(Constants.APP_ID);
        modelAddAttendant.setName(nameET.getText().toString());
        modelAddAttendant.setPassword(passwordET.getText().toString());
        modelAddAttendant.setEmail("");
        modelAddAttendant.setMobileNo(mobileET.getText().toString());
        modelAddAttendant.setActiveStatus(1);
        modelAddAttendant.setUserType("attendant");
        modelAddAttendant.setCity("");
        modelAddAttendant.setState("");
        modelAddAttendant.setTotalSpace(0);
        modelAddAttendant.setVacant(0);
        modelAddAttendant.setLocationId(0);
        modelAddAttendant.setParkingId(ParkingID);
        modelAddAttendant.setLatitude("0");
        modelAddAttendant.setLongitude("0");
        modelAddAttendant.setCreatedBy(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));

        Gson gson = new Gson();
        String payload = gson.toJson(modelAddAttendant);
        Log.e("test", "payload: " + payload);
        Log.e("test", "payload: " + Constants.ADD_ATTENDANT_URL);

        new WebServicePost(this, new HandlerAddAttendantResponse(), Constants.ADD_ATTENDANT_URL, payload).execute();

    }

    private class HandlerAddAttendantResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {
                if (response != null) {
                    ModelAddAttendantResponse modelAddAttendantResponse = new Gson().fromJson(response, ModelAddAttendantResponse.class);
                    if (modelAddAttendantResponse.getStatus() == 200) {
                        if (modelAddAttendantResponse.getMessage().equals("Mobile No Already Exists.")) {
                            Toast.makeText(ActivityAddAttendant.this, modelAddAttendantResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                    Toast.makeText(ActivityAddAttendant.this, modelAddAttendantResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
