package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class WithdrawRequest {

    /**
     * mob_no : 8652319813
     * cust_id : 1040
     * token : b8cd512a-45ab-44af-a40f-3b9e10102300
     * order_id : 1627
     * tax_amt : 10
     */

    @SerializedName("mob_no")
    private String mobNo;
    @SerializedName("cust_id")
    private String custId;
    @SerializedName("token")
    private String token;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("tax_amt")
    private String taxAmt;

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(String taxAmt) {
        this.taxAmt = taxAmt;
    }
}
