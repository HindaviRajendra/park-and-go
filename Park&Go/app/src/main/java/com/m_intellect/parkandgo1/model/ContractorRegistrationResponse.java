package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 1/14/2018.
 */

public class ContractorRegistrationResponse {

    @SerializedName("insertparkinguser")
    @Expose
    private Integer insertparkinguser;

    public Integer getInsertparkinguser() {
        return insertparkinguser;
    }

    public void setInsertparkinguser(Integer insertparkinguser) {
        this.insertparkinguser = insertparkinguser;
    }

}

