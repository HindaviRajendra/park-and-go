
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class ModelAddAttendantResponse {


    /**
     * Status : 200
     * Success : true
     * Message : Attendant Registration Successfully.
     * Id : 348
     * Data : {}
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Id")
    private int Id;
    @SerializedName("Data")
    private DataBean Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public static class DataBean {
    }
}
