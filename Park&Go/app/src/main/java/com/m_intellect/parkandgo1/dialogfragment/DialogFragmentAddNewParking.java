package com.m_intellect.parkandgo1.dialogfragment;


import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivitySearchRegularParking;
import com.m_intellect.parkandgo1.model.SingletonRegularParkingDetail;
import com.m_intellect.parkandgo1.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentAddNewParking extends DialogFragment {


    private Calendar calendar = Calendar.getInstance();
    private Calendar calendar1 = Calendar.getInstance();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    private TextView startDateTV;
    private TextView endDateTV;
    private TextView goTV;
    private TextView dailyTV;
    private TextView weeklyTV;
    private TextView monthlyTV;
    private TextView startTimeTV;
    private TextView endTimeTV;
    private AutoCompleteTextView searchACTV;

    public DialogFragmentAddNewParking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_dialog_fragment_add_new_parking, container, false);

        initView(view);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, SingletonRegularParkingDetail.getInstance().getPlaceNameList());
        searchACTV.setThreshold(1);
        searchACTV.setAdapter(arrayAdapter);

        goTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dailyTV.isSelected() || weeklyTV.isSelected() || monthlyTV.isSelected()) {
                    if (searchACTV.getText().length() != 0) {
                        SetDataForAPI();
                    } else {
                        Toast.makeText(getActivity(), "Please enter place name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select parking type", Toast.LENGTH_SHORT).show();
                }


            }
        });
        dailyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isSelected())
                    v.setSelected(false);
                else
                    v.setSelected(true);
            }
        });
        weeklyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isSelected())
                    v.setSelected(false);
                else
                    v.setSelected(true);
            }
        });
        monthlyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isSelected())
                    v.setSelected(false);
                else
                    v.setSelected(true);
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDateTV.setText(simpleDateFormat.format(calendar.getTime()));
            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar1.set(Calendar.YEAR, year);
                calendar1.set(Calendar.MONTH, monthOfYear);
                calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDateTV.setText(simpleDateFormat.format(calendar1.getTime()));
            }

        };


        startDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        endDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog endDatePickerDialog = new DatePickerDialog(getActivity(), date1, calendar1
                        .get(Calendar.YEAR), calendar1.get(Calendar.MONTH),
                        calendar1.get(Calendar.DAY_OF_MONTH));
                endDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                endDatePickerDialog.show();

            }
        });

        startTimeTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String hourString;
                        if (selectedHour < 10)
                            hourString = "0" + selectedHour;
                        else
                            hourString = "" + selectedHour;

                        String minuteSting;
                        if (selectedMinute < 10)
                            minuteSting = "0" + selectedMinute;
                        else
                            minuteSting = "" + selectedMinute;
                        startTimeTV.setText(hourString + ":" + minuteSting);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        endTimeTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hourString;
                        if (selectedHour < 10)
                            hourString = "0" + selectedHour;
                        else
                            hourString = "" + selectedHour;

                        String minuteSting;
                        if (selectedMinute < 10)
                            minuteSting = "0" + selectedMinute;
                        else
                            minuteSting = "" + selectedMinute;
                        endTimeTV.setText(hourString + ":" + minuteSting);

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        return view;
    }

    private void SetDataForAPI() {

        String parkingType = "";

        if (dailyTV.isSelected()) {
            parkingType = "daily";
        }
        if (weeklyTV.isSelected()) {
            parkingType = "weekly";
        }
        if (monthlyTV.isSelected()) {
            parkingType = "monthly";
        }
        if (dailyTV.isSelected() && weeklyTV.isSelected()) {
            parkingType = "daily_weekly";
        }
        if (dailyTV.isSelected() && monthlyTV.isSelected()) {
            parkingType = "daily_monthly";
        }
        if (weeklyTV.isSelected() && monthlyTV.isSelected()) {
            parkingType = "weekly_monthly";
        }
        if (dailyTV.isSelected() && weeklyTV.isSelected() && monthlyTV.isSelected()) {
            parkingType = "daily_weekly_monthly";
        }

        String startDate = Utility.formatDateFromOnetoAnother(startDateTV.getText().toString(), "dd/MM/yyyy", "yyyy-dd-MM");
        String endDate = Utility.formatDateFromOnetoAnother(endDateTV.getText().toString(), "dd/MM/yyyy", "yyyy-dd-MM");
        String startTime = startTimeTV.getText().toString();
        String endTime = endTimeTV.getText().toString();

        SingletonRegularParkingDetail.getInstance().setParkingType(parkingType);
        SingletonRegularParkingDetail.getInstance().setStartDate(startDate);
        SingletonRegularParkingDetail.getInstance().setEndDate(endDate);
        SingletonRegularParkingDetail.getInstance().setStartTime(startTime);
        SingletonRegularParkingDetail.getInstance().setEndTime(endTime);
        String splitted[] = searchACTV.getText().toString().split(",");
        SingletonRegularParkingDetail.getInstance().setPlaceName(splitted[1]);

        getDialog().dismiss();

        startActivity(new Intent(getActivity(), ActivitySearchRegularParking.class));
    }

    private void initView(View view) {

        goTV = (TextView) view.findViewById(R.id.goTV);
        dailyTV = (TextView) view.findViewById(R.id.dailyTV);
        weeklyTV = (TextView) view.findViewById(R.id.weeklyTV);
        monthlyTV = (TextView) view.findViewById(R.id.monthlyTV);
        startDateTV = (TextView) view.findViewById(R.id.startDateTV);
        endDateTV = (TextView) view.findViewById(R.id.endDateTV);
        startTimeTV = (TextView) view.findViewById(R.id.startTimeTV);
        endTimeTV = (TextView) view.findViewById(R.id.endTimeTV);
        searchACTV = (AutoCompleteTextView) view.findViewById(R.id.searchACTV);

        Calendar calendar = Calendar.getInstance();
        startDateTV.setText(simpleDateFormat.format(calendar.getTime()));
        endDateTV.setText(simpleDateFormat.format(calendar.getTime()));

        startTimeTV.setText(simpleDateFormat1.format(calendar.getTime()));
        endTimeTV.setText(simpleDateFormat1.format(calendar.getTime()));

    }

    public void show(FragmentManager fragmentManager, String add_aprking) {

    }
}
