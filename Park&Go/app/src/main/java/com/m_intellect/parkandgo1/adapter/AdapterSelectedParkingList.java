package com.m_intellect.parkandgo1.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.RouteActivity;
import com.m_intellect.parkandgo1.model.ModelParkingAreaResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanket on 1/22/2018.
 */

public class AdapterSelectedParkingList extends RecyclerView.Adapter<AdapterSelectedParkingList.ViewHolder> {

    private Context context;
    private List<ModelParkingAreaResponse.DataBean> modelParkingAreaResponseList = new ArrayList<>();
    private Dialog dialog;
    private String TAG = getClass().getSimpleName();

    public AdapterSelectedParkingList(Context context, Dialog dialog, List<ModelParkingAreaResponse.DataBean> modelParkingAreaResponseList) {
        this.context = context;
        this.dialog = dialog;
        this.modelParkingAreaResponseList = modelParkingAreaResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_selected_parking, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ModelParkingAreaResponse.DataBean modelParkingDetailResponse = modelParkingAreaResponseList.get(position);

        holder.titleTV.setText(modelParkingDetailResponse.getPlaceName());
        Log.e(TAG, "Name>>>>>" + modelParkingDetailResponse.getPlaceName());
        holder.addressTV.setText(modelParkingDetailResponse.getPlaceLandmark());
        holder.totalSpaceTV.setText("Total Space - " + modelParkingDetailResponse.getTotalSpace());
        Log.e(TAG, "Name>>>>>" + modelParkingDetailResponse.getPlaceName());
        holder.route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, RouteActivity.class);
                i.putExtra("X", modelParkingDetailResponse.getCoordinates().getX());
                i.putExtra("Y", modelParkingDetailResponse.getCoordinates().getY());
                context.startActivity(i);
            }
        });
        holder.vacantTV.setText("Vacant - " + modelParkingDetailResponse.getVacant());
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return modelParkingAreaResponseList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView titleTV, addressTV, vacantTV, totalSpaceTV;
        private ImageView route;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            totalSpaceTV = (TextView) itemView.findViewById(R.id.totalSpaceTV);
            vacantTV = (TextView) itemView.findViewById(R.id.vacantTV);
            route = (ImageView) itemView.findViewById(R.id.addIV);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            dialog.dismiss();
        }
    }
}