package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelMyProfile;
import com.m_intellect.parkandgo1.model.ModelMyProfileResponse;
import com.m_intellect.parkandgo1.model.SingletonProfileDetail;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityMyProfile extends AppCompatActivity implements View.OnClickListener {

    private EditText mobileET;
    private EditText nameET;
    //  private EditText emailET;
    private EditText passwordET, editcity, editstate, editvno, edit_vName, editAddress;
    private EditText edit_1, edit_2, edit_3, edit_4, edit_5, edit_6, edit_7, edit_8, edit_9, edit_10;

    private SharedPreferences preferences;
    String Vehicleno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initToolbar();
        initView();
        if (Utility.isNetworkAvailable(this))
            getMyProfile();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
    }

    private void initView() {

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);

        mobileET = (EditText) findViewById(R.id.mobileET);
        nameET = (EditText) findViewById(R.id.nameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        editcity = (EditText) findViewById(R.id.edit_city);
        editstate = (EditText) findViewById(R.id.edit_state);
        edit_vName = (EditText) findViewById(R.id.edit_vName);
        editAddress = (EditText) findViewById(R.id.editAddr);

        edit_1 = (EditText) findViewById(R.id.pin_one_edittext);
        edit_2 = (EditText) findViewById(R.id.pin_two_edittext);
        edit_3 = (EditText) findViewById(R.id.pin_three_edittext);
        edit_4 = (EditText) findViewById(R.id.pin_four_edittext);
        edit_5 = (EditText) findViewById(R.id.pin_five_edittext);
        edit_6 = (EditText) findViewById(R.id.pin_six_edittext);
        edit_7 = (EditText) findViewById(R.id.pin_sev_edittext);
        edit_8 = (EditText) findViewById(R.id.pin_eight_edittext);
        edit_9 = (EditText) findViewById(R.id.pin_nine_edittext);
        edit_10 = (EditText) findViewById(R.id.pin_ten_edittext);


    }

    private void getMyProfile() {

        new WebServiceGet(this, new HandlerMyProfileResponse(), Constants.VIEW_PROFILE_URL + preferences.getInt(Constants.USER_ID, 0)).execute();
        Log.e("TAG", "url ::: " + Constants.VIEW_PROFILE_URL + preferences.getInt(Constants.USER_ID, 0));

    }

    private class HandlerMyProfileResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelMyProfileResponse modelMyProfileResponse = new Gson().fromJson(response, ModelMyProfileResponse.class);

                if (modelMyProfileResponse.getStatus() == 200) {
                    SingletonProfileDetail.getInstance().setModelMyProfile(modelMyProfileResponse.getModelMyProfileList().get(0));
                    setData(modelMyProfileResponse.getModelMyProfileList().get(0));

                } else {

                    Toast.makeText(ActivityMyProfile.this, modelMyProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void setData(ModelMyProfile modelMyProfile) {
        Vehicleno = modelMyProfile.getVehicleNo();
        Log.e("TAG","Vehicle NO :: "+Vehicleno);
        mobileET.setText(modelMyProfile.getMobileNo());
        nameET.setText(modelMyProfile.getName());
        passwordET.setText(modelMyProfile.getPassword());
        editcity.setText(modelMyProfile.getCity());
        editstate.setText(modelMyProfile.getState());
        edit_vName.setText(modelMyProfile.getVehicle_name());
        editAddress.setText(modelMyProfile.getAddress());
        try {
            edit_1.setText(""+Vehicleno.charAt(0));
            edit_2.setText(""+Vehicleno.charAt(1));
            edit_3.setText(""+Vehicleno.charAt(3));
            edit_4.setText(""+Vehicleno.charAt(4));
            edit_5.setText(""+Vehicleno.charAt(6));
            edit_6.setText(""+Vehicleno.charAt(7));
            edit_7.setText(""+Vehicleno.charAt(9));
            edit_8.setText(""+Vehicleno.charAt(10));
            edit_9.setText(""+Vehicleno.charAt(11));
            edit_10.setText(""+Vehicleno.charAt(12));

            edit_1.setEnabled(false);
            edit_2.setEnabled(false);
            edit_3.setEnabled(false);
            edit_4.setEnabled(false);
            edit_5.setEnabled(false);
            edit_6.setEnabled(false);
            edit_7.setEnabled(false);
            edit_8.setEnabled(false);
            edit_9.setEnabled(false);
            edit_10.setEnabled(false);


            Vehicleno=edit_1.getText().toString() + "" + edit_2.getText().toString() + "-" + edit_3.getText().toString() + "" + edit_4.getText().toString() + "-" +
                    edit_5.getText().toString() + "" + edit_6.getText().toString() + "-" + edit_7.getText().toString() + "" + edit_8.getText().toString() + edit_9.getText().toString() + "" + edit_10.getText().toString();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setText("Edit");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        locationTV.setLayoutParams(params);
        locationTV.setOnClickListener(this);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("My Profile");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;

            case R.id.locationTV:
                startActivity(new Intent(this, ActivityEditMyProfile.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }
}
