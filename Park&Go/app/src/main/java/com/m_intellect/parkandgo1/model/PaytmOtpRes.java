package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class PaytmOtpRes {


    /**
     * status : SUCCESS
     * message : Otp sent to phone
     * responseCode : 01
     * state : e52151ee-da5b-5072-b9f8-00139c75b3f2
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("responseCode")
    private String responseCode;
    @SerializedName("state")
    private String state;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
