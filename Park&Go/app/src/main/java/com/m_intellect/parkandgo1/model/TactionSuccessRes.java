package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class TactionSuccessRes {


    /**
     * data : {"TXNID":"20190104111212800110168685300128800","BANKTXNID":"6024053","ORDERID":"1","TXNAMOUNT":"10.00","STATUS":"TXN_SUCCESS","TXNTYPE":"SALE","GATEWAYNAME":"WALLET","RESPCODE":"01","RESPMSG":"Txn Success","BANKNAME":"WALLET","MID":"ABRALI53876941570316","PAYMENTMODE":"PPI","REFUNDAMT":"0.00","TXNDATE":"2019-01-04 11:24:24.0"}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * TXNID : 20190104111212800110168685300128800
         * BANKTXNID : 6024053
         * ORDERID : 1
         * TXNAMOUNT : 10.00
         * STATUS : TXN_SUCCESS
         * TXNTYPE : SALE
         * GATEWAYNAME : WALLET
         * RESPCODE : 01
         * RESPMSG : Txn Success
         * BANKNAME : WALLET
         * MID : ABRALI53876941570316
         * PAYMENTMODE : PPI
         * REFUNDAMT : 0.00
         * TXNDATE : 2019-01-04 11:24:24.0
         */

        @SerializedName("TXNID")
        private String TXNID;
        @SerializedName("BANKTXNID")
        private String BANKTXNID;
        @SerializedName("ORDERID")
        private String ORDERID;
        @SerializedName("TXNAMOUNT")
        private String TXNAMOUNT;
        @SerializedName("STATUS")
        private String STATUS;
        @SerializedName("TXNTYPE")
        private String TXNTYPE;
        @SerializedName("GATEWAYNAME")
        private String GATEWAYNAME;
        @SerializedName("RESPCODE")
        private String RESPCODE;
        @SerializedName("RESPMSG")
        private String RESPMSG;
        @SerializedName("BANKNAME")
        private String BANKNAME;
        @SerializedName("MID")
        private String MID;
        @SerializedName("PAYMENTMODE")
        private String PAYMENTMODE;
        @SerializedName("REFUNDAMT")
        private String REFUNDAMT;
        @SerializedName("TXNDATE")
        private String TXNDATE;

        public String getTXNID() {
            return TXNID;
        }

        public void setTXNID(String TXNID) {
            this.TXNID = TXNID;
        }

        public String getBANKTXNID() {
            return BANKTXNID;
        }

        public void setBANKTXNID(String BANKTXNID) {
            this.BANKTXNID = BANKTXNID;
        }

        public String getORDERID() {
            return ORDERID;
        }

        public void setORDERID(String ORDERID) {
            this.ORDERID = ORDERID;
        }

        public String getTXNAMOUNT() {
            return TXNAMOUNT;
        }

        public void setTXNAMOUNT(String TXNAMOUNT) {
            this.TXNAMOUNT = TXNAMOUNT;
        }

        public String getSTATUS() {
            return STATUS;
        }

        public void setSTATUS(String STATUS) {
            this.STATUS = STATUS;
        }

        public String getTXNTYPE() {
            return TXNTYPE;
        }

        public void setTXNTYPE(String TXNTYPE) {
            this.TXNTYPE = TXNTYPE;
        }

        public String getGATEWAYNAME() {
            return GATEWAYNAME;
        }

        public void setGATEWAYNAME(String GATEWAYNAME) {
            this.GATEWAYNAME = GATEWAYNAME;
        }

        public String getRESPCODE() {
            return RESPCODE;
        }

        public void setRESPCODE(String RESPCODE) {
            this.RESPCODE = RESPCODE;
        }

        public String getRESPMSG() {
            return RESPMSG;
        }

        public void setRESPMSG(String RESPMSG) {
            this.RESPMSG = RESPMSG;
        }

        public String getBANKNAME() {
            return BANKNAME;
        }

        public void setBANKNAME(String BANKNAME) {
            this.BANKNAME = BANKNAME;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getPAYMENTMODE() {
            return PAYMENTMODE;
        }

        public void setPAYMENTMODE(String PAYMENTMODE) {
            this.PAYMENTMODE = PAYMENTMODE;
        }

        public String getREFUNDAMT() {
            return REFUNDAMT;
        }

        public void setREFUNDAMT(String REFUNDAMT) {
            this.REFUNDAMT = REFUNDAMT;
        }

        public String getTXNDATE() {
            return TXNDATE;
        }

        public void setTXNDATE(String TXNDATE) {
            this.TXNDATE = TXNDATE;
        }
    }
}
