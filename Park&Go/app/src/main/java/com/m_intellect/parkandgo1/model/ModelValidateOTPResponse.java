
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelValidateOTPResponse {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"validateotp":1}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * validateotp : 1
         */

        @SerializedName("validateotp")
        private int validateotp;

        public int getValidateotp() {
            return validateotp;
        }

        public void setValidateotp(int validateotp) {
            this.validateotp = validateotp;
        }
    }
}
