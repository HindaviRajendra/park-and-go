package com.m_intellect.parkandgo1.activity;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterEventList;
import com.m_intellect.parkandgo1.model.ModelEventListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityEvent extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView eventListRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initToolbar();
        initView();

        if (Utility.isNetworkAvailable(this))
            getEventList();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

    }

    private void getEventList() {

        new WebServiceGet(this, new HandlerEventListResponse(), Constants.VIEW_EVENT_LIST_URL).execute();
    }

    private void initView() {
        eventListRV = (RecyclerView) findViewById(R.id.eventListRV);
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Event");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setImageResource(R.drawable.location);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    private class HandlerEventListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelEventListResponse modelEventListResponse = new Gson().fromJson(response, ModelEventListResponse.class);

                if (modelEventListResponse.getStatus() == 200) {

                    AdapterEventList adapterEventList = new AdapterEventList(ActivityEvent.this, modelEventListResponse.getModelEventResponseList());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityEvent.this);
                    eventListRV.setLayoutManager(mLayoutManager);
                    eventListRV.setItemAnimator(new DefaultItemAnimator());
                    eventListRV.setAdapter(adapterEventList);

                } else {

                    Toast.makeText(ActivityEvent.this, modelEventListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }
}
