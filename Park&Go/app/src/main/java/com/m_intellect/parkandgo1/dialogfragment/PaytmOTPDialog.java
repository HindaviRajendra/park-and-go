package com.m_intellect.parkandgo1.dialogfragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityMain;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.CheckbalanceRes;
import com.m_intellect.parkandgo1.model.CheckminBalanceReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.model.PatymsendOtpReq;
import com.m_intellect.parkandgo1.model.PaytmOtpRes;
import com.m_intellect.parkandgo1.model.TactionSuccessRes;
import com.m_intellect.parkandgo1.model.TransactionReq;
import com.m_intellect.parkandgo1.model.VerifyOTPReq;
import com.m_intellect.parkandgo1.model.VerifyOTPRes;
import com.m_intellect.parkandgo1.model.WithdrawMoneyResponse;
import com.m_intellect.parkandgo1.model.WithdrawRequest;
import com.m_intellect.parkandgo1.network.PaytmPostWebservice;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

@SuppressLint("ValidFragment")
public class PaytmOTPDialog extends android.app.DialogFragment implements View.OnClickListener {
    public String TAG = getClass().getSimpleName();
    public ParkConfigClass parkConfigClass = null;
    EditText et_mobno, et_otp;
    TextView btnsend, btnresend, txt_header, txt_subheader;
    Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String FROMCOME, v_type, vno, isFromOTP, charges;
    private int vacant = 0;

    @SuppressLint("ValidFragment")
    public PaytmOTPDialog(Context context) {
        // Required empty public constructor
        this.context = context;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_paytmotp, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
            FROMCOME = b.getString("fromcome");
            v_type = b.getString("gvtype");
            vno = b.getString("vehicle_no1");
            isFromOTP = b.getString("isFromOTP");
            charges = b.getString("charges");
        }

        preferences = getActivity().getSharedPreferences("ParkAndGo", Context.MODE_PRIVATE);
        et_mobno = (EditText) view.findViewById(R.id.Et_mobno);
        et_otp = (EditText) view.findViewById(R.id.otpET);
        btnsend = (TextView) view.findViewById(R.id.okTV);
        btnsend.setOnClickListener(this);
        btnresend = (TextView) view.findViewById(R.id.resendTV);
        btnresend.setOnClickListener(this);

        txt_header = (TextView) view.findViewById(R.id.txt_header);
        txt_header.setText("Paytm wallet Mobile Number");
        txt_subheader = (TextView) view.findViewById(R.id.txt_subheader);
        txt_subheader.setText("Please enter your Paytm Mobile Number to continue with Payment");

        setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.okTV:
                if (!et_mobno.getText().toString().equals("")) {
                    sendOtp();
                } else {
                    Toast.makeText(getActivity(), "Please Enter your Mobile Number", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.resendTV:
                if (!et_mobno.getText().toString().equals("")) {
                    sendOtp();
                } else {
                    Toast.makeText(getActivity(), "Please Enter your Mobile Number", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    private void sendOtp() {
        PatymsendOtpReq patymsendOtpReq = new PatymsendOtpReq();
        patymsendOtpReq.setClientId("merchant-abralin");
        patymsendOtpReq.setPhone(et_mobno.getText().toString());
        patymsendOtpReq.setResponseType("token");
        patymsendOtpReq.setScope("wallet");

        Gson gson = new Gson();
        String payload = gson.toJson(patymsendOtpReq);
        Log.e(TAG, "URL :: " + Constants.PAYTM_SENDOTP);
        Log.e(TAG, "payload PAY TM : " + payload);
        new WebServicePost(getActivity(), new HandlersendOTP1(), Constants.PAYTM_SENDOTP, payload).execute();

    }

    private void cancelBooking() {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(getActivity(), new Handlecancelbook(), Constants.CANCEL_BOOK1, payload).execute();
        Log.e(TAG, "CancelURL>>>" + payload);
        Log.e(TAG, "CancelURL>>>" + Constants.CANCEL_BOOK1);
    }

    private void showDialog(final String stringtype) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.my_dialog, null, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        Log.e("TAG", "isFundsSufficient true success");
        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);

        ImageView img = (ImageView) dialogView.findViewById(R.id.img);

        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        if (stringtype.equals("success")) {
            txt_text.setText("Success");
            txt_header.setText("You have Booked your Vehicle successfully");
            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
            btnok.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            txt_text.setText("Cancel Booking");
            txt_header.setText("Due to insufficient balance on your paytm wallet. We are not procedding your booking.Please add money to your paytm wallet ");
            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
            btnok.setBackgroundColor(getResources().getColor(R.color.red));
        }


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stringtype.equals("success")) {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("isFromOTP", true);
                    intent.putExtra("gvtype", v_type);
                    intent.putExtra("vehicle_no1", vno);

                    if (FROMCOME.equals("General")) {
                        intent.putExtra("general1", "General");
                    } else {
                        intent.putExtra("general1", "reserved");
                    }
                    getDialog().dismiss();
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    getDialog().dismiss();
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void VerifyOTP() {
        VerifyOTPReq verifyOTPReq = new VerifyOTPReq();
        verifyOTPReq.setOtp(et_otp.getText().toString());
        verifyOTPReq.setState(Constants.STATE);
        Gson gson = new Gson();
        String payload = gson.toJson(verifyOTPReq);
        Log.e(TAG, "Payload PAYTM : " + payload);
        new PaytmPostWebservice(getActivity(), new HandlerVerifyOTP(), Constants.PAYTM_VERIFYOTP, payload).execute();
        Log.e(TAG, "URL :: " + Constants.PAYTM_VERIFYOTP);
    }

    private void Checkminbalance() {
        CheckminBalanceReq checkminBalanceReq = new CheckminBalanceReq();
        checkminBalanceReq.setTaxAmount(charges);
        checkminBalanceReq.setToken(preferences.getString("TOKEN", ""));
        checkminBalanceReq.setBooking_id(Constants.BOOK_ID);
        checkminBalanceReq.setMobile_no(preferences.getString("PAYTMMOB", ""));
        Gson gson = new Gson();
        String payload = gson.toJson(checkminBalanceReq);
        Log.e(TAG, "check balance payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new HandlerMinBalance(), Constants.PAYTM_MINBALANCE, payload).execute();
        Log.e(TAG, "URL :: " + Constants.PAYTM_MINBALANCE);
    }

    private void withdrawmoney() {
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setMobNo(preferences.getString("PAYTMMOB", ""));
        withdrawRequest.setCustId(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));
        withdrawRequest.setToken(preferences.getString("TOKEN", ""));
        withdrawRequest.setOrderId(String.valueOf(Constants.BOOK_ID));
        withdrawRequest.setTaxAmt(charges);
        Gson gson = new Gson();
        String payload = gson.toJson(withdrawRequest);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new Handlerwithdrwamoney(), Constants.PAYTM_WITHDRAW, payload).execute();
    }

    private void transactionStatus() {
        TransactionReq transactionReq = new TransactionReq();
        transactionReq.setOrderId(String.valueOf(Constants.BOOK_ID));
        Gson gson = new Gson();
        String payload = gson.toJson(transactionReq);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new Handlertactionsuccess(), Constants.PAYTM_TRANSACTIONSUCCESS, payload).execute();
    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse) {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setParkingConfigurationId(Constants.Cust_CONFIGURATION_ID);

        if (v_type.equals("Car")) {
            generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1));
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bike")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1);
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bus")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1);
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);


        } else if (v_type.equals("Truck")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1);
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);
        }

        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);


        new WebServicePost(getActivity(), new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    public void vacanT(ParkConfigClass setterViewParkingResponse, String vtype) {

        if (vtype.equals("Car")) {

            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

        } else if (vtype.equals("Bike")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Bus")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Truck")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        }

    }

    public class HandlersendOTP1 extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String jsonresponse = (String) msg.obj;
            Log.e("test", "jsonresponse-----> " + jsonresponse);

            if (jsonresponse != null) {
                PaytmOtpRes paytmOtpRes = new Gson().fromJson(jsonresponse, PaytmOtpRes.class);
                if (paytmOtpRes.getStatus().equals("SUCCESS")) {

                    txt_header.setText("Enter One Time Password (OTP)");
                    txt_subheader.setText("One Time Password (OTP) has been sent to your mobile number. Please enter here to proceed");


                    Constants.STATE = paytmOtpRes.getState();
                    et_otp.setVisibility(View.VISIBLE);
                    Log.e(TAG, "State ::: " + Constants.STATE);
                    preferences.edit();

                    if (!et_otp.getText().toString().equals("")) {
                        VerifyOTP();
                    } else {
                        Toast.makeText(getActivity(), "Please Enter your OTP Number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    cancelBooking();
                }
            } else {
                cancelBooking();
            }
        }
    }

    public class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    showDialog("cancel");
                }
            }
        }
    }

//    private void withdrawmoney() {
//        WithdrawRequest withdrawRequest = new WithdrawRequest();
//        withdrawRequest.setMobNo(preferences.getString("PAYTMMOB",""));
//        withdrawRequest.setCustId(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));
//        withdrawRequest.setToken(Constants.TOKEN);
//        withdrawRequest.setOrderId(String.valueOf(Constants.BOOK_ID));
//        withdrawRequest.setTaxAmt("10");
//        Gson gson = new Gson();
//        String payload = gson.toJson(withdrawRequest);
//        Log.e(TAG, "WithDraw payload PAYTM : " + payload);
//        new WebServicePost(getActivity(), new Handlerwithdrwamoney(), Constants.PAYTM_WITHDRAW, payload).execute();
//    }
//
//    private class Handlerwithdrwamoney extends Handler {
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            String response = (String) msg.obj;
//            Log.e("test", " withdraw response: " + response);
//            if (response != null) {
//                WithdrawMoneyResponse withdrawMoneyResponse = new Gson().fromJson(response, WithdrawMoneyResponse.class);
//                if (withdrawMoneyResponse.getData().getStatus().equals("TXN_SUCCESS")) {
//                    Log.e(TAG, "Withdraw Success :");
//                    transactionStatus();
//                } else {
//                    cancelBooking();
//                }
//            } else {
//                cancelBooking();
//            }
//        }
//    }

    public class HandlerVerifyOTP extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response: " + response);
            try {
                if (response != null) {
                    VerifyOTPRes verifyOTPRes = new Gson().fromJson(response, VerifyOTPRes.class);
                    editor = preferences.edit();
                    editor.putString("TOKEN", verifyOTPRes.getAccessToken());
                    editor.putString("PAYTMMOB", et_mobno.getText().toString());
                    editor.putString("acctoken", verifyOTPRes.getAccessToken());
                    editor.commit();
                    Log.e(TAG, "Verify Token Success ::: " + verifyOTPRes.getAccessToken());
//                    getDialog().dismiss();

                    Checkminbalance();
                } else {
                    cancelBooking();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerMinBalance extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("PAYTM", "Check min balance Response ---->" + response);

            try {
                if (response != null) {
                    CheckbalanceRes checkbalanceRes = new Gson().fromJson(response, CheckbalanceRes.class);
                    if (checkbalanceRes.getData().getBody().getResultInfo().getResultStatus().equals("SUCCESS")) {


                        if (checkbalanceRes.getData().getBody().getResultInfo().getResultMsg().equals("Merchant does not exist.")) {
                            cancelBooking();
                        } else {
                            if (checkbalanceRes.getData().getBody().isFundsSufficient() == false) {
//                                Log.e("TAG", "isFundsSufficient false ");
//                                Intent intent = new Intent(context, ActivityAddMoneyToWallet.class);
//                                intent.putExtra("isFromOTP", true);
//                                intent.putExtra("gvtype", v_type);
//                                intent.putExtra("vehicle_no1", vno);
//
//                                if (FROMCOME.equals("reserved")) {
//                                    intent.putExtra("general1", "reserved");
//                                } else {
//                                    intent.putExtra("general1", "General");
//                                }
//                                context.startActivity(intent);

                                cancelBooking();
                            } else {
                                Log.e("TAG", "isFundsSufficient true outer alertbox");
                                if (FROMCOME.equals("reserved")) {
                                    withdrawmoney();
                                } else {
                                    showDialog("success");
                                }

                            }
                        }
                    } else {
                        cancelBooking();
                    }
                } else {
                    cancelBooking();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class Handlerwithdrwamoney extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " withdraw response: " + response);
            if (response != null) {
                WithdrawMoneyResponse withdrawMoneyResponse = new Gson().fromJson(response, WithdrawMoneyResponse.class);
                if (withdrawMoneyResponse.getData().getStatus().equals("TXN_SUCCESS")) {
                    transactionStatus();
                } else {
                    cancelBooking();
                }
            } else {
                cancelBooking();
            }
        }
    }

    private class Handlertactionsuccess extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "Transaction response: " + response);
            if (response != null) {
                TactionSuccessRes tactionSuccessRes = new Gson().fromJson(response, TactionSuccessRes.class);
                if (tactionSuccessRes.getData().getSTATUS().equals("TXN_SUCCESS")) {
                    Log.e("TAG", "Transaction success");
                    new WebServiceGet(getActivity(), new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null").execute();
                    Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null");

                } else {
                    cancelBooking();
                }
            } else {
                cancelBooking();
            }
        }
    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("VACANT RESPONSE ::", "response: " + response);
            try {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");
                        showDialog("success");
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerparkdetailResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);
            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        Log.d(TAG, "Hour: " + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom());

                        updateVacant(parkConfigClass);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
