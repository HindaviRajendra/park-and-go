package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by sanket on 1/2/2018.
 */

public class ModelMyProfile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("coordinates")
    @Expose
    private Coordinates coordinates;
//    @SerializedName("vehicle_name")
//    @Expose
//    private String vehicleName;
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;

    @SerializedName("active_status")
    @Expose
    private Integer activeStatus;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("wallet_balance")
    @Expose
    private Integer walletBalance;
    @SerializedName("total_space")
    @Expose
    private Object totalSpace;
    @SerializedName("vacant")
    @Expose
    private Object vacant;
    @SerializedName("location_id")
    @Expose
    private Object locationId;
    @SerializedName("parking_id")
    @Expose
    private Object parkingId;

    @SerializedName("vehicle_name")
    @Expose
    private String vehicle_name;

    @SerializedName("address")
    @Expose
    private String address;


    public String getAddress() {
        return address;
    }

    public ModelMyProfile setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public ModelMyProfile setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
//
//    public String getVehicleName() {
//        return vehicleName;
//    }
//
//    public void setVehicleName(String vehicleName) {
//        this.vehicleName = vehicleName;
//    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public Integer getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Integer activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Integer walletBalance) {
        this.walletBalance = walletBalance;
    }

    public Object getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(Object totalSpace) {
        this.totalSpace = totalSpace;
    }

    public Object getVacant() {
        return vacant;
    }

    public void setVacant(Object vacant) {
        this.vacant = vacant;
    }

    public Object getLocationId() {
        return locationId;
    }

    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }

    public Object getParkingId() {
        return parkingId;
    }

    public void setParkingId(Object parkingId) {
        this.parkingId = parkingId;
    }

}

