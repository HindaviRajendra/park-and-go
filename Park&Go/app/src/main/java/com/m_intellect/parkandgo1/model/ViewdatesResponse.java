package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 06-04-2018.
 */

public class ViewdatesResponse {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"calender":"2018-03-26T00:00:00.000Z"},{"calender":"2018-03-27T00:00:00.000Z"},{"calender":"2018-03-28T00:00:00.000Z"},{"calender":"2018-03-26T00:00:00.000Z"},{"calender":"2018-03-27T00:00:00.000Z"},{"calender":"2018-03-28T00:00:00.000Z"},{"calender":"2018-03-26T00:00:00.000Z"},{"calender":"2018-03-27T00:00:00.000Z"},{"calender":"2018-03-28T00:00:00.000Z"},{"calender":"2018-04-12T00:00:00.000Z"},{"calender":"2018-04-13T00:00:00.000Z"},{"calender":"2018-04-20T00:00:00.000Z"}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * calender : 2018-03-26T00:00:00.000Z
         */

        @SerializedName("calender")
        private String calender;

        public String getCalender() {
            return calender;
        }

        public void setCalender(String calender) {
            this.calender = calender;
        }
    }
}
