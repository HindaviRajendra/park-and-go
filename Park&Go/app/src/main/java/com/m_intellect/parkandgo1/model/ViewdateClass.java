package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 06-04-2018.
 */

public class ViewdateClass {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private List<ViewdatesResponse.DataBean> modelParkingResponseList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ViewdatesResponse.DataBean> getModelParkingResponseList() {
        return modelParkingResponseList;
    }

    public void setModelParkingResponseList(List<ViewdatesResponse.DataBean> modelParkingResponseList) {
        this.modelParkingResponseList = modelParkingResponseList;
    }
}
