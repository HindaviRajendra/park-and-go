package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class VerifyOTPRes {


    /**
     * access_token : afa2beb7-321e-441d-ac55-43e5b0862300
     * expires : 1546525053000
     * scope : wallet
     * resourceOwnerId : 9234728337
     */
    @SerializedName("status")
    private String status;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires")
    private String expires;
    @SerializedName("scope")
    private String scope;
    @SerializedName("resourceOwnerId")
    private String resourceOwnerId;

    public String getStatus() {
        return status;
    }

    public VerifyOTPRes setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getResourceOwnerId() {
        return resourceOwnerId;
    }

    public void setResourceOwnerId(String resourceOwnerId) {
        this.resourceOwnerId = resourceOwnerId;
    }
}
