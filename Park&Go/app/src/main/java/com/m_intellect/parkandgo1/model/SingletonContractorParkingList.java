package com.m_intellect.parkandgo1.model;

import java.util.List;

/**
 * Created by sanket on 30-01-2018.
 */

public class SingletonContractorParkingList {

    private static SingletonContractorParkingList singletonContractorParkingList;

    private List<ModelParkingResponse> modelParkingResponseList;

    private SingletonContractorParkingList() {

    }

    public static SingletonContractorParkingList getInstance() {
        if (singletonContractorParkingList == null) {
            singletonContractorParkingList = new SingletonContractorParkingList();
        }
        return singletonContractorParkingList;
    }

    public List<ModelParkingResponse> getModelParkingResponseList() {
        return modelParkingResponseList;
    }

    public void setModelParkingResponseList(List<ModelParkingResponse> modelParkingResponseList) {
        this.modelParkingResponseList = modelParkingResponseList;
    }

    public void reset() {
        singletonContractorParkingList = null;
    }
}
