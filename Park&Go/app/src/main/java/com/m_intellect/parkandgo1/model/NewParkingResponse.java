package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 29-01-2018.
 */

public class NewParkingResponse {

    @SerializedName("insert_parking")
    @Expose
    private Integer insertParking;

    public Integer getInsertParking() {
        return insertParking;
    }

    public void setInsertParking(Integer insertParking) {
        this.insertParking = insertParking;
    }

}

