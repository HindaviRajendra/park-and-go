package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Contractorprofile_response {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"id":383,"app_id":29,"password":"kap","name":"kalpesh","profile_pic":"","email":"","mobile_no":"9604413632","coordinates":{"x":19.1131960508628,"y":72.8903023526073},"vehicle_name":"Hyundai","vehicle_no":"MH-11-HY-5555","active_status":1,"usertype":"customer","city":"Nandurbar","state":"Maharashtra","device_id":"abf683cfe10855ff","wallet_balance":0,"total_space":null,"vacant":null,"location_id":null,"parking_id":null,"created_by":"","address":"5/A, Nalwa Rd, Vedu Govind Nagar, Rupam Nagar, Nandurbar, Maharashtra 425412, India"}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * id : 383
         * app_id : 29
         * password : kap
         * name : kalpesh
         * profile_pic :
         * email :
         * mobile_no : 9604413632
         * coordinates : {"x":19.1131960508628,"y":72.8903023526073}
         * vehicle_name : Hyundai
         * vehicle_no : MH-11-HY-5555
         * active_status : 1
         * usertype : customer
         * city : Nandurbar
         * state : Maharashtra
         * device_id : abf683cfe10855ff
         * wallet_balance : 0
         * total_space : null
         * vacant : null
         * location_id : null
         * parking_id : null
         * created_by :
         * address : 5/A, Nalwa Rd, Vedu Govind Nagar, Rupam Nagar, Nandurbar, Maharashtra 425412, India
         */

        @SerializedName("id")
        private int id;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("password")
        private String password;
        @SerializedName("name")
        private String name;
        @SerializedName("profile_pic")
        private String profilePic;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile_no")
        private String mobileNo;
        @SerializedName("coordinates")
        private CoordinatesBean coordinates;
        @SerializedName("vehicle_name")
        private String vehicleName;
        @SerializedName("vehicle_no")
        private String vehicleNo;
        @SerializedName("active_status")
        private int activeStatus;
        @SerializedName("usertype")
        private String usertype;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("device_id")
        private String deviceId;
        @SerializedName("wallet_balance")
        private int walletBalance;
        @SerializedName("total_space")
        private Object totalSpace;
        @SerializedName("vacant")
        private Object vacant;
        @SerializedName("location_id")
        private Object locationId;
        @SerializedName("parking_id")
        private Object parkingId;
        @SerializedName("created_by")
        private String createdBy;
        @SerializedName("address")
        private String address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public CoordinatesBean getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(CoordinatesBean coordinates) {
            this.coordinates = coordinates;
        }

        public String getVehicleName() {
            return vehicleName;
        }

        public void setVehicleName(String vehicleName) {
            this.vehicleName = vehicleName;
        }

        public String getVehicleNo() {
            return vehicleNo;
        }

        public void setVehicleNo(String vehicleNo) {
            this.vehicleNo = vehicleNo;
        }

        public int getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(int activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public int getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(int walletBalance) {
            this.walletBalance = walletBalance;
        }

        public Object getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(Object totalSpace) {
            this.totalSpace = totalSpace;
        }

        public Object getVacant() {
            return vacant;
        }

        public void setVacant(Object vacant) {
            this.vacant = vacant;
        }

        public Object getLocationId() {
            return locationId;
        }

        public void setLocationId(Object locationId) {
            this.locationId = locationId;
        }

        public Object getParkingId() {
            return parkingId;
        }

        public void setParkingId(Object parkingId) {
            this.parkingId = parkingId;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public static class CoordinatesBean {
            /**
             * x : 19.1131960508628
             * y : 72.8903023526073
             */

            @SerializedName("x")
            private double x;
            @SerializedName("y")
            private double y;

            public double getX() {
                return x;
            }

            public void setX(double x) {
                this.x = x;
            }

            public double getY() {
                return y;
            }

            public void setY(double y) {
                this.y = y;
            }
        }
    }
}
