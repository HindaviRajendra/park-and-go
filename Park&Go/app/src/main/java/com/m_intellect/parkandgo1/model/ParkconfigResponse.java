package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 01-03-2018.
 */

public class ParkconfigResponse {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_configuration_id":29,"app_id":29,"parking_id":64,"user_id":26,"total":"300","general":"98","reserved":"50","regular":"50","event":"50","premium":"50","calender":[{"date":"2018-03-01"}],"start_date":"2018-03-01T00:00:00.000Z","end_date":"2018-03-01T00:00:00.000Z","price_values":[{"price_g":"30","price_p":"50","to":"3","parice_r":"40","from":"1"},{"price_g":"40","price_p":"60","to":"6","parice_r":"50","from":"4"},{"price_g":"50","price_p":"70","to":"9","parice_r":"60","from":"7"},{"price_g":"60","price_p":"80","to":"12","parice_r":"70","from":"10"}],"regular_parking":{"day":{"to":"08:00 AM","price":"50","from":"08:00 AM"},"night":{"to":"08:00 AM","price":"60","from":"08:00 PM"}},"event_parking":{"event3":{"to":"5","price":"50","from":"2"},"event2":{"to":"5","price":"50","from":"2"},"event1":{"to":"5","price":"50","from":"2"}}},{"parking_configuration_id":77,"app_id":29,"parking_id":64,"user_id":26,"total":"300","general":"98","reserved":"50","regular":"50","event":"50","premium":"50","calender":[],"start_date":"2018-03-01T00:00:00.000Z","end_date":"2018-03-02T00:00:00.000Z","price_values":[{"price_g":"","price_p":"","to":"","parice_r":"","from":""},{"price_g":"","price_p":"","to":"","parice_r":"","from":""},{"price_g":"","price_p":"","to":"","parice_r":"","from":""},{"price_g":"","price_p":"","to":"","parice_r":"","from":""}],"regular_parking":{"day":{"to":"(null)","price":"","from":"(null)"},"night":{"to":"(null)","price":"","from":"(null)"}},"event_parking":{"event3":{"to":"","price":"","from":""},"event2":{"to":"","price":"","from":""},"event1":{"to":"","price":"","from":""}}}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_configuration_id : 29
         * app_id : 29
         * parking_id : 64
         * user_id : 26
         * total : 300
         * general : 98
         * reserved : 50
         * regular : 50
         * event : 50
         * premium : 50
         * calender : [{"date":"2018-03-01"}]
         * start_date : 2018-03-01T00:00:00.000Z
         * end_date : 2018-03-01T00:00:00.000Z
         * price_values : [{"price_g":"30","price_p":"50","to":"3","parice_r":"40","from":"1"},{"price_g":"40","price_p":"60","to":"6","parice_r":"50","from":"4"},{"price_g":"50","price_p":"70","to":"9","parice_r":"60","from":"7"},{"price_g":"60","price_p":"80","to":"12","parice_r":"70","from":"10"}]
         * regular_parking : {"day":{"to":"08:00 AM","price":"50","from":"08:00 AM"},"night":{"to":"08:00 AM","price":"60","from":"08:00 PM"}}
         * event_parking : {"event3":{"to":"5","price":"50","from":"2"},"event2":{"to":"5","price":"50","from":"2"},"event1":{"to":"5","price":"50","from":"2"}}
         */

        @SerializedName("parking_configuration_id")
        private int parkingConfigurationId;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("total")
        private String total;
        @SerializedName("general")
        private String general;
        @SerializedName("reserved")
        private String reserved;
        @SerializedName("regular")
        private String regular;
        @SerializedName("event")
        private String event;
        @SerializedName("premium")
        private String premium;
        @SerializedName("start_date")
        private String startDate;
        @SerializedName("end_date")
        private String endDate;
        @SerializedName("regular_parking")
        private RegularParkingBean regularParking;
        @SerializedName("event_parking")
        private EventParkingBean eventParking;
        @SerializedName("calender")
        private List<CalenderBean> calender;
        @SerializedName("price_values")
        private List<PriceValuesBean> priceValues;

        public int getParkingConfigurationId() {
            return parkingConfigurationId;
        }

        public void setParkingConfigurationId(int parkingConfigurationId) {
            this.parkingConfigurationId = parkingConfigurationId;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getGeneral() {
            return general;
        }

        public void setGeneral(String general) {
            this.general = general;
        }

        public String getReserved() {
            return reserved;
        }

        public void setReserved(String reserved) {
            this.reserved = reserved;
        }

        public String getRegular() {
            return regular;
        }

        public void setRegular(String regular) {
            this.regular = regular;
        }

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public String getPremium() {
            return premium;
        }

        public void setPremium(String premium) {
            this.premium = premium;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public RegularParkingBean getRegularParking() {
            return regularParking;
        }

        public void setRegularParking(RegularParkingBean regularParking) {
            this.regularParking = regularParking;
        }

        public EventParkingBean getEventParking() {
            return eventParking;
        }

        public void setEventParking(EventParkingBean eventParking) {
            this.eventParking = eventParking;
        }

        public List<CalenderBean> getCalender() {
            return calender;
        }

        public void setCalender(List<CalenderBean> calender) {
            this.calender = calender;
        }

        public List<PriceValuesBean> getPriceValues() {
            return priceValues;
        }

        public void setPriceValues(List<PriceValuesBean> priceValues) {
            this.priceValues = priceValues;
        }

        public static class RegularParkingBean {
            /**
             * day : {"to":"08:00 AM","price":"50","from":"08:00 AM"}
             * night : {"to":"08:00 AM","price":"60","from":"08:00 PM"}
             */

            @SerializedName("day")
            private DayBean day;
            @SerializedName("night")
            private NightBean night;

            public DayBean getDay() {
                return day;
            }

            public void setDay(DayBean day) {
                this.day = day;
            }

            public NightBean getNight() {
                return night;
            }

            public void setNight(NightBean night) {
                this.night = night;
            }

            public static class DayBean {
                /**
                 * to : 08:00 AM
                 * price : 50
                 * from : 08:00 AM
                 */

                @SerializedName("to")
                private String to;
                @SerializedName("price")
                private String price;
                @SerializedName("from")
                private String from;

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }
            }

            public static class NightBean {
                /**
                 * to : 08:00 AM
                 * price : 60
                 * from : 08:00 PM
                 */

                @SerializedName("to")
                private String to;
                @SerializedName("price")
                private String price;
                @SerializedName("from")
                private String from;

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }
            }
        }

        public static class EventParkingBean {
            /**
             * event3 : {"to":"5","price":"50","from":"2"}
             * event2 : {"to":"5","price":"50","from":"2"}
             * event1 : {"to":"5","price":"50","from":"2"}
             */

            @SerializedName("event3")
            private Event3Bean event3;
            @SerializedName("event2")
            private Event2Bean event2;
            @SerializedName("event1")
            private Event1Bean event1;

            public Event3Bean getEvent3() {
                return event3;
            }

            public void setEvent3(Event3Bean event3) {
                this.event3 = event3;
            }

            public Event2Bean getEvent2() {
                return event2;
            }

            public void setEvent2(Event2Bean event2) {
                this.event2 = event2;
            }

            public Event1Bean getEvent1() {
                return event1;
            }

            public void setEvent1(Event1Bean event1) {
                this.event1 = event1;
            }

            public static class Event3Bean {
                /**
                 * to : 5
                 * price : 50
                 * from : 2
                 */

                @SerializedName("to")
                private String to;
                @SerializedName("price")
                private String price;
                @SerializedName("from")
                private String from;

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }
            }

            public static class Event2Bean {
                /**
                 * to : 5
                 * price : 50
                 * from : 2
                 */

                @SerializedName("to")
                private String to;
                @SerializedName("price")
                private String price;
                @SerializedName("from")
                private String from;

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }
            }

            public static class Event1Bean {
                /**
                 * to : 5
                 * price : 50
                 * from : 2
                 */

                @SerializedName("to")
                private String to;
                @SerializedName("price")
                private String price;
                @SerializedName("from")
                private String from;

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }
            }
        }

        public static class CalenderBean {
            /**
             * date : 2018-03-01
             */

            @SerializedName("date")
            private String date;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }

        public static class PriceValuesBean {
            /**
             * price_g : 30
             * price_p : 50
             * to : 3
             * parice_r : 40
             * from : 1
             */

            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("from")
            private String from;

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }
        }
    }
}
