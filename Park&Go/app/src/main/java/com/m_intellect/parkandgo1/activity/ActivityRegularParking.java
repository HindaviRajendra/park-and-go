package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterRegularParkingList;
import com.m_intellect.parkandgo1.dialogfragment.DialogFragmentAddNewParking;
import com.m_intellect.parkandgo1.model.ModelParkingAreaListResponse;
import com.m_intellect.parkandgo1.model.ModelParkingAreaResponse;
import com.m_intellect.parkandgo1.model.SingletonRegularParkingDetail;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.VIEW_PARKING_AREA_URL;

public class ActivityRegularParking extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView parkingListRV;
    public String TAG = getClass().getSimpleName();
    ModelParkingAreaListResponse modelParkingAreaListResponse;
    public SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regular_parking);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        initView();
        initToolbar();

        if (Utility.isNetworkAvailable(this))
            getParkingList();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
    }

    private void getParkingList() {
        new WebServiceGet(this, new HandlerParkingAreaListResponse(), VIEW_PARKING_AREA_URL+"null"+"/null"+"/null"+"/null").execute();
        Log.e(TAG, "URL" + VIEW_PARKING_AREA_URL+"null"+"/null"+"/null"+"/null");
    }

    public class HandlerParkingAreaListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                modelParkingAreaListResponse = new Gson().fromJson(response, ModelParkingAreaListResponse.class);

                if (modelParkingAreaListResponse.getStatus() == 200) {

                    if (modelParkingAreaListResponse.getModelParkingAreaResponseList() != null) {
                        AdapterRegularParkingList adapterRegularParkingList = new AdapterRegularParkingList(modelParkingAreaListResponse.getModelParkingAreaResponseList());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityRegularParking.this);
                        parkingListRV.setLayoutManager(mLayoutManager);
                        parkingListRV.setItemAnimator(new DefaultItemAnimator());
                        parkingListRV.setAdapter(adapterRegularParkingList);

                        List<String> placeNameList = new ArrayList<>();

                        for (ModelParkingAreaResponse.DataBean modelParkingDetailResponse : modelParkingAreaListResponse.getModelParkingAreaResponseList()) {
                            placeNameList.add(modelParkingDetailResponse.getPlaceName() + "," + modelParkingDetailResponse.getCity());
                        }
                        SingletonRegularParkingDetail.getInstance().setPlaceNameList(placeNameList);
                    }
                } else {
                    Toast.makeText(ActivityRegularParking.this, modelParkingAreaListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initView() {
        parkingListRV = (RecyclerView) findViewById(R.id.parkingListRV);
        TextView addNewParkingTV = (TextView) findViewById(R.id.addNewParkingTV);
        addNewParkingTV.setOnClickListener(this);
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Regular Parking");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;
            case R.id.addNewParkingTV:
                if (preferences.getBoolean(Constants.ISLOGIN, true)) {
                    DialogFragmentAddNewParking dialogFragmentAddNewParking = new DialogFragmentAddNewParking();
                    dialogFragmentAddNewParking.show(getSupportFragmentManager(), "Add Parking Dialog");
                } else {
                    Intent intent = new Intent(ActivityRegularParking.this, ActivityLogin.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }
}
