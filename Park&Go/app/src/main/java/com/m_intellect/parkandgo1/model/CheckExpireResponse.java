package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class CheckExpireResponse {


    /**
     * id : 9234738955
     * email :
     * mobile : 8291080085
     * expires : 1554268883000
     */

    @SerializedName("id")
    private long id;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("expires")
    private String expires;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }
}
