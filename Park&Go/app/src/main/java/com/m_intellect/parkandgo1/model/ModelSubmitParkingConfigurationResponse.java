
package com.m_intellect.parkandgo1.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelSubmitParkingConfigurationResponse {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private List<SubmitParkingConfigurationResponse> submitParkingConfigurationResponseList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubmitParkingConfigurationResponse> getSubmitParkingConfigurationResponseList() {
        return submitParkingConfigurationResponseList;
    }

    public void setSubmitParkingConfigurationResponseList(List<SubmitParkingConfigurationResponse> submitParkingConfigurationResponseList) {
        this.submitParkingConfigurationResponseList = submitParkingConfigurationResponseList;
    }
}
