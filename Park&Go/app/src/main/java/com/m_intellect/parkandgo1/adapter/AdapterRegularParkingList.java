package com.m_intellect.parkandgo1.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelParkingAreaResponse;

import java.util.List;

/**
 * Created by sanket on 12/4/2017.
 */

public class AdapterRegularParkingList extends RecyclerView.Adapter<AdapterRegularParkingList.ViewHolder> {

    private  List<ModelParkingAreaResponse.DataBean> modelParkingDetailResponseList;

    public AdapterRegularParkingList( List<ModelParkingAreaResponse.DataBean> modelParkingDetailResponseList) {

        this.modelParkingDetailResponseList = modelParkingDetailResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_regular_parking, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ModelParkingAreaResponse.DataBean modelParkingDetailResponse = modelParkingDetailResponseList.get(holder.getAdapterPosition());

        holder.radioBtn.setVisibility(View.GONE);
        holder.addressTV.setText(modelParkingDetailResponse.getPlaceLandmark());
        holder.titleTV.setText(modelParkingDetailResponse.getPlaceName());

        //holder.parkingPlanTV.setText(modelParkingDetailResponse.getParkingType());

//        if (modelParkingDetailResponse.getStartDate() != null) {
//            String startDate = modelParkingDetailResponse.getStartDate().substring(0, 10);
//            /*String startTime = " - " + Utility.formatDateFromOnetoAnother(startDate, "yyyy-MM-dd", "dd/MM/yyyy") + "        " +
//                    Utility.formatDateFromOnetoAnother(modelParkingDetailResponse.getStartTime(), "HH:mm:ss", "HH:mm").toLowerCase();
//            */
//            String startTime = " - " + Utility.formatDateFromOnetoAnother(startDate, "yyyy-MM-dd", "dd/MM/yyyy");
//            holder.startTimeTV.setText(startTime);
//        }
//
//        if (modelParkingDetailResponse.getEndDate() != null) {
//            String endDate = modelParkingDetailResponse.getEndDate().substring(0, 10);
//            /*String endTime = " - " + Utility.formatDateFromOnetoAnother(endDate, "yyyy-MM-dd", "dd/MM/yyyy") + "        " +
//                    Utility.formatDateFromOnetoAnother(modelParkingDetailResponse.getEndTime(), "HH:mm:ss", "HH:mm").toLowerCase();*/
//
//            String endTime = " - " + Utility.formatDateFromOnetoAnother(endDate, "yyyy-MM-dd", "dd/MM/yyyy");
//            holder.startTimeTV.setText(endTime);
//        }

    }

    @Override
    public int getItemCount() {
        return modelParkingDetailResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTV, addressTV, parkingPlanTV, startTimeTV, endTimeTV;
        private RadioButton radioBtn;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            parkingPlanTV = (TextView) itemView.findViewById(R.id.parkingPlanTV);
            startTimeTV = (TextView) itemView.findViewById(R.id.startTimeTV);
            endTimeTV = (TextView) itemView.findViewById(R.id.endTimeTV);
            radioBtn = (RadioButton) itemView.findViewById(R.id.radioBtn);

        }
    }
}
