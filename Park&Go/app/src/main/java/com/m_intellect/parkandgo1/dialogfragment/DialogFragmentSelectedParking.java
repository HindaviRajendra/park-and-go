package com.m_intellect.parkandgo1.dialogfragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelParkingAreaListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;

/**
 * Created by sanket on 12/23/2017.
 */

public class DialogFragmentSelectedParking extends DialogFragment {

    // List<ModelParkingAreaResponse.DataBean> modelParkingDetailResponseList1 = new ArrayList<>();
    public String TAG = getClass().getSimpleName();
    ModelParkingAreaListResponse modelParkingAreaListResponse;

    TextView titleTV;
    TextView addressTV;
    TextView totalSpaceTV;
    TextView vacantTV;
    ImageView route;
    ModelParkingAreaListResponse modelParkingDetailListResponse;

    public DialogFragmentSelectedParking() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_list_item_selected_parking, container, false);


        titleTV = (TextView) view.findViewById(R.id.titleTV);
        addressTV = (TextView) view.findViewById(R.id.addressTV);
        totalSpaceTV = (TextView) view.findViewById(R.id.totalSpaceTV);
        vacantTV = (TextView) view.findViewById(R.id.vacantTV);
        Button bntok = (Button) view.findViewById(R.id.btn_ok);
        bntok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        route = (ImageView) view.findViewById(R.id.addIV);
        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri gmmIntentUri = Uri.parse("https://maps.google.com/?daddr=" + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getCoordinates().getX() + "," + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getCoordinates().getY());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

//                Intent i = new Intent(getActivity(), RouteActivity.class);
//                i.putExtra("X", modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getCoordinates().getX());
//                i.putExtra("Y", modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getCoordinates().getY());
//                getContext().startActivity(i);

            }
        });

        getParkingNearToCurrentLocation();
        return view;
    }

    public void getParkingNearToCurrentLocation() {
        new WebServiceGet(getActivity(), new HandlerParkingListResponse(), Constants.VIEW_PARKING_AREA_URL12 + Constants.SELCT_PARKG_ID + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null").execute();
        Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL12 + Constants.SELCT_PARKG_ID + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null");
    }

    private class HandlerParkingListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.e("test", "response: " + response);

            if (response != null) {

                modelParkingDetailListResponse = new Gson().fromJson(response, ModelParkingAreaListResponse.class);

                if (modelParkingDetailListResponse.getStatus() == 200) {
                    try {
                        titleTV.setText(modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getPlaceName());
                        Log.e(TAG, "PLACE NAME>>" + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getPlaceName());
                        addressTV.setText(modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getPlaceLandmark());
                        totalSpaceTV.setText("Total space :" + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getTotalSpace());
                        Log.e(TAG, "SPACE >>" + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getTotalSpace());
                        vacantTV.setText("Vacant :" + modelParkingDetailListResponse.getModelParkingAreaResponseList().get(0).getVacant());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), modelParkingDetailListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}

