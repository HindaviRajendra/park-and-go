package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelAvailbaleConfigResponse {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_configuration_id":493}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_configuration_id : 493
         */

        @SerializedName("parking_configuration_id")
        private int parkingConfigurationId;

        public int getParkingConfigurationId() {
            return parkingConfigurationId;
        }

        public void setParkingConfigurationId(int parkingConfigurationId) {
            this.parkingConfigurationId = parkingConfigurationId;
        }
    }
}
