package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterParkingHistoryList;
import com.m_intellect.parkandgo1.model.ModelParkingHistoryListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityParkingHistory extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView parkingListRV;
    private SharedPreferences preferences;
    public String TAG = getClass().getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_history);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initView();
        initToolbar();

        if (Utility.isNetworkAvailable(this))
            getParkingHistoryList();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getParkingHistoryList();
            }
        });

    }

    public void getParkingHistoryList() {
        new WebServiceGet(this, new HandlerParkingHistoryListResponse(), Constants.PARKING_HISTORY_URL + "/" + preferences.getInt(Constants.USER_ID, 0)).execute();
        Log.e(TAG, "View PARK :::" + Constants.PARKING_HISTORY_URL + "/" + preferences.getInt(Constants.USER_ID, 0));

    }

    private void initView() {
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        parkingListRV = (RecyclerView) findViewById(R.id.parkingListRV);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Parking history");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backArrowIV:
                Intent intent = new Intent(ActivityParkingHistory.this, ActivityMain.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    private class HandlerParkingHistoryListResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelParkingHistoryListResponse modelParkingHistoryListResponse = new Gson().fromJson(response, ModelParkingHistoryListResponse.class);
                if (modelParkingHistoryListResponse.getStatus() == 200) {
                    swipeRefreshLayout.setRefreshing(false);
                    AdapterParkingHistoryList adapterParkingHistoryList = new AdapterParkingHistoryList(ActivityParkingHistory.this, modelParkingHistoryListResponse.getModelParkingHistoryResponseList());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityParkingHistory.this);
                    parkingListRV.setLayoutManager(mLayoutManager);
                    parkingListRV.setItemAnimator(new DefaultItemAnimator());
                    parkingListRV.setAdapter(adapterParkingHistoryList);
                } else {
                    Toast.makeText(ActivityParkingHistory.this, modelParkingHistoryListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            //onBackPressed();
            //overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
            Intent intent = new Intent(ActivityParkingHistory.this, ActivityMain.class);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
