package com.m_intellect.parkandgo1.activity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ViewversionRequest;
import com.m_intellect.parkandgo1.model.ViewversionResponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

import static com.m_intellect.parkandgo1.utils.Constants.APP_ID;

public class SplashActivity extends AppCompatActivity {
    private ImageView imageView;
    String newVersion, currentVersion;
    private Dialog dialog;
    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        checkInternetConnection();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //  checkForUpdate();
                //CheckVersion();
                gotoLoginPage();
//                gotoLoginPage();
            }
        }, 3000);
    }

  /*  private void CheckVersion() {
        ViewversionRequest viewversionRequest = new ViewversionRequest();
        viewversionRequest.setAppId(APP_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(viewversionRequest);
        Log.e("test", "payload: " + payload);
        new WebServicePost(SplashActivity.this, new Handlerupdate(), Constants.UPDATE_VERSION, payload).execute();
        Log.e(TAG, "URL :: " + Constants.UPDATE_VERSION);
    }

    public class Handlerupdate extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response: " + response);
            try{
                if (response != null) {
                    ViewversionResponse viewversionResponse = new Gson().fromJson(response, ViewversionResponse.class);
                    if (viewversionResponse.getStatus() == 200) {
                        if (viewversionResponse.getData() != null) {
                            try {
                                currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                                newVersion = viewversionResponse.getData().get(0).getAppVersion();
                                Log.e(TAG, "Current version >>>" + currentVersion);
                                Log.e(TAG, "New Version >>>" + newVersion);
                                if (!currentVersion.equals(newVersion)) {
                                    onUpdate();
                                } else {
                                    gotoLoginPage();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }*/

    private void gotoLoginPage() {
        startActivity(new Intent(SplashActivity.this, ActivityMain.class));
        finish();
    }

    private void onUpdate() {

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_update, null, false);
        final Dialog dialog = new Dialog(this, R.style.Dialog);

        dialog.setContentView(view);
        dialog.setTitle("Update Available!");

        TextView textViewUpdate = (TextView) view.findViewById(R.id.update);
        textViewUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                //i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.dismiss();
            }
        });

       /* textViewRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
       */
        dialog.setCancelable(false);
        dialog.show();

    }


    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager)
                SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo == null) {
            //finish();
            Toast.makeText(SplashActivity.this, "Please connect to internet", Toast.LENGTH_SHORT).show();
        }
        return networkInfo != null && networkInfo.isConnected();

    }
}
