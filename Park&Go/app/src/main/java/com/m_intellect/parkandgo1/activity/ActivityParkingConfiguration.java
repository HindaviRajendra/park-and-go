package com.m_intellect.parkandgo1.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterParkingList;
import com.m_intellect.parkandgo1.model.ConfigurationCalendar;
import com.m_intellect.parkandgo1.model.ModelConfiguration1;
import com.m_intellect.parkandgo1.model.ModelSubmitParkingConfigurationResponse;
import com.m_intellect.parkandgo1.model.ViewdatesconfigClass;
import com.m_intellect.parkandgo1.model.ViewparkClass;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.m_intellect.parkandgo1.utils.Constants.CONFIGURATION_ID;


public class ActivityParkingConfiguration extends AppCompatActivity implements View.OnClickListener {

    public String park_name;
    public String gbike = "";
    ViewparkClass viewparkClass;
    int SUM = 0;
    int Total = 0;
    ArrayList<String> dates = new ArrayList<>();
    ArrayList<String> dates2 = new ArrayList<>();
    int parkid;
    String date = null;
    String Editdate, str_newdate;
    int configID = 0;
    int count = 0;
    String finalcount;
    List<ModelConfiguration1.GeneralBean> generalBeanslist = new ArrayList<>();
    List<ModelConfiguration1.ReservedBean> reservedBeanList = new ArrayList<>();
    List<ModelConfiguration1.RegularBean> regularBeanList = new ArrayList<>();
    List<ModelConfiguration1.EventBean> eventBeanList = new ArrayList<>();
    List<ModelConfiguration1.PremiumBean> premiumBeanList = new ArrayList<>();
    List<ModelConfiguration1.CarPriceValuesBean> carPriceValuesBeanList = new ArrayList<>();
    List<ModelConfiguration1.BikePriceValuesBean> bikePriceValuesBeanList = new ArrayList<>();
    //Bike Edittext
    List<ModelConfiguration1.BusPriceValuesBean> busPriceValuesBeanList = new ArrayList<>();
    List<ModelConfiguration1.TruckPriceValuesBean> truckPriceValuesBeanList = new ArrayList<>();
    private double finalTotal;
    private LinearLayout calendarDateLayout;
    private TextView startDateTV;
    private TextView endDateTV;
    private ImageView startDateIV;


    //Bus Edittext
    private ImageView endDateIV;
    private ImageView image_car;
    private ImageView image_bike;
    private ImageView image_bus;
    private ImageView image_truck;
    private LinearLayout layout_car;

    //Truck Editetxt
    private LinearLayout layout_bike;
    private LinearLayout layout_bus;
    private LinearLayout layout_truck;
    //car edittext
    private EditText generalET;
    private EditText reservedET;
    private EditText regularET;
    private EditText eventET;
    private EditText premiumET;
    private TextView totalTV;
    private EditText generalET_bike;
    private EditText reservedET_bike;
    private EditText regularET_bike;
    private EditText eventET_bike;
    private EditText premiumET_bike;
    private TextView totalTV_bike;
    private EditText generalET_bus;
    private EditText reservedET_bus;
    private EditText regularET_bus;
    private EditText eventET_bus;
    private EditText premiumET_bus;
    private TextView totalTV_bus;
    private EditText generalET_truck;

    //for Car price
    private EditText reservedET_truck;
    private EditText regularET_truck;
    private EditText eventET_truck;
    private EditText premiumET_truck;
    private TextView totalTV_truck;
    private EditText hoursFrom0ET;
    private EditText hoursTo0ET;
    private EditText hoursFrom1ET;
    private EditText hoursTo1ET;
    private EditText hoursFrom2ET;
    private EditText hoursTo2ET;
    private EditText hoursFrom3ET;
    private EditText hoursTo3ET;
    private EditText hoursFrom4ET;
    private EditText hoursTo4ET;
    private EditText hoursFrom5ET;
    private EditText hoursTo5ET;
    private EditText hoursFrom6ET;
    private EditText hoursTo6ET;
    private EditText hoursFrom7ET;
    private EditText hoursTo7ET;
    private EditText general0ET;
    private EditText general1ET;
    private EditText general2ET;

    //for Bike price
    private EditText general3ET;
    private EditText general4ET;
    private EditText general5ET;
    private EditText general6ET;
    private EditText general7ET;
    private EditText reserve0ET;
    private EditText reserve1ET;
    private EditText reserve2ET;
    private EditText reserve3ET;
    private EditText reserve4ET;
    private EditText reserve5ET;
    private EditText reserve6ET;
    private EditText reserve7ET;
    private EditText premium0ET;
    private EditText premium1ET;
    private EditText premium2ET;
    private EditText premium3ET;
    private EditText premium4ET;
    private EditText premium5ET;
    private EditText premium6ET;
    private EditText premium7ET;
    private EditText general0ET_bike;
    private EditText general1ET_bike;
    private EditText general2ET_bike;


    //for Bus price
    private EditText general3ET_bike;
    private EditText general4ET_bike;
    private EditText general5ET_bike;
    private EditText general6ET_bike;
    private EditText general7ET_bike;
    private EditText reserve0ET_bike;
    private EditText reserve1ET_bike;
    private EditText reserve2ET_bike;
    private EditText reserve3ET_bike;
    private EditText reserve4ET_bike;
    private EditText reserve5ET_bike;
    private EditText reserve6ET_bike;
    private EditText reserve7ET_bike;
    private EditText premium0ET_bike;
    private EditText premium1ET_bike;
    private EditText premium2ET_bike;
    private EditText premium3ET_bike;
    private EditText premium4ET_bike;
    private EditText premium5ET_bike;
    private EditText premium6ET_bike;
    private EditText premium7ET_bike;
    private EditText general0ET_bus;
    private EditText general1ET_bus;
    private EditText general2ET_bus;


    //for Truck price
    private EditText general3ET_bus;
    private EditText general4ET_bus;
    private EditText general5ET_bus;
    private EditText general6ET_bus;
    private EditText general7ET_bus;
    private EditText reserve0ET_bus;
    private EditText reserve1ET_bus;
    private EditText reserve2ET_bus;
    private EditText reserve3ET_bus;
    private EditText reserve4ET_bus;
    private EditText reserve5ET_bus;
    private EditText reserve6ET_bus;
    private EditText reserve7ET_bus;
    private EditText premium0ET_bus;
    private EditText premium1ET_bus;
    private EditText premium2ET_bus;
    private EditText premium3ET_bus;
    private EditText premium4ET_bus;
    private EditText premium5ET_bus;
    private EditText premium6ET_bus;
    private EditText premium7ET_bus;
    private EditText general0ET_truck;
    private EditText general1ET_truck;
    private EditText general2ET_truck;
    private EditText general3ET_truck;
    private EditText general4ET_truck;
    private EditText general5ET_truck;
    private EditText general6ET_truck;
    private EditText general7ET_truck;
    private EditText reserve0ET_truck;
    private EditText reserve1ET_truck;
    private EditText reserve2ET_truck;
    private EditText reserve3ET_truck;
    private EditText reserve4ET_truck;
    private EditText reserve5ET_truck;
    private EditText reserve6ET_truck;
    private EditText reserve7ET_truck;
    private EditText premium0ET_truck;
    private EditText premium1ET_truck;
    private EditText premium2ET_truck;
    private EditText premium3ET_truck;
    private EditText premium4ET_truck;
    private EditText premium5ET_truck;
    private EditText premium6ET_truck;
    private EditText premium7ET_truck;
    private TextView regularDayFromTV;
    private TextView regularDayToTV;
    private EditText regularDayPriceET;
    private TextView regularNightFromTV;
    private TextView regularNightToTV;
    private EditText regularNightPriceET;
    private EditText event1FromET;
    private EditText event1ToET;
    private EditText event1PriceET;
    private EditText event2FromET;
    private EditText event2ToET;
    private EditText event2PriceET;
    private EditText event3FromET;
    private EditText event3ToET;
    private EditText event3PriceET;
    private TextView submitTV;
    private SharedPreferences preferences;
    private String startDate1 = "";
    private String endDate1 = "";
    private RadioButton calendarRadioBtn;
    private RadioButton dateRadioBtn;
    private LinearLayout dateLayout;
    private RelativeLayout calendarDateRelativeLayout;
    private Calendar startCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();
    private TextView fntotalTV;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            startCalendar.set(Calendar.YEAR, year);
            startCalendar.set(Calendar.MONTH, monthOfYear);
            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            startDate1 = simpleDateFormat.format(startCalendar.getTime());
            startDateTV.setText("Start Date " + simpleDateFormat.format(startCalendar.getTime()));

        }

    };
    DatePickerDialog.OnDateSetListener endDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            endCalendar.set(Calendar.YEAR, year);
            endCalendar.set(Calendar.MONTH, monthOfYear);
            endCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            endDate1 = simpleDateFormat.format(endCalendar.getTime());
            endDateTV.setText("End Date " + simpleDateFormat.format(endCalendar.getTime()));
        }

    };
    private List<ConfigurationCalendar> configurationCalendarList = new ArrayList<>();
    DatePickerDialog.OnDateSetListener calendarDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            startCalendar.set(Calendar.YEAR, year);
            startCalendar.set(Calendar.MONTH, monthOfYear);
            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            LayoutInflater inflater = LayoutInflater.from(ActivityParkingConfiguration.this);
            View calendarView = inflater.inflate(R.layout.layout_list_item_calendar_date, null, false);
            TextView dateTV = (TextView) calendarView.findViewById(R.id.dateTV);
            dateTV.setText(simpleDateFormat.format(startCalendar.getTime()));

            ConfigurationCalendar configurationCalendar = new ConfigurationCalendar();
            configurationCalendar.setDate(dateTV.getText().toString());
            configurationCalendarList.add(configurationCalendar);
            calendarDateLayout.addView(calendarView);

        }

    };
    private double general = 0.0;
    private double reserved = 0.0;
    private double regular = 0.0;
    private double event = 0.0;
    private double premium = 0.0;
    private double general_bike = 0.0;
    private double reserved_bike = 0.0;
    private double regular_bike = 0.0;
    private double event_bike = 0.0;
    private double premium_bike = 0.0;
    private double general_bus = 0.0;
    private double reserved_bus = 0.0;
    private double regular_bus = 0.0;
    private double event_bus = 0.0;
    private double premium_bus = 0.0;
    private double general_truck = 0.0;
    private double reserved_truck = 0.0;
    private double regular_truck = 0.0;
    private double event_truck = 0.0;
    private double premium_truck = 0.0;
    private double total = 0.0;
    private double totalbike = 0.0;
    private double totalbus = 0.0;
    private double totaltruck = 0.0;
    private int parkingConfigurationId = 0;
    private boolean fromEdit;
    private ViewSwitcher viewSwitcher;
    private RecyclerView parkingListRV;
    private ScrollView scrollView;
    private LinearLayout linearLayout;
    private int parkingId;
    private int sum = 0;
    private String TAG = getClass().getSimpleName();
    private AdapterParkingList adapterParkingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_configuration);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        startDate1 = simpleDateFormat.format(startCalendar.getTime());
        endDate1 = simpleDateFormat.format(startCalendar.getTime());

        Bundle b = getIntent().getExtras();

        if (b != null) {
            Total = b.getInt("Total");
            park_name = b.getString("parkingname");
            dates = b.getStringArrayList(Constants.SELECTED_DATES);
            parkid = b.getInt("parkID");
            Log.e(TAG, "Dates ArrayList::::" + dates);
            Editdate = b.getString("EDIT_date");
            //   str_newdate = b.getString("New_Date");
            Log.e(TAG, "Edit Dates::::" + Editdate);
            Log.e(TAG, "New dates Arraylist::::" + str_newdate);
        }


        if (getIntent() != null) {
            fromEdit = getIntent().getBooleanExtra(Constants.FROM_EDIT, false);
        }

        initToolbar();
        initView();
        initlistner();
        viewdateconfig();
        parkingPriceCalculation();

    }

    private void initlistner() {

        image_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    layout_car.setVisibility(View.VISIBLE);
                    count++;
                } else {
                    layout_car.setVisibility(View.GONE);
                    count = 0;
                }
            }
        });

        image_bike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    layout_bike.setVisibility(View.VISIBLE);
                    count++;
                } else {
                    layout_bike.setVisibility(View.GONE);
                    count = 0;
                }

            }
        });


        image_bus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    layout_bus.setVisibility(View.VISIBLE);
                    count++;
                } else {
                    layout_bus.setVisibility(View.GONE);
                    count = 0;
                }

            }
        });


        image_truck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    layout_truck.setVisibility(View.VISIBLE);
                    count++;
                } else {
                    layout_truck.setVisibility(View.GONE);
                    count = 0;
                }
            }
        });

    }

    private void viewdateconfig() {

        //Try Catch for Add Date
        try {
            if (!str_newdate.equals("")) {
                dates.add(str_newdate);
                Log.e(TAG, "I am on ADD DATE");
            }

            for (int i = 0; i < dates.size(); i++) {
                date = dates.get(i);
            }

            if (dates.size() > 0) {
                str_newdate = "1";
                new WebServiceGet(this, new HandlerdateListResponse(), Constants.VIEW_DATE_CONFIG + parkid + "/null/" + dates).execute();
                Log.e("TAG", "URL>>>" + Constants.VIEW_DATE_CONFIG + parkid + "/null/" + date);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

//Try Catch for Edit Date
        try {
            if (!Editdate.equals("")) {
                dates2.add(Editdate);
                Log.e(TAG, "I am on EDIT DATE");
            }

            for (int i = 0; i < dates2.size(); i++) {
                date = dates2.get(i);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (!Editdate.equals("")) {
                new WebServiceGet(this, new HandlerdateListResponse(), Constants.VIEW_DATE_CONFIG + parkid + "/null/" + Editdate).execute();
                Log.e("TAG", "URL1>>>" + Constants.VIEW_DATE_CONFIG + parkid + "/null/" + Editdate);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void goToAddConfiguration(int parkingid, int TotalSpace) {
        parkingId = parkingid;
        sum = TotalSpace;
        if (viewSwitcher.getCurrentView() == linearLayout) {
            viewSwitcher.showNext();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void parkingPriceCalculation() {

        //Car Edittext

        generalET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    general = Double.parseDouble(editable.toString());
                } else {
                    general = 0.0;
                }
                calculateTotalPrice_car(general, reserved, regular, event, premium);
            }
        });

        reservedET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    reserved = Double.parseDouble(editable.toString());
                } else {
                    reserved = 0.0;
                }
                calculateTotalPrice_car(general, reserved, regular, event, premium);
            }
        });

        regularET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    regular = Double.parseDouble(editable.toString());
                } else {
                    regular = 0.0;
                }
                calculateTotalPrice_car(general, reserved, regular, event, premium);
            }
        });

        eventET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    event = Double.parseDouble(editable.toString());
                } else {
                    event = 0.0;
                }
                calculateTotalPrice_car(general, reserved, regular, event, premium);
            }
        });

        premiumET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    premium = Double.parseDouble(editable.toString());
                } else {
                    premium = 0.0;
                }
                calculateTotalPrice_car(general, reserved, regular, event, premium);
            }
        });

        //Bike EditText

        generalET_bike.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    general_bike = Double.parseDouble(editable.toString());
                } else {
                    general_bike = 0.0;
                }
                calculateTotalPrice_bike(general_bike, reserved_bike, regular_bike, event_bike, premium_bike);
            }
        });

        reservedET_bike.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    reserved_bike = Double.parseDouble(editable.toString());
                } else {
                    reserved_bike = 0.0;
                }
                calculateTotalPrice_bike(general_bike, reserved_bike, regular_bike, event_bike, premium_bike);
            }
        });

        regularET_bike.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    regular_bike = Double.parseDouble(editable.toString());
                } else {
                    regular_bike = 0.0;
                }
                calculateTotalPrice_bike(general_bike, reserved_bike, regular_bike, event_bike, premium_bike);
            }
        });

        eventET_bike.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    event_bike = Double.parseDouble(editable.toString());
                } else {
                    event_bike = 0.0;
                }
                calculateTotalPrice_bike(general_bike, reserved_bike, regular_bike, event_bike, premium_bike);
            }
        });

        premiumET_bike.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    premium_bike = Double.parseDouble(editable.toString());
                } else {
                    premium_bike = 0.0;
                }
                calculateTotalPrice_bike(general_bike, reserved_bike, regular_bike, event_bike, premium_bike);
            }
        });

        //For bus

        generalET_bus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    general_bus = Double.parseDouble(editable.toString());
                } else {
                    general_bus = 0.0;
                }
                calculateTotalPrice_bus(general_bus, reserved_bus, regular_bus, event_bus, premium_bus);
            }
        });

        reservedET_bus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    reserved_bus = Double.parseDouble(editable.toString());
                } else {
                    reserved_bus = 0.0;
                }
                calculateTotalPrice_bus(general_bus, reserved_bus, regular_bus, event_bus, premium_bus);
            }
        });

        regularET_bus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    regular_bus = Double.parseDouble(editable.toString());
                } else {
                    regular_bus = 0.0;
                }
                calculateTotalPrice_bus(general_bus, reserved_bus, regular_bus, event_bus, premium_bus);
            }
        });

        eventET_bus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    event_bus = Double.parseDouble(editable.toString());
                } else {
                    event_bus = 0.0;
                }
                calculateTotalPrice_bus(general_bus, reserved_bus, regular_bus, event_bus, premium_bus);
            }
        });

        premiumET_bus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    premium_bus = Double.parseDouble(editable.toString());
                } else {
                    premium_bus = 0.0;
                }
                calculateTotalPrice_bus(general_bus, reserved_bus, regular_bus, event_bus, premium_bus);
            }
        });

        //For Truck


        generalET_truck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    general_truck = Double.parseDouble(editable.toString());
                } else {
                    general_truck = 0.0;
                }
                calculateTotalPrice_truck(general_truck, reserved_truck, regular_truck, event_truck, premium_truck);
            }
        });

        reservedET_truck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    reserved_truck = Double.parseDouble(editable.toString());
                } else {
                    reserved_truck = 0.0;
                }
                calculateTotalPrice_truck(general_truck, reserved_truck, regular_truck, event_truck, premium_truck);
            }
        });

        regularET_truck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    regular_truck = Double.parseDouble(editable.toString());
                } else {
                    regular_truck = 0.0;
                }
                calculateTotalPrice_truck(general_truck, reserved_truck, regular_truck, event_truck, premium_truck);
            }
        });

        eventET_truck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    event_truck = Double.parseDouble(editable.toString());
                } else {
                    event_truck = 0.0;
                }
                calculateTotalPrice_truck(general_truck, reserved_truck, regular_truck, event_truck, premium_truck);
            }
        });

        premiumET_truck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    premium_truck = Double.parseDouble(editable.toString());
                } else {
                    premium_truck = 0.0;
                }
                calculateTotalPrice_truck(general_truck, reserved_truck, regular_truck, event_truck, premium_truck);
            }
        });


    }

    private void calculateTotalPrice_car(double general, double reserved, double regular, double event, double premium) {

        total = general + reserved + regular + event + premium;

        totalTV.setText(String.valueOf(total));
        try {
            Double count = Double.parseDouble(finalcount);
            double finaltot = total + totalbike + totalbus + totaltruck + count;
            fntotalTV.setText(String.valueOf(finaltot));

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void calculateTotalPrice_bike(double general, double reserved, double regular, double event, double premium) {

        totalbike = general + reserved + regular + event + premium;

        totalTV_bike.setText(String.valueOf(totalbike));

        try {
            Double count = Double.parseDouble(finalcount);
            double finaltot = total + totalbike + totalbus + totaltruck + count;
            fntotalTV.setText(String.valueOf(finaltot));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void calculateTotalPrice_bus(double general, double reserved, double regular, double event, double premium) {

        totalbus = general + reserved + regular + event + premium;

        totalTV_bus.setText(String.valueOf(totalbus));

        try {
            Double count = Double.parseDouble(finalcount);
            double finaltot = total + totalbike + totalbus + totaltruck + count;
            fntotalTV.setText(String.valueOf(finaltot));
        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void calculateTotalPrice_truck(double general, double reserved, double regular, double event, double premium) {

        totaltruck = general + reserved + regular + event + premium;

        totalTV_truck.setText(String.valueOf(totaltruck));

        try {
            Double count = Double.parseDouble(finalcount);
            double finaltot = total + totalbike + totalbus + totaltruck + count;
            fntotalTV.setText(String.valueOf(finaltot));
        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initView() {
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);

        image_car = (ImageView) findViewById(R.id.addIV_car);
        image_bike = (ImageView) findViewById(R.id.addIV_bike);
        image_bus = (ImageView) findViewById(R.id.addIV_bus);
        image_truck = (ImageView) findViewById(R.id.addIV_truck);

        layout_car = (LinearLayout) findViewById(R.id.layout_car);
        layout_bike = (LinearLayout) findViewById(R.id.layout_bike);
        layout_bus = (LinearLayout) findViewById(R.id.layout_bus);
        layout_truck = (LinearLayout) findViewById(R.id.layout_truck);
        fntotalTV = (TextView) findViewById(R.id.fntotalTV);

        generalET = (EditText) findViewById(R.id.generalET);
        reservedET = (EditText) findViewById(R.id.reservedET);
        regularET = (EditText) findViewById(R.id.regularET);
        eventET = (EditText) findViewById(R.id.eventET);
        premiumET = (EditText) findViewById(R.id.premiumET);

        //Total textview
        totalTV = (TextView) findViewById(R.id.totalTV);
        totalTV_bike = (TextView) findViewById(R.id.ty_bike_total);
        totalTV_bus = (TextView) findViewById(R.id.ty_bus_total);
        totalTV_truck = (TextView) findViewById(R.id.ty_truck_total);

        //Bike Edittext
        generalET_bike = (EditText) findViewById(R.id.generalET_bike);
        reservedET_bike = (EditText) findViewById(R.id.reservedET_bike);
        regularET_bike = (EditText) findViewById(R.id.regularET_bike);
        eventET_bike = (EditText) findViewById(R.id.eventET_bike);
        premiumET_bike = (EditText) findViewById(R.id.premiumET_bike);

        //Bus edittext

        generalET_bus = (EditText) findViewById(R.id.generalET_bus);
        reservedET_bus = (EditText) findViewById(R.id.reservedET_bus);
        regularET_bus = (EditText) findViewById(R.id.regularET_bus);
        eventET_bus = (EditText) findViewById(R.id.eventET_bus);
        premiumET_bus = (EditText) findViewById(R.id.premiumET_bus);


        //Truck Ediitext

        generalET_truck = (EditText) findViewById(R.id.generalET_truck);
        reservedET_truck = (EditText) findViewById(R.id.reservedET_truck);
        regularET_truck = (EditText) findViewById(R.id.regularET_truck);
        eventET_truck = (EditText) findViewById(R.id.eventET_truck);
        premiumET_truck = (EditText) findViewById(R.id.premiumET_truck);


        hoursFrom0ET = (EditText) findViewById(R.id.hoursFrom0ET);
        hoursTo0ET = (EditText) findViewById(R.id.hoursTo0ET);
        hoursFrom1ET = (EditText) findViewById(R.id.hoursFrom1ET);
        hoursTo1ET = (EditText) findViewById(R.id.hoursTo1ET);
        hoursFrom2ET = (EditText) findViewById(R.id.hoursFrom2ET);
        hoursTo2ET = (EditText) findViewById(R.id.hoursTo2ET);
        hoursFrom3ET = (EditText) findViewById(R.id.hoursFrom3ET);
        hoursTo3ET = (EditText) findViewById(R.id.hoursTo3ET);

        hoursFrom4ET = (EditText) findViewById(R.id.hoursFrom4ET);
        hoursTo4ET = (EditText) findViewById(R.id.hoursTo4ET);
        hoursFrom5ET = (EditText) findViewById(R.id.hoursFrom5ET);
        hoursTo5ET = (EditText) findViewById(R.id.hoursTo5ET);
        hoursFrom6ET = (EditText) findViewById(R.id.hoursFrom6ET);
        hoursTo6ET = (EditText) findViewById(R.id.hoursTo6ET);
        hoursFrom7ET = (EditText) findViewById(R.id.hoursFrom7ET);
        hoursTo7ET = (EditText) findViewById(R.id.hoursTo7ET);


        //For Car Price value
        general0ET = (EditText) findViewById(R.id.general0ET);
        general1ET = (EditText) findViewById(R.id.general1ET);
        general2ET = (EditText) findViewById(R.id.general2ET);
        general3ET = (EditText) findViewById(R.id.general3ET);
        general4ET = (EditText) findViewById(R.id.general4ET);
        general5ET = (EditText) findViewById(R.id.general5ET);
        general6ET = (EditText) findViewById(R.id.general6ET);
        general7ET = (EditText) findViewById(R.id.general7ET);


        reserve0ET = (EditText) findViewById(R.id.reserve0ET);
        reserve1ET = (EditText) findViewById(R.id.reserve1ET);
        reserve2ET = (EditText) findViewById(R.id.reserve2ET);
        reserve3ET = (EditText) findViewById(R.id.reserve3ET);

        reserve4ET = (EditText) findViewById(R.id.reserve4ET);
        reserve5ET = (EditText) findViewById(R.id.reserve5ET);
        reserve6ET = (EditText) findViewById(R.id.reserve6ET);
        reserve7ET = (EditText) findViewById(R.id.reserve7ET);

        premium0ET = (EditText) findViewById(R.id.premium0ET);
        premium1ET = (EditText) findViewById(R.id.premium1ET);
        premium2ET = (EditText) findViewById(R.id.premium2ET);
        premium3ET = (EditText) findViewById(R.id.premium3ET);
        premium4ET = (EditText) findViewById(R.id.premium4ET);
        premium5ET = (EditText) findViewById(R.id.premium5ET);
        premium6ET = (EditText) findViewById(R.id.premium6ET);
        premium7ET = (EditText) findViewById(R.id.premium7ET);

        //For Bike Price value
        general0ET_bike = (EditText) findViewById(R.id.general0ET_bike);
        general1ET_bike = (EditText) findViewById(R.id.general1ET_bike);
        general2ET_bike = (EditText) findViewById(R.id.general2ET_bike);
        general3ET_bike = (EditText) findViewById(R.id.general3ET_bike);
        general4ET_bike = (EditText) findViewById(R.id.general4ET_bike);
        general5ET_bike = (EditText) findViewById(R.id.general5ET_bike);
        general6ET_bike = (EditText) findViewById(R.id.general6ET_bike);
        general7ET_bike = (EditText) findViewById(R.id.general7ET_bike);

        reserve0ET_bike = (EditText) findViewById(R.id.reserve0ET_bike);
        reserve1ET_bike = (EditText) findViewById(R.id.reserve1ET_bike);
        reserve2ET_bike = (EditText) findViewById(R.id.reserve2ET_bike);
        reserve3ET_bike = (EditText) findViewById(R.id.reserve3ET_bike);
        reserve4ET_bike = (EditText) findViewById(R.id.reserve4ET_bike);
        reserve5ET_bike = (EditText) findViewById(R.id.reserve5ET_bike);
        reserve6ET_bike = (EditText) findViewById(R.id.reserve6ET_bike);
        reserve7ET_bike = (EditText) findViewById(R.id.reserve7ET_bike);

        premium0ET_bike = (EditText) findViewById(R.id.premium0ET_bike);
        premium1ET_bike = (EditText) findViewById(R.id.premium1ET_bike);
        premium2ET_bike = (EditText) findViewById(R.id.premium2ET_bike);
        premium3ET_bike = (EditText) findViewById(R.id.premium3ET_bike);
        premium4ET_bike = (EditText) findViewById(R.id.premium4ET_bike);
        premium5ET_bike = (EditText) findViewById(R.id.premium5ET_bike);
        premium6ET_bike = (EditText) findViewById(R.id.premium6ET_bike);
        premium7ET_bike = (EditText) findViewById(R.id.premium7ET_bike);

        //For Bus Price value

        general0ET_bus = (EditText) findViewById(R.id.general0ET_bus);
        general1ET_bus = (EditText) findViewById(R.id.general1ET_bus);
        general2ET_bus = (EditText) findViewById(R.id.general2ET_bus);
        general3ET_bus = (EditText) findViewById(R.id.general3ET_bus);
        general4ET_bus = (EditText) findViewById(R.id.general4ET_bus);
        general5ET_bus = (EditText) findViewById(R.id.general5ET_bus);
        general6ET_bus = (EditText) findViewById(R.id.general6ET_bus);
        general7ET_bus = (EditText) findViewById(R.id.general7ET_bus);

        reserve0ET_bus = (EditText) findViewById(R.id.reserve0ET_bus);
        reserve1ET_bus = (EditText) findViewById(R.id.reserve1ET_bus);
        reserve2ET_bus = (EditText) findViewById(R.id.reserve2ET_bus);
        reserve3ET_bus = (EditText) findViewById(R.id.reserve3ET_bus);
        reserve4ET_bus = (EditText) findViewById(R.id.reserve4ET_bus);
        reserve5ET_bus = (EditText) findViewById(R.id.reserve5ET_bus);
        reserve6ET_bus = (EditText) findViewById(R.id.reserve6ET_bus);
        reserve7ET_bus = (EditText) findViewById(R.id.reserve7ET_bus);

        premium0ET_bus = (EditText) findViewById(R.id.premium0ET_bus);
        premium1ET_bus = (EditText) findViewById(R.id.premium1ET_bus);
        premium2ET_bus = (EditText) findViewById(R.id.premium2ET_bus);
        premium3ET_bus = (EditText) findViewById(R.id.premium3ET_bus);
        premium4ET_bus = (EditText) findViewById(R.id.premium4ET_bus);
        premium5ET_bus = (EditText) findViewById(R.id.premium5ET_bus);
        premium6ET_bus = (EditText) findViewById(R.id.premium6ET_bus);
        premium7ET_bus = (EditText) findViewById(R.id.premium7ET_bus);


        //For Truck Price value
        general0ET_truck = (EditText) findViewById(R.id.general0ET_truck);
        general1ET_truck = (EditText) findViewById(R.id.general1ET_truck);
        general2ET_truck = (EditText) findViewById(R.id.general2ET_truck);
        general3ET_truck = (EditText) findViewById(R.id.general3ET_truck);
        general4ET_truck = (EditText) findViewById(R.id.general4ET_truck);
        general5ET_truck = (EditText) findViewById(R.id.general5ET_truck);
        general6ET_truck = (EditText) findViewById(R.id.general6ET_truck);
        general7ET_truck = (EditText) findViewById(R.id.general7ET_truck);

        reserve0ET_truck = (EditText) findViewById(R.id.reserve0ET_truck);
        reserve1ET_truck = (EditText) findViewById(R.id.reserve1ET_truck);
        reserve2ET_truck = (EditText) findViewById(R.id.reserve2ET_truck);
        reserve3ET_truck = (EditText) findViewById(R.id.reserve3ET_truck);
        reserve4ET_truck = (EditText) findViewById(R.id.reserve4ET_truck);
        reserve5ET_truck = (EditText) findViewById(R.id.reserve5ET_truck);
        reserve6ET_truck = (EditText) findViewById(R.id.reserve6ET_truck);
        reserve7ET_truck = (EditText) findViewById(R.id.reserve7ET_truck);

        premium0ET_truck = (EditText) findViewById(R.id.premium0ET_truck);
        premium1ET_truck = (EditText) findViewById(R.id.premium1ET_truck);
        premium2ET_truck = (EditText) findViewById(R.id.premium2ET_truck);
        premium3ET_truck = (EditText) findViewById(R.id.premium3ET_truck);
        premium4ET_truck = (EditText) findViewById(R.id.premium4ET_truck);
        premium5ET_truck = (EditText) findViewById(R.id.premium5ET_truck);
        premium6ET_truck = (EditText) findViewById(R.id.premium6ET_truck);
        premium7ET_truck = (EditText) findViewById(R.id.premium7ET_truck);


        regularDayFromTV = (TextView) findViewById(R.id.regularDayFromTV);
        regularDayFromTV.setOnClickListener(this);
        regularDayToTV = (TextView) findViewById(R.id.regularDayToTV);
        regularDayToTV.setOnClickListener(this);
        regularDayPriceET = (EditText) findViewById(R.id.regularDayPriceET);
        regularNightFromTV = (TextView) findViewById(R.id.regularNightFromTV);
        regularNightFromTV.setOnClickListener(this);
        regularNightToTV = (TextView) findViewById(R.id.regularNightToTV);
        regularNightToTV.setOnClickListener(this);
        regularNightPriceET = (EditText) findViewById(R.id.regularNightPriceET);

        event1FromET = (EditText) findViewById(R.id.event1FromET);
        event1ToET = (EditText) findViewById(R.id.event1ToET);
        event1PriceET = (EditText) findViewById(R.id.event1PriceET);
        event2FromET = (EditText) findViewById(R.id.event2FromET);
        event2ToET = (EditText) findViewById(R.id.event2ToET);
        event2PriceET = (EditText) findViewById(R.id.event2PriceET);
        event3FromET = (EditText) findViewById(R.id.event3FromET);
        event3ToET = (EditText) findViewById(R.id.event3ToET);
        event3PriceET = (EditText) findViewById(R.id.event3PriceET);

        submitTV = (TextView) findViewById(R.id.submitTV);
        submitTV.setOnClickListener(this);

//        calendarDateRelativeLayout = (RelativeLayout) findViewById(R.id.calendarDateRelativeLayout);
//        calendarDateRelativeLayout.setOnClickListener(this);

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        parkingListRV = (RecyclerView) findViewById(R.id.parkingListRV);
        scrollView = (ScrollView) findViewById(R.id.scrollView);


    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText(park_name);
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    private void selectTime(Calendar currentTime, final TextView textView) {

        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String hourString = "";
                if (selectedHour < 12) {
                    hourString = selectedHour < 10 ? "0" + selectedHour : "" + selectedHour;
                } else {
                    hourString = (selectedHour - 12) < 10 ? "0" + (selectedHour - 12) : "" + (selectedHour - 12);
                }
                String minuteString = selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute;
                String am_pm = (selectedHour < 12) ? "AM" : "PM";

                textView.setText(hourString + ":" + minuteString + " " + am_pm);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void submitParkingConfiguration() {

        ModelConfiguration1 modelConfiguration = new ModelConfiguration1();

        //For general of car , bike, bus& truck

        ModelConfiguration1.GeneralBean generalBean = new ModelConfiguration1.GeneralBean();
        if (!generalET.getText().toString().equals("")) {
            generalBean.setCar(Integer.parseInt(generalET.getText().toString()));
        } else {
            generalBean.setCar(0);
        }

        if (!generalET_bike.getText().toString().equals("")) {
            generalBean.setBike(Integer.parseInt(generalET_bike.getText().toString()));
        } else {
            generalBean.setBike(0);
        }
        gbike = "0";


        if (!generalET_bus.getText().toString().equals("")) {
            generalBean.setBus(Integer.parseInt(generalET_bus.getText().toString()));
        } else {
            generalBean.setBus(0);
        }

        if (!generalET_truck.getText().toString().equals("")) {
            generalBean.setTruck(Integer.parseInt(generalET_truck.getText().toString()));
        } else {
            generalBean.setTruck(0);
        }
        generalBeanslist.add(generalBean);
        Log.e(TAG, "GENERALLIST" + generalBeanslist.size());

        //For reserved of car , bike, bus& truck
        ModelConfiguration1.ReservedBean reservedBean = new ModelConfiguration1.ReservedBean();

//        if (!reservedET.getText().toString().equals("") && !reservedET_bike.getText().toString().equals("") && !reservedET_bus.getText().toString().equals("") && !reservedET_truck.getText().toString().equals("")) {
//            reservedBean.setCar(Integer.parseInt(reservedET.getText().toString()));
//            reservedBean.setBike(Integer.parseInt(reservedET_bike.getText().toString()));
//            reservedBean.setBus(Integer.parseInt(reservedET_bus.getText().toString()));
//            reservedBean.setTruck(Integer.parseInt(reservedET_truck.getText().toString()));
//        } else {
//            reservedBean.setCar(0);
//            reservedBean.setBike(0);
//            reservedBean.setBus(0);
//            reservedBean.setTruck(0);
//        }


        if (!reservedET.getText().toString().equals("")) {
            reservedBean.setCar(Integer.parseInt(reservedET.getText().toString()));
        } else {
            reservedBean.setCar(0);
        }

        if (!reservedET_bike.getText().toString().equals("")) {
            reservedBean.setBike(Integer.parseInt(reservedET_bike.getText().toString()));
        } else {
            reservedBean.setBike(0);
        }

        if (!reservedET_bus.getText().toString().equals("")) {
            reservedBean.setBus(Integer.parseInt(reservedET_bus.getText().toString()));
        } else {
            reservedBean.setBus(0);
        }

        if (!reservedET_truck.getText().toString().equals("")) {
            reservedBean.setTruck(Integer.parseInt(reservedET_truck.getText().toString()));
        } else {
            reservedBean.setTruck(0);
        }

        reservedBeanList.add(reservedBean);

        //For regular of car , bike, bus& truck
        ModelConfiguration1.RegularBean regularBean = new ModelConfiguration1.RegularBean();
        if (!regularET.getText().toString().equals("") && !regularET_bike.getText().toString().equals("") && !regularET_bus.getText().toString().equals("") && !regularET_truck.getText().toString().equals("")) {
            regularBean.setCar(Integer.parseInt(regularET.getText().toString()));
            regularBean.setBike(Integer.parseInt(regularET_bike.getText().toString()));
            regularBean.setBus(Integer.parseInt(regularET_bus.getText().toString()));
            regularBean.setTruck(Integer.parseInt(regularET_truck.getText().toString()));
        } else {
            regularBean.setCar(0);
            regularBean.setBike(0);
            regularBean.setBus(0);
            regularBean.setTruck(0);
        }
        regularBeanList.add(regularBean);

        //For event of car , bike, bus& truck
        ModelConfiguration1.EventBean eventBean = new ModelConfiguration1.EventBean();
        if (!eventET.getText().toString().equals("") && !eventET_bike.getText().toString().equals("") && !eventET_bus.getText().toString().equals("") && !eventET_truck.getText().toString().equals("")) {
            eventBean.setCar(Integer.parseInt(eventET.getText().toString()));
            eventBean.setBike(Integer.parseInt(eventET_bike.getText().toString()));
            eventBean.setBus(Integer.parseInt(eventET_bus.getText().toString()));
            eventBean.setTruck(Integer.parseInt(eventET_truck.getText().toString()));
        } else {
            eventBean.setCar(0);
            eventBean.setBike(0);
            eventBean.setBus(0);
            eventBean.setTruck(0);
        }
        eventBeanList.add(eventBean);


        //For premium of car , bike, bus& truck

        ModelConfiguration1.PremiumBean premiumBean = new ModelConfiguration1.PremiumBean();
        if (!premiumET.getText().toString().equals("") && !premiumET_bike.getText().toString().equals("") && !premiumET_bus.getText().toString().equals("") && !premiumET_truck.getText().toString().equals("")) {
            premiumBean.setCar(Integer.parseInt(premiumET.getText().toString()));
            premiumBean.setBike(Integer.parseInt(premiumET_bike.getText().toString()));
            premiumBean.setBus(Integer.parseInt(premiumET_bus.getText().toString()));
            premiumBean.setTruck(Integer.parseInt(premiumET_truck.getText().toString()));
        } else {
            premiumBean.setCar(0);
            premiumBean.setBike(0);
            premiumBean.setBus(0);
            premiumBean.setTruck(0);
        }

        premiumBeanList.add(premiumBean);

        //For car price
        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean.setFrom(hoursFrom0ET.getText().toString());
        carPriceValuesBean.setTo(hoursTo0ET.getText().toString());
        carPriceValuesBean.setPriceG(general0ET.getText().toString());
        carPriceValuesBean.setPariceR(reserve0ET.getText().toString());
        carPriceValuesBean.setPriceP(premium0ET.getText().toString());
        carPriceValuesBeanList.add(0, carPriceValuesBean);


        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean1 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean1.setFrom(hoursFrom1ET.getText().toString());
        carPriceValuesBean1.setTo(hoursTo1ET.getText().toString());
        carPriceValuesBean1.setPriceG(general1ET.getText().toString());
        carPriceValuesBean1.setPariceR(reserve1ET.getText().toString());
        carPriceValuesBean1.setPriceP(premium1ET.getText().toString());
        carPriceValuesBeanList.add(1, carPriceValuesBean1);


        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean2 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean2.setFrom(hoursFrom2ET.getText().toString());
        carPriceValuesBean2.setTo(hoursTo2ET.getText().toString());
        carPriceValuesBean2.setPriceG(general2ET.getText().toString());
        carPriceValuesBean2.setPariceR(reserve2ET.getText().toString());
        carPriceValuesBean2.setPriceP(premium2ET.getText().toString());
        carPriceValuesBeanList.add(2, carPriceValuesBean2);


        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean3 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean3.setFrom(hoursFrom3ET.getText().toString());
        carPriceValuesBean3.setTo(hoursTo3ET.getText().toString());
        carPriceValuesBean3.setPriceG(general3ET.getText().toString());
        carPriceValuesBean3.setPariceR(reserve3ET.getText().toString());
        carPriceValuesBean3.setPriceP(premium3ET.getText().toString());
        carPriceValuesBeanList.add(3, carPriceValuesBean3);

        Log.e(TAG, "Carlist hoursFrom4ET :: " + hoursFrom4ET.getText().toString());
        Log.e(TAG, "Carlist hoursTo4ET :: " + hoursTo4ET.getText().toString());

        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean4 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean4.setFrom(hoursFrom4ET.getText().toString());
        carPriceValuesBean4.setTo(hoursTo4ET.getText().toString());
        carPriceValuesBean4.setPriceG(general4ET.getText().toString());
        carPriceValuesBean4.setPariceR(reserve4ET.getText().toString());
        carPriceValuesBean4.setPriceP(premium4ET.getText().toString());
        carPriceValuesBeanList.add(4, carPriceValuesBean4);

        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean5 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean5.setFrom(hoursFrom5ET.getText().toString());
        carPriceValuesBean5.setTo(hoursTo5ET.getText().toString());
        carPriceValuesBean5.setPriceG(general5ET.getText().toString());
        carPriceValuesBean5.setPariceR(reserve5ET.getText().toString());
        carPriceValuesBean5.setPriceP(premium5ET.getText().toString());
        carPriceValuesBeanList.add(5, carPriceValuesBean5);

        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean6 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean6.setFrom(hoursFrom6ET.getText().toString());
        carPriceValuesBean6.setTo(hoursTo6ET.getText().toString());
        carPriceValuesBean6.setPriceG(general6ET.getText().toString());
        carPriceValuesBean6.setPariceR(reserve6ET.getText().toString());
        carPriceValuesBean6.setPriceP(premium6ET.getText().toString());
        carPriceValuesBeanList.add(6, carPriceValuesBean6);

        ModelConfiguration1.CarPriceValuesBean carPriceValuesBean7 = new ModelConfiguration1.CarPriceValuesBean();
        carPriceValuesBean7.setFrom(hoursFrom7ET.getText().toString());
        carPriceValuesBean7.setTo(hoursTo7ET.getText().toString());
        carPriceValuesBean7.setPriceG(general7ET.getText().toString());
        carPriceValuesBean7.setPariceR(reserve7ET.getText().toString());
        carPriceValuesBean7.setPriceP(premium7ET.getText().toString());
        carPriceValuesBeanList.add(7, carPriceValuesBean7);


        //For Bike price

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean.setFrom(hoursFrom0ET.getText().toString());
        bikePriceValuesBean.setTo(hoursTo0ET.getText().toString());
        bikePriceValuesBean.setPriceG(general0ET_bike.getText().toString());
        bikePriceValuesBean.setPariceR(reserve0ET_bike.getText().toString());
        bikePriceValuesBean.setPriceP(premium0ET_bike.getText().toString());
        bikePriceValuesBeanList.add(0, bikePriceValuesBean);

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean1 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean1.setFrom(hoursFrom1ET.getText().toString());
        bikePriceValuesBean1.setTo(hoursTo1ET.getText().toString());
        bikePriceValuesBean1.setPriceG(general1ET_bike.getText().toString());
        bikePriceValuesBean1.setPariceR(reserve1ET_bike.getText().toString());
        bikePriceValuesBean1.setPriceP(premium1ET_bike.getText().toString());
        bikePriceValuesBeanList.add(1, bikePriceValuesBean1);


        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean2 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean2.setFrom(hoursFrom2ET.getText().toString());
        bikePriceValuesBean2.setTo(hoursTo2ET.getText().toString());
        bikePriceValuesBean2.setPriceG(general2ET_bike.getText().toString());
        bikePriceValuesBean2.setPariceR(reserve2ET_bike.getText().toString());
        bikePriceValuesBean2.setPriceP(premium2ET_bike.getText().toString());
        bikePriceValuesBeanList.add(2, bikePriceValuesBean2);


        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean3 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean3.setFrom(hoursFrom3ET.getText().toString());
        bikePriceValuesBean3.setTo(hoursTo3ET.getText().toString());
        bikePriceValuesBean3.setPriceG(general3ET_bike.getText().toString());
        bikePriceValuesBean3.setPariceR(reserve3ET_bike.getText().toString());
        bikePriceValuesBean3.setPriceP(premium3ET_bike.getText().toString());
        bikePriceValuesBeanList.add(3, bikePriceValuesBean3);

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean4 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean4.setFrom(hoursFrom4ET.getText().toString());
        bikePriceValuesBean4.setTo(hoursTo4ET.getText().toString());
        bikePriceValuesBean4.setPriceG(general4ET_bike.getText().toString());
        bikePriceValuesBean4.setPariceR(reserve4ET_bike.getText().toString());
        bikePriceValuesBean4.setPriceP(premium4ET_bike.getText().toString());
        bikePriceValuesBeanList.add(4, bikePriceValuesBean4);

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean5 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean5.setFrom(hoursFrom5ET.getText().toString());
        bikePriceValuesBean5.setTo(hoursTo5ET.getText().toString());
        bikePriceValuesBean5.setPriceG(general5ET_bike.getText().toString());
        bikePriceValuesBean5.setPariceR(reserve5ET_bike.getText().toString());
        bikePriceValuesBean5.setPriceP(premium5ET_bike.getText().toString());
        bikePriceValuesBeanList.add(5, bikePriceValuesBean5);

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean6 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean6.setFrom(hoursFrom6ET.getText().toString());
        bikePriceValuesBean6.setTo(hoursTo6ET.getText().toString());
        bikePriceValuesBean6.setPriceG(general6ET_bike.getText().toString());
        bikePriceValuesBean6.setPariceR(reserve6ET_bike.getText().toString());
        bikePriceValuesBean6.setPriceP(premium6ET_bike.getText().toString());
        bikePriceValuesBeanList.add(6, bikePriceValuesBean6);

        ModelConfiguration1.BikePriceValuesBean bikePriceValuesBean7 = new ModelConfiguration1.BikePriceValuesBean();
        bikePriceValuesBean7.setFrom(hoursFrom7ET.getText().toString());
        bikePriceValuesBean7.setTo(hoursTo7ET.getText().toString());
        bikePriceValuesBean7.setPriceG(general7ET_bike.getText().toString());
        bikePriceValuesBean7.setPariceR(reserve7ET_bike.getText().toString());
        bikePriceValuesBean7.setPriceP(premium7ET_bike.getText().toString());
        bikePriceValuesBeanList.add(7, bikePriceValuesBean7);


        //For BUS price
        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean.setFrom(hoursFrom0ET.getText().toString());
        busPriceValuesBean.setTo(hoursTo0ET.getText().toString());
        busPriceValuesBean.setPriceG(general0ET_bus.getText().toString());
        busPriceValuesBean.setPariceR(reserve0ET_bus.getText().toString());
        busPriceValuesBean.setPriceP(reserve0ET_bus.getText().toString());
        busPriceValuesBeanList.add(0, busPriceValuesBean);

        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean1 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean1.setFrom(hoursFrom1ET.getText().toString());
        busPriceValuesBean1.setTo(hoursTo1ET.getText().toString());
        busPriceValuesBean1.setPriceG(general1ET_bus.getText().toString());
        busPriceValuesBean1.setPariceR(reserve1ET_bus.getText().toString());
        busPriceValuesBean1.setPriceP(premium1ET_bus.getText().toString());
        busPriceValuesBeanList.add(1, busPriceValuesBean1);


        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean2 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean2.setFrom(hoursFrom2ET.getText().toString());
        busPriceValuesBean2.setTo(hoursTo2ET.getText().toString());
        busPriceValuesBean2.setPriceG(general2ET_bus.getText().toString());
        busPriceValuesBean2.setPariceR(reserve2ET_bus.getText().toString());
        busPriceValuesBean2.setPriceP(premium2ET_bus.getText().toString());
        busPriceValuesBeanList.add(2, busPriceValuesBean2);


        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean3 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean3.setFrom(hoursFrom3ET.getText().toString());
        busPriceValuesBean3.setTo(hoursTo3ET.getText().toString());
        busPriceValuesBean3.setPriceG(general3ET_bus.getText().toString());
        busPriceValuesBean3.setPariceR(reserve3ET_bus.getText().toString());
        busPriceValuesBean3.setPriceP(premium3ET_bus.getText().toString());
        busPriceValuesBeanList.add(3, busPriceValuesBean3);


        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean4 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean4.setFrom(hoursFrom4ET.getText().toString());
        busPriceValuesBean4.setTo(hoursTo4ET.getText().toString());
        busPriceValuesBean4.setPriceG(general4ET_bus.getText().toString());
        busPriceValuesBean4.setPariceR(reserve4ET_bus.getText().toString());
        busPriceValuesBean4.setPriceP(premium4ET_bus.getText().toString());
        busPriceValuesBeanList.add(4, busPriceValuesBean4);


        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean5 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean5.setFrom(hoursFrom5ET.getText().toString());
        busPriceValuesBean5.setTo(hoursTo5ET.getText().toString());
        busPriceValuesBean5.setPriceG(general5ET_bus.getText().toString());
        busPriceValuesBean5.setPariceR(reserve5ET_bus.getText().toString());
        busPriceValuesBean5.setPriceP(premium5ET_bus.getText().toString());
        busPriceValuesBeanList.add(5, busPriceValuesBean5);

        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean6 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean6.setFrom(hoursFrom6ET.getText().toString());
        busPriceValuesBean6.setTo(hoursTo6ET.getText().toString());
        busPriceValuesBean6.setPriceG(general6ET_bus.getText().toString());
        busPriceValuesBean6.setPariceR(reserve6ET_bus.getText().toString());
        busPriceValuesBean6.setPriceP(premium6ET_bus.getText().toString());
        busPriceValuesBeanList.add(6, busPriceValuesBean6);

        ModelConfiguration1.BusPriceValuesBean busPriceValuesBean7 = new ModelConfiguration1.BusPriceValuesBean();
        busPriceValuesBean7.setFrom(hoursFrom7ET.getText().toString());
        busPriceValuesBean7.setTo(hoursTo7ET.getText().toString());
        busPriceValuesBean7.setPriceG(general7ET_bus.getText().toString());
        busPriceValuesBean7.setPariceR(reserve7ET_bus.getText().toString());
        busPriceValuesBean7.setPriceP(premium7ET_bus.getText().toString());
        busPriceValuesBeanList.add(7, busPriceValuesBean7);


        //For TRUCK price
        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean3 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean3.setFrom(hoursFrom0ET.getText().toString());
        truckPriceValuesBean3.setTo(hoursTo0ET.getText().toString());
        truckPriceValuesBean3.setPriceG(general0ET_truck.getText().toString());
        truckPriceValuesBean3.setPariceR(reserve0ET_truck.getText().toString());
        truckPriceValuesBean3.setPriceP(premium0ET_truck.getText().toString());
        truckPriceValuesBeanList.add(0, truckPriceValuesBean3);


        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean2 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean2.setFrom(hoursFrom1ET.getText().toString());
        truckPriceValuesBean2.setTo(hoursTo1ET.getText().toString());
        truckPriceValuesBean2.setPriceG(general1ET_truck.getText().toString());
        truckPriceValuesBean2.setPariceR(reserve2ET_truck.getText().toString());
        truckPriceValuesBean2.setPriceP(premium1ET_truck.getText().toString());
        truckPriceValuesBeanList.add(1, truckPriceValuesBean2);

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean1 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean1.setFrom(hoursFrom2ET.getText().toString());
        truckPriceValuesBean1.setTo(hoursTo2ET.getText().toString());
        truckPriceValuesBean1.setPriceG(general2ET_truck.getText().toString());
        truckPriceValuesBean1.setPariceR(reserve1ET_truck.getText().toString());
        truckPriceValuesBean1.setPriceP(premium2ET_truck.getText().toString());
        truckPriceValuesBeanList.add(2, truckPriceValuesBean1);

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean.setFrom(hoursFrom3ET.getText().toString());
        truckPriceValuesBean.setTo(hoursTo3ET.getText().toString());
        truckPriceValuesBean.setPriceG(general3ET_truck.getText().toString());
        truckPriceValuesBean.setPariceR(reserve3ET_truck.getText().toString());
        truckPriceValuesBean.setPriceP(premium3ET_truck.getText().toString());
        truckPriceValuesBeanList.add(3, truckPriceValuesBean);
        Log.e(TAG, "reserve1ET_truck :: " + reserve1ET_truck.getText().toString());

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean4 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean4.setFrom(hoursFrom4ET.getText().toString());
        truckPriceValuesBean4.setTo(hoursTo4ET.getText().toString());
        truckPriceValuesBean4.setPriceG(general4ET_truck.getText().toString());
        truckPriceValuesBean4.setPariceR(reserve4ET_truck.getText().toString());
        truckPriceValuesBean4.setPriceP(premium4ET_truck.getText().toString());
        truckPriceValuesBeanList.add(4, truckPriceValuesBean4);

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean5 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean5.setFrom(hoursFrom5ET.getText().toString());
        truckPriceValuesBean5.setTo(hoursTo5ET.getText().toString());
        truckPriceValuesBean5.setPriceG(general5ET_truck.getText().toString());
        truckPriceValuesBean5.setPariceR(reserve5ET_truck.getText().toString());
        truckPriceValuesBean5.setPriceP(premium5ET_truck.getText().toString());
        truckPriceValuesBeanList.add(5, truckPriceValuesBean5);

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean6 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean6.setFrom(hoursFrom6ET.getText().toString());
        truckPriceValuesBean6.setTo(hoursTo6ET.getText().toString());
        truckPriceValuesBean6.setPriceG(general6ET_truck.getText().toString());
        truckPriceValuesBean6.setPariceR(reserve6ET_truck.getText().toString());
        truckPriceValuesBean6.setPriceP(premium6ET_truck.getText().toString());
        truckPriceValuesBeanList.add(6, truckPriceValuesBean6);

        ModelConfiguration1.TruckPriceValuesBean truckPriceValuesBean7 = new ModelConfiguration1.TruckPriceValuesBean();
        truckPriceValuesBean7.setFrom(hoursFrom7ET.getText().toString());
        truckPriceValuesBean7.setTo(hoursTo7ET.getText().toString());
        truckPriceValuesBean7.setPriceG(general7ET_truck.getText().toString());
        truckPriceValuesBean7.setPariceR(reserve7ET_truck.getText().toString());
        truckPriceValuesBean7.setPriceP(premium7ET_truck.getText().toString());
        truckPriceValuesBeanList.add(7, truckPriceValuesBean7);


        modelConfiguration.setUserId(preferences.getInt(Constants.USER_ID, 0));
        modelConfiguration.setParkingConfigurationId(0);
        modelConfiguration.setParkingId(parkid);
        modelConfiguration.setAppId(Constants.APP_ID);
        modelConfiguration.setTotal(totalTV.getText().toString());
        modelConfiguration.setGeneral(generalBeanslist);
        modelConfiguration.setReserved(reservedBeanList);
        modelConfiguration.setRegular(regularBeanList);
        modelConfiguration.setEvent(eventBeanList);
        modelConfiguration.setPremium(premiumBeanList);
        modelConfiguration.setTotal("0");
        modelConfiguration.setTotalSpace(0);

        String gcar = generalET.getText().toString();
        gbike = generalET_bike.getText().toString();
        String gtruck = generalET_truck.getText().toString();
        String gbus = generalET_bus.getText().toString();
        Log.e(TAG, "" + gcar);
        Log.e(TAG, "R Car ::" + reserve0ET.getText().toString());

        if ((generalET.getText().toString().equals("0") || generalET.getText().toString().equals("")) && (reservedET.getText().toString().equals("0") || reservedET.getText().toString().equals(""))) {
            modelConfiguration.setCar_status(0);
        } else {
            modelConfiguration.setCar_status(1);
        }

        if ((generalET_bike.getText().toString().equals("0") || generalET_bike.getText().toString().equals("")) && ((reservedET_bike.getText().toString().equals("0") || reservedET_bike.getText().toString().equals("")))) {
            modelConfiguration.setBike_status(0);
        } else {
            modelConfiguration.setBike_status(1);
        }

        if ((generalET_bus.getText().toString().equals("0") || generalET_bus.getText().toString().equals("")) && ((reservedET_bus.getText().toString().equals("0") || reservedET_bus.getText().toString().equals("")))) {
            modelConfiguration.setBus_status(0);
        } else {
            modelConfiguration.setBus_status(1);
        }

        if ((generalET_truck.getText().toString().equals("0") || generalET_truck.getText().toString().equals("")) && (reservedET_truck.getText().toString().equals("0") || reservedET_truck.getText().toString().equals(""))) {
            modelConfiguration.setTruck_status(0);
        } else {
            modelConfiguration.setTruck_status(1);
        }


//             try {
//            if (!str_newdate.equals("1"))
//                dates.clear();
//           // dates.add(str_newdate);
//            modelConfiguration.setCalender(dates);
//            modelConfiguration.setParkingConfigurationId(0);
//            Log.e(TAG, "Submit New Add:::" + dates);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        modelConfiguration.setCalender(dates);
        modelConfiguration.setParkingConfigurationId(0);

        try {
            if (!Editdate.equals("")) {
                dates2.clear();
                dates2.add(Editdate);
                modelConfiguration.setCalender(dates2);
                modelConfiguration.setParkingConfigurationId(CONFIGURATION_ID);
                Log.e(TAG, "Submit Edit date :::" + dates2);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // modelConfiguration.setCalender(dates);
        modelConfiguration.setCarPriceValues(carPriceValuesBeanList);
        modelConfiguration.setBikePriceValues(bikePriceValuesBeanList);
        modelConfiguration.setBusPriceValues(busPriceValuesBeanList);
        modelConfiguration.setTruckPriceValues(truckPriceValuesBeanList);


        Double db_total = total + totalbike + totalbus + totaltruck;
        Integer itotal = db_total.intValue();
        String str_total = String.valueOf(itotal);
        Log.e(TAG, "Total  <><><>" + str_total);

        modelConfiguration.setTotal(str_total);
        try {
            int Strtotal = Integer.parseInt(str_total);
            modelConfiguration.setTotalSpace(Strtotal);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        ModelConfiguration1.RegularParkingBean regularParking = new ModelConfiguration1.RegularParkingBean();
        ModelConfiguration1.RegularParkingBean.DayBean day = new ModelConfiguration1.RegularParkingBean.DayBean();
        day.setFrom(regularDayFromTV.getText().toString());
        day.setTo(regularDayToTV.getText().toString());
        day.setPrice(regularDayPriceET.getText().toString());
        regularParking.setDay(day);

        ModelConfiguration1.RegularParkingBean.NightBean night = new ModelConfiguration1.RegularParkingBean.NightBean();
        night.setFrom(regularNightFromTV.getText().toString());
        night.setTo(regularNightToTV.getText().toString());
        night.setPrice(regularNightPriceET.getText().toString());
        regularParking.setNight(night);

        modelConfiguration.setRegularParking(regularParking);

        ModelConfiguration1.EventParkingBean eventParking = new ModelConfiguration1.EventParkingBean();

        ModelConfiguration1.EventParkingBean.Event1Bean event1 = new ModelConfiguration1.EventParkingBean.Event1Bean();
        event1.setFrom(event1FromET.getText().toString());
        event1.setTo(event1ToET.getText().toString());
        event1.setPrice(event1PriceET.getText().toString());
        eventParking.setEvent1(event1);

        ModelConfiguration1.EventParkingBean.Event2Bean event2 = new ModelConfiguration1.EventParkingBean.Event2Bean();
        event2.setFrom(event2FromET.getText().toString());
        event2.setTo(event2ToET.getText().toString());
        event2.setPrice(event2PriceET.getText().toString());
        eventParking.setEvent2(event2);

        ModelConfiguration1.EventParkingBean.Event3Bean event3 = new ModelConfiguration1.EventParkingBean.Event3Bean();
        event3.setFrom(event3FromET.getText().toString());
        event3.setTo(event3ToET.getText().toString());
        event3.setPrice(event3PriceET.getText().toString());
        eventParking.setEvent3(event3);

        modelConfiguration.setEventParking(eventParking);

        Gson gson = new Gson();
        String payload = gson.toJson(modelConfiguration);
        Log.e("test", "payload: " + payload);

        new WebServicePost(this, new HandlerSubmitParkingConfigurationResponse(), Constants.ADD_PARKING_CONFIGURATION_URL, payload).execute();
        Log.e("test", "URL  : " + Constants.ADD_PARKING_CONFIGURATION_URL);

    }

    private class HandlerdateListResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ViewdatesconfigClass viewdatesconfigClass = new Gson().fromJson(response, ViewdatesconfigClass.class);

                if (viewdatesconfigClass.getStatus() == 200) {
                    Toast.makeText(ActivityParkingConfiguration.this, "successfully dates", Toast.LENGTH_SHORT).show();

                    try {
                        CONFIGURATION_ID = viewdatesconfigClass.getModelAttendantList().get(0).getParkingConfigurationId();
                        Log.e(TAG, "Configuration ID :::" + CONFIGURATION_ID);
                        if (viewdatesconfigClass.getModelAttendantList().get(0).getCount() != null) {
                            finalcount = viewdatesconfigClass.getModelAttendantList().get(0).getCount();
                        } else {
                            finalcount = "0";
                        }

                        // General  car
                        generalET_bike.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getGeneral().get(0).getBike());
                        generalET_bus.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getGeneral().get(0).getBus());
                        generalET.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getGeneral().get(0).getCar());
                        generalET_truck.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getGeneral().get(0).getTruck());

                        //For Reserved
                        reservedET_bike.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getReserved().get(0).getBike());
                        reservedET_bus.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getReserved().get(0).getBus());
                        reservedET.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getReserved().get(0).getCar());
                        reservedET_truck.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getReserved().get(0).getTruck());

                        totalTV.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTotal());

                        //For Regular

                        regularET_bike.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getRegular().get(0).getBike());
                        regularET_bus.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getRegular().get(0).getBus());
                        regularET.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getRegular().get(0).getCar());
                        regularET_truck.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getRegular().get(0).getTruck());


                        //For EVENT

                        eventET_bike.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getEvent().get(0).getBike());
                        eventET_bus.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getEvent().get(0).getBus());
                        eventET.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getEvent().get(0).getCar());
                        eventET_truck.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getEvent().get(0).getTruck());

                        //for Premium
                        premiumET_bike.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getPremium().get(0).getBike());
                        premiumET_bus.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getPremium().get(0).getBus());
                        premiumET.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getPremium().get(0).getCar());
                        premiumET_truck.setText("" + viewdatesconfigClass.getModelAttendantList().get(0).getPremium().get(0).getTruck());

                        //Set Hour
                        hoursFrom0ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(0).getFrom());
                        hoursTo0ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(0).getTo());

                        hoursFrom1ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(1).getFrom());
                        hoursTo1ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(1).getTo());

                        hoursFrom2ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(2).getFrom());
                        hoursTo2ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(2).getTo());

                        hoursFrom3ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(3).getFrom());
                        hoursTo3ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(3).getTo());

                        hoursFrom4ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(4).getFrom());
                        hoursTo4ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(4).getTo());

                        hoursFrom5ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(5).getFrom());
                        hoursTo5ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(5).getTo());

                        hoursFrom6ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(6).getFrom());
                        hoursTo6ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(6).getTo());

                        hoursFrom7ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(7).getFrom());
                        hoursTo7ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(7).getTo());


                        //for car price
                        general0ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(0).getPriceG());
                        reserve0ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(0).getPariceR());
                        premium0ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(0).getPriceP());

                        general1ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(1).getPriceG());
                        reserve1ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(1).getPariceR());
                        premium1ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(1).getPriceP());


                        general2ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(2).getPriceG());
                        reserve2ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(2).getPariceR());
                        premium2ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(2).getPriceP());


                        general3ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(3).getPriceG());
                        reserve3ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(3).getPariceR());
                        premium3ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(3).getPriceP());

                        general4ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(4).getPriceG());
                        reserve4ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(4).getPariceR());
                        premium4ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(4).getPriceP());

                        general5ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(5).getPriceG());
                        reserve5ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(5).getPariceR());
                        premium5ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(5).getPriceP());

                        general6ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(6).getPriceG());
                        reserve6ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(6).getPariceR());
                        premium6ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(6).getPriceP());

                        general7ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(7).getPriceG());
                        reserve7ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(7).getPariceR());
                        premium7ET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCarPriceValues().get(7).getPriceP());

                        //for Bike price

                        general0ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(0).getPriceG());
                        reserve0ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(0).getPariceR());
                        premium0ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(0).getPriceP());

                        general1ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(1).getPriceG());
                        reserve1ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(1).getPariceR());
                        premium1ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(1).getPriceP());


                        general2ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(2).getPriceG());
                        reserve2ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(2).getPariceR());
                        premium2ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(2).getPriceP());


                        general3ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(3).getPriceG());
                        reserve3ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(3).getPariceR());
                        premium3ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(3).getPriceP());

                        general4ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(4).getPriceG());
                        reserve4ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(4).getPariceR());
                        premium4ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(4).getPriceP());

                        general5ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(5).getPriceG());
                        reserve5ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(5).getPariceR());
                        premium5ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(5).getPriceP());

                        general6ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(6).getPriceG());
                        reserve6ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(6).getPariceR());
                        premium6ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(6).getPriceP());

                        general7ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(7).getPriceG());
                        reserve7ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(7).getPariceR());
                        premium7ET_bike.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBikePriceValues().get(7).getPriceP());


                        //for BUS price

                        general0ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(0).getPriceG());
                        reserve0ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(0).getPariceR());
                        premium0ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(0).getPriceP());

                        general1ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(1).getPriceG());
                        reserve1ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(1).getPariceR());
                        premium1ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(1).getPriceP());


                        general2ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(2).getPriceG());
                        reserve2ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(2).getPariceR());
                        premium2ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(2).getPriceP());


                        general3ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(3).getPriceG());
                        reserve3ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(3).getPariceR());
                        premium3ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(3).getPriceP());


                        general4ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(4).getPriceG());
                        reserve4ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(4).getPariceR());
                        premium4ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(4).getPriceP());

                        general5ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(5).getPriceG());
                        reserve5ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(5).getPariceR());
                        premium5ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(5).getPriceP());

                        general6ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(6).getPriceG());
                        reserve6ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(6).getPariceR());
                        premium6ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(6).getPriceP());

                        general7ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(7).getPriceG());
                        reserve7ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(7).getPariceR());
                        premium7ET_bus.setText(viewdatesconfigClass.getModelAttendantList().get(0).getBusPriceValues().get(7).getPriceP());

                        //for Truck price
                        general0ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(0).getPriceG());
                        reserve0ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(0).getPariceR());
                        premium0ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(0).getPriceP());

                        general1ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(1).getPriceG());
                        reserve2ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(1).getPariceR());
                        premium1ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(1).getPriceP());


                        general2ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(2).getPriceG());
                        //reserve2ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(2).getPariceR());
                        reserve1ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(2).getPariceR());
                        premium2ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(2).getPriceP());


                        general3ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(3).getPriceG());
                        reserve3ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(3).getPariceR());
                        premium3ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(3).getPriceP());

                        general4ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(4).getPriceG());
                        reserve4ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(4).getPariceR());
                        premium4ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(4).getPriceP());

                        general5ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(5).getPriceG());
                        reserve5ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(5).getPariceR());
                        premium5ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(5).getPriceP());

                        general6ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(6).getPriceG());
                        reserve6ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(6).getPariceR());
                        premium6ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(6).getPriceP());

                        general7ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(7).getPriceG());
                        reserve7ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(7).getPariceR());
                        premium7ET_truck.setText(viewdatesconfigClass.getModelAttendantList().get(0).getTruckPriceValues().get(7).getPriceP());


                        regularDayFromTV.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getDay().getFrom());
                        regularDayToTV.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getDay().getTo());
                        regularDayPriceET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getDay().getPrice());
                        regularNightFromTV.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getNight().getFrom());
                        regularNightToTV.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getNight().getTo());
                        regularNightPriceET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getRegularParking().getNight().getPrice());
////
                        event1FromET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent1().getFrom());
                        event1ToET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent1().getTo());
                        event1PriceET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent1().getPrice());
                        event2FromET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent2().getFrom());
                        event2ToET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent2().getTo());
                        event2PriceET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent2().getPrice());
                        event3FromET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent3().getFrom());
                        event3ToET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent3().getTo());
                        event3PriceET.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEventParking().getEvent3().getPrice());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ActivityParkingConfiguration.this, viewdatesconfigClass.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private class HandlerSubmitParkingConfigurationResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelSubmitParkingConfigurationResponse modelSubmitParkingConfigurationResponse = new Gson().fromJson(response, ModelSubmitParkingConfigurationResponse.class);
                if (modelSubmitParkingConfigurationResponse.getStatus() == 200) {
                    if (fromEdit) {
                        configID = modelSubmitParkingConfigurationResponse.getSubmitParkingConfigurationResponseList().get(0).getInsertParkingConfiguration();
                    }
                    Log.e(TAG, "CONFIG:::" + configID);
                    setResult(RESULT_OK);
                    finish();
                }
                Toast.makeText(ActivityParkingConfiguration.this, modelSubmitParkingConfigurationResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                finish();
                break;
            case R.id.submitTV:
                if (Utility.isNetworkAvailable(this)) {
                    String totalspace = String.valueOf(Total);
                    double total_space = Double.parseDouble(totalspace);
                    Log.e(TAG, "TOTAL SPACE" + total_space);
                    Log.e(TAG, "TOTAL SPACE DOUBLE" + total);
//                    if (!hoursFrom0ET.getText().toString().equals("") && !hoursTo0ET.getText().toString().equals("") && !hoursFrom1ET.getText().toString().equals("")
//                            && !hoursTo1ET.getText().toString().equals("") && !hoursFrom2ET.getText().toString().equals("") && !hoursTo2ET.getText().toString().equals("")
//                            && !hoursFrom3ET.getText().toString().equals("") && !hoursTo3ET.getText().toString().equals("")) {
//                        //  Toast.makeText(ActivityParkingConfiguration.this, "Please Enter at least one vacant Type ", Toast.LENGTH_SHORT).show();
//
//                        if (!generalET.getText().toString().equals("")) {
//                            if (!general0ET.getText().toString().equals("") && !general1ET.getText().toString().equals("") && !general2ET.getText().toString().equals("") && !general3ET.getText().toString().equals("")) {
//                                submitParkingConfiguration();
//                            } else
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please Fill General Car price Fields", Toast.LENGTH_SHORT).show();
//                        }
//
//                        if (!generalET_bike.getText().toString().equals("")) {
//                            if (!general0ET_bike.getText().toString().equals("") && !general1ET_bike.getText().toString().equals("") && !general2ET_bike.getText().toString().equals("") && !general3ET_bike.getText().toString().equals("")) {
//                                submitParkingConfiguration();
//                            } else
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please Fill General Bike price Fields", Toast.LENGTH_SHORT).show();
//                        } else if (!generalET_bus.getText().toString().equals("")) {
//                            if (!general0ET_bus.getText().toString().equals("") && !general1ET_bus.getText().toString().equals("") && !general2ET_bus.getText().toString().equals("") && !general3ET_bus.getText().toString().equals("")) {
//                                submitParkingConfiguration();
//                            } else
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please Fill General Bus price Fields", Toast.LENGTH_SHORT).show();
//                        } else if (!generalET_truck.getText().toString().equals("")) {
//                            if (!general0ET_truck.getText().toString().equals("") && !general1ET_truck.getText().toString().equals("") && !general2ET_truck.getText().toString().equals("") && !general3ET_truck.getText().toString().equals("")) {
//                                submitParkingConfiguration();
//                            } else
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please Fill General Truck price Fields", Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        Toast.makeText(ActivityParkingConfiguration.this, "Please Fill Hours Fields", Toast.LENGTH_SHORT).show();
//                    }
                    submitParkingConfiguration();
//                    if (generalET.getText().toString().equals("")) {
//
//                    } else {
//                        if (hoursFrom0ET.getText().toString().equals("") || hoursTo0ET.getText().toString().equals("") || hoursFrom1ET.getText().toString().equals("")
//                                || hoursTo1ET.getText().toString().equals("") || hoursFrom2ET.getText().toString().equals("") || hoursTo2ET.getText().toString().equals("")
//                                || hoursFrom3ET.getText().toString().equals("") || hoursTo3ET.getText().toString().equals("")) {
//                            Toast.makeText(ActivityParkingConfiguration.this, "Please enter time slots", Toast.LENGTH_SHORT).show();
//                        } else {
//                            if (general0ET.getText().toString().equals("") || general1ET.getText().toString().equals("") || general2ET.getText().toString().equals("") || general3ET.getText().toString().equals("")) {
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please enter Car General Prices", Toast.LENGTH_SHORT).show();
//                            } else {
//                                submitParkingConfiguration();
////                                Toast.makeText(ActivityParkingConfiguration.this, "Validations Ok", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    if (general0ET_bike.getText().toString().equals("")) {
//
//                    } else {
//                        if (hoursFrom0ET.getText().toString().equals("") || hoursTo0ET.getText().toString().equals("") || hoursFrom1ET.getText().toString().equals("")
//                                || hoursTo1ET.getText().toString().equals("") || hoursFrom2ET.getText().toString().equals("") || hoursTo2ET.getText().toString().equals("")
//                                || hoursFrom3ET.getText().toString().equals("") || hoursTo3ET.getText().toString().equals("")) {
//                            Toast.makeText(ActivityParkingConfiguration.this, "Please enter time slots", Toast.LENGTH_SHORT).show();
//                        } else {
//                            if (general0ET_bike.getText().toString().equals("") || general1ET_bike.getText().toString().equals("") || general2ET_bike.getText().toString().equals("") || general3ET_bike.getText().toString().equals("")) {
//                                Toast.makeText(ActivityParkingConfiguration.this, "Please enter Bike General Prices", Toast.LENGTH_SHORT).show();
//                            } else {
//                                  submitParkingConfiguration();
////                                Toast.makeText(ActivityParkingConfiguration.this, "Validations Ok", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }

                } else {
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;

//            case R.id.calendarDateRelativeLayout:
//                new DatePickerDialog(this, calendarDate, Calendar.getInstance()
//                        .get(Calendar.YEAR), startCalendar.get(Calendar.MONTH),
//                        startCalendar.get(Calendar.DAY_OF_MONTH)).show();

            //Intent i = new Intent(ActivityParkingConfiguration.this, Picker.class);
            //  startActivity(i);

            //  break;

            case R.id.regularDayFromTV:
                selectTime(Calendar.getInstance(), regularDayFromTV);
                break;
            case R.id.regularDayToTV:
                selectTime(Calendar.getInstance(), regularDayToTV);
                break;
            case R.id.regularNightFromTV:
                selectTime(Calendar.getInstance(), regularNightFromTV);
                break;
            case R.id.regularNightToTV:
                selectTime(Calendar.getInstance(), regularNightToTV);
                break;
            default:
                break;
        }
    }


}
