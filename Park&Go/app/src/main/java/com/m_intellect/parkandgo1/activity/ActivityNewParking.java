package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelNewParking;
import com.m_intellect.parkandgo1.model.ModelNewParkingResponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityNewParking extends AppCompatActivity implements View.OnClickListener {

    private EditText cityET;
    private EditText stateET;
    private EditText mapLocationET;
    private EditText locationNameET;
    private EditText parkingNameET;
    private EditText pincodeET;
    private EditText totalSpaceET;
    private String latitude, longitude, locationName;
    private ImageView locationIV;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_parking);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initToolbar();
        initView();
    }

    private void initView() {

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        TextView submitTV = (TextView) findViewById(R.id.submitTV);
        submitTV.setOnClickListener(this);

        pincodeET = (EditText) findViewById(R.id.pincodeET);
        cityET = (EditText) findViewById(R.id.cityET);
        stateET = (EditText) findViewById(R.id.stateET);
        mapLocationET = (EditText) findViewById(R.id.mapLocationET);
        mapLocationET.setHint("Add your map location");
        mapLocationET.setOnClickListener(this);
        locationNameET = (EditText) findViewById(R.id.locationNameET);
        parkingNameET = (EditText) findViewById(R.id.parkingNameET);
        totalSpaceET = (EditText) findViewById(R.id.totalSpaceET);
        locationIV = (ImageView) findViewById(R.id.locationIV);

    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("New Parking");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;
            case R.id.submitTV:
                if (Utility.isNetworkAvailable(this)) {
                    if (isValid()) {
                        if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty())
                            newParking();
                        else
                            Toast.makeText(this, R.string.error_map_location, Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                break;
            case R.id.mapLocationET:
                Intent intent = new Intent(this, ActivityAddMapLocation.class);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                startActivityForResult(intent, 111);
                break;
            default:
                break;
        }
    }

    private boolean isValid() {

        if (pincodeET.getText().toString().length() < 6) {
            pincodeET.setError(getString(R.string.error_pincode));
        } else {
            pincodeET.setError(null);
        }
        if (cityET.getText().toString().length() == 0) {
            cityET.setError(getString(R.string.error_city));
        } else {
            cityET.setError(null);
        }
        if (stateET.getText().toString().length() == 0) {
            stateET.setError(getString(R.string.error_state));
        } else {
            stateET.setError(null);
        }

        if (locationNameET.getText().toString().length() == 0) {
            locationNameET.setError(getString(R.string.error_location_name));
        } else {
            locationNameET.setError(null);
        }
        if (parkingNameET.getText().toString().length() == 0) {
            parkingNameET.setError(getString(R.string.error_parking_name));
        } else {
            parkingNameET.setError(null);
        }
        if (totalSpaceET.getText().toString().length() == 0) {
            totalSpaceET.setError(getString(R.string.error_total_space));
        } else {
            totalSpaceET.setError(null);
        }

        return pincodeET.getError() == null && cityET.getError() == null &&
                stateET.getError() == null && locationNameET.getError() == null &&
                parkingNameET.getError() == null && totalSpaceET.getError() == null;
    }

    private void newParking() {

        ModelNewParking modelNewParking = new ModelNewParking();
        modelNewParking.setParkingId(0);
        modelNewParking.setAppId(Constants.APP_ID);
        modelNewParking.setLocationId(0);
        modelNewParking.setLatitude(latitude);
        modelNewParking.setLongitude(longitude);
        modelNewParking.setPlaceName(parkingNameET.getText().toString());
        modelNewParking.setPlaceLandmark(locationNameET.getText().toString());
        modelNewParking.setCity(cityET.getText().toString());
        modelNewParking.setState(stateET.getText().toString());
        modelNewParking.setPincode(Integer.parseInt(pincodeET.getText().toString()));
        modelNewParking.setEmail(preferences.getString(Constants.EMAIL_ID, ""));
        modelNewParking.setMobileNo(preferences.getString(Constants.MOBILE_NO, ""));
        modelNewParking.setTotalSpace(Integer.parseInt(totalSpaceET.getText().toString()));
        modelNewParking.setUserId(preferences.getInt(Constants.USER_ID, 0));

        Gson gson = new Gson();
        String payload = gson.toJson(modelNewParking);
        Log.d("test", "payload: " + payload);

        new WebServicePost(this, new HandlerNewParkingResponse(), Constants.NEW_PARKING_URL, payload).execute();

    }

    private class HandlerNewParkingResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelNewParkingResponse modelNewParkingResponse = new Gson().fromJson(response, ModelNewParkingResponse.class);
                if (modelNewParkingResponse.getStatus() == 200) {
                    setResult(RESULT_OK);
                    finish();
                }
                Toast.makeText(ActivityNewParking.this, modelNewParkingResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {

            if (data != null) {
                latitude = data.getStringExtra(Constants.LATITUDE);
                longitude = data.getStringExtra(Constants.LONGITUDE);
                locationName = data.getStringExtra(Constants.LOCATION_NAME);

                //mapLocationET.setText(latitude + ", " + longitude);
                if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty()) {
                    locationIV.setVisibility(View.VISIBLE);
                    mapLocationET.setHint("Map location is added");
                } else {
                    locationIV.setVisibility(View.GONE);
                    mapLocationET.setHint("Add your map location");
                }
                locationNameET.setText(locationName);
            }

        }
    }
}
