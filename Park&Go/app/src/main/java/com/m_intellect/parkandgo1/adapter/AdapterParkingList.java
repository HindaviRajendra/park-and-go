package com.m_intellect.parkandgo1.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityParkingConfiguration;
import com.m_intellect.parkandgo1.activity.DailySnapshot;
import com.m_intellect.parkandgo1.activity.Picker;
import com.m_intellect.parkandgo1.model.DeleteParkRequest;
import com.m_intellect.parkandgo1.model.DeleteParkingClass;
import com.m_intellect.parkandgo1.model.Viewparkresponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanket on 29-01-2018.
 */

public class AdapterParkingList extends RecyclerView.Adapter<AdapterParkingList.ViewHolder> {

    public List<Viewparkresponse.DataBean> modelParkingResponseList;
    int sum = 0;
    private Context context;
    private boolean isFromParkingList;
    private String Total_coll;
    private String TAG = getClass().getSimpleName();
    private List<Viewparkresponse.DataBean.GeneralBean> generalBeanList = new ArrayList<>();
    private int carvac, bikevac, busvac, truckvac, vacant;
    private int general_vacant = 0, reserved_vac = 0, regular_vac = 0, pre_vacant = 0, event_vac = 0;

    public AdapterParkingList(Context context, List<Viewparkresponse.DataBean> modelParkingResponseList, boolean isFromParkingList) {
        this.context = context;
        this.modelParkingResponseList = modelParkingResponseList;
        this.isFromParkingList = isFromParkingList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_parking, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Constants.COLL_TOTAL = 0;
        final Viewparkresponse.DataBean modelParkingResponse = modelParkingResponseList.get(position);


        holder.titleTV.setText(modelParkingResponse.getPlaceName());
        holder.addressTV.setText(modelParkingResponse.getPlaceLandmark());
        if (modelParkingResponse.getCash() == null) {
            holder.txt_cash.setText("Cash - " + 0);
        } else {
            holder.txt_cash.setText("Cash - " + modelParkingResponse.getCash());
        }

        if (modelParkingResponse.getWallet() == null) {
            holder.txt_wallet.setText("Wallet - " + 0);
        } else {
            holder.txt_wallet.setText("Wallet - " + modelParkingResponse.getWallet());
        }

        if (modelParkingResponse.getGeneral() != null && !modelParkingResponse.getGeneral().isEmpty()) {
            general_vacant = modelParkingResponse.getGeneral().get(0).getBike() +
                    modelParkingResponse.getGeneral().get(0).getBus() +
                    modelParkingResponse.getGeneral().get(0).getCar() +
                    modelParkingResponse.getGeneral().get(0).getTruck();
            Log.e(TAG, "GENERAL VACANT>>>>" + general_vacant);
        } else {
            general_vacant = 0;
        }


        if (modelParkingResponse.getReserved() != null && !modelParkingResponse.getReserved().isEmpty()) {
            reserved_vac = modelParkingResponse.getReserved().get(0).getBike() +
                    modelParkingResponse.getReserved().get(0).getBus() +
                    modelParkingResponse.getReserved().get(0).getCar() +
                    modelParkingResponse.getReserved().get(0).getTruck();

            Log.e(TAG, "Reserved VACANT>>>>" + reserved_vac);
        } else {
            reserved_vac = 0;
        }


        if (modelParkingResponse.getRegular() != null && !modelParkingResponse.getRegular().isEmpty()) {

            regular_vac = modelParkingResponse.getRegular().get(0).getBike() +
                    modelParkingResponse.getRegular().get(0).getBus() +
                    modelParkingResponse.getRegular().get(0).getCar() +
                    modelParkingResponse.getRegular().get(0).getTruck();

            Log.e(TAG, "Regular VACANT>>>>" + regular_vac);
        } else {
            regular_vac = 0;
        }


        if (modelParkingResponse.getEvent() != null && !modelParkingResponse.getEvent().isEmpty()) {
            event_vac = modelParkingResponse.getEvent().get(0).getBike() +
                    modelParkingResponse.getEvent().get(0).getBus() +
                    modelParkingResponse.getEvent().get(0).getCar() +
                    modelParkingResponse.getEvent().get(0).getTruck();
            Log.e(TAG, "Event VACANT>>>>" + event_vac);
        } else {
            event_vac = 0;
        }

        if (modelParkingResponse.getPremium() != null && !modelParkingResponse.getPremium().isEmpty()) {
            pre_vacant = modelParkingResponse.getPremium().get(0).getBike() +
                    modelParkingResponse.getPremium().get(0).getBus() +
                    modelParkingResponse.getPremium().get(0).getCar() +
                    modelParkingResponse.getPremium().get(0).getTruck();
            Log.e(TAG, "Premium VACANT>>>>" + pre_vacant);
        } else {
            pre_vacant = 0;
        }

        vacant = general_vacant + reserved_vac + regular_vac + event_vac + pre_vacant;
        Log.e(TAG, "Total VACANT>>>>" + vacant);

        if (!modelParkingResponse.getGeneral().isEmpty() && !modelParkingResponse.getReserved().isEmpty() &&
                !modelParkingResponse.getRegular().isEmpty() && !modelParkingResponse.getEvent().isEmpty() && !modelParkingResponse.getPremium().isEmpty()) {
            carvac = modelParkingResponse.getGeneral().get(0).getCar() +
                    modelParkingResponse.getReserved().get(0).getCar() +
                    modelParkingResponse.getRegular().get(0).getCar() +
                    modelParkingResponse.getEvent().get(0).getCar() +
                    modelParkingResponse.getPremium().get(0).getCar();
            Log.e(TAG, "carvac VACANT>>>>" + carvac);

            bikevac = modelParkingResponse.getGeneral().get(0).getBike() +
                    modelParkingResponse.getReserved().get(0).getBike() +
                    modelParkingResponse.getRegular().get(0).getBike() +
                    modelParkingResponse.getEvent().get(0).getBike() + modelParkingResponse.getPremium().get(0).getBike();
            Log.e(TAG, "bikevac VACANT>>>>" + bikevac);


            busvac = modelParkingResponse.getGeneral().get(0).getBus() +
                    modelParkingResponse.getReserved().get(0).getBus() +
                    modelParkingResponse.getRegular().get(0).getBus() +
                    modelParkingResponse.getEvent().get(0).getBus() + modelParkingResponse.getPremium().get(0).getBus();
            Log.e(TAG, "busvac VACANT>>>>" + busvac);


            truckvac = modelParkingResponse.getGeneral().get(0).getTruck() +
                    modelParkingResponse.getReserved().get(0).getTruck() +
                    modelParkingResponse.getRegular().get(0).getTruck() +
                    modelParkingResponse.getEvent().get(0).getTruck() + modelParkingResponse.getPremium().get(0).getTruck();
            Log.e(TAG, "truckvac VACANT>>>>" + truckvac);

        } else {
            carvac = 0;
            bikevac = 0;
            busvac = 0;
            truckvac = 0;
        }

        holder.car_vac.setText("" + carvac);
        holder.Bike_vac.setText("" + bikevac);
        holder.Bus_vac_.setText("" + busvac);
        holder.Truck_vac.setText("" + truckvac);


        if (carvac == 0) {
            holder.vacantTV.setText("Vacant - " + modelParkingResponse.getTotalSpace());

        } else {
            holder.vacantTV.setText("Vacant - " + vacant);
        }


        if (isFromParkingList) {
            holder.totalSpaceTV.setVisibility(View.VISIBLE);
            holder.vacantTV.setVisibility(View.VISIBLE);
            holder.totalCollectionTV.setVisibility(View.VISIBLE);
            holder.viewDetailsTV.setVisibility(View.VISIBLE);
            int totalsp = 0;

            if (modelParkingResponse.getCount() != null) {
                totalsp = modelParkingResponse.getVacant() + Integer.parseInt(modelParkingResponse.getCount());
                holder.totalSpaceTV.setText("Total Space - " + totalsp);
            } else {
                totalsp = modelParkingResponse.getVacant() + 0;
                holder.totalSpaceTV.setText("Total Space - " + totalsp);
            }


            if (modelParkingResponse.getSum() != null && !modelParkingResponse.getSum().isEmpty()) {
                holder.totalCollectionTV.setText("Collection ₹" + modelParkingResponse.getTotal_collection());
                Total_coll = modelParkingResponse.getSum();
                sum += Integer.parseInt(Total_coll);
                Constants.COLL_TOTAL = sum;
            } else {
                holder.totalCollectionTV.setText("Collection ₹0");
            }
        } else {
            holder.totalSpaceTV.setVisibility(View.GONE);
            holder.vacantTV.setVisibility(View.GONE);
            holder.totalCollectionTV.setVisibility(View.GONE);
            holder.viewDetailsTV.setVisibility(View.GONE);
        }

        holder.viewDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Picker.class);
                intent.putExtra("parkingname", modelParkingResponse.getPlaceName());
                intent.putExtra("parkID", modelParkingResponse.getParkingId());
                Log.e(TAG, "parkingID" + modelParkingResponse.getParkingId());
                context.startActivity(intent);
            }
        });

        holder.view_dailysnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DailySnapshot.class);
                i.putExtra("parkID", modelParkingResponse.getParkingId());
                context.startActivity(i);
            }
        });

        holder.img_delete.setTag(modelParkingResponse.getParkingId());
        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Are you sure you want to delete ?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteparking(modelParkingResponse.getParkingId());
                        modelParkingResponseList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, modelParkingResponseList.size());
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return modelParkingResponseList.size();
    }

    private void deleteparking(int parkingId) {
        DeleteParkRequest deleteParkRequest = new DeleteParkRequest();
        deleteParkRequest.setAppId(Constants.APP_ID);
        deleteParkRequest.setParkingId(parkingId);
        Gson gson = new Gson();
        String payload = gson.toJson(deleteParkRequest);
        Log.e("test", "payload: " + payload);
        new WebServicePost(context, new HandlerAddAttendantResponse(), Constants.DELETE_PARK, payload).execute();
    }

    private class HandlerAddAttendantResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            if (response != null) {
                DeleteParkingClass deleteParkingClass = new Gson().fromJson(response, DeleteParkingClass.class);
                if (deleteParkingClass.getStatus() == 200) {
                    Toast.makeText(context, "Attendance Successfully deleted", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView titleTV, addressTV, totalSpaceTV, vacantTV, viewDetailsTV, totalCollectionTV, view_dailysnap;
        private TextView car_vac, Bike_vac, Bus_vac_, Truck_vac, txt_cash, txt_wallet;
        private ImageView img_delete;
        private LinearLayout linear_Tspace, linear_Vspace, linear_main;
        private View view;


        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            totalSpaceTV = (TextView) itemView.findViewById(R.id.totalSpaceTV);
            viewDetailsTV = (TextView) itemView.findViewById(R.id.viewDetailsTV);
            vacantTV = (TextView) itemView.findViewById(R.id.vacantTV);
            view_dailysnap = (TextView) itemView.findViewById(R.id.viewDetails);
            totalCollectionTV = (TextView) itemView.findViewById(R.id.totalCollectionTV);
            img_delete = (ImageView) itemView.findViewById(R.id.delete);
            linear_Vspace = (LinearLayout) itemView.findViewById(R.id.linear_Vspace);
            linear_main = (LinearLayout) itemView.findViewById(R.id.linear_main);

            car_vac = (TextView) itemView.findViewById(R.id.pzerotooneTV);
            Bike_vac = (TextView) itemView.findViewById(R.id.ptwotothreeTV);
            Bus_vac_ = (TextView) itemView.findViewById(R.id.pthreetosixTV);
            Truck_vac = (TextView) itemView.findViewById(R.id.psixtotwelveTV);
            txt_cash = (TextView) itemView.findViewById(R.id.txt_cash);
            txt_wallet = (TextView) itemView.findViewById(R.id.txt_wallet);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (!isFromParkingList) {
                ((ActivityParkingConfiguration) context).goToAddConfiguration(modelParkingResponseList.get(getAdapterPosition()).getParkingId(), modelParkingResponseList.get(getAdapterPosition()).getTotalSpace());
            }
        }
    }
}
