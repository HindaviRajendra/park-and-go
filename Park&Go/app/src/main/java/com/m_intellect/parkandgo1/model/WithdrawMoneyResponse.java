package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class WithdrawMoneyResponse {


    /**
     * data : {"TxnId":"20190104111212800110168685300128800","MID":"ABRALI53876941570316","OrderId":"1","TxnAmount":"10.00","BankTxnId":"6024053","ResponseCode":"01","ResponseMessage":"Txn Successful.","Status":"TXN_SUCCESS","PaymentMode":"PPI","BankName":"WALLET","CustId":"1040","MBID":"WALLET","CheckSum":"YQQCVQsnTEowJvAOTps4kjY0YzDKj+opz52SSUWevLNVEHTJc3rmY8a32WGrPPY39OM17DEhrcMf0xKVIIy4O0ylQWrsb012/T2up0qr+Q0="}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * TxnId : 20190104111212800110168685300128800
         * MID : ABRALI53876941570316
         * OrderId : 1
         * TxnAmount : 10.00
         * BankTxnId : 6024053
         * ResponseCode : 01
         * ResponseMessage : Txn Successful.
         * Status : TXN_SUCCESS
         * PaymentMode : PPI
         * BankName : WALLET
         * CustId : 1040
         * MBID : WALLET
         * CheckSum : YQQCVQsnTEowJvAOTps4kjY0YzDKj+opz52SSUWevLNVEHTJc3rmY8a32WGrPPY39OM17DEhrcMf0xKVIIy4O0ylQWrsb012/T2up0qr+Q0=
         */

        @SerializedName("TxnId")
        private String TxnId;
        @SerializedName("MID")
        private String MID;
        @SerializedName("OrderId")
        private String OrderId;
        @SerializedName("TxnAmount")
        private String TxnAmount;
        @SerializedName("BankTxnId")
        private String BankTxnId;
        @SerializedName("ResponseCode")
        private String ResponseCode;
        @SerializedName("ResponseMessage")
        private String ResponseMessage;
        @SerializedName("Status")
        private String Status;
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @SerializedName("BankName")
        private String BankName;
        @SerializedName("CustId")
        private String CustId;
        @SerializedName("MBID")
        private String MBID;
        @SerializedName("CheckSum")
        private String CheckSum;

        public String getTxnId() {
            return TxnId;
        }

        public void setTxnId(String TxnId) {
            this.TxnId = TxnId;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getOrderId() {
            return OrderId;
        }

        public void setOrderId(String OrderId) {
            this.OrderId = OrderId;
        }

        public String getTxnAmount() {
            return TxnAmount;
        }

        public void setTxnAmount(String TxnAmount) {
            this.TxnAmount = TxnAmount;
        }

        public String getBankTxnId() {
            return BankTxnId;
        }

        public void setBankTxnId(String BankTxnId) {
            this.BankTxnId = BankTxnId;
        }

        public String getResponseCode() {
            return ResponseCode;
        }

        public void setResponseCode(String ResponseCode) {
            this.ResponseCode = ResponseCode;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getBankName() {
            return BankName;
        }

        public void setBankName(String BankName) {
            this.BankName = BankName;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getMBID() {
            return MBID;
        }

        public void setMBID(String MBID) {
            this.MBID = MBID;
        }

        public String getCheckSum() {
            return CheckSum;
        }

        public void setCheckSum(String CheckSum) {
            this.CheckSum = CheckSum;
        }
    }
}
