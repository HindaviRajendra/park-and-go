package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.DeleteConfigReq;
import com.m_intellect.parkandgo1.model.DeleteDateConfigClass;
import com.m_intellect.parkandgo1.model.ViewdateClass;
import com.m_intellect.parkandgo1.model.ViewdatesResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.EventDecorator;
import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.DefaultDayViewAdapter;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.APP_ID;
import static com.m_intellect.parkandgo1.utils.Constants.GET_DATES;

/**
 * Created by Admin on 25-04-2017.
 */

public class Picker extends AppCompatActivity implements View.OnClickListener {
    Button button;
    CalendarPickerView.FluentInitializer fluentInitializer;
    ViewdateClass viewdateClass;
    ArrayList<String> datelist = new ArrayList<>();
    List<ViewdatesResponse.DataBean> list = new ArrayList<>();
    java.util.Date date = null;
    Button done_button, edit_button, delete_button;
    boolean adddate = true;
    ArrayList<String> datparselist = new ArrayList<>();
    SimpleDateFormat df;
    private CalendarPickerView calendar;
    private String TAG = getClass().getSimpleName();
    private ArrayList<java.util.Date> dates = new ArrayList<>();
    private ArrayList<String> dateStrings = new ArrayList<>();
    private String park_name;
    private int parkID;
    private Calendar nextYear, lastYear;
    private String str_date;
    private Date myDate = null;
    private String strDate;
    private int count = 0;
    private int ParkingconfigID = 0;

    // @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datepicker);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Bundle b = getIntent().getExtras();
        if (b != null) {
            park_name = b.getString("parkingname");
            parkID = b.getInt("parkID");
            Log.e(TAG, "parkingID" + parkID);
        }

        inittoolbar();
        button = (Button) findViewById(R.id.done_button);
        button.setOnClickListener(this);

        delete_button = (Button) findViewById(R.id.delete_button);
        delete_button.setOnClickListener(this);

        edit_button = (Button) findViewById(R.id.edit_button);
        edit_button.setOnClickListener(this);

        nextYear = Calendar.getInstance();
        if (dates.size() > 0) {
            dates.clear();
        }
        nextYear.add(Calendar.YEAR, 10);

        lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -10);
        calendar = (CalendarPickerView) findViewById(R.id.FROMD);
        calendar.setCustomDayView(new DefaultDayViewAdapter());
        calendar.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        calendar.init(lastYear.getTime(), nextYear.getTime())
                .withSelectedDate((new java.util.Date()))
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE);


        viewdates();
        Log.e(TAG, "INITIAL Dates array::::" + dates);

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(java.util.Date date) {

                df = new SimpleDateFormat("yyyy-MM-dd");
                strDate = df.format(date);

                datparselist.add(strDate);

                count = 1;

                for (int i = 0; i < list.size(); i++) {
                    //T00:00:00.000Z
                    if (list.get(i).getCalender().equals(strDate + "T18:30:00.000Z")) {
                        button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                        edit_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        delete_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        button.setEnabled(false);
                        edit_button.setEnabled(true);
                        delete_button.setEnabled(true);
                        //   Toast.makeText(Picker.this, "Date are Already Configured", Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        dates.add(date);
//                        df = new SimpleDateFormat("yyyy-MM-dd");
//                        strDate = df.format(date);
//                        datparselist.add(strDate);
                        button.setEnabled(true);
                        button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        edit_button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                        edit_button.setEnabled(false);
                        delete_button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                        delete_button.setEnabled(false);
                    }
                }

            }

            @Override
            public void onDateUnselected(java.util.Date data) {

                dates.remove(data);
                df = new SimpleDateFormat("yyyy-MM-dd");
                strDate = df.format(data);
                datparselist.remove(strDate);

                button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                edit_button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                delete_button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                button.setEnabled(true);
                edit_button.setEnabled(false);
                delete_button.setEnabled(false);

            }
        });

        if (strDate == null && count == 0) {
            button.setEnabled(false);
            // Toast.makeText(Picker.this, "Please  select at least one date", Toast.LENGTH_SHORT).show();
            button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
        }

    }

    private void viewdates() {

        new WebServiceGet(this, new HandlerdatesResponse(), Constants.VIEW_DATES + parkID).execute();
        Log.e(TAG, "VIEW DATES URL:::" + Constants.VIEW_DATES + parkID);

    }

    private void inittoolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);

        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText(park_name);

        ImageView Attendance = (ImageView) toolbar.findViewById(R.id.attendance);
        Attendance.setOnClickListener(this);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);
    }

    private void setDateTimeField() {

        //  Calendar[] calendar=new Calendar[5];
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 6);

        Log.e("199 ", "" + nextYear.getTime());
        Date today = new Date(System.currentTimeMillis());
        calendar.init(today, nextYear.getTime())
                .withSelectedDate(today);
        calendar.init(today, nextYear.getTime())
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE).withHighlightedDate(today);
        calendar.setCustomDayView(new DefaultDayViewAdapter());
        Calendar one = Calendar.getInstance();

        ArrayList<java.util.Date> dates1 = new ArrayList<java.util.Date>();
        for (int i = 0; i < dates.size(); i++) {
            one.add(Calendar.DAY_OF_MONTH, 3);
            dates1.add(nextYear.getTime());
            Toast.makeText(Picker.this, today + "This", Toast.LENGTH_LONG).show();
        }

        calendar.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        calendar.init(new java.util.Date(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE) //
                .withSelectedDates(dates1);
        Toast.makeText(Picker.this, dates1 + "This", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.done_button:

                if (strDate == null) {
                    button.setEnabled(false);
                    // Toast.makeText(Picker.this, "Please  select at least one date", Toast.LENGTH_SHORT).show();
                    button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                }

                dateStrings.clear();

                for (int i = 0; i < dates.size(); i++) {
                    dateStrings.clear();
                    if (strDate.equals(dates.get(i).getTime())) {
                        dateStrings.add(getDateFromMills(dates.get(i).getTime()));
                        Log.e(TAG, "date Array::" + dateStrings);
                        break;
                    } else {
                        Log.e(TAG, "select one date");
                    }
                }

                intent = new Intent(Picker.this, ActivityParkingConfiguration.class);
                intent.putStringArrayListExtra(Constants.SELECTED_DATES, datparselist);
                intent.putExtra("New_Date", strDate);
                setResult(GET_DATES, intent);
                intent.putExtra("parkingname", park_name);
                intent.putExtra("parkID", parkID);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;

            case R.id.edit_button:
                intent = new Intent(Picker.this, ActivityParkingConfiguration.class);
                intent.putExtra("EDIT_date", strDate);
                setResult(GET_DATES, intent);
                intent.putExtra("parkingname", park_name);
                intent.putExtra("parkID", parkID);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;

            case R.id.delete_button:
                deleteconfig();
                break;

            case R.id.backArrowIV:
                finish();
                break;

            case R.id.attendance:
                intent = new Intent(Picker.this, ActivityAttendant.class);
                intent.putExtra("parkingname", park_name);
                intent.putExtra("parkID", parkID);
                Log.e(TAG, "parkingID ::" + parkID);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;
        }
    }

    private void deleteconfig() {
        DeleteConfigReq deleteConfigReq = new DeleteConfigReq();
        deleteConfigReq.setAppId(APP_ID);
        deleteConfigReq.setCalender(strDate);
        deleteConfigReq.setParkingId(parkID);
        Gson gson = new Gson();
        String payload = gson.toJson(deleteConfigReq);
        Log.e("test", "payload: " + payload);

        new WebServicePost(Picker.this, new HandlerAddAttendantResponse(), Constants.DELETE_D_CONFIG, payload).execute();
    }

    private String getDateFromMills(long time) {
        Timestamp timestamp = new Timestamp(time);
        Date date = new Date(timestamp.getTime());
        SimpleDateFormat sf1 = new SimpleDateFormat();
        SimpleDateFormat sf2 = new SimpleDateFormat();
        return date.toString();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class HandlerdatesResponse extends Handler {


        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            if (response != null) {

                viewdateClass = new Gson().fromJson(response, ViewdateClass.class);

                if (viewdateClass.getStatus() == 200) {
                    dates = new ArrayList<>();
                    list = new ArrayList<>();
                    List<CalendarCellDecorator> decoratorList = new ArrayList<>();

                    Toast.makeText(Picker.this, "Successs", Toast.LENGTH_SHORT).show();
                    list.addAll(viewdateClass.getModelParkingResponseList());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    //simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));

                    for (int i = 0; i < list.size(); i++) {
                        str_date = list.get(i).getCalender();
                        Log.e(TAG, "St_dates::::" + list.get(i).getCalender());
                        try {
                            date = simpleDateFormat.parse(str_date);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            cal.add(Calendar.DATE, 0);
                            date = cal.getTime();
                            Log.e("Date ", "" + date);
                            dates.add(date);

                            df = new SimpleDateFormat("yyyy-MM-dd");
                            strDate = df.format(date);

                            try {
                                decoratorList.add(new EventDecorator(date, getResources().getColor(R.color.calendar_highlighted_day_bg)));
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    Log.e(TAG, "INITIAL Dates array  >>> ::::" + dates);
                    if (dates.size() > 0) {
                        delete_button.setEnabled(true);
                        edit_button.setEnabled(true);
                        button.setEnabled(false);

                        button.setBackgroundColor(getResources().getColor(R.color.calendar_divider));
                        delete_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        edit_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        calendar.setCustomDayView(new DefaultDayViewAdapter());
                        //  calendar.setDecorators(decoratorList);
                        calendar.init(lastYear.getTime(), nextYear.getTime())
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                //.withSelectedDate((new java.util.Date()))
                                .withSelectedDates(dates);
                    } else {
                        delete_button.setEnabled(false);
                        edit_button.setEnabled(false);
                        button.setEnabled(true);
                        button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        delete_button.setBackgroundColor(getResources().getColor(R.color.calendar_text_inactive));
                        edit_button.setBackgroundColor(getResources().getColor(R.color.calendar_text_inactive));
                    }
                }
            } else {
                Toast.makeText(Picker.this, viewdateClass.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class HandlerAddAttendantResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                try {
                    DeleteDateConfigClass deleteDateConfigClass = new Gson().fromJson(response, DeleteDateConfigClass.class);
                    if (deleteDateConfigClass.getStatus() == 200) {
                        // Toast.makeText(Picker.this, "You have delete " + strDate + " Configuration", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}