package com.m_intellect.parkandgo1.model;

/**
 * Created by sanket on 1/2/2018.
 */

public class SingletonProfileDetail {

    private static SingletonProfileDetail singletonProfileDetail;
    private ModelMyProfile modelMyProfile;

    private SingletonProfileDetail() {

    }

    public static SingletonProfileDetail getInstance() {
        if (singletonProfileDetail == null) {
            singletonProfileDetail = new SingletonProfileDetail();
        }
        return singletonProfileDetail;
    }

    public ModelMyProfile getModelMyProfile() {
        return modelMyProfile;
    }

    public void setModelMyProfile(ModelMyProfile modelMyProfile) {
        this.modelMyProfile = modelMyProfile;
    }
}
