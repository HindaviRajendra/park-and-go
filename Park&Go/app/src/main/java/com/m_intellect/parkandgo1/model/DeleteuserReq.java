package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class DeleteuserReq {

    /**
     * app_id : 29
     * mobile_no : 9604413632
     */

    @SerializedName("app_id")
    private int appId;
    @SerializedName("mobile_no")
    private String mobileNo;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
