package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

public class DeleteAttendaneReq {

    /**
     * id : 264
     * app_id : 29
     */

    @SerializedName("id")
    private int id;
    @SerializedName("app_id")
    private int appId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }
}
