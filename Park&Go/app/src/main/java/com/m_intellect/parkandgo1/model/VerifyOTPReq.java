package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class VerifyOTPReq {

    /**
     * otp : 371023
     * state : 3c1bed6e-2c6d-5dbe-bf7d-9a9c81c86cf6
     */

    @SerializedName("otp")
    private String otp;
    @SerializedName("state")
    private String state;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
