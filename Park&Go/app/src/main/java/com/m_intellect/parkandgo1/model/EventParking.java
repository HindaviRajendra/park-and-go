
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventParking {

    @SerializedName("event1")
    @Expose
    private Event1 event1;
    @SerializedName("event2")
    @Expose
    private Event2 event2;
    @SerializedName("event3")
    @Expose
    private Event3 event3;

    public Event1 getEvent1() {
        return event1;
    }

    public void setEvent1(Event1 event1) {
        this.event1 = event1;
    }

    public Event2 getEvent2() {
        return event2;
    }

    public void setEvent2(Event2 event2) {
        this.event2 = event2;
    }

    public Event3 getEvent3() {
        return event3;
    }

    public void setEvent3(Event3 event3) {
        this.event3 = event3;
    }

}
