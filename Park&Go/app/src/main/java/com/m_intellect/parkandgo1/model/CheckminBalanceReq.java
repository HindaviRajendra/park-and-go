package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class CheckminBalanceReq {

    /**
     * tax_amount : 20
     * token : afa2beb7-321e-441d-ac55-43e5b0862300
     */

    @SerializedName("tax_amount")
    private String taxAmount;
    @SerializedName("token")
    private String token;
    @SerializedName("booking_id")
    private int booking_id;

    @SerializedName("mobile_no")
    private String mobile_no;

    public String getMobile_no() {
        return mobile_no;
    }

    public CheckminBalanceReq setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
        return this;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public CheckminBalanceReq setBooking_id(int booking_id) {
        this.booking_id = booking_id;
        return this;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
