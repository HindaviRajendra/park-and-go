package com.m_intellect.parkandgo1.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelEventResponse;
import com.m_intellect.parkandgo1.utils.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sanket on 12/3/2017.
 */

public class AdapterEventList extends RecyclerView.Adapter<AdapterEventList.ViewHolder> {

    private Context context;
    private List<ModelEventResponse> modelEventResponseList;

    public AdapterEventList(Context context, List<ModelEventResponse> modelEventResponseList) {

        this.context = context;
        this.modelEventResponseList = modelEventResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_event, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ModelEventResponse modelEventResponse = modelEventResponseList.get(holder.getAdapterPosition());

        holder.eventNameTV.setText(modelEventResponse.getEventName());
        holder.eventAddressTV.setText(modelEventResponse.getEventLandmark() + ", " + modelEventResponse.getCity()
                + ", " + modelEventResponse.getState());
        holder.eventTimeTV.setText(Utility.formatDateFromOnetoAnother(modelEventResponse.getStartTime(), "HH:mm:ss","hh:mm aa").toLowerCase());
        String date = modelEventResponse.getStartDate().substring(0, 10);
        holder.eventDateTV.setText(Utility.formatDateFromOnetoAnother(date, "yyyy-MM-dd","MMM dd, E"));

        Picasso.with(context).load(modelEventResponse.getImage()).into(holder.eventImageIV);
    }

    @Override
    public int getItemCount() {
        return modelEventResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView eventNameTV, eventAddressTV, eventTimeTV, eventDateTV;
        private ImageView eventImageIV;

        public ViewHolder(View itemView) {
            super(itemView);

            eventNameTV = (TextView) itemView.findViewById(R.id.eventNameTV);
            eventAddressTV = (TextView) itemView.findViewById(R.id.eventAddressTV);
            eventTimeTV = (TextView) itemView.findViewById(R.id.eventTimeTV);
            eventDateTV = (TextView) itemView.findViewById(R.id.eventDateTV);
            eventImageIV = (ImageView) itemView.findViewById(R.id.eventImageIV);
        }
    }

}
