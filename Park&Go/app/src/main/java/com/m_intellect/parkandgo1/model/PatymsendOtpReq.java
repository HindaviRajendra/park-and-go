package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class PatymsendOtpReq {


    /**
     * phone : 9604413632
     * clientId : merchant-abralin-stagging
     * scope : wallet
     * responseType : token
     */

    @SerializedName("phone")
    private String phone;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("scope")
    private String scope;
    @SerializedName("responseType")
    private String responseType;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
