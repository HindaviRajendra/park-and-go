package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 07-03-2018.
 */

public class ParkConfigClass {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private List<ParkConfigResponse1.DataBean> modelParkingResponseList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ParkConfigResponse1.DataBean> getParkingConfigResponseList() {
        return modelParkingResponseList;
    }

    public void setModelParkingConfigResponseList(List<ParkConfigResponse1.DataBean> modelParkingResponseList) {
        this.modelParkingResponseList = modelParkingResponseList;
    }
}
