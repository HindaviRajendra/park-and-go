package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 29-01-2018.
 */

public class ModelParkingResponse {

    @SerializedName("parking_id")
    @Expose
    private Integer parkingId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("coordinates")
    @Expose
    private Coordinates coordinates;
    @SerializedName("place_name")
    @Expose
    private String placeName;
    @SerializedName("place_landmark")
    @Expose
    private String placeLandmark;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("total_space")
    @Expose
    private Integer totalSpace;
    @SerializedName("general")
    @Expose
    private String general;
    @SerializedName("reserved")
    @Expose
    private String reserved;
    @SerializedName("regular")
    @Expose
    private String regular;
    @SerializedName("event")
    @Expose
    private String event;
    @SerializedName("premium")
    @Expose
    private String premium;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("last_updated_on")
    @Expose
    private String lastUpdatedOn;
    @SerializedName("sum")
    @Expose
    private String sum;

    @SerializedName("calender")
    @Expose
    private List<ConfigurationCalendar> calender = null;

    @SerializedName("regular_parking")
    @Expose
    private RegularParking regularParking;
    @SerializedName("event_parking")
    @Expose
    private EventParking eventParking;
    @SerializedName("price_values")
    @Expose
    private List<PriceValue> priceValues = null;

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLandmark() {
        return placeLandmark;
    }

    public void setPlaceLandmark(String placeLandmark) {
        this.placeLandmark = placeLandmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(Integer totalSpace) {
        this.totalSpace = totalSpace;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public List<ConfigurationCalendar> getCalender() {
        return calender;
    }

    public void setCalender(List<ConfigurationCalendar> calender) {
        this.calender = calender;
    }


    public RegularParking getRegularParking() {
        return regularParking;
    }

    public void setRegularParking(RegularParking regularParking) {
        this.regularParking = regularParking;
    }

    public EventParking getEventParking() {
        return eventParking;
    }

    public void setEventParking(EventParking eventParking) {
        this.eventParking = eventParking;
    }


    public List<PriceValue> getPriceValues() {
        return priceValues;
    }

}