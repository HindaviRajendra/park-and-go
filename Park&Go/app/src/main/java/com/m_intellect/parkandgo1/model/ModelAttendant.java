package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 1/21/2018.
 */

public class ModelAttendant {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"id":242,"app_id":29,"password":"kap","name":"kap","profile_pic":"","email":"","mobile_no":"9604413632","coordinates":{"x":0,"y":0},"vehicle_name":"","vehicle_no":"","active_status":1,"usertype":"attendant","city":"","state":"","device_id":"","wallet_balance":null,"total_space":null,"vacant":null,"location_id":0,"parking_id":64,"created_by":"26"},{"id":239,"app_id":29,"password":"123456","name":"venki","profile_pic":"","email":"","mobile_no":"9494230691","coordinates":{"x":19.061089,"y":72.8785014},"vehicle_name":"","vehicle_no":"","active_status":1,"usertype":"attendant","city":"","state":"","device_id":"","wallet_balance":null,"total_space":null,"vacant":null,"location_id":0,"parking_id":64,"created_by":"26"},{"id":139,"app_id":29,"password":"12345678","name":"Danish","profile_pic":"","email":"","mobile_no":"9594939291","coordinates":{"x":19.061089,"y":72.8785014},"vehicle_name":"","vehicle_no":"","active_status":1,"usertype":"attendant","city":"","state":"","device_id":"","wallet_balance":null,"total_space":null,"vacant":null,"location_id":0,"parking_id":64,"created_by":"26"}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * id : 242
         * app_id : 29
         * password : kap
         * name : kap
         * profile_pic :
         * email :
         * mobile_no : 9604413632
         * coordinates : {"x":0,"y":0}
         * vehicle_name :
         * vehicle_no :
         * active_status : 1
         * usertype : attendant
         * city :
         * state :
         * device_id :
         * wallet_balance : null
         * total_space : null
         * vacant : null
         * location_id : 0
         * parking_id : 64
         * created_by : 26
         */

        @SerializedName("id")
        private int id;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("password")
        private String password;
        @SerializedName("name")
        private String name;
        @SerializedName("profile_pic")
        private String profilePic;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile_no")
        private String mobileNo;
        @SerializedName("coordinates")
        private CoordinatesBean coordinates;
        @SerializedName("vehicle_name")
        private String vehicleName;
        @SerializedName("vehicle_no")
        private String vehicleNo;
        @SerializedName("active_status")
        private int activeStatus;
        @SerializedName("usertype")
        private String usertype;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("device_id")
        private String deviceId;
        @SerializedName("wallet_balance")
        private Object walletBalance;
        @SerializedName("total_space")
        private Object totalSpace;
        @SerializedName("vacant")
        private Object vacant;
        @SerializedName("location_id")
        private int locationId;
        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("created_by")
        private String createdBy;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public CoordinatesBean getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(CoordinatesBean coordinates) {
            this.coordinates = coordinates;
        }

        public String getVehicleName() {
            return vehicleName;
        }

        public void setVehicleName(String vehicleName) {
            this.vehicleName = vehicleName;
        }

        public String getVehicleNo() {
            return vehicleNo;
        }

        public void setVehicleNo(String vehicleNo) {
            this.vehicleNo = vehicleNo;
        }

        public int getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(int activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Object getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(Object walletBalance) {
            this.walletBalance = walletBalance;
        }

        public Object getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(Object totalSpace) {
            this.totalSpace = totalSpace;
        }

        public Object getVacant() {
            return vacant;
        }

        public void setVacant(Object vacant) {
            this.vacant = vacant;
        }

        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public static class CoordinatesBean {
            /**
             * x : 0
             * y : 0
             */

            @SerializedName("x")
            private double x;
            @SerializedName("y")
            private double y;

            public double getX() {
                return x;
            }

            public void setX(double x) {
                this.x = x;
            }

            public double getY() {
                return y;
            }

            public void setY(double y) {
                this.y = y;
            }
        }
    }
}

