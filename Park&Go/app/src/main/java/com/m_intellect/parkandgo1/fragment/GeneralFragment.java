package com.m_intellect.parkandgo1.fragment;


import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityMain;
import com.m_intellect.parkandgo1.dialogfragment.PaytmOTPDialog;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.CheckExpireResponse;
import com.m_intellect.parkandgo1.model.CheckbalanceRes;
import com.m_intellect.parkandgo1.model.CheckminBalanceReq;
import com.m_intellect.parkandgo1.model.ModelViewParkingResponse;
import com.m_intellect.parkandgo1.model.SetterPayload;
import com.m_intellect.parkandgo1.model.SetterViewParkingResponse;
import com.m_intellect.parkandgo1.model.TactionSuccessRes;
import com.m_intellect.parkandgo1.model.TransactionReq;
import com.m_intellect.parkandgo1.model.WithdrawMoneyResponse;
import com.m_intellect.parkandgo1.model.WithdrawRequest;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.network.WebServiceTimestamp;
import com.m_intellect.parkandgo1.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

public class GeneralFragment extends android.app.Fragment implements View.OnClickListener {

    public String TAG = getClass().getSimpleName();
    public int G_car = 0, G_Bike = 0, G_Bus = 0, G_Truck = 0;
    EditText edit_h1, edit_h2, edit_m1, edit_m2, edit_h3, edit_h4, editm3, editm4, Edit_A1, edit_A, edit_M1, edit_M;
    EditText edit_v5, edit_v6, edit_v7, edit_v8, edit_v9, edit_v10, edit_v11, edit_v12, edit_v13, edit_v14, edit_v15, edit_v16;
    RadioButton btn_radio1, btnradio2;
    RelativeLayout r_proceed;
    String charges, str_vno, strother;
    ImageView info_ETA, info_EHS, ck1, ck2;
    TextView tv_balance;
    RadioGroup radioGroup;
    RadioButton bt_radio;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String v_type;
    String vehicleNo;
    TextView spin_vtype;
    boolean bookingstatus = false;
    private Button btnconfirm;
    private String Arr_time, Time, outTime, ETA_time, EHS_time, token = "";
    private int count = 0;

    public GeneralFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.general_layout, container, false);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        preferences = getActivity().getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        vehicleNo = preferences.getString("VH_NO", "");
        Constants.TOKEN = preferences.getString("TOKEN", "");
        initview(view);
        setlistner();
        //  setSpinner();

    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        if (bookingstatus == false) {
//            cancelBooking();
//        }
//    }


    //    private void setHours() {
//
//        final Calendar mcurrentTime1 = Calendar.getInstance();
//        int hour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);
//        int minute1 = mcurrentTime1.get(Calendar.MINUTE);
//
//        TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),
//                new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay,
//                                          int minute) {
//
//                        String minuteSting = "";
//                        if (hourOfDay < 12 && hourOfDay >= 0) {
//
//                            if (minute < 10) {
//                                minuteSting = "0";
//                            } else {
//                                minuteSting = "";
//                            }
//
//                            //edit_intime.setText(hourOfDay + ":" + minuteSting + minute + " AM");
//                            // ETA_time = hourOfDay + ":" + minuteSting + minute + " AM";
//
//                            EHS_time = hourOfDay + ":" + minuteSting + minute + " hr";
//                            Log.e(TAG, "EHS_time AM:::" + EHS_time);
//
//
//                            if (EHS_time.length() == 7) {
//                                edit_h3.setText("" + 0);
//                                edit_h4.setText("" + EHS_time.charAt(0));
//                                editm3.setText("" + EHS_time.charAt(2));
//                                editm4.setText("" + EHS_time.charAt(3));
//
//                                edit_A.setText("" + EHS_time.charAt(5));
//                                edit_M.setText("" + EHS_time.charAt(6));
//                            } else {
//                                edit_h3.setText("" + EHS_time.charAt(0));
//                                edit_h4.setText("" + EHS_time.charAt(1));
//                                editm3.setText("" + EHS_time.charAt(3));
//                                editm4.setText("" + EHS_time.charAt(4));
//                                edit_A.setText("" + EHS_time.charAt(6));
//                                edit_M.setText("" + EHS_time.charAt(7));
//                            }
//                        } else {
//                            hourOfDay -= 12;
//                            if (hourOfDay == 0) {
//                                hourOfDay = 12;
//                            }
//
//                            if (minute < 10) {
//                                minuteSting = "0";
//                            } else {
//                                minuteSting = "";
//                            }
//                            EHS_time = hourOfDay + ":" + minuteSting + minute + " hr";
//                            Log.e(TAG, "EHS_time  PM:::" + EHS_time);
//                            Log.e(TAG, "ETA EHS_time length:::" + EHS_time.length());
//
//                            if (EHS_time.length() == 7) {
//                                edit_h3.setText("" + 0);
//                                edit_h4.setText("" + EHS_time.charAt(0));
//                                editm3.setText("" + EHS_time.charAt(2));
//                                editm4.setText("" + EHS_time.charAt(3));
//                                edit_A.setText("" + EHS_time.charAt(5));
//                                edit_M.setText("" + EHS_time.charAt(6));
//                            } else {
//                                edit_h3.setText("" + EHS_time.charAt(0));
//                                edit_h4.setText("" + EHS_time.charAt(1));
//                                editm3.setText("" + EHS_time.charAt(3));
//                                editm4.setText("" + EHS_time.charAt(4));
//                                edit_A.setText("" + EHS_time.charAt(6));
//                                edit_M.setText("" + EHS_time.charAt(7));
//                            }
//
//                        }
//                    }
//                }, hour1, minute1, false);
//
//        timePickerDialog1.show();
//    }

    private void initview(View view) {
        btnconfirm = (Button) view.findViewById(R.id.btnconfirm);

        edit_h1 = (EditText) view.findViewById(R.id.edit1);
        edit_h2 = (EditText) view.findViewById(R.id.edit2);
        edit_h3 = (EditText) view.findViewById(R.id.edit5);
        edit_h4 = (EditText) view.findViewById(R.id.edit6);
        edit_m1 = (EditText) view.findViewById(R.id.edit3);
        edit_m2 = (EditText) view.findViewById(R.id.edit4);
        editm3 = (EditText) view.findViewById(R.id.edit7);
        editm4 = (EditText) view.findViewById(R.id.edit8);

        Edit_A1 = (EditText) view.findViewById(R.id.edit_a1);
        edit_A = (EditText) view.findViewById(R.id.edit_a);
        edit_M1 = (EditText) view.findViewById(R.id.edit_m1);
        edit_M = (EditText) view.findViewById(R.id.edit_m);

        spin_vtype = (TextView) view.findViewById(R.id.spin_vtype);
        spin_vtype.setText(Constants.VTYPE_NAME);
        v_type = Constants.VTYPE_NAME;

        //vehicle no
        edit_v5 = (EditText) view.findViewById(R.id.pin_fifth_edittext);
        edit_v6 = (EditText) view.findViewById(R.id.pin_sixth_edittext);
        edit_v7 = (EditText) view.findViewById(R.id.pin_seven_edittext);
        edit_v8 = (EditText) view.findViewById(R.id.pin_eight_edittext);
        edit_v9 = (EditText) view.findViewById(R.id.pin_nine_edittext);
        edit_v10 = (EditText) view.findViewById(R.id.pin_tenth_edittext);

        //other
        edit_v11 = (EditText) view.findViewById(R.id.pin_fifth_edittext1);
        edit_v12 = (EditText) view.findViewById(R.id.pin_sixth_edittext1);
        edit_v13 = (EditText) view.findViewById(R.id.pin_seven_edittext1);
        edit_v14 = (EditText) view.findViewById(R.id.pin_eight_edittext1);
        edit_v15 = (EditText) view.findViewById(R.id.pin_nine_edittext1);
        edit_v16 = (EditText) view.findViewById(R.id.pin_tenth_edittext1);


        tv_balance = (TextView) view.findViewById(R.id.wallet_balance);
        r_proceed = (RelativeLayout) view.findViewById(R.id.r_proceed);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);


        btn_radio1 = (RadioButton) view.findViewById(R.id.radio1);
        btnradio2 = (RadioButton) view.findViewById(R.id.radio2);

        info_ETA = (ImageView) view.findViewById(R.id.info_ETA);
        info_EHS = (ImageView) view.findViewById(R.id.info_EHS);
        ck1 = (ImageView) view.findViewById(R.id.ck_1);
        ck2 = (ImageView) view.findViewById(R.id.ck_2);

        Log.e(TAG, "Vehicle No :: " + vehicleNo);

        try {
            edit_v5.setText("" + vehicleNo.charAt(6));
            edit_v6.setText("" + vehicleNo.charAt(7));
            edit_v7.setText("" + vehicleNo.charAt(9));
            edit_v8.setText("" + vehicleNo.charAt(10));
            edit_v9.setText("" + vehicleNo.charAt(11));
            edit_v10.setText("" + vehicleNo.charAt(12));

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // setHours();
    }

    private void setlistner() {

        info_ETA.setOnClickListener(this);
        info_EHS.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                bt_radio = (RadioButton) group.findViewById(checkedId);

            }
        });

        btnconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (edit_h3.getText().toString().equals("") && edit_h4.getText().toString().equals("") && editm3.getText().toString().equals("") && editm4.getText().toString().equals("") && edit_h1.getText().toString().equals("") && edit_h2.getText().toString().equals("") && edit_m1.getText().toString().equals("") && edit_m2.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter ETA & EHS time", Toast.LENGTH_SHORT).show();
                } else {
                    if (spin_vtype.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), "Please Choose Vehicle Type", Toast.LENGTH_SHORT).show();
                    } else {
                        if (bt_radio == null) {
                            Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
                        } else {
                            final String url = Constants.VIEW_CHARGES + Constants.SELCT_PARKG_ID + "/" + "null";
                            new WebServiceGet(getActivity(), new HandlerViewParking(), url).execute();
                            Log.e(TAG, "URL   ::::  " + url);
                            str_vno = edit_v5.getText().toString() + edit_v6.getText().toString() + "-" + edit_v7.getText().toString() + edit_v8.getText().toString() + edit_v9.getText().toString() + edit_v10.getText().toString();
                            strother = edit_v11.getText().toString() + edit_v12.getText().toString() + "-" + edit_v13.getText().toString() + edit_v14.getText().toString() + edit_v15.getText().toString() + edit_v16.getText().toString();
                        }
                    }
                }
            }
        });

        btn_radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (btn_radio1.isChecked()) {

                    btn_radio1.setChecked(true);
                    btnradio2.setChecked(false);
                    edit_v5.setEnabled(true);
                    edit_v6.setEnabled(true);
                    edit_v7.setEnabled(true);
                    edit_v8.setEnabled(true);
                    edit_v9.setEnabled(true);
                    edit_v10.setEnabled(true);

                    showEditCusor();

                    edit_v11.setEnabled(false);
                    edit_v12.setEnabled(false);
                    edit_v13.setEnabled(false);
                    edit_v14.setEnabled(false);
                    edit_v15.setEnabled(false);
                    edit_v16.setEnabled(false);

                    edit_v11.getText().clear();
                    edit_v12.getText().clear();
                    edit_v13.getText().clear();
                    edit_v14.getText().clear();
                    edit_v15.getText().clear();
                    edit_v16.getText().clear();

                    str_vno = edit_v5.getText().toString() + edit_v6.getText().toString() + edit_v7.getText().toString() + edit_v8.getText().toString() + edit_v9.getText().toString() + edit_v10.getText().toString();

                }
            }
        });

        btnradio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (btnradio2.isChecked()) {
                    btnradio2.setChecked(true);
                    btn_radio1.setChecked(false);
                    edit_v11.setEnabled(true);
                    edit_v12.setEnabled(true);
                    edit_v13.setEnabled(true);
                    edit_v14.setEnabled(true);
                    edit_v15.setEnabled(true);
                    edit_v16.setEnabled(true);

                    showOtherEditCusor();

                    edit_v5.setEnabled(false);
                    edit_v6.setEnabled(false);
                    edit_v7.setEnabled(false);
                    edit_v8.setEnabled(false);
                    edit_v9.setEnabled(false);
                    edit_v10.setEnabled(false);
                    strother = edit_v11.getText().toString() + edit_v12.getText().toString() + edit_v13.getText().toString() + edit_v14.getText().toString() + edit_v15.getText().toString() + edit_v16.getText().toString();
                }
            }
        });


        r_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count == 0) {
                    Toast.makeText(getActivity(), "Please Confirm your Parking", Toast.LENGTH_SHORT).show();
                } else {

                    if (btn_radio1.isChecked()) {
                        if (!edit_h3.getText().toString().equals("") && !edit_h4.getText().toString().equals("") && !editm3.getText().toString().equals("") && !editm4.getText().toString().equals("") && !edit_h1.getText().toString().equals("") && !edit_h2.getText().toString().equals("") && !edit_m1.getText().toString().equals("") && !edit_m2.getText().toString().equals("")) {
                            if (!edit_v5.getText().toString().equals("") && !edit_v6.getText().toString().equals("") && !edit_v7.getText().toString().equals("") && !edit_v8.getText().toString().equals("") &&
                                    !edit_v9.getText().toString().equals("") && !edit_v10.getText().toString().equals("")) {
                                if (bt_radio == null) {
                                    Toast.makeText(getActivity(), "Please select Payment Method", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (charges == null) {
                                        Toast.makeText(getActivity(), "Please click on confirm button ", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.e(TAG, "Charges ::: " + charges);

                                        if (v_type.equals("Car")) {
                                            if (G_car != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Car", Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (v_type.equals("Bike")) {
                                            if (G_Bike != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Bike", Toast.LENGTH_SHORT).show();
                                            }

                                        } else if (v_type.equals("Bus")) {
                                            if (G_Bus != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Bus", Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (v_type.equals("Truck")) {
                                            if (G_Truck != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Truck", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        String Hour1 = edit_h1.getText().toString();
                                        String Hour2 = edit_h2.getText().toString();
                                        String minute1 = edit_m1.getText().toString();
                                        String minute2 = edit_m2.getText().toString();

                                        Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
                                        Log.e(TAG, "Arrival time >>" + Arr_time);

                                        //Estimated time
                                        String Hour3 = edit_h3.getText().toString();
                                        String Hour4 = edit_h4.getText().toString();
                                        String minute3 = editm3.getText().toString();
                                        String minute4 = editm4.getText().toString();
                                        Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
                                        Log.e(TAG, "HOUR >>" + Time);
                                    }
                                }
                            } else {
                                Toast.makeText(getActivity(), "Please enter valid last six-digit vehicle number", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please enter ETA & EHS time", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        if (!edit_h3.getText().toString().equals("") && !edit_h4.getText().toString().equals("") && !editm3.getText().toString().equals("") && !editm4.getText().toString().equals("") && !edit_h1.getText().toString().equals("") && !edit_h2.getText().toString().equals("") && !edit_m1.getText().toString().equals("") && !edit_m2.getText().toString().equals("")) {
                            if (!edit_v11.getText().toString().equals("") && !edit_v12.getText().toString().equals("") && !edit_v13.getText().toString().equals("") &&
                                    !edit_v14.getText().toString().equals("") && !edit_v15.getText().toString().equals("") && !edit_v16.getText().toString().equals("")) {
                                if (bt_radio == null) {
                                    Toast.makeText(getActivity(), "Please select Payment Method", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (charges == null) {
                                        Toast.makeText(getActivity(), "Please click on confirm button ", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.e(TAG, "Charges ::: " + charges);

                                        if (v_type.equals("Car")) {
                                            if (G_car != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Car", Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (v_type.equals("Bike")) {
                                            if (G_Bike != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Bike", Toast.LENGTH_SHORT).show();
                                            }

                                        } else if (v_type.equals("Bus")) {
                                            if (G_Bus != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Bus", Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (v_type.equals("Truck")) {
                                            if (G_Truck != 0) {
                                                insertBooking();
                                            } else {
                                                Toast.makeText(getActivity(), "No more vacant for General Truck", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        String Hour1 = edit_h1.getText().toString();
                                        String Hour2 = edit_h2.getText().toString();
                                        String minute1 = edit_m1.getText().toString();
                                        String minute2 = edit_m2.getText().toString();

                                        Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
                                        Log.e(TAG, "Arrival time >>" + Arr_time);

                                        //Estimated time
                                        String Hour3 = edit_h3.getText().toString();
                                        String Hour4 = edit_h4.getText().toString();
                                        String minute3 = editm3.getText().toString();
                                        String minute4 = editm4.getText().toString();
                                        Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
                                        Log.e(TAG, "HOUR >>" + Time);
                                        // Toast.makeText(getActivity(), "You successfully booking this Parking", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                Toast.makeText(getActivity(), "Please enter valid last six-digit vehicle number", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please enter ETA & EHS time", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            }
        });


        ck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // selectTime(v,ETA_time);

                view.getTag();
                String x = (String) view.getTag();
                final Calendar mcurrentTime1 = Calendar.getInstance();

                int hour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);
                int minute1 = mcurrentTime1.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String minuteSting = "";

                                Calendar datetime = Calendar.getInstance();
                                Calendar c = Calendar.getInstance();
                                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                datetime.set(Calendar.MINUTE, minute);

                                if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                                    //it's after current
                                    int hour = hourOfDay % 24;
                                    String selectedTime = String.format("%02d:%02d %s", hour == 0 ? 24 : hour,
                                            minute, hourOfDay < 24 ? "am" : "pm");
                                    //
                                    //edit_event_time.setText(selectedTime);

                                    Log.e(TAG, "Length :: " + selectedTime.length());

                                    if (selectedTime.length() == 8) {
                                        edit_h1.setText("" + selectedTime.charAt(0));
                                        edit_h2.setText("" + selectedTime.charAt(1));
                                        edit_m1.setText("" + selectedTime.charAt(3));
                                        edit_m2.setText("" + selectedTime.charAt(4));
//                                        Edit_A1.setText("" + selectedTime.charAt(6));
//                                        edit_M1.setText("" + selectedTime.charAt(7));
                                    } else if (selectedTime.length() == 7) {
                                        edit_h1.setText("" + selectedTime.charAt(0));
                                        edit_h2.setText("" + selectedTime.charAt(1));
                                        edit_m1.setText("" + 0);
                                        edit_m2.setText("" + selectedTime.charAt(3));
                                        //Edit_A1.setText(""+ selectedTime.charAt(5));
                                    }

                                    Log.e(TAG, "New Time ::: " + selectedTime);
                                } else {
                                    Toast.makeText(getActivity(), "Please don't select past time", Toast.LENGTH_SHORT).show();
                                }
//


//                                String selectedTime = hourOfDay + ":" + minute + " ";
//                                Log.e(TAG, "selected time :: " + selectedTime);
//
//                                // edit_event_time.setText(selectedTime);
//
//                                if (selectedTime.length() == 6) {
//                                    edit_h1.setText("" + selectedTime.charAt(0));
//                                    edit_h2.setText("" + selectedTime.charAt(1));
//                                    edit_m1.setText("" + selectedTime.charAt(3));
//                                    edit_m2.setText("" + selectedTime.charAt(4));
//                                } else if (selectedTime.length() == 5) {
//                                    edit_h1.setText("" + selectedTime.charAt(0));
//                                    edit_h2.setText("" + selectedTime.charAt(1));
//                                    edit_m1.setText("" + 0);
//                                    edit_m2.setText("" + selectedTime.charAt(3));
//                                }

                            }
                        }, hour1, minute1, false);

                timePickerDialog1.show();

            }
        });

    }

    private void checkExpire() {
        // Constants.TOKEN = "2077cec4-0503-4376-be20-db15ec376800";
        Log.e("test", "TOKEN : " + Constants.TOKEN);
        new WebServiceTimestamp(getActivity(), new HandlerparkdetailResponse(), Constants.PAYTM_TMEXPIRE).execute();
        Log.e("TAG", "Constants.PAYTM_TMEXPIRE----->" + Constants.PAYTM_TMEXPIRE);
    }

    private void Checkminbalance() {
        CheckminBalanceReq checkminBalanceReq = new CheckminBalanceReq();
        checkminBalanceReq.setTaxAmount(charges);
        checkminBalanceReq.setToken(preferences.getString("TOKEN", ""));
        checkminBalanceReq.setBooking_id(Constants.BOOK_ID);
        checkminBalanceReq.setMobile_no(preferences.getString("PAYTMMOB", ""));
        Gson gson = new Gson();
        String payload = gson.toJson(checkminBalanceReq);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new HandlerMinBalance(), Constants.PAYTM_MINBALANCE, payload).execute();
        Log.e(TAG, "URL :: " + Constants.PAYTM_MINBALANCE);
    }

    private void withdrawmoney() {
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setMobNo(preferences.getString("PAYTMMOB", ""));
        withdrawRequest.setCustId(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));
        withdrawRequest.setToken(preferences.getString("TOKEN", ""));
        withdrawRequest.setOrderId(String.valueOf(Constants.BOOK_ID));
        withdrawRequest.setTaxAmt(charges);
        Gson gson = new Gson();
        String payload = gson.toJson(withdrawRequest);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new Handlerwithdrwamoney(), Constants.PAYTM_WITHDRAW, payload).execute();
    }

    private void transactionStatus() {
        TransactionReq transactionReq = new TransactionReq();
        transactionReq.setOrderId(String.valueOf(Constants.BOOK_ID));
        Gson gson = new Gson();
        String payload = gson.toJson(transactionReq);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new Handlertactionsuccess(), Constants.PAYTM_TRANSACTIONSUCCESS, payload).execute();
    }

    public void ShowDialog(final String stringtype) {
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setView(dialogView);
        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        if (stringtype.equals("success")) {
            bookingstatus = true;
            txt_text.setText("Success");
            txt_header.setText("You have Booked your Vehicle successfully");
            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
            btnok.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            txt_text.setText("Cancel Booking");
            txt_header.setText("Due to insufficient balance on your Paytm wallet. We are not Proceeding your booking.Please add money to your paytm wallet ");
            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
            btnok.setBackgroundColor(getResources().getColor(R.color.red));
        }


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stringtype.equals("success")) {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("isFromOTP", true);
                    intent.putExtra("gvtype", v_type);
                    if (btn_radio1.isChecked()) {
                        intent.putExtra("vehicle_no1", str_vno);
                    } else if (btnradio2.isChecked()) {
                        intent.putExtra("vehicle_no1", strother);
                    }
                    intent.putExtra("general1", "General");
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
//                    getActivity().finish();

                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showOtherEditCusor() {
        edit_v11.requestFocus();
        edit_v11.setCursorVisible(true);
        filter(edit_v11);
        filter(edit_v12);
        edit_v11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v11.getText().toString().length() == 1) {
                    edit_v12.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
//
            }
        });

        edit_v12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v12.getText().toString().length() == 1) {
                    edit_v13.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v12.getText().toString().length() == 0) {
                    edit_v11.requestFocus();
                }
            }
        });

        edit_v13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v13.getText().toString().length() == 1) {
                    edit_v14.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v13.getText().toString().length() == 0) {
                    edit_v12.requestFocus();
                }
            }
        });


        edit_v14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v14.getText().toString().length() == 1) {
                    edit_v15.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v14.getText().toString().length() == 0) {
                    edit_v13.requestFocus();
                }
            }
        });


        edit_v15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v15.getText().toString().length() == 1) {
                    edit_v16.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v15.getText().toString().length() == 0) {
                    edit_v14.requestFocus();
                }
            }
        });

        edit_v16.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v16.getText().toString().length() == 0) {
                    edit_v15.requestFocus();
                }
            }
        });
    }

    private void showEditCusor() {
        edit_v5.requestFocus();
        edit_v5.setCursorVisible(true);
        filter(edit_v5);
        filter(edit_v6);

        edit_v5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v5.getText().toString().length() == 1) {
                    edit_v6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_v6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v6.getText().toString().length() == 1) {
                    edit_v7.requestFocus();
                    edit_v6.setSelection(edit_v6.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v6.getText().toString().length() == 0) {
                    edit_v5.requestFocus();
                }
            }
        });

        edit_v7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v7.getText().toString().length() == 1) {
                    edit_v8.requestFocus();
                    edit_v7.setSelection(edit_v7.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v7.getText().toString().length() == 0) {
                    edit_v6.requestFocus();
                }
            }
        });


        edit_v8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v8.getText().toString().length() == 1) {
                    edit_v9.requestFocus();
                    edit_v8.setSelection(edit_v8.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v8.getText().toString().length() == 0) {
                    edit_v7.requestFocus();
                }
            }
        });


        edit_v9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v9.getText().toString().length() == 1) {
                    edit_v10.requestFocus();
                    edit_v9.setSelection(edit_v9.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v9.getText().toString().length() == 0) {
                    edit_v8.requestFocus();
                }
            }
        });

        edit_v10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_v10.setSelection(edit_v10.getText().length());

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v10.getText().toString().length() == 0) {
                    edit_v9.requestFocus();
                }
            }
        });
    }

    private void filter(EditText edit) {
        edit.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[A-Z.]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
    }

    private void insertBooking() {

        //Arrival time
        String Hour1 = edit_h1.getText().toString();
        String Hour2 = edit_h2.getText().toString();
        String minute1 = edit_m1.getText().toString();
        String minute2 = edit_m2.getText().toString();
        Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
        Log.e(TAG, "Arrival time >>" + Arr_time);

        //Estimated time
        String Hour3 = edit_h3.getText().toString();
        String Hour4 = edit_h4.getText().toString();
        String minute3 = editm3.getText().toString();
        String minute4 = editm4.getText().toString();
        Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
        Log.e(TAG, "HOUR >>" + Time);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        String formattedDate3 = df3.format(c.getTime());

        String url = Constants.INSERT_BOOKING;
        SetterPayload setterPayload = new SetterPayload();
        setterPayload.setApp_id(Constants.APP_ID);
        setterPayload.setBookingId(0);
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setUserId(preferences.getInt(Constants.USER_ID, 0));
        setterPayload.setBookingTime(Time);
        setterPayload.setArrivalTime(Arr_time);
        setterPayload.setOnline("online");
        setterPayload.setBookingType("On Spot");
        setterPayload.setBooking_status("1");
        setterPayload.setBookingCharges(0);

        if (bt_radio.getText().toString().equals("By Cash")) {
            setterPayload.setPaymentType("Cash");
        } else {
            setterPayload.setPaymentType("Wallet");
        }

        setterPayload.setStartDate(formattedDate3);
        setterPayload.setEndDate(formattedDate3);
        setterPayload.setLeaveTime("00:00");
        setterPayload.setOtp("");
        setterPayload.setVehicleType(v_type);
        setterPayload.setMobile_no("");

        if (btn_radio1.isChecked()) {
            setterPayload.setVehicleNo(str_vno);
        } else if (btnradio2.isChecked()) {
            setterPayload.setVehicleNo(strother);
        }

        setterPayload.setAttendantName("");
        setterPayload.setOut_status(0);
        setterPayload.setColor_status(0);
        setterPayload.setAdvancePayHours("0");
        setterPayload.setAdvancePaid("0");
        setterPayload.setBalanceCash("");
        setterPayload.setTotal_time("0");
        setterPayload.setRsf(Time);
        setterPayload.setBooking_history(1);
        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("REQUEST>>> ", "payload: " + payload);
        Log.e("URL>>> ", "payload: " + Constants.INSERT_BOOKING);
        new WebServicePost(getActivity(), new HandlervehicleInCashPayment(), url, payload).execute();
    }

    public String getCharges(String startTime, String endTime) {

        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = simpleDateFormat.parse(startTime);
            endDate = simpleDateFormat.parse(endTime);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null;
                Date dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);

            String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
            String min3 = Time.substring(3, 5);
            Log.e(TAG, " min3 :: " + min3);

            String toRemove = ":";
            if (base.contains(toRemove)) {
                base = base.replaceAll(toRemove, "");
            }

            Log.e(TAG, " BASE :: " + Integer.parseInt(base));
            int minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
            Log.e(TAG, " Convert MINUTES::: " + minutes);

            // int min5 = min3.charAt(0);
//            int min6 = min3.charAt(1);
//            minutes = min5 + min6;
//
//            Log.e(TAG, "Min 5 ::: " + min5);
//            Log.e(TAG, "Min 6 ::: " + min6);
//            Log.e(TAG, "By default MINUTES::: " + minutes);

            try {
                if (v_type.equals("Car")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                        }
                    }
                } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                        }
                    }
                } else if (v_type.equals("Bus")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                        }
                    }
                } else if (v_type.equals("Truck")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                        }
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "LAST CHARGE" + charges);
        return charges;
    }

    private void cancelBooking() {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(getActivity(), new Handlecancelbook(), Constants.CANCEL_BOOK1, payload).execute();
        Log.e(TAG, "CancelURL>>>" + payload);
        Log.e(TAG, "CancelURL>>>" + Constants.CANCEL_BOOK1);
    }

    private class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.e("test", "Expire Response ---->" + response);
            if (response != null) {
                CheckExpireResponse checkbalanceRes = new Gson().fromJson(response, CheckExpireResponse.class);
                String numberAsString = checkbalanceRes.getExpires();
                long number = Long.valueOf(numberAsString);

                Log.e("Original number", "----->" + number);
                Log.e("Current TIMESTAMP", "------> " + new java.sql.Timestamp(System.currentTimeMillis()).getTime());

                if (number <= (new java.sql.Timestamp(System.currentTimeMillis()).getTime())) {
                    PaytmOTPDialog paytmOTPDialog = new PaytmOTPDialog(getActivity());
                    Bundle b = new Bundle();
                    b.putString("fromcome", "general");
                    b.putBoolean("isFromOTP", true);
                    b.putString("gvtype", v_type);

                    if (btn_radio1.isChecked()) {
                        b.putString("vehicle_no1", str_vno);
                    } else if (btnradio2.isChecked()) {
                        b.putString("vehicle_no1", strother);
                    }
                    b.putString("general1", "General");
                    b.putString("charges", charges);
                    paytmOTPDialog.setArguments(b);
                    paytmOTPDialog.show(getActivity().getFragmentManager(), "OTP1");
                    Log.e(TAG, "RADIO CHECK ::" + bt_radio.getText().toString());
                } else {
                    Checkminbalance();
                }
            } else {
                cancelBooking();
            }
        }
    }

    private class HandlerMinBalance extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " Check Balance response: " + response);

            try {
                if (response != null) {
                    CheckbalanceRes checkbalanceRes = new Gson().fromJson(response, CheckbalanceRes.class);
                    if (checkbalanceRes.getData().getBody().getResultInfo().getResultStatus().equals("SUCCESS")) {

                        if (checkbalanceRes.getData().getBody().isFundsSufficient() == false) {
//                            Intent intent = new Intent(getActivity(), ActivityAddMoneyToWallet.class);
//                            intent.putExtra("isFromOTP", true);
//                            intent.putExtra("gvtype", v_type);
//                            intent.putExtra("charges", charges);
//                            if (btn_radio1.isChecked()) {
//                                intent.putExtra("vehicle_no1", str_vno);
//                            } else if (btnradio2.isChecked()) {
//                                intent.putExtra("vehicle_no1", strother);
//                            }
//                            intent.putExtra("general1", "General");
//                            getActivity().startActivity(intent);

                            cancelBooking();
                        } else {
                            ShowDialog("success");
                            //Withdraw Money API......
                            //  withdrawmoney();

                        }
                    }
                } else {
                    cancelBooking();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class Handlerwithdrwamoney extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " withdraw response: " + response);
            if (response != null) {
                WithdrawMoneyResponse withdrawMoneyResponse = new Gson().fromJson(response, WithdrawMoneyResponse.class);
                if (withdrawMoneyResponse.getData().getStatus().equals("TXN_SUCCESS")) {
                    transactionStatus();
                } else {
                    cancelBooking();
                }
            } else {
                cancelBooking();
            }
        }
    }

    private class Handlertactionsuccess extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "Transaction response: " + response);
            if (response != null) {
                TactionSuccessRes tactionSuccessRes = new Gson().fromJson(response, TactionSuccessRes.class);
                if (tactionSuccessRes.getData().getSTATUS().equals("TXN_SUCCESS")) {
                    Toast.makeText(getActivity(), "Parking Booked Successfully", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", "Transaction success");
                    ShowDialog("success");
                } else {
                    cancelBooking();
                }
            } else {
                cancelBooking();
            }
        }
    }

    private class HandlervehicleInCashPayment extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "Booking response: " + response);
            try {
                if (response != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            int insert_bookingId = ingredObject.optInt("insert_booking");//so you are going to get   ingredient name
                            Log.e("insert_bookingId", "" + insert_bookingId);
                            Constants.BOOK_ID = insert_bookingId;
                            Log.e("insert_bookingId", "" + Constants.BOOK_ID);
                            preferences = getActivity().getSharedPreferences("ParkAndGo", Context.MODE_PRIVATE);
                            Log.e(TAG, "SHARED TOKEN  ----->" + token);
                            if (insert_bookingId > 0) {
                                if (bt_radio.getText().toString().equals("  Wallet")) {
                                    if (!Constants.TOKEN.isEmpty()) {
                                        checkExpire();
                                    } else {
                                        PaytmOTPDialog paytmOTPDialog = new PaytmOTPDialog(getActivity());
                                        Bundle b = new Bundle();
                                        b.putString("fromcome", "general");
                                        b.putBoolean("isFromOTP", true);
                                        b.putString("gvtype", v_type);
                                        b.putString("charges", charges);

                                        if (btn_radio1.isChecked()) {
                                            b.putString("vehicle_no1", str_vno);
                                        } else if (btnradio2.isChecked()) {
                                            b.putString("vehicle_no1", strother);
                                        }

                                        b.putString("general1", "General");
                                        paytmOTPDialog.setArguments(b);
                                        paytmOTPDialog.show(getActivity().getFragmentManager(), "OTP1");
                                        Log.e(TAG, "RADIO CHECK ::" + bt_radio.getText().toString());
                                    }
                                } else {
                                    ShowDialog("success");
                                }
                            }

                        } else if (jsonObject.optInt("Status") == 406) {
                            Toast.makeText(getActivity(), " You Vehicle Already In for this parking ", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    cancelBooking();
                }
            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerViewParking extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("RESPONSE::::", "response: " + response);

                if (response != null) {
                    SetterViewParkingResponse setterViewParkingResponse = new Gson().fromJson(response, SetterViewParkingResponse.class);
                    if (setterViewParkingResponse.getSuccess() || setterViewParkingResponse.getStatus() == 200) {
                        if (setterViewParkingResponse.getData() != null) {

                            ModelViewParkingResponse.getInstance().setSetterViewParkingResponse(setterViewParkingResponse);
                            G_car = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar();
                            G_Bike = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike();
                            G_Bus = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus();
                            G_Truck = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck();

                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            int second = mcurrentTime.get(Calendar.SECOND);

                            String seconds = "";
                            if (second < 10) {
                                seconds = "0" + second;
                            } else {
                                seconds = "" + second;
                            }

                            String hours = "";
                            if (hour < 10) {
                                hours = "0" + hour;
                            } else {
                                hours = "" + hour;
                            }

                            if (minute < 10) {
                                outTime = hours + ":0" + minute + ":" + seconds;
                            } else {
                                outTime = hours + ":" + minute + ":" + seconds;
                            }

                            String Hour1 = edit_h1.getText().toString();
                            String Hour2 = edit_h2.getText().toString();
                            String minute1 = edit_m1.getText().toString();
                            String minute2 = edit_m2.getText().toString();
                            Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
                            Log.e(TAG, "Arrival time >>" + Arr_time);

                            //Estimated time
                            String Hour3 = edit_h3.getText().toString();
                            String Hour4 = edit_h4.getText().toString();
                            String minute3 = editm3.getText().toString();
                            String minute4 = editm4.getText().toString();
                            Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
                            //Time = "01:00";
                            Log.e(TAG, "HOUR >>" + Time);

                            charges = getCharges("00:00:00", Time);
                            tv_balance.setText(" Your Estimated Parking Charges : " + charges + " Rs");
                        }
                    }
                }
            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    ShowDialog("cancel");
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.info_ETA:
                Toast.makeText(getActivity(), "Approximate Arrival Time", Toast.LENGTH_SHORT).show();
                break;

            case R.id.info_EHS:
                Toast.makeText(getActivity(), "Approximate stay", Toast.LENGTH_SHORT).show();
                break;
        }
    }

}

