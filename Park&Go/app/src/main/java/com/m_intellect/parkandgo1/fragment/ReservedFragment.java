package com.m_intellect.parkandgo1.fragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityMain;
import com.m_intellect.parkandgo1.activity.MerchantActivity;
import com.m_intellect.parkandgo1.dialogfragment.PaytmOTPDialog;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.CheckExpireResponse;
import com.m_intellect.parkandgo1.model.CheckbalanceRes;
import com.m_intellect.parkandgo1.model.CheckminBalanceReq;
import com.m_intellect.parkandgo1.model.GenerateChecksumReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ModelViewParkingResponse;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.model.SetterPayload;
import com.m_intellect.parkandgo1.model.SetterViewParkingResponse;
import com.m_intellect.parkandgo1.model.TactionSuccessRes;
import com.m_intellect.parkandgo1.model.TransactionReq;
import com.m_intellect.parkandgo1.model.WithdrawMoneyResponse;
import com.m_intellect.parkandgo1.model.WithdrawRequest;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.network.WebServiceTimestamp;
import com.m_intellect.parkandgo1.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

public class ReservedFragment extends android.app.Fragment implements View.OnClickListener {

    public ParkConfigClass parkConfigClass;
    public String TAG = getClass().getSimpleName();
    Button btnconfirm;
    EditText edit_vno, edit_other, edit_h1, edit_h2, edit_m1, edit_m2, edit_h3, edit_h4, editm3, editm4, edit11, edit12, edit13, edit14, Edit_A1, edit_A, edit_M1, edit_M;
    RadioButton btn_radio1, btnradio2;
    RelativeLayout r_proceed;
    String charges = "0", charges1, str_vno, strother, RFStime, currtime;
    ImageView info_RSF, info_ETA, info_EHS, ck1, ck2;
    TextView tv_balance;
    EditText edit_v5, edit_v6, edit_v7, edit_v8, edit_v9, edit_v10, edit_v11, edit_v12, edit_v13, edit_v14, edit_v15, edit_v16;
    char[] charArray;
    String localTime, vehicleNo;
    int Total;
    ReservedFragment reservedFragment;
    SetterViewParkingResponse setterViewParkingResponse;
    SharedPreferences preferences;
    boolean bookingstatus = false;
    private int count = 0;
    private String v_type;
    private Spinner spin_vtype;
    private String Arr_time, Time, outTime, ETA_time, EHS_time;
    private TextView spin_vtype1;
    private int vacant = 0;

    private void viewConfiguration() {
        new WebServiceGet(getActivity(), new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null").execute();
        Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null");
    }

    private void initview(View view) {
        btnconfirm = (Button) view.findViewById(R.id.btnconfirm);
        // edit_vno = (EditText) view.findViewById(R.id.state0ET);
        // edit_other = (EditText) view.findViewById(R.id.edit_other);

        //vehicle no
        edit_v5 = (EditText) view.findViewById(R.id.pin_fifth_edittext);
        edit_v6 = (EditText) view.findViewById(R.id.pin_sixth_edittext);
        edit_v7 = (EditText) view.findViewById(R.id.pin_seven_edittext);
        edit_v8 = (EditText) view.findViewById(R.id.pin_eight_edittext);
        edit_v9 = (EditText) view.findViewById(R.id.pin_nine_edittext);
        edit_v10 = (EditText) view.findViewById(R.id.pin_tenth_edittext);

        //other
        edit_v11 = (EditText) view.findViewById(R.id.pin_fifth_edittext1);
        edit_v12 = (EditText) view.findViewById(R.id.pin_sixth_edittext1);
        edit_v13 = (EditText) view.findViewById(R.id.pin_seven_edittext1);
        edit_v14 = (EditText) view.findViewById(R.id.pin_eight_edittext1);
        edit_v15 = (EditText) view.findViewById(R.id.pin_nine_edittext1);
        edit_v16 = (EditText) view.findViewById(R.id.pin_tenth_edittext1);


        edit11 = (EditText) view.findViewById(R.id.edit11);
        edit12 = (EditText) view.findViewById(R.id.edit12);
        edit13 = (EditText) view.findViewById(R.id.edit13);
        edit14 = (EditText) view.findViewById(R.id.edit14);

        edit_h1 = (EditText) view.findViewById(R.id.edit1);
        edit_h2 = (EditText) view.findViewById(R.id.edit2);

        edit_h3 = (EditText) view.findViewById(R.id.edit5);
        edit_h4 = (EditText) view.findViewById(R.id.edit6);
        edit_m1 = (EditText) view.findViewById(R.id.edit3);
        edit_m2 = (EditText) view.findViewById(R.id.edit4);
        editm3 = (EditText) view.findViewById(R.id.edit7);
        editm4 = (EditText) view.findViewById(R.id.edit8);


        spin_vtype1 = (TextView) view.findViewById(R.id.spin_vtype);
        spin_vtype1.setText(Constants.VTYPE_NAME);


        try {
            edit_v5.setText("" + vehicleNo.charAt(6));
            edit_v6.setText("" + vehicleNo.charAt(7));
            edit_v7.setText("" + vehicleNo.charAt(9));
            edit_v8.setText("" + vehicleNo.charAt(10));
            edit_v9.setText("" + vehicleNo.charAt(11));
            edit_v10.setText("" + vehicleNo.charAt(12));

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //Make Disable All Hour Fields

        r_proceed = (RelativeLayout) view.findViewById(R.id.r_proceed);

        tv_balance = (TextView) view.findViewById(R.id.wallet_balance);

        btn_radio1 = (RadioButton) view.findViewById(R.id.radio1);
        btnradio2 = (RadioButton) view.findViewById(R.id.radio2);

        info_RSF = (ImageView) view.findViewById(R.id.info_RSF);
        info_ETA = (ImageView) view.findViewById(R.id.info_ETA);
        info_EHS = (ImageView) view.findViewById(R.id.info_EHS);
        ck1 = (ImageView) view.findViewById(R.id.ck_1);
        ck2 = (ImageView) view.findViewById(R.id.ck_2);

        Edit_A1 = (EditText) view.findViewById(R.id.edit_a1);
        edit_A = (EditText) view.findViewById(R.id.edit_a);
        edit_M1 = (EditText) view.findViewById(R.id.edit_m1);
        edit_M = (EditText) view.findViewById(R.id.edit_m);

        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm a");

        // you can get seconds by adding  "...:ss" to it
        localTime = date.format(currentLocalTime);
        Log.e(TAG, "L O C A L T I M E >>" + localTime);

        edit11.setText("" + localTime.charAt(0));
        edit12.setText("" + localTime.charAt(1));
        edit13.setText("" + localTime.charAt(3));
        edit14.setText("" + localTime.charAt(4));
    }

    private void setlistner() {

        final SharedPreferences preferences = getActivity().getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);

        btnconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (!edit_h1.getText().toString().equals("") && !edit_h3.getText().toString().equals("")) {
                    final String url = Constants.VIEW_CHARGES + Constants.SELCT_PARKG_ID + "/" + "null";
                    new WebServiceGet(getActivity(), new HandlerViewParking(), url).execute();
                    Log.e(TAG, "URL::::" + url);
                    str_vno = edit_v5.getText().toString() + edit_v6.getText().toString() + "-" + edit_v7.getText().toString() + edit_v8.getText().toString() + edit_v9.getText().toString() + edit_v10.getText().toString();
                    strother = edit_v11.getText().toString() + edit_v12.getText().toString() + "-" + edit_v13.getText().toString() + edit_v14.getText().toString() + edit_v15.getText().toString() + edit_v16.getText().toString();
                } else {
                    Toast.makeText(getActivity(), "Please Enter ETA & EHS Time", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (btn_radio1.isChecked()) {
                    btn_radio1.setChecked(true);
                    btnradio2.setChecked(false);
                    edit_v5.setEnabled(true);
                    edit_v6.setEnabled(true);
                    edit_v7.setEnabled(true);
                    edit_v8.setEnabled(true);
                    edit_v9.setEnabled(true);
                    edit_v10.setEnabled(true);
                    showEditCusor();
                    edit_v11.setEnabled(false);
                    edit_v12.setEnabled(false);
                    edit_v13.setEnabled(false);
                    edit_v14.setEnabled(false);
                    edit_v15.setEnabled(false);
                    edit_v16.setEnabled(false);
                    str_vno = edit_v5.getText().toString() + edit_v6.getText().toString() + edit_v7.getText().toString() + edit_v8.getText().toString() + edit_v9.getText().toString() + edit_v10.getText().toString();
                }
            }
        });

        btnradio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (btnradio2.isChecked()) {
                    btnradio2.setChecked(true);
                    btn_radio1.setChecked(false);
                    edit_v11.setEnabled(true);
                    edit_v12.setEnabled(true);
                    edit_v13.setEnabled(true);
                    edit_v14.setEnabled(true);
                    edit_v15.setEnabled(true);
                    edit_v16.setEnabled(true);
                    showOtherEditCusor();
                    edit_v5.setEnabled(false);
                    edit_v6.setEnabled(false);
                    edit_v7.setEnabled(false);
                    edit_v8.setEnabled(false);
                    edit_v9.setEnabled(false);
                    edit_v10.setEnabled(false);

                    strother = edit_v11.getText().toString() + edit_v12.getText().toString() + edit_v13.getText().toString() + edit_v14.getText().toString() + edit_v15.getText().toString() + edit_v16.getText().toString();

                }
            }
        });


        r_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count == 0) {
                    Toast.makeText(getActivity(), "Please Confirm your Parking", Toast.LENGTH_SHORT).show();
                } else {
                    if (btn_radio1.isChecked()) {
                        if (!edit_v5.getText().toString().equals("") && !edit_v6.getText().toString().equals("") && !edit_v7.getText().toString().equals("") && !edit_v8.getText().toString().equals("") &&
                                !edit_v9.getText().toString().equals("") && !edit_v10.getText().toString().equals("")) {

                            if (v_type.equals("Car")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Car Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            } else if (v_type.equals("Bike")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Bike Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }

                            } else if (v_type.equals("Bus")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Bus Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            } else if (v_type.equals("Truck")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Truck Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else if (edit_h1.getText().toString().equals("") && edit_h2.getText().toString().equals("") && edit_m1.getText().toString().equals("") && edit_m2.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please Enter ETA", Toast.LENGTH_SHORT).show();
                        } else if (edit_h3.getText().toString().equals("") && edit_h4.getText().toString().equals("") && editm3.getText().toString().equals("") && editm4.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please Enter EHS", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Please enter valid last six-digit vehicle number", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        if (!edit_v11.getText().toString().equals("") && !edit_v12.getText().toString().equals("") && !edit_v13.getText().toString().equals("") && !edit_v14.getText().toString().equals("") &&
                                !edit_v15.getText().toString().equals("") && !edit_v16.getText().toString().equals("")) {

                            if (v_type.equals("Car")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Car Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            } else if (v_type.equals("Bike")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Bike Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }

                            } else if (v_type.equals("Bus")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Bus Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            } else if (v_type.equals("Truck")) {
                                if (parkConfigClass.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() != 0) {
                                    insertBooking();
                                } else {
                                    Toast.makeText(getActivity(), "Truck Reserved Configuration is not Available", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else if (edit_h1.getText().toString().equals("") && edit_h2.getText().toString().equals("") && edit_m1.getText().toString().equals("") && edit_m2.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please Enter ETA", Toast.LENGTH_SHORT).show();
                        } else if (edit_h3.getText().toString().equals("") && edit_h4.getText().toString().equals("") && editm3.getText().toString().equals("") && editm4.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please Enter EHS", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Please enter valid last six-digit vehicle number", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            }
        });

        ck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // selectTime(v,ETA_time);

                view.getTag();
                String x = (String) view.getTag();
                final Calendar mcurrentTime1 = Calendar.getInstance();

                int hour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);
                int minute1 = mcurrentTime1.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
//
//                                String minuteSting = "";
//                                if (hourOfDay < 12 && hourOfDay >= 0) {
//
//                                    if (minute < 10)
//                                        minuteSting = "0";
//                                    else
//                                        minuteSting = "";
//                                    //edit_intime.setText(hourOfDay + ":" + minuteSting + minute + " AM");
//                                    ETA_time = hourOfDay + ":" + minuteSting + minute + " AM";
//                                    // EHS_time=hourOfDay + ":" + minuteSting + minute + " AM";
//                                    Log.e(TAG, "ETA TIME AM:::" + ETA_time);
//
//                                    if (ETA_time.length() == 7) {
//                                        edit_h1.setText("" + 0);
//                                        edit_h2.setText("" + ETA_time.charAt(0));
//                                        edit_m1.setText("" + ETA_time.charAt(2));
//                                        edit_m2.setText("" + ETA_time.charAt(3));
//                                        Edit_A1.setText("" + ETA_time.charAt(5));
//                                        edit_M1.setText("" + ETA_time.charAt(6));
//                                    } else {
//                                        edit_h1.setText("" + ETA_time.charAt(0));
//                                        edit_h2.setText("" + ETA_time.charAt(1));
//                                        edit_m1.setText("" + ETA_time.charAt(3));
//                                        edit_m2.setText("" + ETA_time.charAt(4));
//                                        Edit_A1.setText("" + ETA_time.charAt(6));
//                                        edit_M1.setText("" + ETA_time.charAt(7));
//                                    }
//                                } else {
//                                    hourOfDay -= 12;
//                                    if (hourOfDay == 0) {
//                                        hourOfDay = 12;
//                                    }
//                                    if (minute < 10)
//                                        minuteSting = "0";
//                                    else
//                                        minuteSting = "";
//                                    //  edit_intime.setText(hourOfDay + ":" + minuteSting + minute + " PM");
//
//                                    ETA_time = hourOfDay + ":" + minuteSting + minute + " PM";
//                                    Log.e(TAG, "ETA TIME PM:::" + ETA_time);
//                                    Log.e(TAG, "ETA TIME length:::" + ETA_time.length());
//                                    if (ETA_time.length() == 7) {
//                                        edit_h1.setText("" + 0);
//                                        edit_h2.setText("" + ETA_time.charAt(0));
//                                        edit_m1.setText("" + ETA_time.charAt(2));
//                                        edit_m2.setText("" + ETA_time.charAt(3));
//                                        Edit_A1.setText("" + ETA_time.charAt(5));
//                                        edit_M1.setText("" + ETA_time.charAt(6));
//                                    } else {
//                                        edit_h1.setText("" + ETA_time.charAt(0));
//                                        edit_h2.setText("" + ETA_time.charAt(1));
//                                        edit_m1.setText("" + ETA_time.charAt(3));
//                                        edit_m2.setText("" + ETA_time.charAt(4));
//                                        Edit_A1.setText("" + ETA_time.charAt(6));
//                                        edit_M1.setText("" + ETA_time.charAt(7));
//                                    }
//                                }

                                String timeFormat = "";
//                                if (hourOfDay == 0) {
//                                    hourOfDay += 12;
//                                    timeFormat = "AM";
//                                } else if (hourOfDay == 12) {
//                                    timeFormat = "PM";
//                                } else if (hourOfDay > 12) {
//                                    hourOfDay -= 12;
//                                    timeFormat = "PM";
//                                } else {
//                                    timeFormat = "AM";
//                                }

                                String selectedTime = hourOfDay + ":" + minute + " " + timeFormat;
                                Log.e(TAG, "selected time :: " + selectedTime);

                                // edit_event_time.setText(selectedTime);

                                if (selectedTime.length() == 6) {
                                    edit_h1.setText("" + selectedTime.charAt(0));
                                    edit_h2.setText("" + selectedTime.charAt(1));
                                    edit_m1.setText("" + selectedTime.charAt(3));
                                    edit_m2.setText("" + selectedTime.charAt(4));
                                } else if (selectedTime.length() == 5) {
                                    edit_h1.setText("" + selectedTime.charAt(0));
                                    edit_h2.setText("" + selectedTime.charAt(1));
                                    edit_m1.setText("" + 0);
                                    edit_m2.setText("" + selectedTime.charAt(3));
                                }

//                                Edit_A1.setText("" + ETA_time.charAt(5));
//                                edit_M1.setText("" + ETA_time.charAt(6));


                            }
                        }, hour1, minute1, false);

                timePickerDialog1.show();

            }
        });


        ck2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // selectTime(v,EHS_time);

                view.getTag();
                String x = (String) view.getTag();
                final Calendar mcurrentTime1 = Calendar.getInstance();

                int hour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);
                int minute1 = mcurrentTime1.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String minuteSting = "";
                                if (hourOfDay < 12 && hourOfDay >= 0) {

                                    if (minute < 10) {
                                        minuteSting = "0";
                                    } else {
                                        minuteSting = "";
                                    }
                                    //edit_intime.setText(hourOfDay + ":" + minuteSting + minute + " AM");
                                    // ETA_time = hourOfDay + ":" + minuteSting + minute + " AM";

                                    EHS_time = hourOfDay + ":" + minuteSting + minute + " hr";
                                    Log.e(TAG, "EHS_time  AM:::" + EHS_time);
                                    // EHS_time = "01:00 hr";


                                    if (EHS_time.length() == 7) {
                                        edit_h3.setText("" + 0);
                                        edit_h4.setText("" + EHS_time.charAt(0));
                                        editm3.setText("" + EHS_time.charAt(2));
                                        editm4.setText("" + EHS_time.charAt(3));

                                        edit_A.setText("" + EHS_time.charAt(5));
                                        edit_M.setText("" + EHS_time.charAt(6));
                                    } else {
                                        edit_h3.setText("" + EHS_time.charAt(0));
                                        edit_h4.setText("" + EHS_time.charAt(1));
                                        editm3.setText("" + EHS_time.charAt(3));
                                        editm4.setText("" + EHS_time.charAt(4));
                                        edit_A.setText("" + EHS_time.charAt(6));
                                        edit_M.setText("" + EHS_time.charAt(7));
                                    }
                                } else {
                                    hourOfDay -= 12;
                                    if (hourOfDay == 0) {
                                        hourOfDay = 12;
                                    }

                                    if (minute < 10) {
                                        minuteSting = "0";
                                    } else {
                                        minuteSting = "";
                                    }
                                    //  edit_intime.setText(hourOfDay + ":" + minuteSting + minute + " PM");

                                    EHS_time = hourOfDay + ":" + minuteSting + minute + " hr";
                                    Log.e(TAG, "EHS_time  PM:::" + EHS_time);
                                    Log.e(TAG, "ETA EHS_time length:::" + EHS_time.length());
                                    // EHS_time = "01:00 hr";

                                    if (EHS_time.length() == 7) {
                                        edit_h3.setText("" + 0);
                                        edit_h4.setText("" + EHS_time.charAt(0));
                                        editm3.setText("" + EHS_time.charAt(2));
                                        editm4.setText("" + EHS_time.charAt(3));
                                        edit_A.setText("" + EHS_time.charAt(5));
                                        edit_M.setText("" + EHS_time.charAt(6));
                                    } else {
                                        edit_h3.setText("" + EHS_time.charAt(0));
                                        edit_h4.setText("" + EHS_time.charAt(1));
                                        editm3.setText("" + EHS_time.charAt(3));
                                        editm4.setText("" + EHS_time.charAt(4));
                                        edit_A.setText("" + EHS_time.charAt(6));
                                        edit_M.setText("" + EHS_time.charAt(7));
                                    }

                                }
                            }
                        }, hour1, minute1, false);

                timePickerDialog1.show();

            }
        });

        info_RSF.setOnClickListener(this);
        info_ETA.setOnClickListener(this);
        info_EHS.setOnClickListener(this);
    }

    private void showEditCusor() {
        edit_v5.requestFocus();
        edit_v5.setCursorVisible(true);
        filter(edit_v5);
        filter(edit_v6);

        edit_v5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v5.getText().toString().length() == 1) {
                    edit_v6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_v6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v6.getText().toString().length() == 1) {
                    edit_v7.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v6.getText().toString().length() == 0) {
                    edit_v5.requestFocus();
                }
            }
        });

        edit_v7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v7.getText().toString().length() == 1) {
                    edit_v8.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v7.getText().toString().length() == 0) {
                    edit_v6.requestFocus();
                }
            }
        });


        edit_v8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v8.getText().toString().length() == 1) {
                    edit_v9.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v8.getText().toString().length() == 0) {
                    edit_v7.requestFocus();
                }
            }
        });


        edit_v9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v9.getText().toString().length() == 1) {
                    edit_v10.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v9.getText().toString().length() == 0) {
                    edit_v8.requestFocus();
                }
            }
        });

        edit_v10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v10.getText().toString().length() == 0) {
                    edit_v9.requestFocus();
                }
            }
        });
    }

    private void showOtherEditCusor() {
        edit_v11.requestFocus();
        edit_v11.setCursorVisible(true);
        filter(edit_v11);
        filter(edit_v12);
        edit_v11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v11.getText().toString().length() == 1) {
                    edit_v12.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
//
            }
        });

        edit_v12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v12.getText().toString().length() == 1) {
                    edit_v13.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v12.getText().toString().length() == 0) {
                    edit_v11.requestFocus();
                }
            }
        });

        edit_v13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v13.getText().toString().length() == 1) {
                    edit_v14.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v13.getText().toString().length() == 0) {
                    edit_v12.requestFocus();
                }
            }
        });


        edit_v14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v14.getText().toString().length() == 1) {
                    edit_v15.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v14.getText().toString().length() == 0) {
                    edit_v13.requestFocus();
                }
            }
        });


        edit_v15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_v15.getText().toString().length() == 1) {
                    edit_v16.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v15.getText().toString().length() == 0) {
                    edit_v14.requestFocus();
                }
            }
        });

        edit_v16.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_v16.getText().toString().length() == 0) {
                    edit_v15.requestFocus();
                }
            }
        });
    }

    private void filter(EditText edit) {
        edit.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[A-Z.]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
    }

    private void insertBooking() {

        //Arrival time
        String Hour1 = edit_h1.getText().toString();
        String Hour2 = edit_h2.getText().toString();
        String minute1 = edit_m1.getText().toString();
        String minute2 = edit_m2.getText().toString();

        Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
        Log.e(TAG, "Arrival time >>" + Arr_time);

        // Estimated time
        String Hour3 = edit_h3.getText().toString();
        String Hour4 = edit_h4.getText().toString();
        String minute3 = editm3.getText().toString();
        String minute4 = editm4.getText().toString();
        Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
        Log.e(TAG, "Estimate HOUR >>" + Time);
        // Time = "01:00:00";

        SharedPreferences preferences;
        preferences = getActivity().getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        String formattedDate3 = df3.format(c.getTime());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
        currtime = df4.format(calendar.getTime());

        String url = Constants.INSERT_BOOKING;

        SetterPayload setterPayload = new SetterPayload();
        setterPayload.setApp_id(Constants.APP_ID);
        setterPayload.setBookingId(0);
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setUserId(preferences.getInt(Constants.USER_ID, 0));

        setterPayload.setBookingTime(RFStime);
        setterPayload.setArrivalTime(Arr_time);

        setterPayload.setBookingCharges(Total);
        setterPayload.setOnline("online");
        setterPayload.setBookingType("Reserved");
        setterPayload.setBooking_status("2");
        setterPayload.setPaymentType("Wallet");

        setterPayload.setBooking_status("2");
        setterPayload.setStartDate(formattedDate3);
        setterPayload.setEndDate(formattedDate3);
        setterPayload.setLeaveTime("00:00");
        setterPayload.setOtp("");
        setterPayload.setMobile_no("");
        setterPayload.setTotal_time("0");
        setterPayload.setRsf(currtime);


        if (btn_radio1.isChecked()) {
            setterPayload.setVehicleNo(str_vno);
        } else if (btnradio2.isChecked()) {
            setterPayload.setVehicleNo(strother);
        }

        setterPayload.setAttendantName("");
        setterPayload.setOut_status(0);
        setterPayload.setColor_status(0);
        setterPayload.setAdvancePayHours(Time);
        setterPayload.setAdvancePaid("0");
        setterPayload.setBalanceCash("0");
        setterPayload.setVehicleType(v_type);
        setterPayload.setBooking_history(1);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("REQUEST>>> ", "payload: " + payload);
        Log.e("URL>>> ", "payload: " + Constants.INSERT_BOOKING);

        new WebServicePost(getActivity(), new HandlervehicleInCashPayment(), url, payload).execute();
    }

    private void generatecheckSum() {
        double dTotal = (double) Total;
        int UserID = preferences.getInt(Constants.USER_ID, 0);
        GenerateChecksumReq generateChecksumReq = new GenerateChecksumReq();
        generateChecksumReq.setCustId("" + UserID);
        generateChecksumReq.setOrderId("" + Constants.BOOK_ID);
        // generateChecksumReq.setTaxAmount(String.valueOf(dTotal));
        generateChecksumReq.setTaxAmount("1.00");
        Gson gson = new Gson();
        String payload = gson.toJson(generateChecksumReq);
        Log.e("test", "payload: " + payload);
        Log.e("URL ", "payload: " + Constants.GEN_CHECKSUM);
        new WebServicePost(getActivity(), new HandleChcksumResponse(), Constants.GEN_CHECKSUM, payload).execute();

    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse) {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setParkingConfigurationId(Constants.Cust_CONFIGURATION_ID);

        if (v_type.equals("Car")) {
            generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1));
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bike")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1);
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bus")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1);
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);


        } else if (v_type.equals("Truck")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1);
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);
        }

        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);


        new WebServicePost(getActivity(), new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    public void vacanT(ParkConfigClass setterViewParkingResponse, String vtype) {

        if (vtype.equals("Car")) {

            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

        } else if (vtype.equals("Bike")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Bus")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Truck")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        }

    }

    public String getCharges(int minutes) {
        // int minutes = 0;
        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
//        try {
//            startDate = simpleDateFormat.parse(startTime);
//            endDate = simpleDateFormat.parse(endTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        try {
//            long difference = endDate.getTime() - startDate.getTime();
//            if (difference < 0) {
//                Date dateMax = null;
//                Date dateMin = null;
//                try {
//                    dateMax = simpleDateFormat.parse("24:00");
//                    dateMin = simpleDateFormat.parse("00:00");
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
//            }
//            int days = (int) (difference / (1000 * 60 * 60 * 24));
//            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
//            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
//            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
//            if (show == 0) {
//                String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
//                String min3 = Time.substring(3, 5);
//                Log.e(TAG, "Reserved min3 :: " + min3);
//
//                String toRemove = ":";
//                if (base.contains(toRemove)) {
//                    base = base.replaceAll(toRemove, "");
//                }
//
//                Log.e(TAG, "Reserved BASE :: " + Integer.parseInt(base));
//                minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
//                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
//            } else {
//                minutes = hours * 60 + min;
//                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
//            }


            try {
                if (v_type.equals("Car")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPariceR();
                        }
                    }
                } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPariceR();
                        }
                    }
                } else if (v_type.equals("Bus")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPariceR();
                        }
                    }
                } else if (v_type.equals("Truck")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPariceR();
                        }
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "LAST CHARGE ----- >" + charges);
        return charges;
    }

    private void checkExpire() {
        // Constants.TOKEN = "2077cec4-0503-4376-be20-db15ec376800";
        Log.e("test", "TOKEN : " + Constants.TOKEN);
        new WebServiceTimestamp(getActivity(), new Handlercheckexpire(), Constants.PAYTM_TMEXPIRE).execute();
        Log.e("TAG", "Constants.PAYTM_TMEXPIRE----->" + Constants.PAYTM_TMEXPIRE);
    }

    private void Checkminbalance() {

        CheckminBalanceReq checkminBalanceReq = new CheckminBalanceReq();
        checkminBalanceReq.setTaxAmount(String.valueOf(Total));
        checkminBalanceReq.setToken(preferences.getString("TOKEN", ""));
        checkminBalanceReq.setBooking_id(Constants.BOOK_ID);
        checkminBalanceReq.setMobile_no(preferences.getString("PAYTMMOB", ""));

        Gson gson = new Gson();
        String payload = gson.toJson(checkminBalanceReq);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new HandlerMinBalance(), Constants.PAYTM_MINBALANCE, payload).execute();
        Log.e(TAG, "URL :: " + Constants.PAYTM_MINBALANCE);

    }

    private void withdrawmoney() {
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setMobNo(preferences.getString("PAYTMMOB", ""));
        withdrawRequest.setCustId(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));
        withdrawRequest.setToken(preferences.getString("TOKEN", ""));
        withdrawRequest.setOrderId(String.valueOf(Constants.BOOK_ID));
        withdrawRequest.setTaxAmt(String.valueOf(Total));
        Gson gson = new Gson();
        String payload = gson.toJson(withdrawRequest);
        Log.e(TAG, "payload PAYTM : " + payload);
        new WebServicePost(getActivity(), new Handlerwithdrwamoney(), Constants.PAYTM_WITHDRAW, payload).execute();
    }

    private void transactionStatus() {
        TransactionReq transactionReq = new TransactionReq();
        transactionReq.setOrderId(String.valueOf(Constants.BOOK_ID));
        Gson gson = new Gson();
        String payload = gson.toJson(transactionReq);
        Log.e(TAG, "payload PAYTM : " + payload);
        Log.e(TAG, "Transaction URL PAYTM : " + Constants.PAYTM_TRANSACTIONSUCCESS);
        new WebServicePost(getActivity(), new Handlertactionsuccess(), Constants.PAYTM_TRANSACTIONSUCCESS, payload).execute();
    }

    public void ShowDialog(final String stringtype) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setView(dialogView);
        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);
        if (stringtype.equals("success")) {
            bookingstatus = true;
            txt_text.setText("Success");
            txt_header.setText("You have Booked your Vehicle successfully");
            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
            btnok.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            txt_text.setText("Cancel Booking");
            txt_header.setText("Due to insufficient balance on your paytm wallet. We are not proceeding your booking.Please add money to your paytm wallet ");
            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
            btnok.setBackgroundColor(getResources().getColor(R.color.red));
        }


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stringtype.equals("success")) {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("isFromOTP", true);
                    intent.putExtra("gvtype", v_type);
                    intent.putExtra("general1", "Reserved");
                    if (btn_radio1.isChecked()) {
                        intent.putExtra("vehicle_no1", str_vno);
                    } else if (btnradio2.isChecked()) {
                        intent.putExtra("vehicle_no1", strother);
                    }
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                }

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
//        if (visible && isResumed()) {
//             Toast.makeText(getActivity(), "The Parking is Coming Soon", Toast.LENGTH_SHORT).show();
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setTitle("The Parking is Coming Soon");
//            builder.setCancelable(false);
//            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//            AlertDialog dialog = builder.create();
//            dialog.show();
//        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        // TODO Auto-generated method stub
        super.onAttachFragment(fragment);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reserved_layout, container, false);
        return view;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        preferences = getActivity().getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        vehicleNo = preferences.getString("VH_NO", "");
        Constants.TOKEN = preferences.getString("TOKEN", "");
        v_type = Constants.VTYPE_NAME;
        initview(view);
        setlistner();

        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.viewPager);
        viewConfiguration();
    }


//    @Override
//    public void onStop() {
//        super.onStop();
//        if (bookingstatus == false) {
//            cancelBooking();
//        }
//    }

    public int Timediff(String startTime, String endTime, int show) {

        int minutes = 0;
        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;

        try {
            startDate = simpleDateFormat.parse(startTime);
            endDate = simpleDateFormat.parse(endTime);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }


        try {
            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null;
                Date dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (show == 0) {
                String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
                String min3 = Time.substring(3, 5);
                Log.e(TAG, "Reserved min3 :: " + min3);

                String toRemove = ":";
                if (base.contains(toRemove)) {
                    base = base.replaceAll(toRemove, "");
                }

                Log.e(TAG, "Reserved BASE :: " + Integer.parseInt(base));
                minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            } else {
                minutes = hours * 60 + min;
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            }


        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return minutes;
    }

    public void cancelBooking() {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(getActivity(), new Handlecancelbook(), Constants.CANCEL_BOOK1, payload).execute();
        Log.e("TAG", "CancelURL>>>" + payload);
        Log.e("TAG", "CancelURL>>>" + Constants.CANCEL_BOOK1);
    }

    public class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    //   Utility.ShowDialog("cancel", mcontext, "Reserved");
                    ShowDialog("cancel");

                }
            }
        }
    }

    private class HandlervehicleInCashPayment extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {

                if (response != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            int insert_bookingId = ingredObject.optInt("insert_booking");//so you are going to get   ingredient name
                            Log.e("insert_bookingId", "" + insert_bookingId);
                            Constants.BOOK_ID = insert_bookingId;
                            Log.e("insert_bookingId", "--->" + Constants.BOOK_ID);

                            if (insert_bookingId > 0) {
                                if (!Constants.TOKEN.isEmpty()) {
                                    checkExpire();
                                } else {

                                    PaytmOTPDialog paytmOTPDialog = new PaytmOTPDialog(getActivity());
                                    Bundle b = new Bundle();
                                    b.putString("fromcome", "reserved");
                                    b.putBoolean("isFromOTP", true);
                                    b.putString("gvtype", v_type);
                                    b.putInt("parkingID", Constants.PARKG_ID);
                                    b.putString("RSF", currtime);
                                    b.putString("stayhour", Time);
                                    b.putString("arrivaltime", Arr_time);
                                    b.putString("charges", String.valueOf(Total));
                                    if (btn_radio1.isChecked()) {
                                        b.putString("vehicle_no1", str_vno);
                                    } else if (btnradio2.isChecked()) {
                                        b.putString("vehicle_no1", strother);
                                    }
                                    paytmOTPDialog.setArguments(b);
                                    paytmOTPDialog.show(getActivity().getFragmentManager(), "OTP1");

                                }


                            }
                        } else {
                            Toast.makeText(getActivity(), jsonObject.optString("Message"), Toast.LENGTH_SHORT).show();
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandleChcksumResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.e("Response", "response: " + response);

            if (response != null) {
                JSONObject jsonObject = null;
                JSONObject jsonObject1 = null;
                try {
                    jsonObject = new JSONObject(response);
                    jsonObject1 = jsonObject.getJSONObject("result");//so you are going to get   ingredient name

                    Intent i = new Intent(getActivity(), MerchantActivity.class);
                    i.putExtra("MID", jsonObject1.getString("MID"));
                    i.putExtra("ORDER_ID", jsonObject1.getString("ORDER_ID"));
                    i.putExtra("CUST_ID", jsonObject1.getString("CUST_ID"));
                    i.putExtra("INDUSTRY_TYPE_ID", jsonObject1.getString("INDUSTRY_TYPE_ID"));
                    i.putExtra("CHANNEL_ID", jsonObject1.getString("CHANNEL_ID"));
                    i.putExtra("TXN_AMOUNT", jsonObject1.getString("TXN_AMOUNT"));
                    i.putExtra("WEBSITE", jsonObject1.getString("WEBSITE"));
                    i.putExtra("CALLBACK_URL", jsonObject1.getString("CALLBACK_URL"));
                    i.putExtra("CHECKSUMHASH", jsonObject1.getString("CHECKSUMHASH"));
                    i.putExtra("VehicleType", v_type);
                    if (btn_radio1.isChecked()) {
                        i.putExtra("vehicle_no1", str_vno);
                    } else if (btnradio2.isChecked()) {
                        i.putExtra("vehicle_no1", strother);
                    }
                    startActivity(i);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);

            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        Log.d(TAG, "Hour: " + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("VACANT RESPONSE ::", "response: " + response);
            try {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");
                        ShowDialog("success");
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerViewParking extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("RESPONSE::::", "response: " + response);

                if (response != null) {
                    SetterViewParkingResponse setterViewParkingResponse = new Gson().fromJson(response, SetterViewParkingResponse.class);
                    if (setterViewParkingResponse.getSuccess() || setterViewParkingResponse.getStatus() == 200) {
                        if (setterViewParkingResponse.getData() != null) {

                            ModelViewParkingResponse.getInstance().setSetterViewParkingResponse(setterViewParkingResponse);

                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            int second = mcurrentTime.get(Calendar.SECOND);

                            String seconds = "";
                            if (second < 10) {
                                seconds = "0" + second;
                            } else {
                                seconds = "" + second;
                            }

                            String hours = "";
                            if (hour < 10) {
                                hours = "0" + hour;
                            } else {
                                hours = "" + hour;
                            }

                            if (minute < 10) {
                                outTime = hours + ":0" + minute + ":" + seconds;
                            } else {
                                outTime = hours + ":" + minute + ":" + seconds;
                            }

                            String Hour11 = edit11.getText().toString();
                            String Hour12 = edit12.getText().toString();
                            String minute13 = edit13.getText().toString();
                            String minute14 = edit14.getText().toString();
                            RFStime = Hour11 + Hour12 + ":" + minute13 + minute14 + ":" + 00;
                            Log.e(TAG, "RFS time >>" + RFStime);

                            String Hour1 = edit_h1.getText().toString();
                            String Hour2 = edit_h2.getText().toString();
                            String minute1 = edit_m1.getText().toString();
                            String minute2 = edit_m2.getText().toString();
                            Arr_time = Hour1 + Hour2 + ":" + minute1 + minute2 + ":" + 00;
                            Log.e(TAG, "Arrival time >>" + Arr_time);

                            //Estimated time
                            String Hour3 = edit_h3.getText().toString();
                            String Hour4 = edit_h4.getText().toString();
                            String minute3 = editm3.getText().toString();
                            String minute4 = editm4.getText().toString();
                            Time = Hour3 + Hour4 + ":" + minute3 + minute4 + ":" + 00;
                            Log.e(TAG, "HOUR >>" + Time);

                            int min1 = Timediff(RFStime, Arr_time, 1);
                            int min2 = Timediff("00:00:00", Time, 0);


                            int min3 = min1 + min2;
                            Log.e(TAG, "min3 --->" + min3);
                            charges1 = getCharges(min3);

                            try {
                                Total = Integer.parseInt(charges1) + Integer.parseInt(charges);
                                Log.e(TAG, "Charges --->" + charges1);
                                Log.e(TAG, "Charges 1 --->" + charges);
                                Log.e(TAG, "TOTAL  ---> " + Total);
                            }
                            catch (NumberFormatException e) {
                                e.printStackTrace();
                            }

                            tv_balance.setText("Your Estimated Parking Charges " + Total + " Rs");

                        }
                    }
                }
            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class Handlercheckexpire extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.e("test", "Expire Response ---->" + response);
            if (response != null) {
                CheckExpireResponse checkbalanceRes = new Gson().fromJson(response, CheckExpireResponse.class);
                String numberAsString = checkbalanceRes.getExpires();
                long number = Long.valueOf(numberAsString);
                Log.e("Original number", "----->" + number);
                Log.e("Current TIMESTAMP", "------> " + new java.sql.Timestamp(System.currentTimeMillis()).getTime());

                if (number <= (new java.sql.Timestamp(System.currentTimeMillis()).getTime())) {
                    PaytmOTPDialog paytmOTPDialog = new PaytmOTPDialog(getActivity());
                    Bundle b = new Bundle();
                    b.putString("general1", "reserved");
                    b.putBoolean("isFromOTP", true);
                    b.putString("gvtype", v_type);
                    b.putInt("parkingID", Constants.PARKG_ID);
                    b.putString("RSF", currtime);
                    b.putString("stayhour", Time);
                    b.putString("arrivaltime", Arr_time);
                    if (btn_radio1.isChecked()) {
                        b.putString("vehicle_no1", str_vno);
                    } else if (btnradio2.isChecked()) {
                        b.putString("vehicle_no1", strother);
                    }
                    b.putString("charges", String.valueOf(Total));
                    paytmOTPDialog.setArguments(b);
                    paytmOTPDialog.show(getActivity().getFragmentManager(), "OTP1");
                } else {
                    Checkminbalance();
                }
            } else {
//                Utility.cancelBooking(getActivity());
                cancelBooking();
            }
        }
    }

    private class HandlerMinBalance extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " Check Balance response: " + response);
            try {
                if (response != null) {
                    CheckbalanceRes checkbalanceRes = new Gson().fromJson(response, CheckbalanceRes.class);
                    if (checkbalanceRes.getData().getBody().getResultInfo().getResultStatus().equals("SUCCESS")) {

                        if (checkbalanceRes.getData().getBody().isFundsSufficient() == false) {

//                            Intent i = new Intent(getActivity(), ActivityAddMoneyToWallet.class);
//                            i.putExtra("general1", "reserved");
//                            i.putExtra("isFromOTP", true);
//                            i.putExtra("gvtype", v_type);
//                            i.putExtra("parkingID", Constants.PARKG_ID);
//                            i.putExtra("RSF", currtime);
//                            i.putExtra("stayhour", Time);
//                            i.putExtra("arrivaltime", Arr_time);
//                            i.putExtra("charges", String.valueOf(Total));
//
//                            if (btn_radio1.isChecked()) {
//                                i.putExtra("vehicle_no1", str_vno);
//                            } else if (btnradio2.isChecked()) {
//                                i.putExtra("vehicle_no1", strother);
//                            }
//                            getActivity().startActivity(i);

//                            Utility.cancelBooking(getActivity());

                            cancelBooking();

                        } else {
                            //Withdraw Money API......
                            withdrawmoney();
                        }
                    }
                } else {
//                    Utility.cancelBooking(getActivity());
                    cancelBooking();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class Handlerwithdrwamoney extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " withdraw response: " + response);
            if (response != null) {
                WithdrawMoneyResponse withdrawMoneyResponse = new Gson().fromJson(response, WithdrawMoneyResponse.class);
                if (withdrawMoneyResponse.getData().getStatus().equals("TXN_SUCCESS")) {
                    Log.e(TAG, "<---- Withdraw Success --->");
                    transactionStatus();
                } else {
//                    Utility.cancelBooking(getActivity());
                    cancelBooking();
                }
            } else {
//                Utility.cancelBooking(getActivity());
                cancelBooking();
            }
        }
    }

    private class Handlertactionsuccess extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "Transaction response: " + response);
            if (response != null) {
                TactionSuccessRes tactionSuccessRes = new Gson().fromJson(response, TactionSuccessRes.class);
                if (tactionSuccessRes.getData().getSTATUS().equals("TXN_SUCCESS")) {
                    //Toast.makeText(getActivity(), "Parking Booked Successfully", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", "Transaction success");
                    new WebServiceGet(getActivity(), new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null").execute();
                    Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null");
                    updateVacant(parkConfigClass);
                } else {
//                    Utility.cancelBooking(getActivity());
                    cancelBooking();
                }
            } else {
//                Utility.cancelBooking(getActivity());
                cancelBooking();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.info_RSF:
                Toast.makeText(getActivity(), "Reserved Start From", Toast.LENGTH_SHORT).show();
                break;

            case R.id.info_ETA:
                Toast.makeText(getActivity(), "Estimate Time Arrival", Toast.LENGTH_SHORT).show();
                break;

            case R.id.info_EHS:
                Toast.makeText(getActivity(), "Estimate Hour Stay", Toast.LENGTH_SHORT).show();
                break;
        }
    }


//    public void ShowDialog(final String stringtype) {
//        //then we will inflate the custom alert dialog xml that we created
//        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.my_dialog, null, false);
//        //Now we need an AlertDialog.Builder object
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setCancelable(false);
//        builder.setView(dialogView);
//        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
//        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
//        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
//        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);
//
//        if (stringtype.equals("success")) {
//            //bookingstatus = true;
//            txt_text.setText("Success");
//            txt_header.setText("You have Booked your Vehicle successfully");
//            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
//            btnok.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//        } else {
//            txt_text.setText("Cancel Booking");
//            txt_header.setText("Due to insufficient balance on your paytm wallet. We are not proceeding your booking.Please add money to your paytm wallet ");
//            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
//            btnok.setBackgroundColor(getResources().getColor(R.color.red));
//        }
//
//
//        btnok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (stringtype.equals("success")) {
//                    Intent intent = new Intent(getActivity(), ActivityMain.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.putExtra("isFromOTP", true);
//                    intent.putExtra("gvtype", v_type);
//                    if (btn_radio1.isChecked()) {
//                        intent.putExtra("vehicle_no1", str_vno);
//                    } else if (btnradio2.isChecked()) {
//                        intent.putExtra("vehicle_no1", strother);
//                    }
//                    intent.putExtra("general1", "General");
//                    startActivity(intent);
//                    getActivity().finish();
//                } else {
//                    Intent intent = new Intent(getActivity(), ActivityMain.class);
//                    startActivity(intent);
////                    getActivity().finish();
//
//                }
//            }
//        });
//
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//    }


}
