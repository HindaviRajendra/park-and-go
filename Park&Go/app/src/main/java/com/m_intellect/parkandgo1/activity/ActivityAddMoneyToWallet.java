package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.model.TactionSuccessRes;
import com.m_intellect.parkandgo1.model.TransactionReq;
import com.m_intellect.parkandgo1.model.WalletchecksumResponse;
import com.m_intellect.parkandgo1.model.WithdrawMoneyResponse;
import com.m_intellect.parkandgo1.model.WithdrawRequest;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

public class ActivityAddMoneyToWallet extends AppCompatActivity implements View.OnClickListener {

    public ParkConfigClass parkConfigClass;
    int ordrID = 0;
    private String TAG = getClass().getSimpleName();
    private SharedPreferences preferences;
    private int pref_userid;
    private LinearLayout ll_main, ll_child;
    private String custID, reqID, checksum;
    private String v_type, vno, fromcome, charges;
    private boolean isFromOTP;
    private EditText edit_amt;
    private int vacant = 0;
    private boolean backstatus = false;

    public static int generateRandomInt(int upperRange) {
        Random random = new Random();
        return random.nextInt(upperRange);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money_to_wallet);
        preferences = getSharedPreferences("ParkAndGo", MODE_PRIVATE);
        pref_userid = preferences.getInt(Constants.USER_ID, 0);
        Log.e(TAG, "USER ID ------->" + pref_userid);
        if (getIntent().getExtras() != null) {
            v_type = getIntent().getExtras().getString("gvtype");
            vno = getIntent().getExtras().getString("vehicle_no1");
            isFromOTP = getIntent().getExtras().getBoolean("isFromOTP");
            fromcome = getIntent().getExtras().getString("general1");
            charges = getIntent().getExtras().getString("charges");
            Log.e(TAG, "Vehicle type ------->" + v_type);
            Log.e(TAG, "Vehicle no ------->" + vno);
            Log.e(TAG, "Vehicle isFromOTP ------->" + isFromOTP);
            Log.e(TAG, "fromcome ------->" + fromcome);
            Log.e(TAG, "charges ------->" + charges);
        }

        initToolbar();

    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView availableBalanceTV = (TextView) toolbar.findViewById(R.id.availableBalanceTV);
        availableBalanceTV.setVisibility(View.VISIBLE);
        availableBalanceTV.setText("");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        locationTV.setLayoutParams(params);


        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        ll_child = (LinearLayout) findViewById(R.id.ll_child);


        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Wallet");

        TextView addwallet = (TextView) findViewById(R.id.addTV);
        addwallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenerateCheckSum();

            }
        });

        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);


        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

        edit_amt = (EditText) findViewById(R.id.mobileET);
    }

    private void GenerateCheckSum() {
        ordrID = generateRandomInt(400);
        Map<String, String> paramap = new HashMap<String, String>();
        if (fromcome.equals("Home")) {
            paramap.put("order_id", String.valueOf(ordrID));
        } else {
            paramap.put("order_id", String.valueOf(Constants.BOOK_ID));
        }

        paramap.put("cust_id", "" + pref_userid);
        paramap.put("tax_amt", edit_amt.getText().toString());
        paramap.put("token", preferences.getString("TOKEN", ""));

        Gson gson = new Gson();
        String payload = gson.toJson(paramap);
        Log.e(TAG, "payload ::: " + payload);

        new WebServicePost(ActivityAddMoneyToWallet.this, new HandlergenerateCheksum(), Constants.PAYTM_ADDWALLET, payload).execute();
        Log.e(TAG, "URL :: " + Constants.PAYTM_ADDWALLET);

    }

    private void AddWallet(String custID, String reqID, String checksum) {
        ll_main.setVisibility(View.GONE);
        ll_child.setVisibility(View.VISIBLE);
        WebView webview = new WebView(this);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        setContentView(webview);
        String url = "http://13.232.8.225/api/addwallethtmllive";

        String postData = null;
        try {
            postData = "order_id=" + URLEncoder.encode(String.valueOf(reqID), "UTF-8") +
                    "&cust_id=" + URLEncoder.encode(custID, "UTF-8") +
                    "&tax_amt=" + URLEncoder.encode(edit_amt.getText().toString(), "UTF-8") +
                    "&token=" + URLEncoder.encode(preferences.getString("TOKEN", ""), "UTF-8") +
                    "&checksum=" + checksum;

            Log.e(TAG, "on finish response ---->" + postData);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        webview.postUrl(url, postData.getBytes());

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                Log.e(TAG, "on finish URL ---->" + url);
                if (url.equals("http://13.232.8.225/api/successlive")) {

                    if (fromcome.equals("reserved")) {
                        withdrawmoney();

//                        Intent intent = new Intent(ActivityAddMoneyToWallet.this, ActivityMain.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("isFromOTP", true);
//                        intent.putExtra("gvtype", v_type);
//                        intent.putExtra("vehicle_no1", vno);
//                        intent.putExtra("general1", "reserved");
//                        startActivity(intent);
//                        ActivityAddMoneyToWallet.this.finish();


                    }
                } else {
                    Log.e(TAG, "ELSE URL ---->" + url);
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            }

            @Override
            public void onReceivedHttpError(
                    WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                           SslError error) {
            }
        });

    }

    private void withdrawmoney() {
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        withdrawRequest.setMobNo(preferences.getString("PAYTMMOB", ""));
        withdrawRequest.setCustId(String.valueOf(preferences.getInt(Constants.USER_ID, 0)));
        withdrawRequest.setToken(preferences.getString("TOKEN", ""));
        withdrawRequest.setOrderId(String.valueOf(Constants.BOOK_ID));
        withdrawRequest.setTaxAmt("10");
        Gson gson = new Gson();

        String payload = gson.toJson(withdrawRequest);
        Log.e(TAG, "payload PAYTM : " + payload);

        new WebServicePost(ActivityAddMoneyToWallet.this, new Handlerwithdrwamoney(), Constants.PAYTM_WITHDRAW, payload).execute();
    }

    private void transactionStatus() {
        TransactionReq transactionReq = new TransactionReq();
        transactionReq.setOrderId(String.valueOf(Constants.BOOK_ID));
        Gson gson = new Gson();
        String payload = gson.toJson(transactionReq);
        Log.e(TAG, "p a y l o a d P A Y T M : " + payload);
        new WebServicePost(ActivityAddMoneyToWallet.this, new Handlertactionsuccess(), Constants.PAYTM_TRANSACTIONSUCCESS, payload).execute();
    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse) {

        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setParkingConfigurationId(Constants.Cust_CONFIGURATION_ID);
        Log.e(TAG, "i am in else");

        if (v_type.equals("Car")) {
            generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1));
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);
        } else if (v_type.equals("Bike")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1);
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bus")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1);
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);


        } else if (v_type.equals("Truck")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1);
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);
        }

        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);


        new WebServicePost(ActivityAddMoneyToWallet.this, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    public void vacanT(ParkConfigClass setterViewParkingResponse, String vtype) {

        if (vtype.equals("Car")) {

            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

        } else if (vtype.equals("Bike")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Bus")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Truck")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        }

    }

    public void ShowDialog(String stringtype) {
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(ActivityAddMoneyToWallet.this).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAddMoneyToWallet.this);
        builder.setCancelable(false);
        builder.setView(dialogView);

        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        if (stringtype.equals("success")) {
            txt_text.setText("Success");
            txt_header.setText("You have Booked your Vehicle successfully");
            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
            btnok.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            txt_text.setText("Cancel Booking");
            txt_header.setText("Sorry Due to some reason your booking cancelled,Please try again");
            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
            btnok.setBackgroundColor(getResources().getColor(R.color.red));
        }


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityAddMoneyToWallet.this, ActivityMain.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("isFromOTP", true);
                intent.putExtra("gvtype", v_type);
                intent.putExtra("vehicle_no1", vno);
                if (fromcome.equals("reserved")) {
                    intent.putExtra("general1", "Reserved");
                } else {
                    intent.putExtra("general1", "General");
                }
                startActivity(intent);
                ActivityAddMoneyToWallet.this.finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        if (backstatus == false) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAddMoneyToWallet.this);
//            builder.setMessage("Are you want to cancel booking");
//            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    cancelBooking();
//                    dialog.dismiss();
//                }
//            });
//            AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//
//
//        } else {
//            super.onBackPressed();
//        }


    }

    private void cancelBooking() {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(ActivityAddMoneyToWallet.this, new Handlecancelbook(), Constants.CANCEL_BOOK1, payload).execute();
        Log.e(TAG, "CancelURL>>>" + payload);
        Log.e(TAG, "CancelURL>>>" + Constants.CANCEL_BOOK1);
    }

    public class HandlergenerateCheksum extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            try {
                if (response != null) {
                    WalletchecksumResponse modelValidateOTPResponse = new Gson().fromJson(response, WalletchecksumResponse.class);
                    custID = modelValidateOTPResponse.getResult().getCUSTID();
                    //reqID = modelValidateOTPResponse.getResult().getORDERID();
                    checksum = modelValidateOTPResponse.getResult().getCHECKSUMHASH();
                    Log.e(TAG, "Success on checksum generate----------> " + checksum);

                    if (fromcome.equals("Home")) {
                        AddWallet(custID, String.valueOf(ordrID), checksum);
                    } else {
                        AddWallet(custID, String.valueOf(Constants.BOOK_ID), checksum);
                    }
                }
            }
            catch (Exception e) {
                Log.e(TAG, "failure  :: " + e.getLocalizedMessage());
            }
        }
    }

    private class Handlerwithdrwamoney extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", " withdraw response: " + response);
            if (response != null) {
                WithdrawMoneyResponse withdrawMoneyResponse = new Gson().fromJson(response, WithdrawMoneyResponse.class);
                if (withdrawMoneyResponse.getData().getStatus().equals("TXN_SUCCESS")) {
                    transactionStatus();
                }
            }
        }
    }

    private class Handlertactionsuccess extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "Transaction response: " + response);
            if (response != null) {
                TactionSuccessRes tactionSuccessRes = new Gson().fromJson(response, TactionSuccessRes.class);
                if (tactionSuccessRes.getData().getSTATUS().equals("TXN_SUCCESS")) {
                    try {
                        new WebServiceGet(ActivityAddMoneyToWallet.this, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null").execute();
                        Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null");
                        updateVacant(parkConfigClass);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;

            Log.d("VACANT RESPONSE ::", "response: " + response);

            try {
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");
                        ShowDialog("success");
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerparkdetailResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);
            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        Log.d(TAG, "Hour: " + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    ShowDialog("cancel");
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;

            default:
                break;

        }
    }


}
