package com.m_intellect.parkandgo1.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivitySearchRegularParking;
import com.m_intellect.parkandgo1.model.ModelParkingDetailResponse;

import java.util.List;

/**
 * Created by sanket on 12/4/2017.
 */

public class AdapterSearchRegularParkingList extends RecyclerView.Adapter<AdapterSearchRegularParkingList.ViewHolder> {

    private Context context;
    private List<ModelParkingDetailResponse> modelParkingDetailResponseList;

    public AdapterSearchRegularParkingList(Context context, List<ModelParkingDetailResponse> modelParkingDetailResponseList) {

        this.context = context;
        this.modelParkingDetailResponseList = modelParkingDetailResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_search_regular_parking, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ModelParkingDetailResponse modelParkingDetailResponse = modelParkingDetailResponseList.get(position);

        holder.titleTV.setText(modelParkingDetailResponse.getPlaceName());
        holder.addressTV.setText(modelParkingDetailResponse.getPlaceLandmark());
        holder.totalSpaceTV.setText("Total Space - " + modelParkingDetailResponse.getTotal());
        if (modelParkingDetailResponse.getCalender() != null && modelParkingDetailResponse.getStartDate() != null && modelParkingDetailResponse.getEndDate() != null) {

            int general = 0;
            int reserved = 0;
            int regular = 0;
            int event = 0;
            int premium = 0;

            if (modelParkingDetailResponse.getGeneral() != null && !modelParkingDetailResponse.getGeneral().isEmpty())
                general = Integer.parseInt(modelParkingDetailResponse.getGeneral());

            if (modelParkingDetailResponse.getReserved() != null && !modelParkingDetailResponse.getReserved().isEmpty())
                reserved = Integer.parseInt(modelParkingDetailResponse.getReserved());

            if (modelParkingDetailResponse.getRegular() != null && !modelParkingDetailResponse.getRegular().isEmpty())
                regular = Integer.parseInt(modelParkingDetailResponse.getRegular());

            if (modelParkingDetailResponse.getEvent() != null && !modelParkingDetailResponse.getEvent().isEmpty())
                event = Integer.parseInt(modelParkingDetailResponse.getEvent());

            if (modelParkingDetailResponse.getPremium() != null && !modelParkingDetailResponse.getPremium().isEmpty())
                premium = Integer.parseInt(modelParkingDetailResponse.getPremium());

            int totalVacant = general + reserved + regular + event + premium;

            holder.vacantTV.setText("Vacant - " + totalVacant);

        } else {
            holder.vacantTV.setText("Vacant - " + modelParkingDetailResponse.getTotal());
        }
        if (modelParkingDetailResponse.isRadioChecked())
            holder.radioBtn.setChecked(true);
        else
            holder.radioBtn.setChecked(false);
        holder.radioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.radioBtn.isChecked()) {

                    ((ActivitySearchRegularParking) context).reservedChecker(holder.getAdapterPosition(), true);
                } else {
                    ((ActivitySearchRegularParking) context).reservedChecker(holder.getAdapterPosition(), false);
                }
            }
        });

        holder.addIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // SingletonParkingDetail.getInstance().setModelParkingDetailResponse(modelParkingDetailResponse);
                // Intent intent = new Intent(context, ActivityHoursAndFareDetails.class);
                // intent.putExtra(Constants.PLACE_NAME, modelParkingDetailResponse.getPlaceName());
                //  context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelParkingDetailResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTV, addressTV, vacantTV, startTimeTV, endTimeTV, totalSpaceTV;
        private RadioButton radioBtn;
        private ImageView addIV;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            totalSpaceTV = (TextView) itemView.findViewById(R.id.totalSpaceTV);
            vacantTV = (TextView) itemView.findViewById(R.id.vacantTV);
            radioBtn = (RadioButton) itemView.findViewById(R.id.radioBtn);
            totalSpaceTV = (TextView) itemView.findViewById(R.id.totalSpaceTV);
            addIV = (ImageView) itemView.findViewById(R.id.addIV);

        }
    }
}
