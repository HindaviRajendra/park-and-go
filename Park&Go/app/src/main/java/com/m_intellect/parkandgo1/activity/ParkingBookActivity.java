package com.m_intellect.parkandgo1.activity;


import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterViewPager;
import com.m_intellect.parkandgo1.fragment.GeneralFragment;
import com.m_intellect.parkandgo1.fragment.RegularFragment;
import com.m_intellect.parkandgo1.fragment.ReservedFragment;
import com.m_intellect.parkandgo1.utils.Constants;

public class ParkingBookActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    Spinner spin_vtype;
    GeneralFragment generalFragment;
    ReservedFragment reservedFragment;
    private Double latitude;
    private Double longitude;
    private String Locationname;
    private int locaID;
    private String TAG = getClass().getSimpleName();
    // SharedPreferences preferences;
    //SharedPreferences.Editor editor=preferences.edit();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_layout);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        if (getIntent().getExtras() != null) {
            latitude = getIntent().getDoubleExtra(Constants.LATITUDE, 0.0);
            longitude = getIntent().getDoubleExtra(Constants.LONGITUDE, 0.0);
            Locationname = getIntent().getStringExtra(Constants.PLACE_NAME);
            Log.e(TAG, "location Name:::" + Locationname);
            locaID = getIntent().getIntExtra(Constants.PARKING_ID, 0);
            Constants.PARKG_ID = locaID;
            Constants.Cust_CONFIGURATION_ID = getIntent().getIntExtra("CONFIGID", 0);
        }

        inittoolbar();
        initView();

    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.e(TAG, "Indian ---->");

    }

    private void inittoolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);

        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText(Constants.SELECT_PARKG_NAME);
        titleTV.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        ImageView Attendance = (ImageView) toolbar.findViewById(R.id.attendance);
        Attendance.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        final AdapterViewPager adapterViewPager = new AdapterViewPager(getFragmentManager());
        adapterViewPager.addFragment(new GeneralFragment(), " GENERAL ");
        adapterViewPager.addFragment(new ReservedFragment(), "RESERVED");
        adapterViewPager.addFragment(new RegularFragment(), "REGULAR");
        adapterViewPager.addFragment(new RegularFragment(), "BULK");
        viewPager.setAdapter(adapterViewPager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //startActivity(new Intent(ParkingBookActivity.this,ActivityMain.class).putExtra("isFromOTP", false));
    }
}
