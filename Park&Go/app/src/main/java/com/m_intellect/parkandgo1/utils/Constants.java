package com.m_intellect.parkandgo1.utils;

/**
 * Created by sanket on 12/30/2017.
 */

public class Constants {

    public static final Integer APP_ID = 29;
    // public static final String SIGNUP_URL = "http://13.232.8.225/fileuploadapi/api/insertparking_customer";
    public static final String SIGNUP_URL = "http://13.233.234.166:6001/api/fileuploadapi/api/insertparking_customer";
    public static final String DELETE_Booking = "http://13.232.8.225/api/api/delete_booking";
    public static final String UPDATE_VERSION = "http://myappcenter.co.in/testapi/api/appversion";
    // KEY
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ISLOGIN = "isLogin";
    public static final String LOCATION_ID = "locationId";
    public static final String USER_ID = "userId";
    public static final String USER_TYPE = "userType";
    public static final String PLACE_NAME = "placeName";
    public static final String LOCATION_NAME = "locationName";
    public static final String PARKING_ID = "parkingId";
    public static final String FROM_EDIT = "fromEdit";
    public static final String EMAIL_ID = "emailId";
    public static final String MOBILE_NO = "mobileNo";
    public static final int GET_DATES = 1002;
    public static final String SELECTED_DATES = "selected_dates";
    public static final String REGISTERED = "register";

    // URL
    // private static final String BASE_URL = "http://13.232.8.225/api/";
    private static final String BASE_URL = "http://13.233.234.166:6001/";
    public static final String VIEW_EVENT_LIST_URL = BASE_URL + "api/view_parking_event/" + APP_ID + "/null";
    public static final String LOGIN_URL = BASE_URL + "api/parkingmemberlogin";
    public static final String PARKING_HISTORY_URL = BASE_URL + "api/view_user_booking/" + APP_ID;
    public static final String VIEW_PROFILE_URL = BASE_URL + "api/viewparkinguserstatus/" + APP_ID + "/1/customer/";
    public static final String VIEW_PARKING_AREA_URL1 = BASE_URL + "api/view_location/" + APP_ID + "/null";
    public static final String VIEW_PARKING_AREA_URL = BASE_URL + "api/view_parking/" + APP_ID + "/null/null/null/null/null/null/null/null/";
    public static final String VIEW_PARKING_AREA_URL12 = BASE_URL + "api/view_parking/" + APP_ID + "/null/";
    public static final String VIEW_PARKING_DETAIL_URL = BASE_URL + "api/view_parking_configuration/" + APP_ID + "/";
    //public static final String VIEW_PARKING_DETAIL_URL = BASE_URL + "api/view_parking/" + APP_ID + "/";
    public static final String CONTRACTOR_REGISTRATION_URL = BASE_URL + "api/insert_vendor";
    public static final String VALIDATE_OTP_URL = BASE_URL + "api/parkingvalidateotp";
    public static final String VALIDATE_OTP_URL1 = BASE_URL + "api/parkingvalidateotp_user";
    public static final String RESEND_OTP_URL = BASE_URL + "api/parking_resendotp/" + APP_ID + "/";
    public static final String PARKING_CONFIGURATION_LIST_URL = BASE_URL + "api/view_parking_configuration/" + APP_ID + "/null/";
    public static final String ATTENDANT_LIST_URL = BASE_URL + "api/viewparkinguserstatus/" + APP_ID + "/1/attendant/null/";
    public static final String ADD_ATTENDANT_URL = BASE_URL + "api/insert_attendant";
    public static final String ADD_PARKING_CONFIGURATION_URL = BASE_URL + "api/insertparking_configuration";
    public static final String VIEW_PARKING_LIST_URL = BASE_URL + "api/view_parking_list/" + APP_ID + "/";
    public static final String NEW_PARKING_URL = BASE_URL + "api/insertparking";
    public static final String VIEW_GRAPH_URL = BASE_URL + "api/view_booking_graph/" + APP_ID + "/";
    public static final String FORGOT_PASSWORD_URL = BASE_URL + "api/parking_forgotpassword";
    public static final String VIEW_MOBILE_PARK = BASE_URL + "api/view_parking_mobile/" + APP_ID + "/null/null/";
    public static final String VIEW_MOBILE_CONFIG = BASE_URL + "api/view_mobile_configuration/" + APP_ID + "/null/";
    public static final String VIEW_ATTENDANCE_RECORD = BASE_URL + "api/view_booking_record/" + Constants.APP_ID;
    public static final String CANCEL_BOOK = BASE_URL + "api/cancle_booking";
    public static final String CANCEL_BOOK1 = BASE_URL + "api/cancle_booking1";
    public static final String POST_BOOKING = BASE_URL + "api/insert_user_booking";
    public static final String VIEW_DATES = BASE_URL + "api/view_dates/";
    public static final String VIEW_DATE_CONFIG = BASE_URL + "api/view_date_configuration/" + Constants.APP_ID + "/null/";
    public static final String VIEW_SEVENDAY_COLL = BASE_URL + "api/view_collection/";
    public static final String VIEW_TOTAL_COLL = BASE_URL + "api/view_total_collection/";
    public static final String VIEW_CONTRACTOR_DETAIL = BASE_URL + "api/viewparkinguserstatus/" + APP_ID + "/1/" + "contractor/";
    public static final String POST_EDIT_DATA = BASE_URL + "api/edit_parking_user";
    public static final String DELETE_PARK = BASE_URL + "api/delete_parking";
    public static final String DELETE_ATTENDANCE = BASE_URL + "api/delete_attendant";
    public static final String VIEW_CHARGES = BASE_URL + "api/view_mobile_configuration/" + APP_ID + "/null/";
    public static final String INSERT_BOOKING = BASE_URL + "api/insert_booking";
    public static final String DELETE_D_CONFIG = BASE_URL + "api/delete_configuration";
    public static final String AVAILABLE_PARKING = BASE_URL + "api/view_available_parking/";
    public static final String INSERT_VACANT = BASE_URL + "api/insert_vacant";
    public static final String GEN_CHECKSUM = BASE_URL + "generate_checksum";
    public static final String DELETE_USER = BASE_URL + "api/delete_contact";
    public static String VTYPE_NAME = "";
    public static int PARKG_ID = 0;
    public static int SELCT_PARKG_ID = 0;
    public static String SELECT_PARKG_NAME = "";
    public static String PARKG_NAME = "parkname";
    public static int BOOK_ID = 0;
    public static int CONFIGURATION_ID = 0;

    //Paytm Constant
    public static int Cust_CONFIGURATION_ID = 0;
    public static int COLL_TOTAL = 0;


    //Paytm Wallet API
//    public static final String PAYTM_SENDOTP = "https://accounts-uat.paytm.com/signin/otp";
//    public static final String PAYTM_VERIFYOTP = "https://accounts-uat.paytm.com/signin/validate/otp";
//    public static final String PAYTM_MINBALANCE = "http://13.232.8.225/api/generateautodebit";
//    public static final String PAYTM_ADDWALLET = "http://13.232.8.225/api/generateaddwalletchecksum";
//    public static final String PAYTM_WITHDRAW = "http://13.232.8.225/api/generatewithdrawwallet";
//    public static final String PAYTM_TRANSACTIONSUCCESS = "http://13.232.8.225/api/transactionstatus";
//    public static final String PAYTM_TMEXPIRE = "https://accounts-uat.paytm.com/user/details";


    public static int CANCELSTATUS = 0;
    public static String VEHICLE_TYPE = "";
    public static String GCM_ID;
    public static boolean Cancelstatus = false;
    public static int FIRSTTIME = 0;
    public static String STATE = "";
    public static String TOKEN = "";


    /* public static final String PAYTM_SENDOTP = " https://accounts.paytm.com/signin/otp";
     public static final String PAYTM_VERIFYOTP = "https://accounts.paytm.com/signin/validate/otp";
     public static final String PAYTM_MINBALANCE = "http://13.232.8.225/api/generateautodebitlive";
     public static final String PAYTM_ADDWALLET = "http://13.232.8.225/api/generateaddwalletchecksumlive";
     public static final String PAYTM_WITHDRAW = "http://13.232.8.225/api/generatewithdrawwalletlive";
     public static final String PAYTM_TRANSACTIONSUCCESS = "http://13.232.8.225/api/transactionstatuslive";
     public static final String PAYTM_TMEXPIRE = "https://accounts.paytm.com/user/details";
 */
    public static final String PAYTM_SENDOTP = " https://accounts.paytm.com/signin/otp";
    public static final String PAYTM_VERIFYOTP = "https://accounts.paytm.com/signin/validate/otp";
    public static final String PAYTM_MINBALANCE = "http://13.233.234.166:6001/api/generateautodebitlive";
    public static final String PAYTM_ADDWALLET = "http://13.233.234.166:6001/api/generateaddwalletchecksumlive";
    public static final String PAYTM_WITHDRAW = "http://13.233.234.166:6001/api/generatewithdrawwalletlive";
    public static final String PAYTM_TRANSACTIONSUCCESS = "http://13.232.8.225/api/transactionstatuslive";
    public static final String PAYTM_TMEXPIRE = "https://accounts.paytm.com/user/details";
}
