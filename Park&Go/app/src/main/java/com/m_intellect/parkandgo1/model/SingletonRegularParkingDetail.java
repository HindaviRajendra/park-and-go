package com.m_intellect.parkandgo1.model;

import java.util.List;

/**
 * Created by sanket on 1/6/2018.
 */

public class SingletonRegularParkingDetail {

    private static SingletonRegularParkingDetail singletonRegularParkingDetail;

    private List<String> placeNameList;
    private String parkingType;
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private String placeName;

    private SingletonRegularParkingDetail() {

    }

    public static SingletonRegularParkingDetail getInstance() {
        if (singletonRegularParkingDetail == null) {
            singletonRegularParkingDetail = new SingletonRegularParkingDetail();
        }
        return singletonRegularParkingDetail;
    }

    public List<String> getPlaceNameList() {
        return placeNameList;
    }

    public void setPlaceNameList(List<String> placeNameList) {
        this.placeNameList = placeNameList;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
}
