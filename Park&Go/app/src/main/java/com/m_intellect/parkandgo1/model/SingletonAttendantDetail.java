package com.m_intellect.parkandgo1.model;

/**
 * Created by sanket on 1/21/2018.
 */

public class SingletonAttendantDetail {

    private static SingletonAttendantDetail singletonAttendantDetail;
    private ModelAttendant.DataBean modelAttendant;

    private SingletonAttendantDetail() {

    }

    public static SingletonAttendantDetail getInstance() {
        if (singletonAttendantDetail == null) {
            singletonAttendantDetail = new SingletonAttendantDetail();
        }
        return singletonAttendantDetail;
    }

    public ModelAttendant.DataBean getModelAttendant() {
        return modelAttendant;
    }

    public void setModelAttendant(ModelAttendant.DataBean modelAttendant) {
        this.modelAttendant = modelAttendant;
    }
}
