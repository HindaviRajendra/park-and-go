package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

public class GenerateChecksumReq {

    /**
     * order_id : ORD_396547
     * cust_id : CUST1234
     * tax_amount : 10.0
     */

    @SerializedName("order_id")
    private String orderId;
    @SerializedName("cust_id")
    private String custId;
    @SerializedName("tax_amount")
    private String taxAmount;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }
}
