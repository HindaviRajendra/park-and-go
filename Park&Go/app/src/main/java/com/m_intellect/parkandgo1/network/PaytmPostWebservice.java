package com.m_intellect.parkandgo1.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PaytmPostWebservice extends AsyncTask<Object, Object, String> {

    private ProgressDialog progressDialog;
    private Context context;
    private Handler handler;
    private String mUrl;
    private String payload;

    public PaytmPostWebservice(Context mContext, Handler mHandler, String mUrl, String mPayload) {

        this.context = mContext;
        this.handler = mHandler;
        this.mUrl = mUrl;
        this.payload = mPayload;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Log.d("test", "URL: " + mUrl);
    }


    @Override
    protected String doInBackground(Object... params) {

        try {
            URL url = new URL(mUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//            httpURLConnection.setRequestProperty ("authorization", "basic bWVyY2hhbnQtYWJyYWxpbi1zdGFnZ2luZzp3VTQyQ2tYckxFUHRsV09TM09IOTFnMkpsVFpRVW5wMw==");
            httpURLConnection.setRequestProperty ("authorization", "basic bWVyY2hhbnQtYWJyYWxpbjpMcTNSQklxbklkM05ZMkdBYlk5THpod1RKY1B4WW1nZQ==");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setConnectTimeout(60000);
            httpURLConnection.setDoInput(true);

            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.writeBytes(payload);
            dataOutputStream.flush();
            dataOutputStream.close();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine).append("\n");
            }
            bufferedReader.close();
            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        progressDialog.dismiss();

        Message message = new Message();
        message.obj = result;
        handler.handleMessage(message);
    }
}
