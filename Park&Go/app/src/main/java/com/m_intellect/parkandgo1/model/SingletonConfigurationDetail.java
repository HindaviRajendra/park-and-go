package com.m_intellect.parkandgo1.model;

/**
 * Created by sanket on 1/15/2018.
 */

public class SingletonConfigurationDetail {

    private static SingletonConfigurationDetail singletonConfigurationDetail;
    private ModelConfiguration modelConfiguration;

    private SingletonConfigurationDetail() {

    }

    public static SingletonConfigurationDetail getInstance() {
        if (singletonConfigurationDetail == null) {
            singletonConfigurationDetail = new SingletonConfigurationDetail();
        }
        return singletonConfigurationDetail;
    }

    public ModelConfiguration getModelConfiguration() {
        return modelConfiguration;
    }

    public void setModelConfiguration(ModelConfiguration modelConfiguration) {
        this.modelConfiguration = modelConfiguration;
    }
}
