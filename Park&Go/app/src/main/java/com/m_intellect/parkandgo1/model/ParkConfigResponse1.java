package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 07-03-2018.
 */

public class ParkConfigResponse1 {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_configuration_id":458,"app_id":29,"parking_id":184,"user_id":294,"total":"268.0","general":[{"bike":50,"bus":50,"car":50,"truck":50}],"reserved":[{"bike":2,"bus":5,"car":1,"truck":5}],"regular":[{"bike":2,"bus":5,"car":1,"truck":5}],"event":[{"bike":5,"bus":5,"car":11,"truck":5}],"premium":[{"bike":5,"bus":5,"car":1,"truck":5}],"calender":"2018-05-19T00:00:00.000Z","start_date":null,"end_date":null,"car_price_values":[{"from":"0","parice_r":"20","price_g":"10","price_p":"20","to":"1"},{"from":"1","parice_r":"20","price_g":"20","price_p":"30","to":"2"},{"from":"2","parice_r":"30","price_g":"30","price_p":"30","to":"3"},{"from":"3","parice_r":"50","price_g":"50","price_p":"50","to":"12"}],"bike_price_values":[{"from":"0","parice_r":"50","price_g":"50","price_p":"50","to":"1"},{"from":"1","parice_r":"60","price_g":"60","price_p":"60","to":"2"},{"from":"2","parice_r":"70","price_g":"70","price_p":"70","to":"3"},{"from":"3","parice_r":"80","price_g":"80","price_p":"80","to":"12"}],"bus_price_values":[{"from":"3","parice_r":"","price_g":"","price_p":"","to":"12"},{"from":"2","parice_r":"","price_g":"","price_p":"","to":"3"},{"from":"1","parice_r":"","price_g":"","price_p":"","to":"2"},{"from":"0","parice_r":"","price_g":"","price_p":"","to":"1"}],"truck_price_values":[{"from":"3","parice_r":"50","price_g":"50","price_p":"","to":"12"},{"from":"2","parice_r":"30","price_g":"30","price_p":"","to":"3"},{"from":"1","parice_r":"20","price_g":"20","price_p":"","to":"2"},{"from":"0","parice_r":"50","price_g":"50","price_p":"","to":"1"}],"regular_parking":{"day":{"from":"","price":"","to":""},"night":{"from":"","price":"","to":""}},"event_parking":{"event1":{"from":"","price":"","to":""},"event2":{"from":"","price":"","to":""},"event3":{"from":"","price":"","to":""}},"place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","place_name":"inocx","total_space":268}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_configuration_id : 458
         * app_id : 29
         * parking_id : 184
         * user_id : 294
         * total : 268.0
         * general : [{"bike":50,"bus":50,"car":50,"truck":50}]
         * reserved : [{"bike":2,"bus":5,"car":1,"truck":5}]
         * regular : [{"bike":2,"bus":5,"car":1,"truck":5}]
         * event : [{"bike":5,"bus":5,"car":11,"truck":5}]
         * premium : [{"bike":5,"bus":5,"car":1,"truck":5}]
         * calender : 2018-05-19T00:00:00.000Z
         * start_date : null
         * end_date : null
         * car_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"20","to":"1"},{"from":"1","parice_r":"20","price_g":"20","price_p":"30","to":"2"},{"from":"2","parice_r":"30","price_g":"30","price_p":"30","to":"3"},{"from":"3","parice_r":"50","price_g":"50","price_p":"50","to":"12"}]
         * bike_price_values : [{"from":"0","parice_r":"50","price_g":"50","price_p":"50","to":"1"},{"from":"1","parice_r":"60","price_g":"60","price_p":"60","to":"2"},{"from":"2","parice_r":"70","price_g":"70","price_p":"70","to":"3"},{"from":"3","parice_r":"80","price_g":"80","price_p":"80","to":"12"}]
         * bus_price_values : [{"from":"3","parice_r":"","price_g":"","price_p":"","to":"12"},{"from":"2","parice_r":"","price_g":"","price_p":"","to":"3"},{"from":"1","parice_r":"","price_g":"","price_p":"","to":"2"},{"from":"0","parice_r":"","price_g":"","price_p":"","to":"1"}]
         * truck_price_values : [{"from":"3","parice_r":"50","price_g":"50","price_p":"","to":"12"},{"from":"2","parice_r":"30","price_g":"30","price_p":"","to":"3"},{"from":"1","parice_r":"20","price_g":"20","price_p":"","to":"2"},{"from":"0","parice_r":"50","price_g":"50","price_p":"","to":"1"}]
         * regular_parking : {"day":{"from":"","price":"","to":""},"night":{"from":"","price":"","to":""}}
         * event_parking : {"event1":{"from":"","price":"","to":""},"event2":{"from":"","price":"","to":""},"event3":{"from":"","price":"","to":""}}
         * place_landmark : A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India
         * place_name : inocx
         * total_space : 268
         */

        @SerializedName("parking_configuration_id")
        private int parkingConfigurationId;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("total")
        private String total;
        @SerializedName("calender")
        private String calender;
        @SerializedName("start_date")
        private Object startDate;
        @SerializedName("end_date")
        private Object endDate;
        @SerializedName("regular_parking")
        private RegularParkingBean regularParking;
        @SerializedName("event_parking")
        private EventParkingBean eventParking;
        @SerializedName("place_landmark")
        private String placeLandmark;
        @SerializedName("place_name")
        private String placeName;
        @SerializedName("total_space")
        private int totalSpace;
        @SerializedName("general")
        private List<GeneralBean> general;
        @SerializedName("reserved")
        private List<ReservedBean> reserved;
        @SerializedName("regular")
        private List<RegularBean> regular;
        @SerializedName("event")
        private List<EventBean> event;
        @SerializedName("premium")
        private List<PremiumBean> premium;
        @SerializedName("car_price_values")
        private List<CarPriceValuesBean> carPriceValues;
        @SerializedName("bike_price_values")
        private List<BikePriceValuesBean> bikePriceValues;
        @SerializedName("bus_price_values")
        private List<BusPriceValuesBean> busPriceValues;
        @SerializedName("truck_price_values")
        private List<TruckPriceValuesBean> truckPriceValues;

        public int getParkingConfigurationId() {
            return parkingConfigurationId;
        }

        public void setParkingConfigurationId(int parkingConfigurationId) {
            this.parkingConfigurationId = parkingConfigurationId;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getCalender() {
            return calender;
        }

        public void setCalender(String calender) {
            this.calender = calender;
        }

        public Object getStartDate() {
            return startDate;
        }

        public void setStartDate(Object startDate) {
            this.startDate = startDate;
        }

        public Object getEndDate() {
            return endDate;
        }

        public void setEndDate(Object endDate) {
            this.endDate = endDate;
        }

        public RegularParkingBean getRegularParking() {
            return regularParking;
        }

        public void setRegularParking(RegularParkingBean regularParking) {
            this.regularParking = regularParking;
        }

        public EventParkingBean getEventParking() {
            return eventParking;
        }

        public void setEventParking(EventParkingBean eventParking) {
            this.eventParking = eventParking;
        }

        public String getPlaceLandmark() {
            return placeLandmark;
        }

        public void setPlaceLandmark(String placeLandmark) {
            this.placeLandmark = placeLandmark;
        }

        public String getPlaceName() {
            return placeName;
        }

        public void setPlaceName(String placeName) {
            this.placeName = placeName;
        }

        public int getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(int totalSpace) {
            this.totalSpace = totalSpace;
        }

        public List<GeneralBean> getGeneral() {
            return general;
        }

        public void setGeneral(List<GeneralBean> general) {
            this.general = general;
        }

        public List<ReservedBean> getReserved() {
            return reserved;
        }

        public void setReserved(List<ReservedBean> reserved) {
            this.reserved = reserved;
        }

        public List<RegularBean> getRegular() {
            return regular;
        }

        public void setRegular(List<RegularBean> regular) {
            this.regular = regular;
        }

        public List<EventBean> getEvent() {
            return event;
        }

        public void setEvent(List<EventBean> event) {
            this.event = event;
        }

        public List<PremiumBean> getPremium() {
            return premium;
        }

        public void setPremium(List<PremiumBean> premium) {
            this.premium = premium;
        }

        public List<CarPriceValuesBean> getCarPriceValues() {
            return carPriceValues;
        }

        public void setCarPriceValues(List<CarPriceValuesBean> carPriceValues) {
            this.carPriceValues = carPriceValues;
        }

        public List<BikePriceValuesBean> getBikePriceValues() {
            return bikePriceValues;
        }

        public void setBikePriceValues(List<BikePriceValuesBean> bikePriceValues) {
            this.bikePriceValues = bikePriceValues;
        }

        public List<BusPriceValuesBean> getBusPriceValues() {
            return busPriceValues;
        }

        public void setBusPriceValues(List<BusPriceValuesBean> busPriceValues) {
            this.busPriceValues = busPriceValues;
        }

        public List<TruckPriceValuesBean> getTruckPriceValues() {
            return truckPriceValues;
        }

        public void setTruckPriceValues(List<TruckPriceValuesBean> truckPriceValues) {
            this.truckPriceValues = truckPriceValues;
        }

        public static class RegularParkingBean {
            /**
             * day : {"from":"","price":"","to":""}
             * night : {"from":"","price":"","to":""}
             */

            @SerializedName("day")
            private DayBean day;
            @SerializedName("night")
            private NightBean night;

            public DayBean getDay() {
                return day;
            }

            public void setDay(DayBean day) {
                this.day = day;
            }

            public NightBean getNight() {
                return night;
            }

            public void setNight(NightBean night) {
                this.night = night;
            }

            public static class DayBean {
                /**
                 * from :
                 * price :
                 * to :
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class NightBean {
                /**
                 * from :
                 * price :
                 * to :
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }
        }

        public static class EventParkingBean {
            /**
             * event1 : {"from":"","price":"","to":""}
             * event2 : {"from":"","price":"","to":""}
             * event3 : {"from":"","price":"","to":""}
             */

            @SerializedName("event1")
            private Event1Bean event1;
            @SerializedName("event2")
            private Event2Bean event2;
            @SerializedName("event3")
            private Event3Bean event3;

            public Event1Bean getEvent1() {
                return event1;
            }

            public void setEvent1(Event1Bean event1) {
                this.event1 = event1;
            }

            public Event2Bean getEvent2() {
                return event2;
            }

            public void setEvent2(Event2Bean event2) {
                this.event2 = event2;
            }

            public Event3Bean getEvent3() {
                return event3;
            }

            public void setEvent3(Event3Bean event3) {
                this.event3 = event3;
            }

            public static class Event1Bean {
                /**
                 * from :
                 * price :
                 * to :
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class Event2Bean {
                /**
                 * from :
                 * price :
                 * to :
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class Event3Bean {
                /**
                 * from :
                 * price :
                 * to :
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }
        }

        public static class GeneralBean {
            /**
             * bike : 50
             * bus : 50
             * car : 50
             * truck : 50
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class ReservedBean {
            /**
             * bike : 2
             * bus : 5
             * car : 1
             * truck : 5
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class RegularBean {
            /**
             * bike : 2
             * bus : 5
             * car : 1
             * truck : 5
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class EventBean {
            /**
             * bike : 5
             * bus : 5
             * car : 11
             * truck : 5
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class PremiumBean {
            /**
             * bike : 5
             * bus : 5
             * car : 1
             * truck : 5
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class CarPriceValuesBean {
            /**
             * from : 0
             * parice_r : 20
             * price_g : 10
             * price_p : 20
             * to : 1
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class BikePriceValuesBean {
            /**
             * from : 0
             * parice_r : 50
             * price_g : 50
             * price_p : 50
             * to : 1
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class BusPriceValuesBean {
            /**
             * from : 3
             * parice_r :
             * price_g :
             * price_p :
             * to : 12
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class TruckPriceValuesBean {
            /**
             * from : 3
             * parice_r : 50
             * price_g : 50
             * price_p :
             * to : 12
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }
    }
}
