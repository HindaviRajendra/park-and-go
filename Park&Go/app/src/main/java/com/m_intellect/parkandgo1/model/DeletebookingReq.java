package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class DeletebookingReq {

    /**
     * booking_id : 5569
     */

    @SerializedName("booking_id")
    private int bookingId;

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }
}
