package com.m_intellect.parkandgo1.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.m_intellect.parkandgo1.R;

public class BulkFragment extends android.app.Fragment {
    BulkFragment bulkFragment;

    public BulkFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.regular_layout, container, false);
       // if (bulkFragment.isVisible()) {
           // Toast.makeText(getActivity(), "This Parking Will Coming Soon", Toast.LENGTH_SHORT).show();
      //  }
        return view;
    }
    @Override
    public void setMenuVisibility(final boolean visible)
    {
        super.setMenuVisibility(visible);
        if (visible && isResumed())
        {
//            Toast.makeText(getActivity(),"The Parking is Coming Soon",Toast.LENGTH_SHORT).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("The Parking is Coming Soon");
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

}
