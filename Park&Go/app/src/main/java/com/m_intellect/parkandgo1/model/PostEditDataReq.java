package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

public class PostEditDataReq {


    /**
     * id : 26
     * app_id : 29
     * password : 123456
     * name : kalpesh
     * email : kalpesh@gmail.com
     * mobile_no : 9898989898
     * city : mumbai
     * state : maharashtra
     */

    @SerializedName("id")
    private int id;
    @SerializedName("app_id")
    private int appId;
    @SerializedName("password")
    private String password;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    private String state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
