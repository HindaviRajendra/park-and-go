package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12-03-2018.
 */

public class RecordData {
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("leave_time")
    @Expose
    private String leaveTime;
    @SerializedName("advance_paid")
    @Expose
    private String advancePaid;
    @SerializedName("balance_cash")
    @Expose
    private String balanceCash;
    @SerializedName("booking_charges")
    @Expose
    private Integer bookingCharges;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getBalanceCash() {
        return balanceCash;
    }

    public void setBalanceCash(String balanceCash) {
        this.balanceCash = balanceCash;
    }

    public Integer getBookingCharges() {
        return bookingCharges;
    }

    public void setBookingCharges(Integer bookingCharges) {
        this.bookingCharges = bookingCharges;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
