package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewversionResponse {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"app_id":29,"app_version":"1.0.3"}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * app_id : 29
         * app_version : 1.0.3
         */

        @SerializedName("app_id")
        private int appId;
        @SerializedName("app_version")
        private String appVersion;

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }
    }
}
