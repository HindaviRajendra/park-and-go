package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeleteConfigRes {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : []
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<?> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<?> getData() {
        return Data;
    }

    public void setData(List<?> Data) {
        this.Data = Data;
    }
}
