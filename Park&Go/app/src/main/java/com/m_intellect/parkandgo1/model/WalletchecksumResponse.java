package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class WalletchecksumResponse {


    /**
     * result : {"MID":"ABRALI53876941570316","ORDER_ID":"12345","CUST_ID":"876","INDUSTRY_TYPE_ID":"Retail","CHANNEL_ID":"WAP","TXN_AMOUNT":"5","WEBSITE":"APPSTAGING","CALLBACK_URL":"http://595ed50d.ngrok.io/paytm/addMoneyResp","SSO_TOKEN":"afa2beb7-321e-441d-ac55-43e5b0862300","CHECKSUMHASH":"YO/RkYUq9IUq9wUPKNJv8qiBL3d2jgnaky0mosBxSxVpsjLngwu5EKUp8Vjg1almN880AjDUM4NHYJcjRRF2kNAxtaExsaCNatiFBkoL9zg="}
     */

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * MID : ABRALI53876941570316
         * ORDER_ID : 12345
         * CUST_ID : 876
         * INDUSTRY_TYPE_ID : Retail
         * CHANNEL_ID : WAP
         * TXN_AMOUNT : 5
         * WEBSITE : APPSTAGING
         * CALLBACK_URL : http://595ed50d.ngrok.io/paytm/addMoneyResp
         * SSO_TOKEN : afa2beb7-321e-441d-ac55-43e5b0862300
         * CHECKSUMHASH : YO/RkYUq9IUq9wUPKNJv8qiBL3d2jgnaky0mosBxSxVpsjLngwu5EKUp8Vjg1almN880AjDUM4NHYJcjRRF2kNAxtaExsaCNatiFBkoL9zg=
         */

        @SerializedName("MID")
        private String MID;
        @SerializedName("ORDER_ID")
        private String ORDERID;
        @SerializedName("CUST_ID")
        private String CUSTID;
        @SerializedName("INDUSTRY_TYPE_ID")
        private String INDUSTRYTYPEID;
        @SerializedName("CHANNEL_ID")
        private String CHANNELID;
        @SerializedName("TXN_AMOUNT")
        private String TXNAMOUNT;
        @SerializedName("WEBSITE")
        private String WEBSITE;
        @SerializedName("CALLBACK_URL")
        private String CALLBACKURL;
        @SerializedName("SSO_TOKEN")
        private String SSOTOKEN;
        @SerializedName("CHECKSUMHASH")
        private String CHECKSUMHASH;

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getORDERID() {
            return ORDERID;
        }

        public void setORDERID(String ORDERID) {
            this.ORDERID = ORDERID;
        }

        public String getCUSTID() {
            return CUSTID;
        }

        public void setCUSTID(String CUSTID) {
            this.CUSTID = CUSTID;
        }

        public String getINDUSTRYTYPEID() {
            return INDUSTRYTYPEID;
        }

        public void setINDUSTRYTYPEID(String INDUSTRYTYPEID) {
            this.INDUSTRYTYPEID = INDUSTRYTYPEID;
        }

        public String getCHANNELID() {
            return CHANNELID;
        }

        public void setCHANNELID(String CHANNELID) {
            this.CHANNELID = CHANNELID;
        }

        public String getTXNAMOUNT() {
            return TXNAMOUNT;
        }

        public void setTXNAMOUNT(String TXNAMOUNT) {
            this.TXNAMOUNT = TXNAMOUNT;
        }

        public String getWEBSITE() {
            return WEBSITE;
        }

        public void setWEBSITE(String WEBSITE) {
            this.WEBSITE = WEBSITE;
        }

        public String getCALLBACKURL() {
            return CALLBACKURL;
        }

        public void setCALLBACKURL(String CALLBACKURL) {
            this.CALLBACKURL = CALLBACKURL;
        }

        public String getSSOTOKEN() {
            return SSOTOKEN;
        }

        public void setSSOTOKEN(String SSOTOKEN) {
            this.SSOTOKEN = SSOTOKEN;
        }

        public String getCHECKSUMHASH() {
            return CHECKSUMHASH;
        }

        public void setCHECKSUMHASH(String CHECKSUMHASH) {
            this.CHECKSUMHASH = CHECKSUMHASH;
        }
    }
}
