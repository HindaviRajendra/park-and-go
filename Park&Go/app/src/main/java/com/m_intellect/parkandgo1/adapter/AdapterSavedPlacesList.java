package com.m_intellect.parkandgo1.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;

/**
 * Created by sanket on 12/7/2017.
 */

public class AdapterSavedPlacesList extends RecyclerView.Adapter<AdapterSavedPlacesList.ViewHolder> {

    private Context context;

    public AdapterSavedPlacesList(Context context) {

        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_saved_place, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (position == 0) {
            holder.titleTV.setText("Save visit Places");
            holder.starIV.setImageResource(R.drawable.ic_next_22dp);
            holder.addressTV.setVisibility(View.GONE);

            holder.titleTV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                   // context.startActivity(new Intent(context, ActivitySaveVisitPlaces.class));
                }
            });
        } else if (position == 1) {
            holder.titleTV.setText("Set Location on Map");
            holder.starIV.setImageResource(R.drawable.ic_pin_location_22dp);
            holder.addressTV.setVisibility(View.GONE);
            holder.titleTV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    ((Activity)context).finish();
                    ((Activity)context).setResult(111);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTV;
        private TextView addressTV;
        private ImageView starIV;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            starIV = (ImageView) itemView.findViewById(R.id.starIV);
        }
    }
}
