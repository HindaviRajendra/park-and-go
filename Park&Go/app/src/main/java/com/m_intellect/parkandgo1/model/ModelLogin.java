
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLogin {

    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("fcm_type")
    @Expose
    private String fcm_type;
    @SerializedName("fcm_id")
    @Expose
    private String fcm_id;

    public String getFcm_id() {
        return fcm_id;
    }

    public ModelLogin setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
        return this;
    }

    public String getFcm_type() {
        return fcm_type;
    }

    public ModelLogin setFcm_type(String fcm_type) {
        this.fcm_type = fcm_type;
        return this;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
