package com.m_intellect.parkandgo1.model;

/**
 * Created by sanket on 1/6/2018.
 */

public class SingletonParkingDetail {

    private static SingletonParkingDetail singletonParkingDetail;
    private ModelParkingDetailResponse modelParkingDetailResponse;

    private SingletonParkingDetail() {

    }

    public static SingletonParkingDetail getInstance() {
        if (singletonParkingDetail == null) {
            singletonParkingDetail = new SingletonParkingDetail();
        }
        return singletonParkingDetail;
    }

    public ModelParkingDetailResponse getModelParkingDetailResponse() {
        return modelParkingDetailResponse;
    }

    public void setModelParkingDetailResponse(ModelParkingDetailResponse modelParkingDetailResponse) {
        this.modelParkingDetailResponse = modelParkingDetailResponse;
    }
}
