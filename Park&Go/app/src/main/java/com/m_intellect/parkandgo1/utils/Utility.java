package com.m_intellect.parkandgo1.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.network.WebServicePost;

import java.text.SimpleDateFormat;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

/**
 * Created by sanket on 6/3/2017.
 */
public class Utility {
    public static Context mcontext;

    public static boolean isNetworkAvailable(Context context) {
        int[] networkTypes = {ConnectivityManager.TYPE_MOBILE,
                ConnectivityManager.TYPE_WIFI};
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null &&
                        activeNetworkInfo.getType() == networkType) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            return false;
        }
        return false;
    }


    public static String formatDateFromOnetoAnother(String date, String givenFormat, String resultFormat) {

        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;

        try {
            sdf = new SimpleDateFormat(givenFormat);
            sdf1 = new SimpleDateFormat(resultFormat);
            result = sdf1.format(sdf.parse(date));
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {

        Location loc1 = new Location("");

        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);

        double distanceInMeters = loc1.distanceTo(loc2) / 1000;
        return distanceInMeters;
    }

    public static Typeface getTypeFace(Context context) {

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "font/gotham_medium.ttf");
        return custom_font;
    }

    public static Typeface getTypeFaceThin(Context context) {

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "font/gotham_book.ttf");
        return custom_font;
    }

    public static void ShowDialog(String stringtype, final Context context, final String type) {
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(dialogView);
        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        if (stringtype.equals("success")) {
            txt_text.setText("Success");
            txt_header.setText("You have Booked your Vehicle successfully");
            img.setBackground(context.getResources().getDrawable(R.drawable.ic_success));
            btnok.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else {
            txt_text.setText("Cancel Booking");
            txt_header.setText("Due to insufficient balance on your paytm wallet. We are not proceeding your booking.Please add money to your paytm wallet ");
            img.setBackground(context.getResources().getDrawable(R.mipmap.cancel));
            btnok.setBackgroundColor(context.getResources().getColor(R.color.red));
        }

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, ActivityMain.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                if (type.equals("Reserved")) {
//                    intent.putExtra("general1", "Reserved");
//                } else
//                    intent.putExtra("general1", "General");
//                context.startActivity(intent);
                ((AppCompatActivity) context).finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void cancelBooking(Context context) {
        mcontext = context;
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(context, new Handlecancelbook(), Constants.CANCEL_BOOK1, payload).execute();
        Log.e("TAG", "CancelURL>>>" + payload);
        Log.e("TAG", "CancelURL>>>" + Constants.CANCEL_BOOK1);
    }

    public static class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    Utility.ShowDialog("cancel", mcontext, "Reserved");
                }
            }
        }
    }


}
