package com.m_intellect.parkandgo1.utils;

import android.util.Log;

import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarCellView;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Divyesh on 24-04-2018.
 */

public class EventDecorator implements CalendarCellDecorator {
    private Date evd;
    private int BackgroundColor;
    public EventDecorator (Date evd, int BackgroundColor) {
        this.evd = evd;
        this.BackgroundColor = BackgroundColor;
    }

    @Override
    public void decorate(CalendarCellView calendarCellView, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(evd);
        //cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 00);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        evd = cal.getTime();
        Log.d("Test","date:"+date);
        Log.d("Test","evd:"+evd);
        if (date.equals(evd) && calendarCellView.isSelectable()) {
            calendarCellView.setBackgroundColor(BackgroundColor);
        }

    }
}
