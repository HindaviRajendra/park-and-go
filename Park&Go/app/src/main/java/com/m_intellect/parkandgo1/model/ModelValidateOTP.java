
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelValidateOTP {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("mail_noti")
    @Expose
    private Integer mailNoti;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getMailNoti() {
        return mailNoti;
    }

    public void setMailNoti(Integer mailNoti) {
        this.mailNoti = mailNoti;
    }

}
