package com.m_intellect.parkandgo1.adapter;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.RecordData;

import java.util.List;

/**
 * Created by Admin on 12-03-2018.
 */

public class RecordsAdapter  extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {

    private Context context;
    private List<RecordData> recordDataList;
    private String TAG = getClass().getSimpleName();
    public int from;

    public RecordsAdapter(Context mContext, List<RecordData> recordDataList) {
        this.context = mContext;
        this.recordDataList = recordDataList;
        Log.e("test", "recordDataList: " + recordDataList.size());
    }

//    public RecordsAdapter(Context mContext, int fromFragment) {
//        this.context = mContext;
//        this.from = fromFragment;
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.records_list_items, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

//        holder.vehicleText.setTypeface(Utility.getTypeFaceThin(context));
//        holder.inText.setTypeface(Utility.getTypeFaceThin(context));
//        holder.outText.setTypeface(Utility.getTypeFaceThin(context));
//        holder.amountText.setTypeface(Utility.getTypeFaceThin(context));
        final RecordData var=recordDataList.get(position);

        try {

            if (!var.getVehicleNo().isEmpty()) {
                int lenght = recordDataList.get(position).getVehicleNo().length();
                holder.vehicleText.setText(var.getVehicleNo());
                holder.vehicleText.setText(recordDataList.get(position).getVehicleNo().substring(lenght - 7, lenght));
            } else
                holder.vehicleText.setText("---");

            if (recordDataList.get(position).getVehicleType() != null) {
                if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Car")) {
                    Drawable img = context.getResources().getDrawable(R.drawable.car);
                    img.setBounds(0, 0, 60, 60);
                    holder.vehicleText.setCompoundDrawables(img, null, null, null);
                } else if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Bike")) {
                    Drawable img = context.getResources().getDrawable(R.drawable.bike);
                    img.setBounds(0, 0, 60, 60);
                    holder.vehicleText.setCompoundDrawables(img, null, null, null);
                } else {
                    Drawable img = context.getResources().getDrawable(R.mipmap.ic_launcher);
                    img.setBounds(0, 0, 60, 60);
                    holder.vehicleText.setCompoundDrawables(img, null, null, null);
                }
            } else {
                Drawable img = context.getResources().getDrawable(R.mipmap.ic_launcher);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            }

            if (recordDataList.get(position).getBookingTime() != null)
                holder.inText.setText(recordDataList.get(position).getBookingTime().substring(0, recordDataList.get(position).getBookingTime().lastIndexOf(":")));
            else
                holder.inText.setText("---");

            if (recordDataList.get(position).getLeaveTime() != null) {
                if (recordDataList.get(position).getLeaveTime().equalsIgnoreCase("00:00:00")) {
                    holder.outText.setText("---");
                    if (recordDataList.get(position).getAdvancePaid() != null && !recordDataList.get(position).getAdvancePaid().equalsIgnoreCase("undefined") && !recordDataList.get(position).getAdvancePaid().isEmpty()) {

                        if (Integer.parseInt(recordDataList.get(position).getAdvancePaid()) != 0)
                            holder.amountText.setText(context.getString(R.string.Rs) + " " + recordDataList.get(position).getAdvancePaid() + "(A)");
                        else
                            holder.amountText.setText("---");
                    } else
                        holder.amountText.setText("---");
                } else {
                    holder.outText.setText(recordDataList.get(position).getLeaveTime().substring(0, recordDataList.get(position).getLeaveTime().lastIndexOf(":")));
                    holder.amountText.setText(context.getString(R.string.Rs) + " " + recordDataList.get(position).getBookingCharges());
                }
            } else {
                holder.outText.setText("---");
                holder.amountText.setText("---");
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return recordDataList.size();
    }

//    public void updateList(List<DataLeadsList> temp) {
//
//        leadsLists = temp;
//        notifyDataSetChanged();
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vehicleText, inText, outText, amountText, titleTV;

        public ViewHolder(View itemView) {
            super(itemView);
            vehicleText = (TextView) itemView.findViewById(R.id.vehicleText);
            inText = (TextView) itemView.findViewById(R.id.inText);
            outText = (TextView) itemView.findViewById(R.id.outText);
            amountText = (TextView) itemView.findViewById(R.id.amountText);
        }
    }
}
