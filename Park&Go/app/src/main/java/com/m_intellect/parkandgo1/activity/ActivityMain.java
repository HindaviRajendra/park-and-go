package com.m_intellect.parkandgo1.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.PlaceArrayAdapter;
import com.m_intellect.parkandgo1.dialogfragment.DialogFragmentSelectedParking;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ModelAvailbaleConfigClass;
import com.m_intellect.parkandgo1.model.ModelParkingAreaListResponse;
import com.m_intellect.parkandgo1.model.ModelParkingAreaResponse;
import com.m_intellect.parkandgo1.model.ModelViewParkingResponse;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;


public class ActivityMain extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final static int REQUEST_LOCATION = 199;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;
    public ParkConfigClass parkConfigClass;
    public String TAG = getClass().getSimpleName();
    ArrayList markerPoints = new ArrayList();
    ModelParkingAreaListResponse modelParkingAreaListResponse;
    ModelAvailbaleConfigClass modelAvailbaleConfigClass;
    boolean cancelClick = false;
    Spinner spin_vtype;
    int carvac, Bikevac, Busvac, Truckvac, configurationID, parkingID, vacant;
    View marker;
    Marker currmarker = null;
    int markerposition = 0;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationManager manager;
    private LocationRequest mLocationRequest;
    private boolean isFromOTP;
    private AutoCompleteTextView yourDestinationET;
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private Double latitude1;
    private Double longitude1;
    private SharedPreferences preferences;
    private TextView myProfileTV, tv_event, tv_money, tv_regular, tv_parkingArea;
    private TextView loginTV;
    private TextView signupTV;
    private TextView logoutTV;
    private TextView tv_p_name, tv_p_addr, tv_p_total, tv_p_vacant, tvp_cancel, walletBalanceTV, tv_vtypevac,tv_user;
    private Button btn_booknow, btn_viewdetail, btnset_location;
    private LinearLayout registerContractorLayout;
    private TextView registerContractorTV;
    private String locationName, stayhour, Arrivaltime, RSF;
    private List<ModelParkingAreaResponse.DataBean> mModelParkingAreaResponseList;
    private LatLng destinationLatLng;
    private LatLng originLatLng;
    private Polyline polyline;
    private LatLng latLng;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private AppCompatImageView searchACIV;
    private RelativeLayout destinationLayout;
    private ImageView pinIV, current_btn;
    private SupportMapFragment mapFragment;
    private String Vehicle_No, parkingType, selectVType = "", booking_type;
    private int UserID;
    private String v_type = "";
    private String outTime;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.d("test", "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            final Place place = places.get(0);
            latLng = place.getLatLng();
            Log.e("TAG", "Place Lat lang :: " + latLng);
            if (latLng != null) {
                latitude = latLng.latitude;
                longitude = latLng.longitude;
                MarkerOptions markerOptions = new MarkerOptions()
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).position(latLng);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
                Marker currmarker = mGoogleMap.addMarker(markerOptions);
                mGoogleMap.clear();
                ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
                        .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                getParkingNearToCurrentLocation();
            }
            CharSequence attributions = places.getAttributions();
        }
    };
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.d("test", "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.d("test", "Fetching details for ID: " + item.placeId);
        }
    };

    public static LatLng getLocation(double lon, double lat, int radius) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(lat);

        double foundLongitude = new_x + lon;
        double foundLatitude = y + lat;
        System.out.println("Longitude: " + foundLongitude + "  Latitude: "
                + foundLatitude);

        return new LatLng(foundLatitude, foundLongitude);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        Bundle b = getIntent().getExtras();

        if (getIntent() != null) {
            isFromOTP = getIntent().getBooleanExtra("isFromOTP", false);
            if (b != null) {
                Vehicle_No = b.getString("vehicle_no1");
                parkingType = b.getString("general1");
                selectVType = b.getString("gvtype");
                parkingID = b.getInt("parkingID");

                stayhour = b.getString("stayhour");
                Arrivaltime = b.getString("arrivaltime");
                RSF = b.getString("RSF");

                Log.e(TAG, "Stay Hour ::: " + stayhour);
                Log.e(TAG, "Arrival time  ::: " + Arrivaltime);
                Log.e(TAG, "RSF  ::: " + RSF);
                Log.e(TAG, "selectVType  ::: " + selectVType);
            }
        }

        checkLocationPermission();
        if (preferences.getBoolean(Constants.ISLOGIN, false)) {
            if (preferences.getString(Constants.USER_TYPE, "").equalsIgnoreCase("contractor")) {
                Intent intent = new Intent(ActivityMain.this, ActivityDashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                startActivity(intent);
            } else {
                setUpView();
                setSpinner();
            }
        } else {
            setUpView();
            setSpinner();
        }
    }

    private void setSpinner() {
        List<String> categories = new ArrayList<String>();
        categories.add("Car");
        categories.add("Bike");
        categories.add("Bus");
        categories.add("Truck");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ActivityMain.this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_vtype.setAdapter(dataAdapter);

        if (!selectVType.equals("")) {
            if (selectVType.equals("Car")) {
                spin_vtype.setSelection(0);
                Constants.VTYPE_NAME = "Car";
                walletBalanceTV.setText("Car");
            } else if (selectVType.equals("Bike")) {
                spin_vtype.setSelection(1);
                Constants.VTYPE_NAME = "Bike";
                walletBalanceTV.setText("Bike");
            } else if (selectVType.equals("Bus")) {
                spin_vtype.setSelection(2);
                Constants.VTYPE_NAME = "Bus";
                walletBalanceTV.setText("Bus");
            } else if (selectVType.equals("Truck")) {
                spin_vtype.setSelection(3);
                Constants.VTYPE_NAME = "Truck";
                walletBalanceTV.setText("Truck");
            }
        }


        spin_vtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                v_type = parent.getItemAtPosition(position).toString();
                Constants.VTYPE_NAME = v_type;
                Log.e(TAG, "Vehicle Type :: " + v_type);
                walletBalanceTV.setText(v_type);
                findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                getParkingNearToCurrentLocation();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void setUpView() {
        initView();
        initToolbar();
        setupDrawerContent(navigationView);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        yourDestinationET.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        yourDestinationET.setAdapter(mPlaceArrayAdapter);

        AppCompatImageView searchACIV = (AppCompatImageView) findViewById(R.id.searchACIV);
        searchACIV.setOnClickListener(this);

    }

    private void initView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        yourDestinationET = (AutoCompleteTextView) findViewById(R.id.yourDestinationET);
        searchACIV = (AppCompatImageView) findViewById(R.id.searchACIV);
        searchACIV.setOnClickListener(this);
        destinationLayout = (RelativeLayout) findViewById(R.id.destinationLayout);
        pinIV = (ImageView) findViewById(R.id.pinIV);
        current_btn = (ImageView) findViewById(R.id.pinIV1);
        tv_p_name = (TextView) findViewById(R.id.tv_parkname);
        tv_p_addr = (TextView) findViewById(R.id.tv_partaddr);
        tv_p_total = (TextView) findViewById(R.id.tv_parkTotal);
        tv_p_vacant = (TextView) findViewById(R.id.tv_parkvacant);
        tvp_cancel = (TextView) findViewById(R.id.tv_parkcancel);
        walletBalanceTV = (TextView) findViewById(R.id.walletBalanceTV);
        btn_booknow = (Button) findViewById(R.id.btn_booknow);
        btn_viewdetail = (Button) findViewById(R.id.btn_viewdetail);
        btnset_location = (Button) findViewById(R.id.btn_set);
        spin_vtype = (Spinner) findViewById(R.id.spin_vtype);
        tv_vtypevac = (TextView) findViewById(R.id.tv_parkvacantvtype);

        tvp_cancel.setOnClickListener(this);
        btn_viewdetail.setOnClickListener(this);

        Log.e(TAG, "IS FROM ---->" + isFromOTP);

        if (isFromOTP) {

            findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
            if (preferences.getBoolean(Constants.ISLOGIN, true)) {
                findViewById(R.id.OTPLayout).setVisibility(View.VISIBLE);
                findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                destinationLayout.setVisibility(View.VISIBLE);
                Log.e(TAG, "I am in IF  Condition ::: ");
            } else {
                findViewById(R.id.OTPLayout).setVisibility(View.GONE);
                findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                Log.e(TAG, "I am in ELSE  Condition ::: ");
            }

            TextView parkingTypeTV = (TextView) findViewById(R.id.parkingTypeTV);
            parkingTypeTV.setText(parkingType);

            TextView cancel = (TextView) findViewById(R.id.cancelTV);
            cancel.setVisibility(View.GONE);
            cancel.setOnClickListener(this);

            TextView V_NO = (TextView) findViewById(R.id.v_no);
            V_NO.setText(Vehicle_No);
            parkingTypeTV.setText(parkingType);
            TextView park_Name = (TextView) findViewById(R.id.parkname);
            park_Name.setText(Constants.SELECT_PARKG_NAME);
            destinationLayout.setVisibility(View.VISIBLE);

        } else {
            findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
            findViewById(R.id.OTPLayout).setVisibility(View.GONE);
            destinationLayout.setVisibility(View.VISIBLE);
        }

        findViewById(R.id.generalLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragmentSelectedParking dialogFragmentSelectedParking = new DialogFragmentSelectedParking();
                dialogFragmentSelectedParking.show(getSupportFragmentManager(), "SelectedParking");
            }
        });


        myProfileTV = (TextView) navigationView.findViewById(R.id.myProfileTV);
        tv_event = (TextView) navigationView.findViewById(R.id.tv_event);
        //  tv_money = (TextView) navigationView.findViewById(R.id.tv_money);
        tv_regular = (TextView) navigationView.findViewById(R.id.tv_regular);
        tv_user = (TextView) navigationView.findViewById(R.id.userguide);
        loginTV = (TextView) navigationView.findViewById(R.id.loginTV);
        signupTV = (TextView) navigationView.findViewById(R.id.signupTV);
        logoutTV = (TextView) navigationView.findViewById(R.id.logoutTV);
        registerContractorLayout = (LinearLayout) navigationView.findViewById(R.id.registerContractorLayout);
        registerContractorTV = (TextView) navigationView.findViewById(R.id.registerContractorTV);

        Log.e(TAG, "PREF USERID ---->" + preferences.getInt(Constants.USER_ID, 0));

        if (preferences.getBoolean(Constants.ISLOGIN, false)) {
            myProfileTV.setVisibility(View.VISIBLE);
            logoutTV.setVisibility(View.VISIBLE);
            loginTV.setVisibility(View.GONE);
            signupTV.setVisibility(View.GONE);
            registerContractorLayout.setVisibility(View.GONE);
        } else {
            myProfileTV.setVisibility(View.GONE);
            logoutTV.setVisibility(View.GONE);
            loginTV.setVisibility(View.VISIBLE);
            signupTV.setVisibility(View.VISIBLE);
            registerContractorLayout.setVisibility(View.VISIBLE);
        }

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout logoLayout = (RelativeLayout) toolbar.findViewById(R.id.logoLayout);
        logoLayout.setVisibility(View.VISIBLE);
        ImageView drawerIV = (ImageView) toolbar.findViewById(R.id.drawerIV);
        drawerIV.setOnClickListener(this);
        toolbar.findViewById(R.id.walletBalanceLayout).setOnClickListener(this);

    }

    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.findViewById(R.id.homeTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (polyline != null) {
                    polyline.remove();
                }
                if (cancelClick) {
                    findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.bottomMenuLayout).setVisibility(View.VISIBLE);
                    btnset_location.setVisibility(View.GONE);
                }

                findViewById(R.id.OTPLayout).setVisibility(View.GONE);
                destinationLayout.setVisibility(View.VISIBLE);
                drawerLayout.closeDrawers();
            }
        });

        myProfileTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityMyProfile.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

        tv_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityEvent.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });
        tv_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityUserGuide.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

//        tv_money.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ActivityMain.this, ActivityAddMoneyToWallet.class)
//                .putExtra("general1", "Home"));
//                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
//                drawerLayout.closeDrawers();
//            }
//        });

        tv_regular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityRegularParking.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

        navigationView.findViewById(R.id.parkingHistoryTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getBoolean(Constants.ISLOGIN, false)) {
                    startActivity(new Intent(ActivityMain.this, ActivityParkingHistory.class));
                    overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                } else {
                    startActivity(new Intent(ActivityMain.this, ActivityLogin.class));
                    overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                }
                drawerLayout.closeDrawers();
            }
        });

        navigationView.findViewById(R.id.aboutUsTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityAboutUs.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

        navigationView.findViewById(R.id.referAndEarnTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityReferAndEarn.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });

//        if (preferences.getInt(Constants.USER_ID, 0) == 0) {
//            loginTV.setVisibility(View.VISIBLE);
//            signupTV.setVisibility(View.VISIBLE);
//            logoutTV.setVisibility(View.GONE);
//        } else {
//            loginTV.setVisibility(View.GONE);
//            signupTV.setVisibility(View.GONE);
//            logoutTV.setVisibility(View.VISIBLE);
//        }

        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startActivityLogin = new Intent(ActivityMain.this, ActivityLogin.class);
                startActivityLogin.putExtra(Constants.LATITUDE, latitude);
                startActivityLogin.putExtra(Constants.LONGITUDE, longitude);
                startActivity(startActivityLogin);
                // startActivity(new Intent(ActivityMain.this, ActivityParkingHistory.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });
        signupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startActivitySignUp = new Intent(ActivityMain.this, ActivitySignUp.class);
                startActivitySignUp.putExtra(Constants.LATITUDE, latitude);
                startActivitySignUp.putExtra(Constants.LONGITUDE, longitude);
                startActivity(startActivitySignUp);
                //startActivity(new Intent(ActivityMain.this, ActivityParkingHistory.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                drawerLayout.closeDrawers();
            }
        });
        logoutTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.ISLOGIN, false);
                editor.putInt(Constants.USER_ID, 0);
                editor.putString("TOKEN", "");
                editor.putString("PAYTMMOB", "");
                editor.clear();
                editor.apply();
                if (polyline != null) {
                    polyline.remove();
                }
                findViewById(R.id.bottomMenuLayout).setVisibility(View.VISIBLE);
                findViewById(R.id.OTPLayout).setVisibility(View.GONE);
                destinationLayout.setVisibility(View.VISIBLE);
                Intent intent = getIntent();
                finish();
                startActivity(intent);
                drawerLayout.closeDrawers();
            }
        });

        registerContractorTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, ActivityContractorRegistration.class));
                //startActivity(new Intent(ActivityMain.this, ActivityParkingHistory.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
            }
        });

    }

    private void cancelBooking() {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(ActivityMain.this, new Handlecancelbook(), Constants.CANCEL_BOOK, payload).execute();
        Log.e(TAG, "CancelURL>>>" + payload);
        Log.e(TAG, "CancelURL>>>" + Constants.CANCEL_BOOK);
    }

    public String getCharges(String startTime, String endTime, int show) {
        int minutes = 0;
        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = simpleDateFormat.parse(startTime);
            endDate = simpleDateFormat.parse(endTime);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null;
                Date dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (show == 0) {
                String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
                String min3 = outTime.substring(3, 5);
                Log.e(TAG, "Reserved min3 :: " + min3);

                String toRemove = ":";
                if (base.contains(toRemove)) {
                    base = base.replaceAll(toRemove, "");
                }

                Log.e(TAG, "Reserved BASE :: " + Integer.parseInt(base));
                minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            } else {
                minutes = hours * 60 + min;
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            }


            try {
                if (parkingType.equals("Reserved")) {
                    if (v_type.equals("Car")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }

                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Bus")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Truck")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    }
                } else {
                    if (v_type.equals("Car")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }

                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Bus")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Truck")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    }
                }


            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "LAST CHARGE ----- >" + charges);
        return charges;
    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse, String booking_type) {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(parkingID);
        setterPayload.setParkingConfigurationId(configurationID);
        Log.e(TAG, "i am in else");
        if (booking_type.equals("Reserved")) {
            if (v_type.equals("Car")) {
                generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() + 1));
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bike")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() + 1);
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bus")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() + 1);
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);


            } else if (v_type.equals("Truck")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() + 1);
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);
            }

        } else {
            if (v_type.equals("Car")) {
                generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() + 1));
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bike")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() + 1);
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bus")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1);
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);


            } else if (v_type.equals("Truck")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1);
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);
            }

        }

        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);

        new WebServicePost(ActivityMain.this, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    public void vacanT(ParkConfigClass setterViewParkingResponse, String vtype, String booking_type) {
        if (booking_type.equals("Reserved")) {
            if (vtype.equals("Car")) {

                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

            } else if (vtype.equals("Bike")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() + 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            }
        } else {
            if (vtype.equals("Car")) {

                vacant = (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar())
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

            } else if (vtype.equals("Bike")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck() + 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d("test", "onMapReady");
        googleMap.setOnMarkerClickListener(this);
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mGoogleMap.setMyLocationEnabled(true);
        // mGoogleMap.setTrafficEnabled(true);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                // Todo Location Already on  ... start
                manager = (LocationManager) ActivityMain.this.getSystemService(Context.LOCATION_SERVICE);
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(ActivityMain.this)) {

                }
                // Todo Location Already on  ... end

                if (!hasGPSDevice(ActivityMain.this)) {
                    //Toast.makeText(ActivityMain.this,"Gps not Supported",Toast.LENGTH_SHORT).show();
                }

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(ActivityMain.this)) {
                    buildGoogleApiClient();
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                    cameraIdle();
                    checkLocationPermission();

                } else {
                    buildGoogleApiClient();
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                    checkLocationPermission();
                }

            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
        }

        current_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(ActivityMain.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityMain.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        return;
                    }
                    checkLocationPermission();
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f);
                    mGoogleMap.animateCamera(cameraUpdate, null);
                    mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    pindropeffect();

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .build();
            mGoogleApiClient.connect();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onLocationChanged(final Location location) {

        mGoogleMap.clear();
        mLastLocation = location;

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        Log.d("test", "onLocationChanged");
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        cameraIdle();
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());


        pinIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow(v);
                btnset_location.setVisibility(View.VISIBLE);
            }
        });

        pindropeffect();


        btnset_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yourDestinationET.setText(locationName);
                btnset_location.setVisibility(View.GONE);
                LatLng latLng1 = null;

                if (location != null) {
                    latLng1 = new LatLng(latitude, longitude);
                }
                Drawable circleDrawable = getResources().getDrawable(R.drawable.pin_location_red);
                BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
                MarkerOptions markerOptions = new MarkerOptions()
                        .icon(markerIcon).title(locationName).position(latLng1);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 12));
                if (currmarker != null) {
                    currmarker.remove();
                }
                currmarker = mGoogleMap.addMarker(markerOptions);
            }
        });

        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        getParkingNearToCurrentLocation();
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(35, 60, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, 35, 60);
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void pindropeffect() {
        TranslateAnimation transAnim = new TranslateAnimation(0, 0, -getDisplayHeight() / 10, 0);
        transAnim.setStartOffset(500);
        transAnim.setDuration(3000);
        transAnim.setRepeatCount(-1);
        transAnim.setRepeatMode(Animation.REVERSE);
        transAnim.setInterpolator(new BounceInterpolator());
        transAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
                pinIV.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                final int left = pinIV.getLeft();
                final int top = pinIV.getTop();
                final int right = pinIV.getRight();
                final int bottom = pinIV.getBottom();
                pinIV.layout(left, top, right, bottom);

            }
        });

        pinIV.startAnimation(transAnim);
    }

    private int getDisplayHeight() {
        return this.getResources().getDisplayMetrics().heightPixels;
    }

    private void popWindow(View v) {
        PopupWindow popup = new PopupWindow(ActivityMain.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_content, null);
        popup.setContentView(layout);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(v);

    }

    public void getParkingNearToCurrentLocation() {
        try {
            if (v_type.equals("Car")) {
                new WebServiceGet(this, new HandlerParkingAreaListResponse(), Constants.VIEW_PARKING_AREA_URL + 1 + "/null" + "/" + "null" + "/" + "null").execute();
                Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL + 1 + "/null" + "/" + "null" + "/" + "null");
            } else if (v_type.equals("Bike")) {
                new WebServiceGet(this, new HandlerParkingAreaListResponse(), Constants.VIEW_PARKING_AREA_URL + "null/" + 1 + "/null" + "/null").execute();
                Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL + "null/" + 1 + "/null" + "/null");
            } else if (v_type.equals("Bus")) {
                new WebServiceGet(this, new HandlerParkingAreaListResponse(), Constants.VIEW_PARKING_AREA_URL + "null/" + "null/" + 1 + "/null").execute();
                Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL + "null/" + "null/" + 1 + "/null");
            } else if (v_type.equals("Truck")) {
                new WebServiceGet(this, new HandlerParkingAreaListResponse(), Constants.VIEW_PARKING_AREA_URL + "null" + "/null" + "/null/" + 1).execute();
                Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL + "null" + "/null" + "/null/" + 1);
            } else {
                new WebServiceGet(this, new HandlerParkingAreaListResponse(), Constants.VIEW_PARKING_AREA_URL + "null" + "/null" + "/null" + "/null").execute();
            }
            Log.e(TAG, "123>>>" + Constants.VIEW_PARKING_AREA_URL);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setParkingAreaOnMap(final List<ModelParkingAreaResponse.DataBean> modelParkingAreaResponseList) {

        try {
            mGoogleMap.clear();
            mModelParkingAreaResponseList = modelParkingAreaResponseList;
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        List<LatLng> points = new ArrayList<LatLng>();
        LatLng latLng = new LatLng(latitude, longitude);
        originLatLng = latLng;
        Log.e(TAG, "originLatLng::::::::::" + originLatLng);


        for (ModelParkingAreaResponse.DataBean modelParkingAreaResponse : modelParkingAreaResponseList) {
            //Below code if condition only Four Parking under 1 km
            //if (Utility.distance(latitude, longitude, modelParkingAreaResponse.getCoordinates().getX(), modelParkingAreaResponse.getCoordinates().getY()) <= 1.00)
            points.add(new LatLng(modelParkingAreaResponse.getCoordinates().getX(), modelParkingAreaResponse.getCoordinates().getY()));
            Log.e(TAG, "PONINT::::::::::" + points);
            Log.e(TAG, "LIST SIZE::::::::::" + modelParkingAreaResponseList.size());
        }

        points.add(0, latLng);

        if (points.size() == 1) {
            Collections.sort(modelParkingAreaResponseList, new Comparator<ModelParkingAreaResponse.DataBean>() {
                @Override
                public int compare(ModelParkingAreaResponse.DataBean modelParkingAreaResponse, ModelParkingAreaResponse.DataBean o2) {

                    return Double.compare(Utility.distance(latitude, longitude, modelParkingAreaResponse.getCoordinates().getX(), modelParkingAreaResponse.getCoordinates().getY()),
                            Utility.distance(latitude, longitude, o2.getCoordinates().getX(), o2.getCoordinates().getY()));
                }
            });

            if (modelParkingAreaResponseList.size() >= 4) {
                for (int i = 0; i < 4; i++) {
                    ModelParkingAreaResponse.DataBean modelParkingAreaResponse = modelParkingAreaResponseList.get(i);
                    points.add(new LatLng(modelParkingAreaResponse.getCoordinates().getX(), modelParkingAreaResponse.getCoordinates().getY()));
                }
            } else {
                for (ModelParkingAreaResponse.DataBean modelParkingAreaResponse : modelParkingAreaResponseList) {
                    points.add(new LatLng(modelParkingAreaResponse.getCoordinates().getX(), modelParkingAreaResponse.getCoordinates().getY()));
                }
            }

        }

        for (int i = 0; i < points.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(points.get(i));
            if (i == 0) {
            } else {
                destinationLatLng = points.get(i);
                marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_map_marker, null);
                marker.setTag(modelParkingAreaResponseList);
                Constants.PARKG_ID = modelParkingAreaResponseList.get(i - 1).getParkingId();
                Constants.PARKG_NAME = modelParkingAreaResponseList.get(i - 1).getPlaceName();
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker)));
                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                mGoogleMap.addMarker(markerOptions);
                mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

            }

            markerposition = i;
            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    for (int j = 0; j < modelParkingAreaResponseList.size(); j++) {
                        try {
                            if (modelParkingAreaResponseList.get(j).getCoordinates().getX() == marker.getPosition().latitude) {
                                Constants.SELCT_PARKG_ID = modelParkingAreaResponseList.get(j).getParkingId();
                                Constants.SELECT_PARKG_NAME = modelParkingAreaResponseList.get(j).getPlaceName();
                                latitude1 = marker.getPosition().latitude;
                                longitude1 = marker.getPosition().longitude;
                                Log.e(TAG, " Constant Parking LAT>>" + latitude1);
                                Log.e(TAG, " Constant Parking LONG>>" + longitude1);
                                String parkname = modelParkingAreaResponseList.get(j).getPlaceName();
                                showParkingInfo(Constants.SELCT_PARKG_ID, Constants.SELECT_PARKG_NAME);
                                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                                break;
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return false;
                }
            });
        }

        CircleOptions circleOptions = new CircleOptions()
                .center(latLng)
                .radius(1000)
                .strokeWidth(1)
                .strokeColor(ContextCompat.getColor(this, R.color.colorLogo))
                .fillColor(Color.parseColor("#3FFFFFFF"));//#3304C9F8
        mCurrLocationMarker.remove();
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.f));

    }

    private void showParkingInfo(int parkgId, String parkname) {
        Constants.SELCT_PARKG_ID = parkgId;
        new WebServiceGet(this, new HandlerParkingAreaListResponse1(), Constants.VIEW_PARKING_AREA_URL12 + parkgId + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null").execute();
        Log.e(TAG, "523>>>" + Constants.VIEW_PARKING_AREA_URL12 + parkgId + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null" + "/null");
    }

    private void CheckParkConfig() {
        new WebServiceGet(this, new HandlerdateListResponse1(), Constants.AVAILABLE_PARKING + Constants.SELCT_PARKG_ID).execute();
        Log.e(TAG, "123>>>" + Constants.AVAILABLE_PARKING + Constants.SELCT_PARKG_ID);
    }

    private void cameraIdle() {

        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                CameraPosition position = mGoogleMap.getCameraPosition();

                latLng = mGoogleMap.getCameraPosition().target;

                latitude = latLng.latitude;
                longitude = latLng.longitude;

                try {
                    locationName = getAddress(ActivityMain.this, latLng.latitude, latLng.longitude);
                    //  yourDestinationET.setText(locationName);
                    Log.e("Testing", "" + locationName);
                    // getParkingNearToCurrentLocation();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getAddress(Context context, double lat, double lng) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);

            String add = obj.getAddressLine(0);
            Log.d("test", add);
            return add;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3600000);
        mLocationRequest.setFastestInterval(3600000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        current_btn.performClick();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ActivityMain.this, REQUEST_LOCATION);


                        }
                        catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                }
            }
        });

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                findViewById(R.id.OTPLayout).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(ActivityMain.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerIV:
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
                break;

            case R.id.yourDestinationET:
                yourDestinationET.getText().clear();
                break;

            case R.id.cancelTV:
                //  DeleteBooking();
                if (parkingType.equals("Reserved")) {
                    findViewById(R.id.OTPLayout).setVisibility(View.GONE);
                    findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                    btnset_location.setVisibility(View.GONE);
                    cancelBooking();
                } else {
                    cancelBooking();
                }

                break;

            case R.id.tv_parkcancel:
                cancelClick = true;
                // Log.e(TAG, "CANCEL TAG on click" + cancelClick);
                Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slidedown);
                findViewById(R.id.bottomMenuLayout).startAnimation(slide_down);
                slide_down.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.searchACIV:
                if (latLng != null) {
                    latitude = latLng.latitude;
                    longitude = latLng.longitude;
                    // getAddress(ActivityMain.this,latitude,longitude);
                    // mGoogleMap.clear();
                    // getParkingNearToCurrentLocation();

                }
                break;
            default:
                break;
        }
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null) {
            return false;
        }
        final List<String> providers = mgr.getAllProviders();
        if (providers == null) {
            return false;
        }
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111) {
            if (mGoogleApiClient == null) {
                buildGoogleApiClient();
            }
            if (data != null) {
                latitude = data.getDoubleExtra("lat", 0.0);
                longitude = data.getDoubleExtra("long", 0.0);
                Log.e(TAG, " Lat  >>>>>>" + latitude1);
                Log.e(TAG, " Long >>>>>>" + longitude1);
                locationName = getAddress(ActivityMain.this, latitude, longitude);
                // yourDestinationET.setText(locationName);
                Log.e("Testing >>>>", " ::" + locationName);
                pinIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popWindow(v);
                        btnset_location.setVisibility(View.VISIBLE);
                    }
                });

                btnset_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        yourDestinationET.setText(locationName);
                        btnset_location.setVisibility(View.GONE);

                        LatLng latLng1 = null;
                        Marker currmarker = null;
                        if (latLng1 != null) {
                            latLng1 = new LatLng(latLng.latitude, latLng.longitude);
                        }

                        MarkerOptions markerOptions = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).position(latLng1);
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 12));
                        if (currmarker != null) {
                            currmarker.remove();
                        }
                        currmarker = mGoogleMap.addMarker(markerOptions);
                    }
                });
                getParkingNearToCurrentLocation();
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        //    mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public class Handlecancelbook extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 0;
                    Toast.makeText(ActivityMain.this, "Your Booking is Canceled", Toast.LENGTH_SHORT).show();
                    findViewById(R.id.OTPLayout).setVisibility(View.GONE);
                    findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                    btnset_location.setVisibility(View.GONE);
                    new WebServiceGet(ActivityMain.this, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null").execute();
                    Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null");
                }
            }
        }
    }

    public class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);

            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        configurationID = parkConfigClass.getParkingConfigResponseList().get(0).getParkingConfigurationId();
                        Log.d(TAG, "configurationID  ------->  " + configurationID);

                        if (parkingType.equals("Reserved")) {
                            updateVacant(parkConfigClass, parkingType);
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            int second = mcurrentTime.get(Calendar.SECOND);

                            String seconds = "";
                            if (second < 10) {
                                seconds = "0" + second;
                            } else {
                                seconds = "" + second;
                            }

                            String hours = "";
                            if (hour < 10) {
                                hours = "0" + hour;
                            } else {
                                hours = "" + hour;
                            }

                            if (minute < 10) {
                                outTime = hours + ":0" + minute + ":" + seconds;
                            } else {
                                outTime = hours + ":" + minute + ":" + seconds;
                            }

                            outTime = hour + ":" + minute + ":";
                            String charges = getCharges(RSF, outTime, 1);
                            Log.e(TAG, "Charges ::: " + charges);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("VACANT RESPONSE ::", "response: " + response);
            try {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public class HandlerParkingAreaListResponse extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                modelParkingAreaListResponse = new Gson().fromJson(response, ModelParkingAreaListResponse.class);
                if (modelParkingAreaListResponse.getStatus() == 200) {
                    if (modelParkingAreaListResponse.getModelParkingAreaResponseList() != null) {
                        setParkingAreaOnMap(modelParkingAreaListResponse.getModelParkingAreaResponseList());
                    }
                } else if (modelParkingAreaListResponse.getStatus() == 204) {
                    findViewById(R.id.bottomMenuLayout).setVisibility(View.GONE);
                    mGoogleMap.clear();
                } else {
                    Toast.makeText(ActivityMain.this, modelParkingAreaListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class HandlerParkingAreaListResponse1 extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                modelParkingAreaListResponse = new Gson().fromJson(response, ModelParkingAreaListResponse.class);

                if (modelParkingAreaListResponse.getStatus() == 200) {

                    if (modelParkingAreaListResponse.getModelParkingAreaResponseList() != null) {
                        try {
                            findViewById(R.id.bottomMenuLayout).setVisibility(View.VISIBLE);

                            Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                                    R.anim.slideup);

                            findViewById(R.id.bottomLayout).startAnimation(slide_up);


                            btnset_location.setVisibility(View.GONE);

                            tv_p_name.setText(modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPlaceName());
                            tv_p_addr.setText(modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPlaceLandmark());
                            tv_p_total.setText("Total Space : " + modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getTotalSpace());
                            tv_p_total.setVisibility(View.GONE);
                            if (modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getVacant() == 0) {
                                tv_p_vacant.setText("Vacant : " + modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getTotalSpace());
                                tv_p_vacant.setVisibility(View.GONE);
                            } else {
                                tv_p_vacant.setText("Vacant : " + modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getVacant());
                            }
                            tv_p_vacant.setVisibility(View.GONE);
                            if (v_type.equals("Car")) {
                                carvac = modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getGeneral().get(0).getCar() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getReserved().get(0).getCar() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getRegular().get(0).getCar() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getEvent().get(0).getCar() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPremium().get(0).getCar();
                                tv_vtypevac.setText("Car vacant : " + carvac);
                            } else if (v_type.equals("Bike")) {
                                Bikevac = modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getGeneral().get(0).getBike() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getReserved().get(0).getBike() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getRegular().get(0).getBike() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getEvent().get(0).getBike() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPremium().get(0).getBike();

                                tv_vtypevac.setText("Bike vacant : " + Bikevac);
                            } else if (v_type.equals("Bus")) {
                                Busvac = modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getGeneral().get(0).getBus() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getReserved().get(0).getBus() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getRegular().get(0).getBus() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getEvent().get(0).getBus() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPremium().get(0).getBus();
                                tv_vtypevac.setText("Bus vacant : " + Busvac);
                            } else {
                                Truckvac = modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getGeneral().get(0).getTruck() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getReserved().get(0).getTruck() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getRegular().get(0).getTruck() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getEvent().get(0).getTruck() +
                                        modelParkingAreaListResponse.getModelParkingAreaResponseList().get(0).getPremium().get(0).getTruck();
                                tv_vtypevac.setText("Truck vacant : " + Truckvac);
                            }

                            btn_viewdetail.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (preferences.getBoolean(Constants.ISLOGIN, false)) {
                                        Intent intent = new Intent(ActivityMain.this, ActivityContractorParkingDetail.class);
                                        intent.putExtra("PARK_ID", Constants.SELCT_PARKG_ID);
                                        intent.putExtra("PARK NAME", Constants.SELECT_PARKG_NAME);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(ActivityMain.this, ActivityLogin.class);
                                        startActivity(intent);
                                    }

                                }
                            });

                            btn_booknow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (preferences.getBoolean(Constants.ISLOGIN, false)) {
                                        if (v_type.equals("Car") && carvac != 0 || v_type.equals("Bike") && Bikevac != 0 || v_type.equals("Bus") && Busvac != 0 || v_type.equals("Truck") && Truckvac != 0) {
                                            CheckParkConfig();
                                        } else {
                                            if (v_type.equals("Car")) {
                                                Toast.makeText(ActivityMain.this, "No more Vacant for car ", Toast.LENGTH_SHORT).show();
                                            } else if (v_type.equals("Bike")) {
                                                Toast.makeText(ActivityMain.this, "No more Vacant for Bike ", Toast.LENGTH_SHORT).show();
                                            } else if (v_type.equals("Bus")) {
                                                Toast.makeText(ActivityMain.this, "No more Vacant for Bus ", Toast.LENGTH_SHORT).show();
                                            } else if (v_type.equals("Truck")) {
                                                Toast.makeText(ActivityMain.this, "No more Vacant for Truck ", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    } else {
                                        Intent intent = new Intent(ActivityMain.this, ActivityLogin.class);
                                        startActivity(intent);
                                    }
                                }
                            });

                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private class HandlerdateListResponse1 extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("Response>>>", "response: " + response);

            if (response != null) {
                ModelAvailbaleConfigClass viewdatesconfigClass = new Gson().fromJson(response, ModelAvailbaleConfigClass.class);

                if (viewdatesconfigClass.getStatus() == 200) {
                    Log.e(TAG, "USERID :::  " + UserID);
                    if (viewdatesconfigClass.getModelParkingAreaResponseList() != null) {
                        if (preferences.getBoolean(Constants.ISLOGIN, true)) {
                            Intent intent = new Intent(ActivityMain.this, ParkingBookActivity.class);
                            intent.putExtra(Constants.PARKING_ID, Constants.SELCT_PARKG_ID);
                            intent.putExtra(Constants.PLACE_NAME, Constants.SELECT_PARKG_NAME);
                            intent.putExtra("CONFIGID", viewdatesconfigClass.getModelParkingAreaResponseList().get(0).getParkingConfigurationId());
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(ActivityMain.this, ActivityLogin.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(ActivityMain.this, " Today Date Configuration is not available ", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }




}




