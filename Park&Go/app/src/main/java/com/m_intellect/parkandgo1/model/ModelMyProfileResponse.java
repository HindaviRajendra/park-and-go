
package com.m_intellect.parkandgo1.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMyProfileResponse {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private List<ModelMyProfile> modelMyProfileList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ModelMyProfile> getModelMyProfileList() {
        return modelMyProfileList;
    }

    public void setModelMyProfileList(List<ModelMyProfile> modelMyProfileList) {
        this.modelMyProfileList = modelMyProfileList;
    }
}
