package com.m_intellect.parkandgo1.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityParkingHistory;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ModelParkingHistoryResponse;
import com.m_intellect.parkandgo1.model.ModelViewParkingResponse;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.m_intellect.parkandgo1.utils.Constants.CANCELSTATUS;

/**
 * Created by Kalpesh
 */

public class AdapterParkingHistoryList extends RecyclerView.Adapter<AdapterParkingHistoryList.ViewHolder> {

    private Context context;
    private List<ModelParkingHistoryResponse.DataBean> modelParkingHistoryResponseList;
    int bID;
    int iCount = 1, configurationID;
    public String TAG = getClass().getSimpleName();
    int deleteposition, parkingID;
    public ParkConfigClass parkConfigClass;
    private String v_type, booking_type;
    private int vacant = 0;
    private String outTime, BookingTime;
    private String time2;
    private SharedPreferences preferences;


    public AdapterParkingHistoryList(Context context, List<ModelParkingHistoryResponse.DataBean> modelParkingHistoryResponseList) {
        this.context = context;
        this.modelParkingHistoryResponseList = modelParkingHistoryResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_parking_history, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        preferences = context.getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        final ModelParkingHistoryResponse.DataBean modelParkingHistoryResponse = modelParkingHistoryResponseList.get(holder.getAdapterPosition());

        holder.titleTV.setText(modelParkingHistoryResponse.getPlace_name());

        String time1 = modelParkingHistoryResponse.getStart_date().replace(".000Z", "");

        try {
            time2 = "" + time1.charAt(11) + "" + time1.charAt(12) + "" + time1.charAt(13) + "" + time1.charAt(14) + "" + time1.charAt(15) + "" + time1.charAt(16)
                    + "" + time1.charAt(17) + "" + time1.charAt(18);

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (modelParkingHistoryResponse.getBooking_history() == 1) {
            holder.tv_status.setText("STATUS : BOOKED");
            holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.colorAccent1));

            if (modelParkingHistoryResponse.getCancle() == 1) {
                holder.tvcancel.setText("Cancelled");
                holder.tvcancel.setVisibility(View.VISIBLE);
            } else if (modelParkingHistoryResponse.getCancle() == 0) {
                holder.tvcancel.setText("Cancel");
                holder.tvcancel.setVisibility(View.VISIBLE);
            } else {
                holder.tvcancel.setVisibility(View.GONE);
            }

            holder.tvcancel.setVisibility(View.VISIBLE);
            holder.tvcancel.setTag(modelParkingHistoryResponse);


            holder.tvcancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.tvcancel.getText().toString().equals("Cancel")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Are you sure you want to delete ?");
                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                parkingID = modelParkingHistoryResponse.getParking_id();
                                cancelBooking(modelParkingHistoryResponse.getBooking_id());
                                v_type = modelParkingHistoryResponse.getVehicle_type();
                                booking_type = modelParkingHistoryResponse.getBooking_type();
                                holder.tvcancel.setVisibility(View.GONE);
                                BookingTime = modelParkingHistoryResponse.getBooking_time();


                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });


            Log.e(TAG, "TIME ----->" + time2);
            if (modelParkingHistoryResponse.getBooking_history() == 1) {
                holder.bookingTime1.setText(time2);
                //holder.bookingTime1.setText(modelParkingHistoryResponse.getArrival_time());
            } else {
                holder.bookingTime.setText("---");
            }

        } else if (modelParkingHistoryResponse.getBooking_history() == 2) {
            holder.tv_status.setText("STATUS : ONGOING");
            holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.yellow));
            holder.bookingTime1.setText(time2);
            holder.bookingTime.setText(modelParkingHistoryResponse.getReal_time());
        } else {
            holder.tv_status.setText("STATUS : COMPLETED");
            holder.bookingTime1.setText(time2);
            holder.bookingTime.setText(modelParkingHistoryResponse.getArrival_time());
            holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }

        holder.addressTV.setText(modelParkingHistoryResponse.getPlace_landmark() + ", " + modelParkingHistoryResponse.getCity());
        holder.iImageView.setTag(modelParkingHistoryResponseList);
        holder.iImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCount++;
                Log.e("59", "Count" + iCount);

                if (iCount % 2 == 0) {
                    holder.linearLayout.setVisibility(View.VISIBLE);
                    //info();
                } else if (iCount % 2 == 1) {
                    holder.linearLayout.setVisibility(View.GONE);
                }
            }
        });


        if (modelParkingHistoryResponse.getBooking_id() != 0) {
            holder.bookingId.setText("" + modelParkingHistoryResponse.getBooking_id());
        } else {
            holder.llBookingID.setVisibility(View.GONE);
        }


        if (!modelParkingHistoryResponse.getTotal_time().equals("0")) {
            holder.tv_totaltime.setText(modelParkingHistoryResponse.getTotal_time());
        } else {
            holder.ll_totaltime.setVisibility(View.GONE);
        }


        if (modelParkingHistoryResponse.getBooking_charges() != 0) {
            holder.bookingCharges.setText("" + modelParkingHistoryResponse.getBooking_charges() + " Rs");
        } else {
            holder.llBookingCharges.setVisibility(View.GONE);
        }

        if (modelParkingHistoryResponse.getBooking_type().equals("On Spot")) {
            if (modelParkingHistoryResponse.getOnline().equals("online")) {
                holder.bookingType.setText("General");
            } else {
                holder.bookingType.setText(modelParkingHistoryResponse.getBooking_type());
            }
        } else if (modelParkingHistoryResponse.getBooking_type().equals("Reserved")) {
            holder.bookingType.setText("Reserved");

        } else
            holder.llBookingType.setVisibility(View.GONE);

        if (!modelParkingHistoryResponse.getPayment_type().equals(" ")) {
            holder.paymentType.setText(modelParkingHistoryResponse.getPayment_type());
        } else {
            holder.llPaymentType.setVisibility(View.GONE);
        }

        if (!modelParkingHistoryResponse.getLeave_time().equals("00:00:00")) {
            holder.leavetime.setText(modelParkingHistoryResponse.getLeave_time());

        } else {
            holder.llLeaveTime.setVisibility(View.GONE);
        }


        if (!modelParkingHistoryResponse.getVehicle_no().equals("")) {
            holder.vehicleNumber.setText(modelParkingHistoryResponse.getVehicle_no());

        } else {
            holder.llVehicleNumber.setVisibility(View.GONE);
        }

        if (!modelParkingHistoryResponse.getVehicle_type().equals(" ")) {
            holder.vehicleType.setText(modelParkingHistoryResponse.getVehicle_type());
        } else {
            holder.llVehicleType.setVisibility(View.GONE);
        }


        if (modelParkingHistoryResponse.getStart_date() != null) {
            String date = modelParkingHistoryResponse.getStart_date().substring(0, 10);
            holder.dateTV.setText(Utility.formatDateFromOnetoAnother(date, "yyyy-MM-dd", "MMM dd"));
        }
    }

    public void cancelBooking(int bookingId) {
        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setBookingId(bookingId);
        Gson gson = new Gson();
        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);
        new WebServicePost(context, new Handlecancelbook(), Constants.CANCEL_BOOK, payload).execute();
        Log.e(TAG, "CancelURL>>>" + payload);

    }

    public class Handlecancelbook extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    CANCELSTATUS = 1;
                    ((ActivityParkingHistory) context).getParkingHistoryList();
                    Toast.makeText(context, "Your Booking is Canceled", Toast.LENGTH_SHORT).show();
                    new WebServiceGet(context, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null").execute();
                    Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null");
                }
            }
        }
    }

    public class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);

            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        configurationID = parkConfigClass.getParkingConfigResponseList().get(0).getParkingConfigurationId();
                        Log.d(TAG, "configurationID  --=----->  " + configurationID);
                        if (booking_type.equals("Reserved")) {
                            updateVacant(parkConfigClass, booking_type);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse, String booking_type) {

        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(parkingID);
        setterPayload.setParkingConfigurationId(configurationID);
        Log.e(TAG, "i am in else");

        if (booking_type.equals("Reserved")) {
            if (v_type.equals("Car")) {
                generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() + 1));
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bike")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() + 1);
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bus")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() + 1);
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);


            } else if (v_type.equals("Truck")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() + 1);
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);
            }

        } else {
            if (v_type.equals("Car")) {
                generalBean.setCar((setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() + 1));
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bike")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() + 1);
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);

            } else if (v_type.equals("Bus")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() - 1);
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);


            } else if (v_type.equals("Truck")) {
                generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck() - 1);
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, v_type, booking_type);
            }

        }
        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);

        new WebServicePost(context, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }


    public class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("VACANT RESPONSE ::", "response: " + response);
            try {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");


                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        int second = mcurrentTime.get(Calendar.SECOND);

                        String seconds = "";
                        if (second < 10)
                            seconds = "0" + second;
                        else
                            seconds = "" + second;

                        String hours = "";
                        if (hour < 10)
                            hours = "0" + hour;
                        else
                            hours = "" + hour;

                        if (minute < 10)
                            outTime = hours + ":0" + minute + ":" + seconds;
                        else
                            outTime = hours + ":" + minute + ":" + seconds;

                        outTime = hour + ":" + minute + ":";
                        String charges = getCharges(BookingTime, outTime, 1);
                        Log.e(TAG, "Charges ::::: " + charges);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public String getCharges(String startTime, String endTime, int show) {
        int minutes = 0;
        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = simpleDateFormat.parse(startTime);
            endDate = simpleDateFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null;
                Date dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (show == 0) {
                String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
                String min3 = outTime.substring(3, 5);
                Log.e(TAG, "Reserved min3 :: " + min3);

                String toRemove = ":";
                if (base.contains(toRemove)) {
                    base = base.replaceAll(toRemove, "");
                }

                Log.e(TAG, "Reserved BASE :: " + Integer.parseInt(base));
                minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            } else {
                minutes = hours * 60 + min;
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            }


            try {
                if (booking_type.equals("Reserved")) {
                    if (v_type.equals("Car")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }

                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Bus")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    } else if (v_type.equals("Truck")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPariceR();
                            }
                        }
                    }
                } else {
                    if (v_type.equals("Car")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Bike")) {

//                    if (minutes < 1000 * 60 * 60 && hours == 0) {
//                        charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                        i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                    } else

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }

                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Bus")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    } else if (v_type.equals("Truck")) {
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG());
                                } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                    Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG());
                                }
                            }
                        }
                        if (charges.equalsIgnoreCase("0")) {
                            int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                            if (size > 0) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                            }
                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "LAST CHARGE ----- >" + charges);
        return charges;
    }


    public void vacanT(ParkConfigClass setterViewParkingResponse, String vtype, String booking_type) {
        if (booking_type.equals("Reserved")) {
            if (vtype.equals("Car")) {

                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

            } else if (vtype.equals("Bike")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() + 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() + 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            }
        } else {
            if (vtype.equals("Car")) {

                vacant = (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar())
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();

            } else if (vtype.equals("Bike")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() + 1) +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                        (setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck() + 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                        + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
            }
        }
    }


    @Override
    public int getItemCount() {
        return modelParkingHistoryResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTV, tvcancel;
        private TextView dateTV;
        private TextView addressTV;
        private TextView durationTV;
        private TextView bookingId;
        private TextView bookingTime, bookingTime1, tv_status, tv_totaltime, arrivalTime, bookingCharges, bookingType, paymentType, startDate, endDate, city, state, leavetime, mob_no, vehicleNumber;
        private TextView attendantName, advancepayHour, advancePaid, balanceCash, vehicleType;
        private ImageView iImageView;
        private LinearLayout linearLayout, llAdvancePayHour, llAdvancePaid, llBookingID, llBookingTime, llBookingTime2, llArrivalTime, llBalanceCash, llVehicleType;
        private LinearLayout llBookingCharges, llBookingType, llPaymentType, llStartDate, llEndDate, llCity, llState, llLeaveTime, llMobileNumber, llVehicleNumber, llAttendantName, ll_totaltime;


        public ViewHolder(View itemView) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.titleTV);
            tvcancel = itemView.findViewById(R.id.tv_cancel);
            dateTV = itemView.findViewById(R.id.dateTV);
            addressTV = itemView.findViewById(R.id.addressTV);
            durationTV = itemView.findViewById(R.id.durationTV);
            tv_totaltime = itemView.findViewById(R.id.txt_totaltime);
            tv_status = itemView.findViewById(R.id.tv_status);


            iImageView = itemView.findViewById(R.id.iImageview);
            linearLayout = itemView.findViewById(R.id.ll_parking_history);

            // llAdvancePayHour = itemView.findViewById(R.id.llAdvancePayHour);
            //  llAdvancePaid = itemView.findViewById(R.id.llAdvancePaid);
            llBookingID = itemView.findViewById(R.id.llBookingID);
            llBookingTime = itemView.findViewById(R.id.llBookingTime);
            llBookingTime2 = itemView.findViewById(R.id.llBookingTime2);
            //    llArrivalTime = itemView.findViewById(R.id.llArrivalTime);
            llBookingCharges = itemView.findViewById(R.id.llBookingCharges);
            llBookingType = itemView.findViewById(R.id.llBookingType);
            llPaymentType = itemView.findViewById(R.id.llPaymentType);
            ll_totaltime = itemView.findViewById(R.id.ll_totaltime);
            //   llStartDate = itemView.findViewById(R.id.llStartDate);
            //  llEndDate = itemView.findViewById(R.id.llEndDate);
            //   llCity = itemView.findViewById(R.id.llCity);
            //    llState = itemView.findViewById(R.id.llState);
            llLeaveTime = itemView.findViewById(R.id.llLeaveTime);
            //   llMobileNumber = itemView.findViewById(R.id.llMobileNumber);
            llVehicleNumber = itemView.findViewById(R.id.llVehicleNumber);
            //  llAttendantName = itemView.findViewById(R.id.llAttendantName);
            // llBalanceCash = itemView.findViewById(R.id.llBalanceCash);
            llVehicleType = itemView.findViewById(R.id.llVehicleType);


            bookingId = itemView.findViewById(R.id.bookingID);
            bookingTime = itemView.findViewById(R.id.bookingTime);
            bookingTime1 = itemView.findViewById(R.id.bookingTime1);
            //    arrivalTime = itemView.findViewById(R.id.arrivalTime);
            bookingCharges = itemView.findViewById(R.id.bookingCharges);
            bookingType = itemView.findViewById(R.id.bookingType);
            paymentType = itemView.findViewById(R.id.paymentType);
            //    startDate = itemView.findViewById(R.id.startDate);
            //  endDate = itemView.findViewById(R.id.endDate);
            //  city = itemView.findViewById(R.id.city);
            //  state = itemView.findViewById(R.id.state);
            leavetime = itemView.findViewById(R.id.leaveTime);
            //   mob_no = itemView.findViewById(R.id.mobileNumber);
            vehicleNumber = itemView.findViewById(R.id.vehicleNumber);
            //  attendantName = itemView.findViewById(R.id.attendantName);
            //   advancepayHour = itemView.findViewById(R.id.advancePayHour);
            //   advancePaid = itemView.findViewById(R.id.advancePaid);
            // balanceCash = itemView.findViewById(R.id.balanceCash);
            vehicleType = itemView.findViewById(R.id.advanceVehicleType);
        }
    }
}
