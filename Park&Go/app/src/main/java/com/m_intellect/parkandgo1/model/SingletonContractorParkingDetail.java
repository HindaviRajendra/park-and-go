package com.m_intellect.parkandgo1.model;

/**
 * Created by sanket on 01-02-2018.
 */

public class SingletonContractorParkingDetail {

    private static SingletonContractorParkingDetail singletonContractorParkingDetail;
    private ModelParkingResponse modelParkingResponse;

    private SingletonContractorParkingDetail() {

    }

    public static SingletonContractorParkingDetail getInstance() {
        if (singletonContractorParkingDetail == null) {
            singletonContractorParkingDetail = new SingletonContractorParkingDetail();
        }
        return singletonContractorParkingDetail;
    }

    public ModelParkingResponse getModelParkingResponse() {
        return modelParkingResponse;
    }

    public void setModelParkingResponse(ModelParkingResponse modelParkingResponse) {
        this.modelParkingResponse = modelParkingResponse;
    }
}
