
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceValue {

    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("price_g")
    @Expose
    private String priceG;
    @SerializedName("parice_r")
    @Expose
    private String pariceR;
    @SerializedName("price_p")
    @Expose
    private String priceP;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriceG() {
        return priceG;
    }

    public void setPriceG(String priceG) {
        this.priceG = priceG;
    }

    public String getPariceR() {
        return pariceR;
    }

    public void setPariceR(String pariceR) {
        this.pariceR = pariceR;
    }

    public String getPriceP() {
        return priceP;
    }

    public void setPriceP(String priceP) {
        this.priceP = priceP;
    }

}
