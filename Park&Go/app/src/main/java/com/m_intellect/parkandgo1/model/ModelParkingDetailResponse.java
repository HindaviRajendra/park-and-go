package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 1/2/2018.
 */

public class ModelParkingDetailResponse {

    @Expose
    private Integer parkingConfigurationId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("parking_id")
    @Expose
    private Integer parkingId;
    @SerializedName("user_id")
    @Expose
    private Object userId;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("general")
    @Expose
    private String general;
    @SerializedName("reserved")
    @Expose
    private String reserved;
    @SerializedName("regular")
    @Expose
    private String regular;
    @SerializedName("event")
    @Expose
    private String event;
    @SerializedName("premium")
    @Expose
    private String premium;
    @SerializedName("calender")
    @Expose
    private List<ConfigurationCalendar> calender = null;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("price_values")
    @Expose
    private List<PriceValue> priceValues = null;
    @SerializedName("regular_parking")
    @Expose
    private RegularParking regularParking;
    @SerializedName("event_parking")
    @Expose
    private EventParking eventParking;
    @SerializedName("place_name")
    @Expose
    private String placeName;
    @SerializedName("place_landmark")
    @Expose
    private String placeLandmark;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    private boolean isChecked;

    private boolean isRadioChecked;


    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isRadioChecked() {
        return isRadioChecked;
    }

    public void setRadioChecked(boolean radioChecked) {
        isRadioChecked = radioChecked;
    }

    public Integer getParkingConfigurationId() {
        return parkingConfigurationId;
    }

    public void setParkingConfigurationId(Integer parkingConfigurationId) {
        this.parkingConfigurationId = parkingConfigurationId;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public List<ConfigurationCalendar> getCalender() {
        return calender;
    }

    public void setCalender(List<ConfigurationCalendar> calender) {
        this.calender = calender;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<PriceValue> getPriceValues() {
        return priceValues;
    }

    public void setPriceValues(List<PriceValue> priceValues) {
        this.priceValues = priceValues;
    }

    public RegularParking getRegularParking() {
        return regularParking;
    }

    public void setRegularParking(RegularParking regularParking) {
        this.regularParking = regularParking;
    }

    public EventParking getEventParking() {
        return eventParking;
    }

    public void setEventParking(EventParking eventParking) {
        this.eventParking = eventParking;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLandmark() {
        return placeLandmark;
    }

    public void setPlaceLandmark(String placeLandmark) {
        this.placeLandmark = placeLandmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

