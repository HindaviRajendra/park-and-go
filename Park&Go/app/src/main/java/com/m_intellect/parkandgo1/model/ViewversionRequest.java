package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class ViewversionRequest {

    /**
     * app_id : 20
     */

    @SerializedName("app_id")
    private int appId;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }
}
