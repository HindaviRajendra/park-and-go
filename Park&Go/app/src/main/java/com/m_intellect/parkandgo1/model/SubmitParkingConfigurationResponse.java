package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 1/21/2018.
 */

public class SubmitParkingConfigurationResponse {

    @SerializedName("insert_parking_configuration")
    @Expose
    private Integer insertParkingConfiguration;

    public Integer getInsertParkingConfiguration() {
        return insertParkingConfiguration;
    }

    public void setInsertParkingConfiguration(Integer insertParkingConfiguration) {
        this.insertParkingConfiguration = insertParkingConfiguration;
    }

}

