package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 11-02-2018.
 */

public class GraphDetailResponse {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("live")
    @Expose
    private Integer live;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getLive() {
        return live;
    }

    public void setLive(Integer live) {
        this.live = live;
    }

}
