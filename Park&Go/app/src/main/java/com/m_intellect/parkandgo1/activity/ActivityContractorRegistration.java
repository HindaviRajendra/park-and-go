package com.m_intellect.parkandgo1.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.dialogfragment.DialogFragmentOTP;
import com.m_intellect.parkandgo1.model.DeleteuserReq;
import com.m_intellect.parkandgo1.model.ModelContractorRegistration;
import com.m_intellect.parkandgo1.model.ModelContractorRegistrationResponse;
import com.m_intellect.parkandgo1.model.ViewversionResponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityContractorRegistration extends AppCompatActivity implements View.OnClickListener,DialogInterface.OnDismissListener {

    private EditText nameET;
    private EditText passwordET;
    private EditText emailET;
    private EditText mobileNoET;
    private EditText cityET;
    private EditText stateET;
    private EditText mapLocationET;
    private EditText locationNameET;
    private EditText parkingNameET;
    private EditText totalSpaceET;
    private String latitude, longitude, locationName;
    private ImageView locationIV;
    private String TAG=getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_registration);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initToolbar();
        initView();
        }

    private void initView() {

        TextView submitTV = (TextView) findViewById(R.id.submitTV);
        submitTV.setOnClickListener(this);

        nameET = (EditText) findViewById(R.id.nameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        emailET = (EditText) findViewById(R.id.emailET);
        mobileNoET = (EditText) findViewById(R.id.mobileNoET);
        cityET = (EditText) findViewById(R.id.cityET);
        stateET = (EditText) findViewById(R.id.stateET);
        mapLocationET = (EditText) findViewById(R.id.mapLocationET);
        mapLocationET.setHint("Add your map location");
        mapLocationET.setOnClickListener(this);
        locationNameET = (EditText) findViewById(R.id.locationNameET);
        parkingNameET = (EditText) findViewById(R.id.parkingNameET);
        totalSpaceET = (EditText) findViewById(R.id.totalSpaceET);
        locationIV = (ImageView) findViewById(R.id.locationIV);

    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Contractor Registration");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
            case R.id.submitTV:
                if (Utility.isNetworkAvailable(this)) {
                    if (isValid()) {
                        if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty())
                            registration();
                        else
                            Toast.makeText(this, R.string.error_map_location, Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                break;
            case R.id.mapLocationET:
                Intent intent = new Intent(this, ActivityAddMapLocation.class);
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                startActivityForResult(intent, 111);
                break;
            default:
                break;
        }
    }

    private boolean isValid() {

        if (nameET.getText().toString().length() == 0) {
            nameET.setError(getString(R.string.error_name));
        } else {
            nameET.setError(null);
        }
        if (passwordET.getText().toString().length() == 0) {
            passwordET.setError(getString(R.string.error_password));
        } else {
            passwordET.setError(null);
        }
        if (emailET.getText().toString().length() == 0 || !Utility.isValidEmail(emailET.getText().toString())) {
            emailET.setError(getString(R.string.error_email));
        } else {
            emailET.setError(null);
        }
        if (mobileNoET.getText().toString().length() < 10) {
            mobileNoET.setError(getString(R.string.error_mobile_no));
        } else {
            mobileNoET.setError(null);
        }
        if (cityET.getText().toString().length() == 0) {
            cityET.setError(getString(R.string.error_city));
        } else {
            cityET.setError(null);
        }
        if (stateET.getText().toString().length() == 0) {
            stateET.setError(getString(R.string.error_state));
        } else {
            stateET.setError(null);
        }

        if (locationNameET.getText().toString().length() == 0) {
            locationNameET.setError(getString(R.string.error_location_name));
        } else {
            locationNameET.setError(null);
        }
        if (parkingNameET.getText().toString().length() == 0) {
            parkingNameET.setError(getString(R.string.error_parking_name));
        } else {
            parkingNameET.setError(null);
        }
        if (totalSpaceET.getText().toString().length() == 0) {
            totalSpaceET.setError(getString(R.string.error_total_space));
        } else {
            totalSpaceET.setError(null);
        }

        return nameET.getError() == null && passwordET.getError() == null &&
                emailET.getError() == null && mobileNoET.getError() == null &&
                cityET.getError() == null && stateET.getError() == null &&
                locationNameET.getError() == null && parkingNameET.getError() == null &&
                totalSpaceET.getError() == null;
    }


    private void registration() {

        ModelContractorRegistration modelContractorRegistration = new ModelContractorRegistration();
        modelContractorRegistration.setId(0);
        modelContractorRegistration.setAppId(Constants.APP_ID);
        modelContractorRegistration.setName(nameET.getText().toString());
        modelContractorRegistration.setPassword(passwordET.getText().toString());
        modelContractorRegistration.setEmail(emailET.getText().toString());
        modelContractorRegistration.setMobileNo(mobileNoET.getText().toString());
        modelContractorRegistration.setTotalSpace(Integer.parseInt(totalSpaceET.getText().toString()));
        modelContractorRegistration.setVacant(0);
        modelContractorRegistration.setLocationId(0);
        modelContractorRegistration.setParkingId(0);
        modelContractorRegistration.setCity(cityET.getText().toString());
        modelContractorRegistration.setState(stateET.getText().toString());
        modelContractorRegistration.setLatitude(latitude);
        modelContractorRegistration.setLongitude(longitude);
        modelContractorRegistration.setActiveStatus(0);
        modelContractorRegistration.setUserType("contractor");
        modelContractorRegistration.setVehicleName(parkingNameET.getText().toString());

        Gson gson = new Gson();
        String payload = gson.toJson(modelContractorRegistration);
        Log.d("test", "payload: " + payload);

        new WebServicePost(this, new HandlerRegistrationResponse(), Constants.CONTRACTOR_REGISTRATION_URL, payload).execute();

    }

    private class HandlerRegistrationResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelContractorRegistrationResponse modelContractorRegistrationResponse = new Gson().fromJson(response, ModelContractorRegistrationResponse.class);
                if (modelContractorRegistrationResponse.getStatus() == 200) {
                    if (modelContractorRegistrationResponse.getContractorRegistrationResponses().get(0).getInsertparkinguser() == -1) {
                        Toast.makeText(ActivityContractorRegistration.this, "User Already Exist", Toast.LENGTH_SHORT).show();
                    } else {
                        DialogFragmentOTP dialogFragmentOTP = new DialogFragmentOTP();
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.USER_ID, modelContractorRegistrationResponse.getContractorRegistrationResponses().get(0).getInsertparkinguser());
                        dialogFragmentOTP.setArguments(bundle);

                        dialogFragmentOTP.show(getSupportFragmentManager(), "OTP");
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {

            if (data != null) {
                latitude = data.getStringExtra(Constants.LATITUDE);
                longitude = data.getStringExtra(Constants.LONGITUDE);
                locationName = data.getStringExtra(Constants.LOCATION_NAME);

                //mapLocationET.setText(latitude + ", " + longitude);
                if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty()) {
                    locationIV.setVisibility(View.VISIBLE);
                    mapLocationET.setHint("Map location is added");
                } else {
                    locationIV.setVisibility(View.GONE);
                    mapLocationET.setHint("Add your map location");
                }
                locationNameET.setText(locationName);
            }
        }
    }
    @Override
    public void onDismiss(DialogInterface dialog) {

//        if (!Constants.Cancelstatus){
//            Log.e("TAG","Dialog true");
//        }else {
//            Log.e("TAG","Dialog dismiss");
//        }

        deleteuser();

    }
    private void deleteuser() {
        DeleteuserReq deletebookingReq=new DeleteuserReq();
        deletebookingReq.setAppId(29);
        deletebookingReq.setMobileNo(mobileNoET.getText().toString());
        Gson gson = new Gson();
        String payload = gson.toJson(deletebookingReq);
        Log.e("test", "payload: " + payload);
        new WebServicePost(ActivityContractorRegistration.this, new ActivityContractorRegistration.Handlerupdate(), Constants.DELETE_USER,payload).execute();
        Log.e(TAG,"URL :: "+Constants.DELETE_USER);


    }
    public class Handlerupdate extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response: " + response);

            if (response != null) {
                ViewversionResponse viewversionResponse = new Gson().fromJson(response, ViewversionResponse.class);
                if (viewversionResponse.getStatus() == 200) {
                    Log.e(TAG,"Success on delete user");
                }
            }
        }
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }
}
