package com.m_intellect.parkandgo1.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;

/**
 * Created by sanket on 12/12/2017.
 */

public class AdapterPaymentList extends RecyclerView.Adapter<AdapterPaymentList.ViewHolder> {

    private Context context;
    private String tag;

    public AdapterPaymentList(Context context, String tag) {

        this.context = context;
        this.tag = tag;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_payment, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (tag.equalsIgnoreCase("paid")) {

        } else if (tag.equalsIgnoreCase("added")) {
            holder.walletIV.setImageResource(R.drawable.wallet_add);
            holder.titleTV.setText("Added in wallet");
            holder.amountTV.setText("+ ₹40");
        } else if (tag.equalsIgnoreCase("all")) {

            if (position == 0) {

            } else if (position == 1) {
                holder.walletIV.setImageResource(R.drawable.wallet_add);
                holder.titleTV.setText("Added in wallet");
                holder.amountTV.setText("+ ₹40");
            } else if (position == 3) {
                holder.walletIV.setImageResource(R.drawable.wallet_add);
                holder.titleTV.setText("Added in wallet");
                holder.amountTV.setText("+ ₹40");
            } else {

            }
        }
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView walletIV;
        private TextView titleTV;
        private TextView amountTV;

        public ViewHolder(View itemView) {
            super(itemView);

            walletIV = itemView.findViewById(R.id.walletIV);
            titleTV = itemView.findViewById(R.id.titleTV);
            amountTV = itemView.findViewById(R.id.amountTV);
        }
    }
}
