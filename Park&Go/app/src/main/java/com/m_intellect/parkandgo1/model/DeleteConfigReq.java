package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

public class DeleteConfigReq {


    /**
     * calender : 2018-05-30
     * parking_id : 180
     * app_id : 29
     */

    @SerializedName("calender")
    private String calender;
    @SerializedName("parking_id")
    private int parkingId;
    @SerializedName("app_id")
    private int appId;

    public String getCalender() {
        return calender;
    }

    public void setCalender(String calender) {
        this.calender = calender;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }
}
