package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelForgotPassword;
import com.m_intellect.parkandgo1.model.ModelForgotPasswordResponse;
import com.m_intellect.parkandgo1.model.ModelLogin;
import com.m_intellect.parkandgo1.model.ModelLoginResponse;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {

    private EditText mobileNumberET;
    private EditText passwordET;
    private Double latitude;
    private Double longitude;
    private SharedPreferences preferences;
    private int passwordNotVisible = 1;
    private String Str_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (getIntent().getExtras() != null) {
            latitude = getIntent().getDoubleExtra(Constants.LATITUDE, 0.0);
            longitude = getIntent().getDoubleExtra(Constants.LONGITUDE, 0.0);
        }
        initView();

    }

    private void initView() {

        mobileNumberET = (EditText) findViewById(R.id.mobileNumberET);
        passwordET = (EditText) findViewById(R.id.passwordET);

        TextView loginTV = (TextView) findViewById(R.id.loginTV);
        loginTV.setOnClickListener(this);

        TextView signupTV = (TextView) findViewById(R.id.signupTV);
        signupTV.setOnClickListener(this);

        TextView forgotPasswordTV = (TextView) findViewById(R.id.forgotPasswordTV);
        forgotPasswordTV.setOnClickListener(this);

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);

        Constants.GCM_ID = preferences.getString("GCMID", "");

        Str_token = FirebaseInstanceId.getInstance().getToken();
        Log.e("TAG", "GCM ID----->>" + Str_token);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.loginTV:
                if (Utility.isNetworkAvailable(this)) {
                    if (isValid())
                        login();
                } else
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                break;
            case R.id.signupTV:
                startActivity(new Intent(ActivityLogin.this, ActivitySignUp.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_in_left);
                break;

            case R.id.forgotPasswordTV:
                forgotPasswordDialog();
                break;
            default:
                break;
        }
    }

    private void forgotPasswordDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(ActivityLogin.this);
        View view = layoutInflater.inflate(R.layout.layout_dialog_forgot_password, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this);
        builder.setView(view);

        final EditText emailET = (EditText) view.findViewById(R.id.emailET);
        final EditText mobileNoET = (EditText) view.findViewById(R.id.mobileNoET);
        mobileNoET.setVisibility(View.VISIBLE);
        TextView submitTV = (TextView) view.findViewById(R.id.submitTV);
        TextView cancelTV = (TextView) view.findViewById(R.id.cancelTV);
        final RadioButton mobileNoRB = (RadioButton) view.findViewById(R.id.mobileNoRB);
        mobileNoRB.setChecked(true);
        final RadioButton emailRB = (RadioButton) view.findViewById(R.id.emailRB);

        mobileNoRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNoRB.setChecked(true);
                emailRB.setChecked(false);
                emailET.setVisibility(View.GONE);
                mobileNoET.setVisibility(View.VISIBLE);

            }
        });

        emailRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNoRB.setChecked(false);
                emailRB.setChecked(true);
                emailET.setVisibility(View.VISIBLE);
                mobileNoET.setVisibility(View.GONE);

            }
        });

        final AlertDialog alertDialog = builder.create();
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailRB.isChecked()) {
                    if (emailET.getText().toString().length() == 0 || !Utility.isValidEmail(emailET.getText().toString())) {
                        emailET.setError(getString(R.string.error_email));
                    } else {
                        emailET.setError(null);
                        if (Utility.isNetworkAvailable(ActivityLogin.this)) {
                            forgotPassword(emailET.getText().toString(), false);
                        } else {
                            Toast.makeText(ActivityLogin.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                        }

                        alertDialog.dismiss();
                    }
                } else if (mobileNoRB.isChecked()) {
                    if (mobileNoET.getText().toString().length() < 10) {
                        mobileNoET.setError(getString(R.string.error_mobile_no));
                    } else {
                        mobileNoET.setError(null);
                        if (Utility.isNetworkAvailable(ActivityLogin.this)) {
                            forgotPassword(mobileNoET.getText().toString(), true);
                        } else {
                            Toast.makeText(ActivityLogin.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                        }

                        alertDialog.dismiss();
                    }
                }
            }
        });
        alertDialog.show();
    }

    private void forgotPassword(String contact, boolean isMobile) {
        ModelForgotPassword modelForgotPassword = new ModelForgotPassword();
        modelForgotPassword.setAppId(Constants.APP_ID);
        modelForgotPassword.setActiveStatus(1);
        modelForgotPassword.setAppName("Parking");
        if (isMobile) {
            modelForgotPassword.setMobileNo(contact);
            modelForgotPassword.setEmail("null");
        } else {
            modelForgotPassword.setMobileNo("null");
            modelForgotPassword.setEmail(contact);
        }

        Gson gson = new Gson();
        String payload = gson.toJson(modelForgotPassword);
        Log.e("test", "payload: " + payload);
        Log.e("test", "payload: " + Constants.FORGOT_PASSWORD_URL);
        new WebServicePost(this, new HandlerForgotPasswordResponse(), Constants.FORGOT_PASSWORD_URL, payload).execute();
    }

    private class HandlerForgotPasswordResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelForgotPasswordResponse modelForgotPasswordResponse = new Gson().fromJson(response, ModelForgotPasswordResponse.class);
                Toast.makeText(ActivityLogin.this, "password sent successfully", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private boolean isValid() {

        if (mobileNumberET.getText().toString().length() < 10) {
            mobileNumberET.setError(getString(R.string.error_mobile_no));
        } else {
            mobileNumberET.setError(null);
        }
        if (passwordET.getText().toString().length() == 0) {
            passwordET.setError(getString(R.string.error_password));
            //showPasswordIV.setVisibility(View.GONE);
        } else {
            //showPasswordIV.setVisibility(View.VISIBLE);
            passwordET.setError(null);
        }

        return mobileNumberET.getError() == null && passwordET.getError() == null;
    }

    private void login() {

        ModelLogin modelLogin = new ModelLogin();
        modelLogin.setMobileNo(mobileNumberET.getText().toString());
        modelLogin.setPassword(passwordET.getText().toString());
        modelLogin.setAppId(Constants.APP_ID);
        modelLogin.setLatitude(String.valueOf(latitude));
        modelLogin.setLongitude(String.valueOf(longitude));
        modelLogin.setFcm_id(Str_token);

        Gson gson = new Gson();
        String payload = gson.toJson(modelLogin);
        Log.e("test", "payload: " + payload);
        Log.e("URL ", "URL ----> " + Constants.LOGIN_URL);
        new WebServicePost(this, new HandlerLoginResponse(), Constants.LOGIN_URL, payload).execute();

    }

    private class HandlerLoginResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelLoginResponse modelLoginResponse = new Gson().fromJson(response, ModelLoginResponse.class);
                if (modelLoginResponse.getStatus() == 200) {
                    if (modelLoginResponse.getModelLoginDataResponseList().size() > 0) {

                        Log.e("UID", "---->" + modelLoginResponse.getModelLoginDataResponseList().get(0).getUserId());

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(Constants.ISLOGIN, true);
                        editor.putInt(Constants.USER_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getUserId());
                        editor.putString(Constants.USER_TYPE, modelLoginResponse.getModelLoginDataResponseList().get(0).getUsertype());
                        editor.putString(Constants.EMAIL_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getEmail());
                        editor.putString(Constants.MOBILE_NO, modelLoginResponse.getModelLoginDataResponseList().get(0).getMobileNo());
                        editor.putString("VH_NO", modelLoginResponse.getModelLoginDataResponseList().get(0).getVehicle_no());
                        if (modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() != null)
                            editor.putInt(Constants.PARKING_ID, modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId());
                        editor.apply();

                        if (modelLoginResponse.getModelLoginDataResponseList().get(0).getUsertype().equalsIgnoreCase("customer")) {
                            Intent intent = new Intent(ActivityLogin.this, ActivityMain.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            if (modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() == null ||
                                    modelLoginResponse.getModelLoginDataResponseList().get(0).getParkingId() == 0) {
                            } else {
                                Intent intent1 = new Intent(ActivityLogin.this, ActivityDashboard.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                            }
                        }
                    } else {
                        Toast.makeText(ActivityLogin.this, "Invalid email id or password ", Toast.LENGTH_SHORT).show();
                    }
                }
                if (modelLoginResponse.getStatus() == 204) {
                    Toast.makeText(ActivityLogin.this, modelLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }
}
