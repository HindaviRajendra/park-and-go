package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterAttendantList;
import com.m_intellect.parkandgo1.model.ModelAttendantListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityAttendant extends AppCompatActivity implements View.OnClickListener {

    private FloatingActionButton fab;
    private RecyclerView attendantListRV;
    private SharedPreferences preferences;
    private AdapterAttendantList adapterAttendantList;
    public int ParkID;
    public String TAG = getClass().getSimpleName();
    public SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendant);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        Bundle b = getIntent().getExtras();
        if (b != null) {
            ParkID = b.getInt("parkID");
        }

        initToolbar();
        initView();

        if (Utility.isNetworkAvailable(this))
            getAttendantList();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAttendantList();
            }
        });

    }

    private void getAttendantList() {

        new WebServiceGet(this, new HandlerAttendantListResponse(), Constants.ATTENDANT_LIST_URL + ParkID + "/" + preferences.getInt(Constants.USER_ID, 0)).execute();
        Log.e("TAG", "URL>>>" + Constants.ATTENDANT_LIST_URL + ParkID + "/" + preferences.getInt(Constants.USER_ID, 0));
    }

    private class HandlerAttendantListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                ModelAttendantListResponse modelAttendantListResponse = new Gson().fromJson(response, ModelAttendantListResponse.class);

                if (modelAttendantListResponse.getStatus() == 200) {
                    if (adapterAttendantList == null) {
                        swipeRefreshLayout.setRefreshing(false);
                        adapterAttendantList = new AdapterAttendantList(ActivityAttendant.this, modelAttendantListResponse.getModelAttendantList());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityAttendant.this);
                        attendantListRV.setLayoutManager(mLayoutManager);
                        attendantListRV.setItemAnimator(new DefaultItemAnimator());
                        attendantListRV.setAdapter(adapterAttendantList);
                    } else {
                        adapterAttendantList.modelAttendantList = modelAttendantListResponse.getModelAttendantList();
                        adapterAttendantList.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(ActivityAttendant.this, modelAttendantListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initView() {
        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        attendantListRV = (RecyclerView) findViewById(R.id.attendantListRV);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Attendant");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
            case R.id.fab:
                Intent intent = new Intent(this, ActivityAddAttendant.class);
                intent.putExtra(Constants.FROM_EDIT, false);
                intent.putExtra("parkID", ParkID);
                startActivityForResult(intent, 111);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111 && resultCode == RESULT_OK) {
            if (Utility.isNetworkAvailable(this))
                getAttendantList();
            else
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }
}
