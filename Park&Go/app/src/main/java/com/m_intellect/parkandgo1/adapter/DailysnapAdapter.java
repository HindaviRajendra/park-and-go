package com.m_intellect.parkandgo1.adapter;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ViewsevendayResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DailysnapAdapter extends RecyclerView.Adapter<DailysnapAdapter.Snapholder> {

    public List<ViewsevendayResponse.DataBean> modelParkingResponseList;
    private Context context;
    private boolean isFromParkingList;
    private String Total_coll;
    private String TAG = getClass().getSimpleName();


    public DailysnapAdapter(List<ViewsevendayResponse.DataBean> modelParkingResponseList, Context context) {

        this.modelParkingResponseList = modelParkingResponseList;
        this.context = context;
    }


    @Override
    public DailysnapAdapter.Snapholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dailysnap_adpater, parent, false);

        return new DailysnapAdapter.Snapholder(itemView);

    }

    @Override
    public void onBindViewHolder(DailysnapAdapter.Snapholder holder, int position) {

        SimpleDateFormat sf1 = new SimpleDateFormat("");
        SimpleDateFormat sf2 = new SimpleDateFormat("");
        String date = modelParkingResponseList.get(position).getCreatedOn();
        date.replace("T00:00:00.000Z", "");
        Date date1 = null;

        Log.e(TAG,"Date ---->"+date);

        holder.tv_date.setText("Date : " + date);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date1 = inputFormat.parse(date);
            String outputDateStr = outputFormat.format(date1);
           // holder.tv_date.setText("Date : " + outputDateStr);
            holder.tv_sum.setText("Total : " + modelParkingResponseList.get(position).getSum());
        }
        catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return modelParkingResponseList.size();
    }

    public class Snapholder extends RecyclerView.ViewHolder {
        private TextView tv_date;
        private TextView tv_sum;


        public Snapholder(View itemView) {
            super(itemView);

            tv_date = (TextView) itemView.findViewById(R.id.date);
            tv_sum = (TextView) itemView.findViewById(R.id.sum);

        }
    }
}
