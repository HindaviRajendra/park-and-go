package com.m_intellect.parkandgo1.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.m_intellect.parkandgo1.R;

public class ActivityWallet extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        initToolbar();

       // LinearLayout addMoneyLayout = (LinearLayout) findViewById(R.id.addMoneyLayout);
        LinearLayout walletHistoryLayout = (LinearLayout) findViewById(R.id.walletHistoryLayout);
       // addMoneyLayout.setOnClickListener(this);
        walletHistoryLayout.setOnClickListener(this);
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("Wallet");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
         //   case R.id.addMoneyLayout:
               // startActivity(new Intent(ActivityWallet.this, ActivityAddMoneyToWallet.class));
          //      break;
            case R.id.walletHistoryLayout:
               // startActivity(new Intent(ActivityWallet.this, ActivityPaymentHistory.class));
                break;

            default:
                break;
        }
    }
}
