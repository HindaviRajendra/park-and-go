package com.m_intellect.parkandgo1.model;


import java.util.List;

/**
 * Created by sanket on 12/31/2017.
 */

public class ModelParkingHistoryResponse {
    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"booking_id":2507,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T14:39:42.000Z","end_date":"2018-07-26T14:39:42.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:40:00","otp":"","mobile_no":"","vehicle_no":"RG-6111","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2505,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T14:28:54.000Z","end_date":"2018-07-26T14:28:54.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:30:00","otp":"","mobile_no":"","vehicle_no":"YY-8080","attendant_name":"","advance_pay_hours":"1","advance_paid":"10","balance_cash":"0","vehicle_type":"Car"},{"booking_id":2504,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T14:23:52.000Z","end_date":"2018-07-26T14:23:52.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:24:00","otp":"","mobile_no":"","vehicle_no":"YH-3633","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2502,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T14:16:14.000Z","end_date":"2018-07-26T14:16:14.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:17:00","otp":"","mobile_no":"","vehicle_no":"HB-2525","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2500,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"12:52:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T12:52:43.000Z","end_date":"2018-07-26T12:52:43.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"YH-3636","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2497,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T12:33:04.000Z","end_date":"2018-07-26T12:33:04.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"12:34:00","otp":"","mobile_no":"","vehicle_no":"RE-5858","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2493,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T12:17:48.000Z","end_date":"2018-07-26T12:17:48.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"12:18:00","otp":"","mobile_no":"","vehicle_no":"UH-3636","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2491,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T12:11:42.000Z","end_date":"2018-07-26T12:11:42.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"12:13:00","otp":"","mobile_no":"","vehicle_no":"YY-3333","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2489,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T12:07:13.000Z","end_date":"2018-07-26T12:07:13.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"12:07:00","otp":"","mobile_no":"","vehicle_no":"UU-3333","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2488,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T11:45:12.000Z","end_date":"2018-07-26T11:45:12.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"11:47:00","otp":"","mobile_no":"","vehicle_no":"TT-1111","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2486,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-26T11:24:27.000Z","end_date":"2018-07-26T11:24:27.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"11:26:00","otp":"","mobile_no":"","vehicle_no":"VV-2525","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2485,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"18:58:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T18:58:50.000Z","end_date":"2018-07-25T18:58:50.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"GV-6699","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2476,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"16:53:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:53:09.000Z","end_date":"2018-07-25T16:53:09.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"BB-5959","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Truck"},{"booking_id":2475,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"16:51:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:51:22.000Z","end_date":"2018-07-25T16:51:22.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"HH-2623","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bus"},{"booking_id":2474,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"16:48:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:48:49.000Z","end_date":"2018-07-25T16:48:49.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"MH-6666","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2471,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"16:42:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:42:08.000Z","end_date":"2018-07-25T16:42:08.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"RE-2323","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2467,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:35:44.000Z","end_date":"2018-07-25T16:35:44.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"16:36:00","otp":"","mobile_no":"","vehicle_no":"AS-1111","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2466,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:24:51.000Z","end_date":"2018-07-25T16:24:51.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"16:25:00","otp":"","mobile_no":"","vehicle_no":"VV-2222","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2465,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T16:10:06.000Z","end_date":"2018-07-25T16:10:06.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"16:10:00","otp":"","mobile_no":"","vehicle_no":"BV-1111","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2464,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:51:13.000Z","end_date":"2018-07-25T15:51:13.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"15:51:00","otp":"","mobile_no":"","vehicle_no":"FV-5656","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2463,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:43:18.000Z","end_date":"2018-07-25T15:43:18.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"15:43:00","otp":"","mobile_no":"","vehicle_no":"UU-8888","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2462,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:36:58.000Z","end_date":"2018-07-25T15:36:58.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"15:37:00","otp":"","mobile_no":"","vehicle_no":"UU-2222","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2461,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:35:26.000Z","end_date":"2018-07-25T15:35:26.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"15:35:00","otp":"","mobile_no":"","vehicle_no":"RR-2478","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2460,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:29:09.000Z","end_date":"2018-07-25T15:29:09.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"15:29:00","otp":"","mobile_no":"","vehicle_no":"RT-5151","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2459,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T15:19:44.000Z","end_date":"2018-07-25T15:19:44.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"15:20:00","otp":"","mobile_no":"","vehicle_no":"YG-3939","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2458,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:48:44.000Z","end_date":"2018-07-25T14:48:44.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:49:00","otp":"","mobile_no":"","vehicle_no":"GH-8080","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2457,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"02:43:00","arrival_time":"02:43:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:43:17.000Z","end_date":"2018-07-25T14:43:17.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:44:00","otp":"","mobile_no":"","vehicle_no":"TY-6666","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"0","vehicle_type":"Car"},{"booking_id":2456,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:36:16.000Z","end_date":"2018-07-25T14:36:16.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:38:00","otp":"","mobile_no":"","vehicle_no":"RR-2524","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"0","vehicle_type":"Car"},{"booking_id":2455,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":40,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:32:10.000Z","end_date":"2018-07-25T14:32:10.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:32:00","otp":"","mobile_no":"","vehicle_no":"GC-5555","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"40","vehicle_type":"Car"},{"booking_id":2454,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:30:49.000Z","end_date":"2018-07-25T14:30:49.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:31:00","otp":"","mobile_no":"","vehicle_no":"GH-5444","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"0","vehicle_type":"Car"},{"booking_id":2453,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":40,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:15:28.000Z","end_date":"2018-07-25T14:15:28.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:15:00","otp":"","mobile_no":"","vehicle_no":"GG-6868","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"40","vehicle_type":"Bike"},{"booking_id":2452,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T14:13:46.000Z","end_date":"2018-07-25T14:13:46.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"14:14:00","otp":"","mobile_no":"","vehicle_no":"YY-6666","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"0","vehicle_type":"Bike"},{"booking_id":2451,"app_id":29,"parking_id":212,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:57:35.000Z","end_date":"2018-07-25T12:57:35.000Z","place_name":"Mahakali Caves","place_landmark":"WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India","city":"Mumbai","state":"Maharashtra","leave_time":"12:58:00","otp":"","mobile_no":"","vehicle_no":"GB-6666","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"10","vehicle_type":"Bike"},{"booking_id":2450,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:51:08.000Z","end_date":"2018-07-25T12:51:08.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:51:00","otp":"","mobile_no":"","vehicle_no":"YY-2525","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2449,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:47:39.000Z","end_date":"2018-07-25T12:47:39.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:48:00","otp":"","mobile_no":"","vehicle_no":"HH-3333","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2448,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:43:20.000Z","end_date":"2018-07-25T12:43:20.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:43:00","otp":"","mobile_no":"","vehicle_no":"HN-6666","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2447,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:42:06.000Z","end_date":"2018-07-25T12:42:06.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:42:00","otp":"","mobile_no":"","vehicle_no":"GG-3333","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2446,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:39:19.000Z","end_date":"2018-07-25T12:39:19.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:39:00","otp":"","mobile_no":"","vehicle_no":"EE-5454","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2445,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-25T12:26:34.000Z","end_date":"2018-07-25T12:26:34.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"12:28:00","otp":"","mobile_no":"","vehicle_no":"FF-2222","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2442,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-24T18:43:18.000Z","end_date":"2018-07-24T18:43:18.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"18:43:00","otp":"","mobile_no":"","vehicle_no":"EE-1111","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2441,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"01:00:00","booking_charges":10,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-24T18:35:42.000Z","end_date":"2018-07-24T18:35:42.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"18:37:00","otp":"","mobile_no":"","vehicle_no":"RR-2528","attendant_name":"","advance_pay_hours":"","advance_paid":"0","balance_cash":"10","vehicle_type":"Car"},{"booking_id":2440,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"18:28:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-24T18:28:06.000Z","end_date":"2018-07-24T18:28:06.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"RR-2525","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2437,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"16:18:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-24T16:20:24.000Z","end_date":"2018-07-24T16:20:24.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"GH-2222","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2436,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"16:56:00","arrival_time":"16:56:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T04:56:34.000Z","end_date":"2018-07-18T04:56:34.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TP 7846","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2435,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"16:41:00","arrival_time":"16:41:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T04:41:34.000Z","end_date":"2018-07-18T04:41:34.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 3285","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2434,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"16:41:00","arrival_time":"16:41:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T04:41:34.000Z","end_date":"2018-07-18T04:41:34.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 3284","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2433,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:22:00","arrival_time":"12:22:00","booking_charges":50,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T12:22:28.000Z","end_date":"2018-07-18T12:22:28.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"tg 6752","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2432,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:58:00","arrival_time":"11:58:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:58:31.000Z","end_date":"2018-07-18T11:58:31.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 6670","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2431,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:58:00","arrival_time":"11:58:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:58:31.000Z","end_date":"2018-07-18T11:58:31.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 6679","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2430,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:45:00","arrival_time":"11:45:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:45:17.000Z","end_date":"2018-07-18T11:45:17.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"tg 1446","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2429,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:36:00","arrival_time":"11:36:00","booking_charges":50,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:36:02.000Z","end_date":"2018-07-18T11:36:02.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 5688","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2428,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:05:00","arrival_time":"11:05:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:05:16.000Z","end_date":"2018-07-18T11:05:16.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4672","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2427,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:05:00","arrival_time":"11:05:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:05:16.000Z","end_date":"2018-07-18T11:05:16.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4678","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2426,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:02:00","arrival_time":"11:02:00","booking_charges":60,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T11:02:58.000Z","end_date":"2018-07-18T11:02:58.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 2234","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2425,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"10:43:00","arrival_time":"10:43:00","booking_charges":50,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T10:43:21.000Z","end_date":"2018-07-18T10:43:21.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 1222","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2424,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"10:43:00","arrival_time":"10:43:00","booking_charges":50,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-18T10:43:21.000Z","end_date":"2018-07-18T10:43:21.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 1221","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"},{"booking_id":2423,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"18:21:00","arrival_time":"18:21:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T06:21:47.000Z","end_date":"2018-07-17T06:21:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AG 5678","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2422,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"18:18:00","arrival_time":"18:18:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T06:18:44.000Z","end_date":"2018-07-17T06:18:44.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AH 7788","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2421,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"18:15:00","arrival_time":"18:15:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T06:15:58.000Z","end_date":"2018-07-17T06:15:58.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"GH 7788","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2420,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"18:12:00","arrival_time":"18:12:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T06:12:44.000Z","end_date":"2018-07-17T06:12:44.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"GH 6789","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2419,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"18:09:00","arrival_time":"18:09:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T06:09:58.000Z","end_date":"2018-07-17T06:09:58.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TH 1567","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2418,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"16:11:00","arrival_time":"16:11:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T04:11:51.000Z","end_date":"2018-07-17T04:11:51.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 6677","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2417,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:58:00","arrival_time":"15:58:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:58:07.000Z","end_date":"2018-07-17T03:58:07.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 5678","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2416,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:51:00","arrival_time":"15:51:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:51:20.000Z","end_date":"2018-07-17T03:51:20.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AP 6565","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2415,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:48:00","arrival_time":"15:48:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:48:35.000Z","end_date":"2018-07-17T03:48:35.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 6678","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2412,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4562","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2411,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4566","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2410,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4565","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2409,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4564","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2408,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4563","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2407,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"15:02:00","arrival_time":"15:02:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T03:02:54.000Z","end_date":"2018-07-17T03:02:54.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 4567","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2406,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:59:00","arrival_time":"14:59:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:59:17.000Z","end_date":"2018-07-17T02:59:17.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 6778","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2404,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:53:00","arrival_time":"14:53:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:53:45.000Z","end_date":"2018-07-17T02:53:45.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 3567","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2403,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:46:00","arrival_time":"14:46:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:46:56.000Z","end_date":"2018-07-17T02:46:56.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7669","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2402,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:46:00","arrival_time":"14:46:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:46:56.000Z","end_date":"2018-07-17T02:46:56.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7667","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2401,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:46:00","arrival_time":"14:46:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:46:56.000Z","end_date":"2018-07-17T02:46:56.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7665","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2400,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"14:12:00","arrival_time":"14:12:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T02:12:22.000Z","end_date":"2018-07-17T02:12:22.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 2567","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2399,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:50:00","arrival_time":"12:50:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:50:25.000Z","end_date":"2018-07-17T12:50:25.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7676","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2398,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:50:00","arrival_time":"12:50:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:50:25.000Z","end_date":"2018-07-17T12:50:25.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7678","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2397,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:50:00","arrival_time":"12:50:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:50:25.000Z","end_date":"2018-07-17T12:50:25.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 7677","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2396,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:39:00","arrival_time":"12:39:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:39:47.000Z","end_date":"2018-07-17T12:39:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TS 5569","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2395,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:39:00","arrival_time":"12:39:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:39:47.000Z","end_date":"2018-07-17T12:39:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TS 5565","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2394,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:39:00","arrival_time":"12:39:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:39:47.000Z","end_date":"2018-07-17T12:39:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TS 5568","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2393,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:39:00","arrival_time":"12:39:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:39:47.000Z","end_date":"2018-07-17T12:39:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TS 5562","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2386,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:39:00","arrival_time":"12:39:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:39:47.000Z","end_date":"2018-07-17T12:39:47.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TS 5566","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2385,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:35:00","arrival_time":"12:35:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:35:35.000Z","end_date":"2018-07-17T12:35:35.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AB 4567","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2384,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:35:00","arrival_time":"12:35:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:35:35.000Z","end_date":"2018-07-17T12:35:35.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AB 4566","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2383,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"12:27:00","arrival_time":"00:54:00","booking_charges":105,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T12:27:13.000Z","end_date":"2018-07-17T12:27:13.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"TG 5445","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2372,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:45:00","arrival_time":"11:45:00","booking_charges":80,"booking_type":"reserved","payment_type":"Cash","start_date":"2018-07-17T11:45:12.000Z","end_date":"2018-07-17T11:45:12.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"GR 5675","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bus"},{"booking_id":2371,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"11:45:00","arrival_time":"11:45:00","booking_charges":100,"booking_type":"general","payment_type":"Cash","start_date":"2018-07-17T11:45:12.000Z","end_date":"2018-07-17T11:45:12.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"AV 1365","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2346,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"18:09:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-02T18:10:04.000Z","end_date":"2018-07-02T18:10:04.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"BD-6656","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bus"},{"booking_id":2345,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"18:08:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-02T18:08:45.000Z","end_date":"2018-07-02T18:08:45.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"BD-5999","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Bike"},{"booking_id":2344,"app_id":29,"parking_id":184,"user_id":314,"booking_time":"01:00:00","arrival_time":"18:08:00","booking_charges":0,"booking_type":"On Spot","payment_type":"Cash","start_date":"2018-07-02T18:08:24.000Z","end_date":"2018-07-02T18:08:24.000Z","place_name":"inocx","place_landmark":"A-41, Andheri - Kurla Rd, Central Park, Mota Nagar, Andheri East, Mumbai, Maharashtra 400059, India","city":"mumbai","state":"Maharashtra","leave_time":"00:00:00","otp":"","mobile_no":"","vehicle_no":"MH-9559","attendant_name":"","advance_pay_hours":"0","advance_paid":"0","balance_cash":"","vehicle_type":"Car"}]
     */

    private int Status;
    private boolean Success;
    private String Message;
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * booking_id : 2507
         * app_id : 29
         * parking_id : 212
         * user_id : 314
         * booking_time : 01:00:00
         * arrival_time : 01:00:00
         * booking_charges : 10
         * booking_type : On Spot
         * payment_type : Cash
         * start_date : 2018-07-26T14:39:42.000Z
         * end_date : 2018-07-26T14:39:42.000Z
         * place_name : Mahakali Caves
         * place_landmark : WING A-603, Mahakali Caves Rd, Sunder Nagar, Jogeshwari East, Mumbai, Maharashtra 400093, India
         * city : Mumbai
         * state : Maharashtra
         * leave_time : 14:40:00
         * otp :
         * mobile_no :
         * vehicle_no : RG-6111
         * attendant_name :
         * advance_pay_hours :
         * advance_paid : 0
         * balance_cash : 10
         * vehicle_type : Car
         */

        private int booking_id;
        private int app_id;
        private int parking_id;
        private int user_id;
        private String booking_time;
        private String arrival_time;
        private int booking_charges;
        private String booking_type;
        private String payment_type;
        private String start_date;
        private String end_date;
        private String place_name;
        private String place_landmark;
        private String city;
        private String state;
        private String leave_time;
        private String otp;
        private String mobile_no;
        private String vehicle_no;
        private String attendant_name;
        private String advance_pay_hours;
        private String advance_paid;
        private String balance_cash;
        private String vehicle_type;
        public String total_time;
        private String booking_status;
        private String online;
        private int booking_history;
        private int cancle;
        private String rsf;
        private String real_time;

        public String getReal_time() {
            return real_time;
        }

        public DataBean setReal_time(String real_time) {
            this.real_time = real_time;
            return this;
        }

        public String getRsf() {
            return rsf;
        }

        public DataBean setRsf(String rsf) {
            this.rsf = rsf;
            return this;
        }

        public int getCancle() {
            return cancle;
        }

        public DataBean setCancle(int cancle) {
            this.cancle = cancle;
            return this;
        }

        public int getBooking_history() {
            return booking_history;
        }

        public DataBean setBooking_history(int booking_history) {
            this.booking_history = booking_history;
            return this;
        }

        public String getOnline() {
            return online;
        }

        public DataBean setOnline(String online) {
            this.online = online;
            return this;
        }

        public String getBooking_status() {
            return booking_status;
        }

        public DataBean setBooking_status(String booking_status) {
            this.booking_status = booking_status;
            return this;
        }

        public String getTotal_time() {
            return total_time;
        }

        public DataBean setTotal_time(String total_time) {
            this.total_time = total_time;
            return this;
        }

        public int getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(int booking_id) {
            this.booking_id = booking_id;
        }

        public int getApp_id() {
            return app_id;
        }

        public void setApp_id(int app_id) {
            this.app_id = app_id;
        }

        public int getParking_id() {
            return parking_id;
        }

        public void setParking_id(int parking_id) {
            this.parking_id = parking_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getBooking_time() {
            return booking_time;
        }

        public void setBooking_time(String booking_time) {
            this.booking_time = booking_time;
        }

        public String getArrival_time() {
            return arrival_time;
        }

        public void setArrival_time(String arrival_time) {
            this.arrival_time = arrival_time;
        }

        public int getBooking_charges() {
            return booking_charges;
        }

        public void setBooking_charges(int booking_charges) {
            this.booking_charges = booking_charges;
        }

        public String getBooking_type() {
            return booking_type;
        }

        public void setBooking_type(String booking_type) {
            this.booking_type = booking_type;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public String getPlace_name() {
            return place_name;
        }

        public void setPlace_name(String place_name) {
            this.place_name = place_name;
        }

        public String getPlace_landmark() {
            return place_landmark;
        }

        public void setPlace_landmark(String place_landmark) {
            this.place_landmark = place_landmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLeave_time() {
            return leave_time;
        }

        public void setLeave_time(String leave_time) {
            this.leave_time = leave_time;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public String getAttendant_name() {
            return attendant_name;
        }

        public void setAttendant_name(String attendant_name) {
            this.attendant_name = attendant_name;
        }

        public String getAdvance_pay_hours() {
            return advance_pay_hours;
        }

        public void setAdvance_pay_hours(String advance_pay_hours) {
            this.advance_pay_hours = advance_pay_hours;
        }

        public String getAdvance_paid() {
            return advance_paid;
        }

        public void setAdvance_paid(String advance_paid) {
            this.advance_paid = advance_paid;
        }

        public String getBalance_cash() {
            return balance_cash;
        }

        public void setBalance_cash(String balance_cash) {
            this.balance_cash = balance_cash;
        }

        public String getVehicle_type() {
            return vehicle_type;
        }

        public void setVehicle_type(String vehicle_type) {
            this.vehicle_type = vehicle_type;
        }
    }

   /* @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("parking_id")
    @Expose
    private Integer parkingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("booking_charges")
    @Expose
    private Integer bookingCharges;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("place_name")
    @Expose
    private String placeName;
    @SerializedName("place_landmark")
    @Expose
    private String placeLandmark;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("leave_time")
    @Expose
    private String leaveTime;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("attendant_name")
    @Expose
    private String attendantName;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getBookingCharges() {
        return bookingCharges;
    }

    public void setBookingCharges(Integer bookingCharges) {
        this.bookingCharges = bookingCharges;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLandmark() {
        return placeLandmark;
    }

    public void setPlaceLandmark(String placeLandmark) {
        this.placeLandmark = placeLandmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getAttendantName() {
        return attendantName;
    }

    public void setAttendantName(String attendantName) {
        this.attendantName = attendantName;
    }*/


}

