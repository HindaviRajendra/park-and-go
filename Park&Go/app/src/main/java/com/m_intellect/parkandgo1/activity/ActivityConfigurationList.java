package com.m_intellect.parkandgo1.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterConfigurationList;
import com.m_intellect.parkandgo1.model.ModelConfigurationListResponse;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

public class ActivityConfigurationList extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView configurationListRV;

    private SharedPreferences preferences;

    private AdapterConfigurationList adapterConfigurationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_list);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initToolbar();

        initView();

        if (Utility.isNetworkAvailable(this))
            getConfigurationList();
        else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

    }

    private void getConfigurationList() {
        new WebServiceGet(this, new HandlerConfigurationListResponse(), Constants.PARKING_CONFIGURATION_LIST_URL +
                "null/" + preferences.getInt(Constants.USER_ID, 0)).execute();
        Log.e("Configuration URL>>>",""+Constants.PARKING_CONFIGURATION_LIST_URL +
                "null/" + preferences.getInt(Constants.USER_ID, 0));
    }

    private class HandlerConfigurationListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelConfigurationListResponse modelConfigurationListResponse = new Gson().fromJson(response, ModelConfigurationListResponse.class);

                if (modelConfigurationListResponse.getStatus() == 200) {
                    if (adapterConfigurationList == null) {
                        adapterConfigurationList = new AdapterConfigurationList(ActivityConfigurationList.this, modelConfigurationListResponse.getModelConfigurationList());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivityConfigurationList.this);
                        configurationListRV.setLayoutManager(mLayoutManager);
                        configurationListRV.setItemAnimator(new DefaultItemAnimator());
                        configurationListRV.setAdapter(adapterConfigurationList);
                    } else {

                        adapterConfigurationList.modelConfigurationList = modelConfigurationListResponse.getModelConfigurationList();
                        adapterConfigurationList.notifyDataSetChanged();
                    }


                } else {

                    Toast.makeText(ActivityConfigurationList.this, modelConfigurationListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("My Parking Configuration");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);
        titleLayout.setVisibility(View.VISIBLE);
        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    private void initView() {

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        configurationListRV = (RecyclerView) findViewById(R.id.configurationListRV);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111 && resultCode == RESULT_OK) {
            if (Utility.isNetworkAvailable(this))
                getConfigurationList();
            else
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

}
