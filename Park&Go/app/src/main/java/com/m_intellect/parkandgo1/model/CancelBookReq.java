package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 14-03-2018.
 */

public class CancelBookReq {


    /**
     * app_id : 29
     * booking_id : 738
     * parking_id : [92]
     * user_id : 27
     */

    @SerializedName("app_id")
    private int appId;
    @SerializedName("booking_id")
    private int bookingId;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("parking_id")
    private List<Integer> parkingId;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Integer> getParkingId() {
        return parkingId;
    }

    public void setParkingId(List<Integer> parkingId) {
        this.parkingId = parkingId;
    }
}
