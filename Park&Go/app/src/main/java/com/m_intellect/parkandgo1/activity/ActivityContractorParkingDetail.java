package com.m_intellect.parkandgo1.activity;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelGraphDetailResponse;
import com.m_intellect.parkandgo1.model.ModelParkingResponse;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.model.ParkconfigResponse;
import com.m_intellect.parkandgo1.model.SingletonContractorParkingDetail;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.util.ArrayList;
import java.util.List;

public class ActivityContractorParkingDetail extends AppCompatActivity implements View.OnClickListener {

    private String placeName;
    private int parkingID;

    private TextView generalSeatsTV;
    private TextView reservedSeatsTV;

    private TextView gzerotooneTV;
    private TextView gtwotothreeTV;
    private TextView gthreetosixTV;
    private TextView gsixtotwelveTV;

    private TextView gfourtofiveTV;
    private TextView gfivetosixTV;
    private TextView gsixtosevenTV;
    private TextView gsevtoeightTV;

    private TextView rzerotooneTV;
    private TextView rtwotothreeTV;
    private TextView rthreetosixTV;
    private TextView rsixtotwelveTV;
    private TextView rfourtofiveTV;
    private TextView rfivetosixTV;
    private TextView rsixtosevenTV;
    private TextView rsevtoeightTV;


    private TextView pzerotooneTV;
    private TextView ptwotothreeTV;
    private TextView pthreetosixTV;
    private TextView psixtotwelveTV;
    private TextView pfourtofiveTV;
    private TextView pfivetosixTV;
    private TextView psixtosevenTV;
    private TextView psevtoeightTV;


    private TextView hours0TV;
    private TextView hours1TV;
    private TextView hours2TV;
    private TextView hours3TV;

    private TextView hours4TV;
    private TextView hours5TV;
    private TextView hours6TV;
    private TextView hours7TV;

    private int parkingId;
    RelativeLayout r_spin;

    // private LineChart lineChart;
    public String TAG = getClass().getSimpleName();
    private SharedPreferences preferences;
    List<ParkconfigResponse.DataBean.PriceValuesBean> parkconfiglist = new ArrayList<>();
    ParkConfigClass parkConfigClass;
    Spinner spinner;
    private String v_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_parking_detail);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (getIntent().getExtras() != null) {
            placeName = getIntent().getExtras().getString("PARK_NAME");
            parkingID = getIntent().getExtras().getInt("PARK_ID");
            Log.e(TAG, "PARKING ID  ----- > " + parkingID);
            Log.e(TAG, "PARK NAME   ----- > " + placeName);
        }

        initToolbar();
        initView();
        setData();

        // new WebServiceGet(this, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + preferences.getInt(Constants.USER_ID, 0)).execute();
        //Log.e(TAG, "URL>>>" + Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + preferences.getInt(Constants.USER_ID, 0));

        if (Utility.isNetworkAvailable(this)) {
            getGraphDetail();
        } else
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

    }

    private void getGraphDetail() {
        //  new WebServiceGet(this, new HandlerGraphDetailResponse(), Constants.VIEW_GRAPH_URL + "/null" + parkingId).execute();
        new WebServiceGet(this, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null").execute();
        Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + parkingID + "/" + "null");

    }

    private class HandlerGraphDetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelGraphDetailResponse modelGraphDetailResponse = new Gson().fromJson(response, ModelGraphDetailResponse.class);

                if (modelGraphDetailResponse.getStatus() == 200) {
//                    LineData data = new LineData(getXAxisValues(), getDataSet(modelGraphDetailResponse.getGraphDetailResponseList()));
//                    lineChart.setData(data);
//                    lineChart.setDescription("My Chart");
//                    lineChart.animateXY(2000, 2000);
//                    lineChart.invalidate();
                } else {
                    Toast.makeText(ActivityContractorParkingDetail.this, modelGraphDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);

            if (response != null) {

                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {

                    try {
                        Log.e(TAG, "Hour: " + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom());
                        if (v_type.equals("Car")) {
                            hours0TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getTo());
                            hours1TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(1).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(1).getTo());
                            hours2TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(2).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(2).getTo());
                            hours3TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(3).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(3).getTo());
                            hours4TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(4).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(4).getTo());
                            hours5TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(5).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(5).getTo());
                            hours6TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(6).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(6).getTo());
                            hours7TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(7).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(7).getTo());


                            gzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getPriceG());
                            gtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(1).getPriceG());
                            gthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(2).getPriceG());
                            gsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(3).getPriceG());
                            gfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(4).getPriceG());
                            gfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(5).getPriceG());
                            gsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(6).getPriceG());
                            gsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(7).getPriceG());

                            rzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getPariceR());
                            rtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(1).getPariceR());
                            rthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(2).getPariceR());
                            rsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(3).getPariceR());
                            rfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(4).getPariceR());
                            rfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(5).getPariceR());
                            rsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(6).getPariceR());
                            rsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(7).getPariceR());

                            pzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getPriceP());
                            ptwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(1).getPriceP());
                            pthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(2).getPriceP());
                            psixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(3).getPriceP());
                            pfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(4).getPriceP());
                            pfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(5).getPriceP());
                            psixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(6).getPriceP());
                            psevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(7).getPriceP());

                        } else if (v_type.equals("Bike")) {
                            hours0TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(0).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(0).getTo());
                            hours1TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(1).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(1).getTo());
                            hours2TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(2).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(2).getTo());
                            hours3TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(3).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(3).getTo());
                            hours4TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(4).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(4).getTo());
                            hours5TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(5).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(5).getTo());
                            hours6TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(6).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(6).getTo());
                            hours7TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(7).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(7).getTo());

                            gzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(0).getPriceG());
                            gtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(1).getPriceG());
                            gthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(2).getPriceG());
                            gsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(3).getPriceG());

                            gfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(4).getPriceG());
                            gfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(5).getPriceG());
                            gsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(6).getPriceG());
                            gsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(7).getPriceG());
//
                            rzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(0).getPariceR());
                            rtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(1).getPariceR());
                            rthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(2).getPariceR());
                            rsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(3).getPariceR());
                            rfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(4).getPariceR());
                            rfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(5).getPariceR());
                            rsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(6).getPariceR());
                            rsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(7).getPariceR());
//
                            pzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(0).getPriceP());
                            ptwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(1).getPriceP());
                            pthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(2).getPriceP());
                            psixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(3).getPriceP());
                            pfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(4).getPriceP());
                            pfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(5).getPriceP());
                            psixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(6).getPriceP());
                            psevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBikePriceValues().get(7).getPriceP());
                        } else if (v_type.equals("Bus")) {

                            hours0TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(0).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(0).getTo());
                            hours1TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(1).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(1).getTo());
                            hours2TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(2).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(2).getTo());
                            hours3TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(3).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(3).getTo());
                            hours4TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(4).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(4).getTo());
                            hours5TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(5).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(5).getTo());
                            hours6TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(6).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(6).getTo());
                            hours7TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(7).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(7).getTo());

                            gzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(0).getPriceG());
                            gtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(1).getPriceG());
                            gthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(2).getPriceG());
                            gsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(3).getPriceG());
                            gfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(4).getPriceG());
                            gfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(5).getPriceG());
                            gsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(6).getPriceG());
                            gsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(7).getPriceG());

                            rzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(0).getPariceR());
                            rtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(1).getPariceR());
                            rthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(2).getPariceR());
                            rsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(3).getPariceR());
                            rfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(4).getPariceR());
                            rfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(5).getPariceR());
                            rsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(6).getPariceR());
                            rsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(7).getPariceR());
//
                            pzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(0).getPriceP());
                            ptwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(1).getPriceP());
                            pthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(2).getPriceP());
                            psixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(3).getPriceP());
                            pfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(4).getPriceP());
                            pfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(5).getPriceP());
                            psixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(6).getPriceP());
                            psevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getBusPriceValues().get(7).getPriceP());
                        } else if (v_type.equals("Truck")) {
                            hours0TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(0).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(0).getTo());
                            hours1TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(1).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(1).getTo());
                            hours2TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(2).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(2).getTo());
                            hours3TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(3).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(3).getTo());
                            hours4TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(4).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(4).getTo());
                            hours5TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(5).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(5).getTo());
                            hours6TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(6).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(6).getTo());
                            hours7TV.setText(parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(7).getFrom() + "-" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(7).getTo());

                            gzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(0).getPriceG());
                            gtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(1).getPriceG());
                            gthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(2).getPriceG());
                            gsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(3).getPriceG());
                            gfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(4).getPriceG());
                            gfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(5).getPriceG());
                            gsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(6).getPriceG());
                            gsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(7).getPriceG());
//
                            rzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(0).getPariceR());
                            rtwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(1).getPariceR());
                            rthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(2).getPariceR());
                            rsixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(3).getPariceR());
                            rfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(4).getPariceR());
                            rfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(5).getPariceR());
                            rsixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(6).getPariceR());
                            rsevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(7).getPariceR());
//
                            pzerotooneTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(0).getPriceP());
                            ptwotothreeTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(1).getPriceP());
                            pthreetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(2).getPriceP());
                            psixtotwelveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(3).getPriceP());
                            pfourtofiveTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(4).getPriceP());
                            pfivetosixTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(5).getPriceP());
                            psixtosevenTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(6).getPriceP());
                            psevtoeightTV.setText("₹" + parkConfigClass.getParkingConfigResponseList().get(0).getTruckPriceValues().get(7).getPriceP());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ActivityContractorParkingDetail.this, "Today date Configuration is not available", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ActivityContractorParkingDetail.this, "Today date Configuration is not available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setData() {

        try {
            ModelParkingResponse modelParkingResponse = SingletonContractorParkingDetail.getInstance().getModelParkingResponse();

            parkingId = modelParkingResponse.getParkingId();

            hours0TV.setText(modelParkingResponse.getPriceValues().get(0).getFrom() + "-" + modelParkingResponse.getPriceValues().get(0).getTo());
            hours1TV.setText(modelParkingResponse.getPriceValues().get(1).getFrom() + "-" + modelParkingResponse.getPriceValues().get(1).getTo());
            hours2TV.setText(modelParkingResponse.getPriceValues().get(2).getFrom() + "-" + modelParkingResponse.getPriceValues().get(2).getTo());
            hours3TV.setText(modelParkingResponse.getPriceValues().get(3).getFrom() + "-" + modelParkingResponse.getPriceValues().get(3).getTo());

            hours4TV.setText(modelParkingResponse.getPriceValues().get(4).getFrom() + "-" + modelParkingResponse.getPriceValues().get(4).getTo());
            hours5TV.setText(modelParkingResponse.getPriceValues().get(5).getFrom() + "-" + modelParkingResponse.getPriceValues().get(5).getTo());
            hours6TV.setText(modelParkingResponse.getPriceValues().get(6).getFrom() + "-" + modelParkingResponse.getPriceValues().get(6).getTo());
            hours7TV.setText(modelParkingResponse.getPriceValues().get(7).getFrom() + "-" + modelParkingResponse.getPriceValues().get(7).getTo());


            gzerotooneTV.setText("₹" + modelParkingResponse.getPriceValues().get(0).getPriceG());
            gtwotothreeTV.setText("₹" + modelParkingResponse.getPriceValues().get(1).getPriceG());
            gthreetosixTV.setText("₹" + modelParkingResponse.getPriceValues().get(2).getPriceG());
            gsixtotwelveTV.setText("₹" + modelParkingResponse.getPriceValues().get(3).getPriceG());


            rzerotooneTV.setText("₹" + modelParkingResponse.getPriceValues().get(0).getPariceR());
            rtwotothreeTV.setText("₹" + modelParkingResponse.getPriceValues().get(1).getPariceR());
            rthreetosixTV.setText("₹" + modelParkingResponse.getPriceValues().get(2).getPariceR());
            rsixtotwelveTV.setText("₹" + modelParkingResponse.getPriceValues().get(3).getPariceR());

            pzerotooneTV.setText("₹" + modelParkingResponse.getPriceValues().get(0).getPriceP());
            ptwotothreeTV.setText("₹" + modelParkingResponse.getPriceValues().get(1).getPriceP());
            pthreetosixTV.setText("₹" + modelParkingResponse.getPriceValues().get(2).getPriceP());
            psixtotwelveTV.setText("₹" + modelParkingResponse.getPriceValues().get(3).getPriceP());
        } catch (Exception e) {
            Log.e(TAG, "++++" + e.getMessage());
        }
    }

    private void initView() {

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);


        generalSeatsTV = (TextView) findViewById(R.id.generalSeatsTV);
        reservedSeatsTV = (TextView) findViewById(R.id.reservedSeatsTV);

        gzerotooneTV = (TextView) findViewById(R.id.gzerotooneTV);
        gtwotothreeTV = (TextView) findViewById(R.id.gtwotothreeTV);
        gthreetosixTV = (TextView) findViewById(R.id.gthreetosixTV);
        gsixtotwelveTV = (TextView) findViewById(R.id.gsixtotwelveTV);
        gfourtofiveTV = (TextView) findViewById(R.id.gfourtofiveTV);
        gfivetosixTV = (TextView) findViewById(R.id.gfivetosixTV);
        gsixtosevenTV = (TextView) findViewById(R.id.gsixtosevTV);
        gsevtoeightTV = (TextView) findViewById(R.id.gsevtoeightTV);

        rzerotooneTV = (TextView) findViewById(R.id.rzerotooneTV);
        rtwotothreeTV = (TextView) findViewById(R.id.rtwotothreeTV);
        rthreetosixTV = (TextView) findViewById(R.id.rthreetosixTV);
        rsixtotwelveTV = (TextView) findViewById(R.id.rsixtotwelveTV);
        rzerotooneTV = (TextView) findViewById(R.id.rzerotooneTV);
        rtwotothreeTV = (TextView) findViewById(R.id.rtwotothreeTV);
        rthreetosixTV = (TextView) findViewById(R.id.rthreetosixTV);
        rfourtofiveTV = (TextView) findViewById(R.id.rfourtofiveTV);
        rfivetosixTV = (TextView) findViewById(R.id.rfivetosixTV);
        rsixtosevenTV = (TextView) findViewById(R.id.rsixtosevTV);
        rsevtoeightTV = (TextView) findViewById(R.id.rsevtoeightTV);


        pzerotooneTV = (TextView) findViewById(R.id.pzerotooneTV);
        ptwotothreeTV = (TextView) findViewById(R.id.ptwotothreeTV);
        pthreetosixTV = (TextView) findViewById(R.id.pthreetosixTV);
        psixtotwelveTV = (TextView) findViewById(R.id.psixtotwelveTV);
        pfourtofiveTV = (TextView) findViewById(R.id.pfourtofiveTV);
        pfivetosixTV = (TextView) findViewById(R.id.pfivetosixTV);
        psixtosevenTV = (TextView) findViewById(R.id.psixtosevTV);
        psevtoeightTV = (TextView) findViewById(R.id.psevtoeightTV);

        hours0TV = (TextView) findViewById(R.id.hours0TV);
        hours1TV = (TextView) findViewById(R.id.hours1TV);
        hours2TV = (TextView) findViewById(R.id.hours2TV);
        hours3TV = (TextView) findViewById(R.id.hours3TV);
        hours4TV = (TextView) findViewById(R.id.hours4TV);
        hours5TV = (TextView) findViewById(R.id.hours5TV);
        hours6TV = (TextView) findViewById(R.id.hours6TV);
        hours7TV = (TextView) findViewById(R.id.hours7TV);

        //  lineChart = (LineChart) findViewById(R.id.lineChart);

        spinner = (Spinner) findViewById(R.id.spin_vtype);
        spinner.setVisibility(View.VISIBLE);
        setSpinner();

    }

    private void setSpinner() {
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Car");
        categories.add("Bike");
        categories.add("Bus");
        categories.add("Truck");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                v_type = parent.getItemAtPosition(position).toString();
                Log.e(TAG, "V type ::" + v_type);
                getGraphDetail();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText(placeName);
        titleTV.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        ImageView Attendance = (ImageView) toolbar.findViewById(R.id.attendance);
        Attendance.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backArrowIV:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("00");
        xAxis.add("02");
        xAxis.add("04");
        xAxis.add("06");
        xAxis.add("08");
        xAxis.add("10");
        xAxis.add("12");
        xAxis.add("14");
        xAxis.add("16");
        xAxis.add("18");
        xAxis.add("20");
        xAxis.add("22");
        xAxis.add("24");
        return xAxis;
    }

//    private LineDataSet getDataSet(List<GraphDetailResponse> graphDetailResponseList) {
//        ArrayList<Entry> entries = new ArrayList<>();
//        for (int i = 0; i < graphDetailResponseList.size(); i++) {
//            entries.add(new Entry(graphDetailResponseList.get(i).getLive(), i));
//        }
//
//        return new LineDataSet(entries, "Parking Time");
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
        }
        return true;
    }

}
