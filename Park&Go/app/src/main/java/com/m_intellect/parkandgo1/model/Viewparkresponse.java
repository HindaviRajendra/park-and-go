package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 28-02-2018.
 */

public class Viewparkresponse {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_id":175,"app_id":29,"coordinates":{"x":19.1138407244508,"y":72.8940034657717},"place_name":"moment","place_landmark":"IBU","city":"Mumbai","state":"Maharashtra","pincode":253525,"mobile_no":"8879388950","email":"sandip19july@gmail.com","parking_type":null,"start_date":null,"end_date":null,"start_time":null,"end_time":null,"total_space":300,"vacant":300,"general":[{"car":10,"bike":10,"bus":10,"truck":10}],"reserved":[{"car":10,"bike":10,"bus":10,"truck":10}],"regular":[{"car":10,"bike":10,"bus":10,"truck":10}],"event":[{"car":10,"bike":10,"bus":10,"truck":10}],"premium":[{"car":10,"bike":10,"bus":10,"truck":10}],"sum":null}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_id : 175
         * app_id : 29
         * coordinates : {"x":19.1138407244508,"y":72.8940034657717}
         * place_name : moment
         * place_landmark : IBU
         * city : Mumbai
         * state : Maharashtra
         * pincode : 253525
         * mobile_no : 8879388950
         * email : sandip19july@gmail.com
         * parking_type : null
         * start_date : null
         * end_date : null
         * start_time : null
         * end_time : null
         * total_space : 300
         * vacant : 300
         * general : [{"car":10,"bike":10,"bus":10,"truck":10}]
         * reserved : [{"car":10,"bike":10,"bus":10,"truck":10}]
         * regular : [{"car":10,"bike":10,"bus":10,"truck":10}]
         * event : [{"car":10,"bike":10,"bus":10,"truck":10}]
         * premium : [{"car":10,"bike":10,"bus":10,"truck":10}]
         * sum : null
         */

        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("coordinates")
        private CoordinatesBean coordinates;
        @SerializedName("place_name")
        private String placeName;
        @SerializedName("place_landmark")
        private String placeLandmark;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("pincode")
        private int pincode;
        @SerializedName("mobile_no")
        private String mobileNo;
        @SerializedName("email")
        private String email;
        @SerializedName("parking_type")
        private Object parkingType;
        @SerializedName("start_date")
        private Object startDate;
        @SerializedName("end_date")
        private Object endDate;
        @SerializedName("start_time")
        private Object startTime;
        @SerializedName("end_time")
        private Object endTime;
        @SerializedName("total_space")
        private int totalSpace;
        @SerializedName("vacant")
        private int vacant;
        @SerializedName("sum")
        private String sum;
        @SerializedName("cash")
        private String cash;
        @SerializedName("wallet")
        private String wallet;
        @SerializedName("total_collection")
        private String total_collection;

        @SerializedName("count")
        private String count;

        @SerializedName("general")
        private List<GeneralBean> general;
        @SerializedName("reserved")
        private List<ReservedBean> reserved;
        @SerializedName("regular")
        private List<RegularBean> regular;
        @SerializedName("event")
        private List<EventBean> event;
        @SerializedName("premium")
        private List<PremiumBean> premium;

        public String getCount() {
            return count;
        }

        public String getCash() {
            return cash;
        }

        public String getWallet() {
            return wallet;
        }

        public String getTotal_collection() {
            return total_collection;
        }

        public void setTotal_collection(String total_collection) {
            this.total_collection = total_collection;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public CoordinatesBean getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(CoordinatesBean coordinates) {
            this.coordinates = coordinates;
        }

        public String getPlaceName() {
            return placeName;
        }

        public void setPlaceName(String placeName) {
            this.placeName = placeName;
        }

        public String getPlaceLandmark() {
            return placeLandmark;
        }

        public void setPlaceLandmark(String placeLandmark) {
            this.placeLandmark = placeLandmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getParkingType() {
            return parkingType;
        }

        public void setParkingType(Object parkingType) {
            this.parkingType = parkingType;
        }

        public Object getStartDate() {
            return startDate;
        }

        public void setStartDate(Object startDate) {
            this.startDate = startDate;
        }

        public Object getEndDate() {
            return endDate;
        }

        public void setEndDate(Object endDate) {
            this.endDate = endDate;
        }

        public Object getStartTime() {
            return startTime;
        }

        public void setStartTime(Object startTime) {
            this.startTime = startTime;
        }

        public Object getEndTime() {
            return endTime;
        }

        public void setEndTime(Object endTime) {
            this.endTime = endTime;
        }

        public int getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(int totalSpace) {
            this.totalSpace = totalSpace;
        }

        public int getVacant() {
            return vacant;
        }

        public void setVacant(int vacant) {
            this.vacant = vacant;
        }

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }

        public List<GeneralBean> getGeneral() {
            return general;
        }

        public void setGeneral(List<GeneralBean> general) {
            this.general = general;
        }

        public List<ReservedBean> getReserved() {
            return reserved;
        }

        public void setReserved(List<ReservedBean> reserved) {
            this.reserved = reserved;
        }

        public List<RegularBean> getRegular() {
            return regular;
        }

        public void setRegular(List<RegularBean> regular) {
            this.regular = regular;
        }

        public List<EventBean> getEvent() {
            return event;
        }

        public void setEvent(List<EventBean> event) {
            this.event = event;
        }

        public List<PremiumBean> getPremium() {
            return premium;
        }

        public void setPremium(List<PremiumBean> premium) {
            this.premium = premium;
        }

        public static class CoordinatesBean {
            /**
             * x : 19.1138407244508
             * y : 72.8940034657717
             */

            @SerializedName("x")
            private double x;
            @SerializedName("y")
            private double y;

            public double getX() {
                return x;
            }

            public void setX(double x) {
                this.x = x;
            }

            public double getY() {
                return y;
            }

            public void setY(double y) {
                this.y = y;
            }
        }

        public static class GeneralBean {
            /**
             * car : 10
             * bike : 10
             * bus : 10
             * truck : 10
             */

            @SerializedName("car")
            private int car;
            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("truck")
            private int truck;

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class ReservedBean {
            /**
             * car : 10
             * bike : 10
             * bus : 10
             * truck : 10
             */

            @SerializedName("car")
            private int car;
            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("truck")
            private int truck;

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class RegularBean {
            /**
             * car : 10
             * bike : 10
             * bus : 10
             * truck : 10
             */

            @SerializedName("car")
            private int car;
            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("truck")
            private int truck;

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class EventBean {
            /**
             * car : 10
             * bike : 10
             * bus : 10
             * truck : 10
             */

            @SerializedName("car")
            private int car;
            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("truck")
            private int truck;

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class PremiumBean {
            /**
             * car : 10
             * bike : 10
             * bus : 10
             * truck : 10
             */

            @SerializedName("car")
            private int car;
            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("truck")
            private int truck;

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }
    }
}
