package com.m_intellect.parkandgo1.activity;

import android.app.Application;
import android.util.Log;

/**
 * Created by Swapnesh on 28-03-2019.
 * This ${class} is used for
 */
public class BaseApplication extends Application {

    public String TAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.e(TAG,"Base Application ---->");
    }
    
}
