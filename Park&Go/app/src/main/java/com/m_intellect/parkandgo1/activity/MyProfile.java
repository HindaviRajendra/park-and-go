package com.m_intellect.parkandgo1.activity;


import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ConratctorProfileClass;
import com.m_intellect.parkandgo1.model.ModelAddAttendantResponse;
import com.m_intellect.parkandgo1.model.PostEditDataReq;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

public class MyProfile extends AppCompatActivity implements View.OnClickListener {
    EditText editname, editmob, editpass, editcity, editstate, edit_email, edit_addr;
    private int clicked = 0;
    private int ID;
    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile_layout);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        inittoolbar();
        initview();
        viewdetail();
    }

    private void inittoolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);

        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("My Profile");


        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //ImageView backArrowIV1 = (ImageView) toolbar.findViewById(R.id.searchIV);
        // backArrowIV1.setVisibility(View.GONE);

        ImageView attendance = (ImageView) toolbar.findViewById(R.id.attendance);
        attendance.setVisibility(View.GONE);

        ImageView edit = (ImageView) toolbar.findViewById(R.id.editimage);
        edit.setVisibility(View.VISIBLE);
        edit.setOnClickListener(this);


    }

    private void initview() {
        editname = (EditText) findViewById(R.id.editname);
        editmob = (EditText) findViewById(R.id.editmob);
        editpass = (EditText) findViewById(R.id.editpass);
        editcity = (EditText) findViewById(R.id.editcity);
        editstate = (EditText) findViewById(R.id.editstate);
        edit_email = (EditText) findViewById(R.id.editemail);
        //edit_addr = (EditText) findViewById(R.id.editaddr);

    }

    private void viewdetail() {
        SharedPreferences pref = getSharedPreferences("ParkAndGo", MODE_PRIVATE);
        ID = pref.getInt(Constants.USER_ID, 0);
        new WebServiceGet(this, new HandlerdateListResponse1(), Constants.VIEW_CONTRACTOR_DETAIL + ID).execute();
        Log.e(TAG, "URL>>>" + Constants.VIEW_CONTRACTOR_DETAIL + ID);
    }


    private class HandlerdateListResponse1 extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("Response>>>", "response: " + response);
            if (response != null) {
                ConratctorProfileClass viewdatesconfigClass = new Gson().fromJson(response, ConratctorProfileClass.class);
                if (viewdatesconfigClass.getStatus() == 200) {
                    try {
                        setData(viewdatesconfigClass);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MyProfile.this, viewdatesconfigClass.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void setData(ConratctorProfileClass viewdatesconfigClass) {
        editname.setText(viewdatesconfigClass.getModelAttendantList().get(0).getName());
        editmob.setText(viewdatesconfigClass.getModelAttendantList().get(0).getMobileNo());
        edit_email.setText(viewdatesconfigClass.getModelAttendantList().get(0).getEmail());
        editpass.setText(viewdatesconfigClass.getModelAttendantList().get(0).getPassword());
        editcity.setText(viewdatesconfigClass.getModelAttendantList().get(0).getCity());
     //  edit_addr.setText(viewdatesconfigClass.getModelAttendantList().get(0).getAddress());
        editstate.setText(viewdatesconfigClass.getModelAttendantList().get(0).getState());
        Log.e(TAG, "City ::: " + viewdatesconfigClass.getModelAttendantList().get(0).getCity());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editimage:
                editable();
                break;

        }
    }

    private void editable() {

        if (clicked == 0) {
            editname.setEnabled(true);
            editmob.setEnabled(true);
            editpass.setEnabled(true);
            editcity.setEnabled(true);
            editstate.setEnabled(true);
            edit_email.setEnabled(true);
          //  edit_addr.setEnabled(true);
            clicked++;
            //postData();
        } else {
            editname.setEnabled(false);
            editmob.setEnabled(false);
            editpass.setEnabled(false);
            editcity.setEnabled(false);
            editstate.setEnabled(false);
            edit_email.setEnabled(false);
         //   edit_addr.setEnabled(false);
            clicked = 0;
            postData();
        }
    }

    private void postData() {

        PostEditDataReq postEditDataReq = new PostEditDataReq();
        postEditDataReq.setId(ID);
        postEditDataReq.setAppId(Constants.APP_ID);
        postEditDataReq.setName(editname.getText().toString());
        postEditDataReq.setEmail(edit_email.getText().toString());
        postEditDataReq.setMobileNo(editmob.getText().toString());
        postEditDataReq.setPassword(editpass.getText().toString());
        postEditDataReq.setCity(editcity.getText().toString());
        postEditDataReq.setState(editstate.getText().toString());


        Gson gson = new Gson();
        String payload = gson.toJson(postEditDataReq);
        Log.e("test", "payload: " + payload);

        new WebServicePost(this, new HandlerAddAttendantResponse(), Constants.POST_EDIT_DATA, payload).execute();
        Log.e(TAG,"URL :::"+Constants.POST_EDIT_DATA);

    }

    private class HandlerAddAttendantResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("Response", "response: " + response);
             try {
                 if (response != null) {
                     ModelAddAttendantResponse modelAddAttendantResponse = new Gson().fromJson(response, ModelAddAttendantResponse.class);
                     if (modelAddAttendantResponse.getStatus() == 200) {
                         if (modelAddAttendantResponse.getData()!=null){
                             //  setResult(RESULT_OK);
                             //finish();
                             Toast.makeText(MyProfile.this, "Submited Successfully", Toast.LENGTH_SHORT).show();
                         }

                     }

                 }
             }catch (Exception e){
                 e.printStackTrace();
             }

        }
    }

}
