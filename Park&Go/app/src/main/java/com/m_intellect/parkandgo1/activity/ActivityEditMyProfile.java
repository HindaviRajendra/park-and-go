package com.m_intellect.parkandgo1.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.ModelMyProfile;
import com.m_intellect.parkandgo1.model.ModelSignUp;
import com.m_intellect.parkandgo1.model.ModelSignUpResponse;
import com.m_intellect.parkandgo1.model.SingletonProfileDetail;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

public class ActivityEditMyProfile extends AppCompatActivity implements View.OnClickListener {

    private EditText mobileET;
    private EditText nameET;
    private EditText emailET;
    private EditText passwordET, editcity, editstate, edit_vno, edit_vName, editaddress;
    private EditText edit_1, edit_2, edit_3, edit_4, edit_5, edit_6, edit_7, edit_8, edit_9, edit_10;
    String Vehicleno, Vehicleno1;
    public String TAG = getClass().getSimpleName();
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initToolbar();
        initView();
        setData(SingletonProfileDetail.getInstance().getModelMyProfile());

    }



    private void setData(ModelMyProfile modelMyProfile) {

        mobileET.setText(modelMyProfile.getMobileNo());
        nameET.setText(modelMyProfile.getName());
        passwordET.setText(modelMyProfile.getPassword());
        editcity.setText(modelMyProfile.getCity());
        editstate.setText(modelMyProfile.getState());
        edit_vName.setText(modelMyProfile.getVehicle_name());
        editaddress.setText(modelMyProfile.getAddress());
        Vehicleno1 = modelMyProfile.getVehicleNo();
        Log.e(TAG, "vNo ::: " + Vehicleno1);
        try {
            edit_1.setText("" + Vehicleno1.charAt(0));
            edit_2.setText("" + Vehicleno1.charAt(1));
            edit_3.setText("" + Vehicleno1.charAt(3));
            edit_4.setText("" + Vehicleno1.charAt(4));
            edit_5.setText("" + Vehicleno1.charAt(6));
            edit_6.setText("" + Vehicleno1.charAt(7));
            edit_7.setText("" + Vehicleno1.charAt(9));
            edit_8.setText("" + Vehicleno1.charAt(10));
            edit_9.setText("" + Vehicleno1.charAt(11));
            edit_10.setText("" + Vehicleno1.charAt(12));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setText("Save");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        locationTV.setLayoutParams(params);
        locationTV.setOnClickListener(this);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText("My Profile");
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    private void initView() {

        mobileET = (EditText) findViewById(R.id.mobileET);
        nameET = (EditText) findViewById(R.id.nameET);
        // emailET = (EditText) findViewById(R.id.emailET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        editcity = (EditText) findViewById(R.id.edit_city);
        editstate = (EditText) findViewById(R.id.edit_state);
        edit_vName = (EditText) findViewById(R.id.edit_vName);
        editaddress = (EditText) findViewById(R.id.editAddr);

        edit_1 = (EditText) findViewById(R.id.pin_one_edittext);
        edit_2 = (EditText) findViewById(R.id.pin_two_edittext);
        edit_3 = (EditText) findViewById(R.id.pin_three_edittext);
        edit_4 = (EditText) findViewById(R.id.pin_four_edittext);
        edit_5 = (EditText) findViewById(R.id.pin_five_edittext);
        edit_6 = (EditText) findViewById(R.id.pin_six_edittext);
        edit_7 = (EditText) findViewById(R.id.pin_sev_edittext);
        edit_8 = (EditText) findViewById(R.id.pin_eight_edittext);
        edit_9 = (EditText) findViewById(R.id.pin_nine_edittext);
        edit_10 = (EditText) findViewById(R.id.pin_ten_edittext);

        mobileET.setCursorVisible(true);
        mobileET.setFocusable(true);
        mobileET.setFocusableInTouchMode(true);

        nameET.setCursorVisible(true);
        nameET.setFocusable(true);
        nameET.setFocusableInTouchMode(true);


        passwordET.setCursorVisible(true);
        passwordET.setFocusable(true);
        passwordET.setFocusableInTouchMode(true);

        editcity.setCursorVisible(true);
        editcity.setFocusable(true);
        editcity.setFocusableInTouchMode(true);

        editstate.setCursorVisible(true);
        editstate.setFocusable(true);
        editstate.setFocusableInTouchMode(true);


        edit_vName.setCursorVisible(true);
        edit_vName.setFocusable(true);
        edit_vName.setFocusableInTouchMode(true);

        editaddress.setCursorVisible(true);
        editaddress.setFocusable(true);
        editaddress.setFocusableInTouchMode(true);

        edit_1 = (EditText) findViewById(R.id.pin_one_edittext);
        edit_2 = (EditText) findViewById(R.id.pin_two_edittext);
        edit_3 = (EditText) findViewById(R.id.pin_three_edittext);
        edit_4 = (EditText) findViewById(R.id.pin_four_edittext);
        edit_5 = (EditText) findViewById(R.id.pin_five_edittext);
        edit_6 = (EditText) findViewById(R.id.pin_six_edittext);
        edit_7 = (EditText) findViewById(R.id.pin_sev_edittext);
        edit_8 = (EditText) findViewById(R.id.pin_eight_edittext);
        edit_9 = (EditText) findViewById(R.id.pin_nine_edittext);
        edit_10 = (EditText) findViewById(R.id.pin_ten_edittext);
        addTextListner();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                break;

            case R.id.locationTV:
                    if (edit_1.getText().toString().equals("")) {
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    } else if (edit_2.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_3.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_4.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_5.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_6.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_7.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_8.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_9.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else  if (edit_10.getText().toString().equals("")){
                        Toast.makeText(ActivityEditMyProfile.this, "Please enter ten digit Vehicle Number", Toast.LENGTH_SHORT).show();
                    }else {
                        updateProfile();
                    }

                break;

            default:
                break;
        }
    }

    private void addTextListner() {
        edit_1.requestFocus();
        edit_1.setCursorVisible(true);
        edit_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_1.getText().toString().length() == 1) {
                    edit_2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_2.getText().toString().length() == 1) {
                    edit_3.requestFocus();
                    edit_2.setSelection(edit_2.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_2.getText().toString().length() == 0) {
                    edit_1.requestFocus();

                }
            }
        });

        edit_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_3.getText().toString().length() == 1) {
                    edit_4.requestFocus();
                    edit_3.setSelection(edit_3.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_3.getText().toString().length() == 0) {
                    edit_2.requestFocus();
                }
            }
        });


        edit_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_4.getText().toString().length() == 1) {
                    edit_5.requestFocus();
                    edit_4.setSelection(edit_4.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_4.getText().toString().length() == 0) {
                    edit_3.requestFocus();
                }
            }
        });


        edit_5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_5.getText().toString().length() == 1) {
                    edit_6.requestFocus();
                    edit_5.setSelection(edit_5.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_5.getText().toString().length() == 0) {
                    edit_4.requestFocus();
                }
            }
        });


        edit_6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_6.getText().toString().length() == 1) {
                    edit_7.requestFocus();
                    edit_6.setSelection(edit_6.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_6.getText().toString().length() == 0) {
                    edit_5.requestFocus();
                }
            }
        });


        edit_7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_7.getText().toString().length() == 1) {
                    edit_8.requestFocus();
                    edit_7.setSelection(edit_7.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_7.getText().toString().length() == 0) {
                    edit_6.requestFocus();
                }
            }
        });


        edit_8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_8.getText().toString().length() == 1) {
                    edit_9.requestFocus();
                    edit_8.setSelection(edit_8.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_8.getText().toString().length() == 0) {
                    edit_7.requestFocus();

                }
            }
        });

        edit_9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit_9.getText().toString().length() == 1) {
                    edit_10.requestFocus();
                    edit_9.setSelection(edit_9.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_9.getText().toString().length() == 0 ) {
                    edit_8.requestFocus();

                }
            }
        });

        edit_10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_10.setSelection(edit_10.getText().length());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_10.getText().toString().length() == 0) {
                    edit_9.requestFocus();
                }
            }
        });

    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            //generateTV.setVisibility(View.VISIBLE);
            Log.d("Test", "Coming VISIBLE:");
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } else {
            Log.d("Test", "Coming GONE:");
            //generateTV.setVisibility(View.GONE);
        }
        return super.dispatchTouchEvent(ev);
    }


    private void updateProfile() {

        ModelSignUp modelSignUp = new ModelSignUp();
        modelSignUp.setId(SingletonProfileDetail.getInstance().getModelMyProfile().getId());
        modelSignUp.setAppId(Constants.APP_ID);
        modelSignUp.setName(nameET.getText().toString());
        modelSignUp.setEmail("");
        modelSignUp.setMobileNo(mobileET.getText().toString());
        modelSignUp.setVehicleName(edit_vName.getText().toString());
        modelSignUp.setVehicleNo(edit_1.getText().toString() + "" + edit_2.getText().toString() + "-" + edit_3.getText().toString() + "" + edit_4.getText().toString() + "-" +
                edit_5.getText().toString() + "" + edit_6.getText().toString() + "-" + edit_7.getText().toString() + "" + edit_8.getText().toString() + edit_9.getText().toString() + "" + edit_10.getText().toString());
        modelSignUp.setCity(editcity.getText().toString());
        modelSignUp.setState(editstate.getText().toString());
        modelSignUp.setPassword(passwordET.getText().toString());
        modelSignUp.setAddress(editaddress.getText().toString());
        modelSignUp.setLatitude(SingletonProfileDetail.getInstance().getModelMyProfile().getCoordinates().getX());
        modelSignUp.setLongitude(SingletonProfileDetail.getInstance().getModelMyProfile().getCoordinates().getY());
        modelSignUp.setActiveStatus(1);
        modelSignUp.setDeviceId(SingletonProfileDetail.getInstance().getModelMyProfile().getDeviceId());
        modelSignUp.setUserType("customer");

        Gson gson = new Gson();
        String payload = gson.toJson(modelSignUp);
        Log.d("test", "payload: " + payload);
        new WebServicePost(this, new HandlerUpdateProfileResponse(), Constants.SIGNUP_URL, payload).execute();

        preferences = getSharedPreferences("ParkAndGo", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("VH_NO", edit_1.getText().toString() + "" + edit_2.getText().toString() + "-" + edit_3.getText().toString() + "" + edit_4.getText().toString() + "-" +
                edit_5.getText().toString() + "" + edit_6.getText().toString() + "-" + edit_7.getText().toString() + "" + edit_8.getText().toString() + edit_9.getText().toString() + "" + edit_10.getText().toString());
        editor.apply();

    }

    private class HandlerUpdateProfileResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelSignUpResponse modelSignUpResponse = new Gson().fromJson(response, ModelSignUpResponse.class);
                if (modelSignUpResponse.getStatus() == 200) {
                    Intent intent = new Intent(ActivityEditMyProfile.this, ActivityMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                Toast.makeText(ActivityEditMyProfile.this, modelSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
