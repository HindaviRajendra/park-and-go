package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

public class CheckbalanceRes {

    /**
     * data : {"head":{"responseTimestamp":"1540298103049","version":"v1","clientId":"merchant-abralin-stagging","signature":"muo3p9lhkYbs/Cj2SGmlSa3/zhYr4IBYJhguuoUI1YTSe8eSuEbY8c3tx+cnODCMJtHW7ITN2d40EeAv/DfJ3CPPYbcjxL93f+03KqCWJs0="},"body":{"resultInfo":{"resultStatus":"SUCCESS","resultCode":"CS_0000","resultMsg":"Request served successfully."},"fundsSufficient":false,"addMoneyAllowed":false}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * head : {"responseTimestamp":"1540298103049","version":"v1","clientId":"merchant-abralin-stagging","signature":"muo3p9lhkYbs/Cj2SGmlSa3/zhYr4IBYJhguuoUI1YTSe8eSuEbY8c3tx+cnODCMJtHW7ITN2d40EeAv/DfJ3CPPYbcjxL93f+03KqCWJs0="}
         * body : {"resultInfo":{"resultStatus":"SUCCESS","resultCode":"CS_0000","resultMsg":"Request served successfully."},"fundsSufficient":false,"addMoneyAllowed":false}
         */

        @SerializedName("head")
        private HeadBean head;
        @SerializedName("body")
        private BodyBean body;

        public HeadBean getHead() {
            return head;
        }

        public void setHead(HeadBean head) {
            this.head = head;
        }

        public BodyBean getBody() {
            return body;
        }

        public void setBody(BodyBean body) {
            this.body = body;
        }

        public static class HeadBean {
            /**
             * responseTimestamp : 1540298103049
             * version : v1
             * clientId : merchant-abralin-stagging
             * signature : muo3p9lhkYbs/Cj2SGmlSa3/zhYr4IBYJhguuoUI1YTSe8eSuEbY8c3tx+cnODCMJtHW7ITN2d40EeAv/DfJ3CPPYbcjxL93f+03KqCWJs0=
             */

            @SerializedName("responseTimestamp")
            private String responseTimestamp;
            @SerializedName("version")
            private String version;
            @SerializedName("clientId")
            private String clientId;
            @SerializedName("signature")
            private String signature;

            public String getResponseTimestamp() {
                return responseTimestamp;
            }

            public void setResponseTimestamp(String responseTimestamp) {
                this.responseTimestamp = responseTimestamp;
            }

            public String getVersion() {
                return version;
            }

            public void setVersion(String version) {
                this.version = version;
            }

            public String getClientId() {
                return clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }

            public String getSignature() {
                return signature;
            }

            public void setSignature(String signature) {
                this.signature = signature;
            }
        }

        public static class BodyBean {
            /**
             * resultInfo : {"resultStatus":"SUCCESS","resultCode":"CS_0000","resultMsg":"Request served successfully."}
             * fundsSufficient : false
             * addMoneyAllowed : false
             */

            @SerializedName("resultInfo")
            private ResultInfoBean resultInfo;
            @SerializedName("fundsSufficient")
            private boolean fundsSufficient;
            @SerializedName("addMoneyAllowed")
            private boolean addMoneyAllowed;

            public ResultInfoBean getResultInfo() {
                return resultInfo;
            }

            public void setResultInfo(ResultInfoBean resultInfo) {
                this.resultInfo = resultInfo;
            }

            public boolean isFundsSufficient() {
                return fundsSufficient;
            }

            public void setFundsSufficient(boolean fundsSufficient) {
                this.fundsSufficient = fundsSufficient;
            }

            public boolean isAddMoneyAllowed() {
                return addMoneyAllowed;
            }

            public void setAddMoneyAllowed(boolean addMoneyAllowed) {
                this.addMoneyAllowed = addMoneyAllowed;
            }

            public static class ResultInfoBean {
                /**
                 * resultStatus : SUCCESS
                 * resultCode : CS_0000
                 * resultMsg : Request served successfully.
                 */

                @SerializedName("resultStatus")
                private String resultStatus;
                @SerializedName("resultCode")
                private String resultCode;
                @SerializedName("resultMsg")
                private String resultMsg;

                public String getResultStatus() {
                    return resultStatus;
                }

                public void setResultStatus(String resultStatus) {
                    this.resultStatus = resultStatus;
                }

                public String getResultCode() {
                    return resultCode;
                }

                public void setResultCode(String resultCode) {
                    this.resultCode = resultCode;
                }

                public String getResultMsg() {
                    return resultMsg;
                }

                public void setResultMsg(String resultMsg) {
                    this.resultMsg = resultMsg;
                }
            }
        }
    }
}
