//NAYA WALA
package com.m_intellect.parkandgo1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelConfiguration1 {


    /**
     * app_id : 29
     * calender : ["2018-05-05"]
     * event : [{"car":10,"bike":10,"bus":10,"truck":10}]
     * event_parking : {"event1":{"from":"0","price":"200","to":"1"},"event2":{"from":"0","price":"200","to":"5"},"event3":{"from":"0","price":"500","to":"5"}}
     * general : [{"car":10,"bike":10,"bus":10,"truck":10}]
     * parking_configuration_id : 402
     * parking_id : 175
     * premium : [{"car":10,"bike":10,"bus":10,"truck":10}]
     * car_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"40","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"100","price_g":"90","price_p":"110","to":"4"}]
     * bike_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"40","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"100","price_g":"90","price_p":"110","to":"4"}]
     * bus_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"40","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"100","price_g":"90","price_p":"110","to":"4"}]
     * truck_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"40","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"100","price_g":"90","price_p":"110","to":"4"}]
     * regular : [{"car":10,"bike":10,"bus":10,"truck":10}]
     * regular_parking : {"day":{"from":"11:33 AM","price":"100","to":"04:33 AM"},"night":{"from":"11:34 AM","price":"200","to":"05:34 AM"}}
     * reserved : [{"car":10,"bike":10,"bus":10,"truck":10}]
     * total : 300.0
     * total_space : 0
     * user_id : 244
     * vehicle_type : a
     */

    @SerializedName("app_id")
    private int appId;
    @SerializedName("event_parking")
    private EventParkingBean eventParking;
    @SerializedName("parking_configuration_id")
    private int parkingConfigurationId;
    @SerializedName("parking_id")
    private int parkingId;
    @SerializedName("regular_parking")
    private RegularParkingBean regularParking;
    @SerializedName("total")
    private String total;
    @SerializedName("total_space")
    private int totalSpace;
    @SerializedName("car_status")
    private int car_status;

    @SerializedName("bike_status")
    private int bike_status;

    @SerializedName("bus_status")
    private int bus_status;

    @SerializedName("truck_status")
    private int truck_status;

    @SerializedName("user_id")
    private int userId;
    @SerializedName("vehicle_type")
    private String vehicleType;
    @SerializedName("calender")
    private List<String> calender;
    @SerializedName("event")
    private List<EventBean> event;
    @SerializedName("general")
    private List<GeneralBean> general;
    @SerializedName("premium")
    private List<PremiumBean> premium;
    @SerializedName("car_price_values")
    private List<CarPriceValuesBean> carPriceValues;
    @SerializedName("bike_price_values")
    private List<BikePriceValuesBean> bikePriceValues;
    @SerializedName("bus_price_values")
    private List<BusPriceValuesBean> busPriceValues;
    @SerializedName("truck_price_values")
    private List<TruckPriceValuesBean> truckPriceValues;
    @SerializedName("regular")
    private List<RegularBean> regular;
    @SerializedName("reserved")
    private List<ReservedBean> reserved;


    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public EventParkingBean getEventParking() {
        return eventParking;
    }

    public void setEventParking(EventParkingBean eventParking) {
        this.eventParking = eventParking;
    }

    public int getParkingConfigurationId() {
        return parkingConfigurationId;
    }

    public void setParkingConfigurationId(int parkingConfigurationId) {
        this.parkingConfigurationId = parkingConfigurationId;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    public RegularParkingBean getRegularParking() {
        return regularParking;
    }

    public void setRegularParking(RegularParkingBean regularParking) {
        this.regularParking = regularParking;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getCar_status() {
        return car_status;
    }

    public ModelConfiguration1 setCar_status(int car_status) {
        this.car_status = car_status;
        return this;
    }

    public int getBike_status() {
        return bike_status;
    }

    public ModelConfiguration1 setBike_status(int bike_status) {
        this.bike_status = bike_status;
        return this;
    }

    public int getBus_status() {
        return bus_status;
    }

    public ModelConfiguration1 setBus_status(int bus_status) {
        this.bus_status = bus_status;
        return this;
    }

    public int getTruck_status() {
        return truck_status;
    }

    public ModelConfiguration1 setTruck_status(int truck_status) {
        this.truck_status = truck_status;
        return this;
    }

    public int getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(int totalSpace) {
        this.totalSpace = totalSpace;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public List<String> getCalender() {
        return calender;
    }

    public void setCalender(List<String> calender) {
        this.calender = calender;
    }

    public List<EventBean> getEvent() {
        return event;
    }

    public void setEvent(List<EventBean> event) {
        this.event = event;
    }

    public List<GeneralBean> getGeneral() {
        return general;
    }

    public void setGeneral(List<GeneralBean> general) {
        this.general = general;
    }

    public List<PremiumBean> getPremium() {
        return premium;
    }

    public void setPremium(List<PremiumBean> premium) {
        this.premium = premium;
    }

    public List<CarPriceValuesBean> getCarPriceValues() {
        return carPriceValues;
    }

    public void setCarPriceValues(List<CarPriceValuesBean> carPriceValues) {
        this.carPriceValues = carPriceValues;
    }

    public List<BikePriceValuesBean> getBikePriceValues() {
        return bikePriceValues;
    }

    public void setBikePriceValues(List<BikePriceValuesBean> bikePriceValues) {
        this.bikePriceValues = bikePriceValues;
    }

    public List<BusPriceValuesBean> getBusPriceValues() {
        return busPriceValues;
    }

    public void setBusPriceValues(List<BusPriceValuesBean> busPriceValues) {
        this.busPriceValues = busPriceValues;
    }

    public List<TruckPriceValuesBean> getTruckPriceValues() {
        return truckPriceValues;
    }

    public void setTruckPriceValues(List<TruckPriceValuesBean> truckPriceValues) {
        this.truckPriceValues = truckPriceValues;
    }

    public List<RegularBean> getRegular() {
        return regular;
    }

    public void setRegular(List<RegularBean> regular) {
        this.regular = regular;
    }

    public List<ReservedBean> getReserved() {
        return reserved;
    }

    public void setReserved(List<ReservedBean> reserved) {
        this.reserved = reserved;
    }

    public static class EventParkingBean {
        /**
         * event1 : {"from":"0","price":"200","to":"1"}
         * event2 : {"from":"0","price":"200","to":"5"}
         * event3 : {"from":"0","price":"500","to":"5"}
         */

        @SerializedName("event1")
        private Event1Bean event1;
        @SerializedName("event2")
        private Event2Bean event2;
        @SerializedName("event3")
        private Event3Bean event3;

        public Event1Bean getEvent1() {
            return event1;
        }

        public void setEvent1(Event1Bean event1) {
            this.event1 = event1;
        }

        public Event2Bean getEvent2() {
            return event2;
        }

        public void setEvent2(Event2Bean event2) {
            this.event2 = event2;
        }

        public Event3Bean getEvent3() {
            return event3;
        }

        public void setEvent3(Event3Bean event3) {
            this.event3 = event3;
        }

        public static class Event1Bean {
            /**
             * from : 0
             * price : 200
             * to : 1
             */

            @SerializedName("from")
            private String from;
            @SerializedName("price")
            private String price;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class Event2Bean {
            /**
             * from : 0
             * price : 200
             * to : 5
             */

            @SerializedName("from")
            private String from;
            @SerializedName("price")
            private String price;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class Event3Bean {
            /**
             * from : 0
             * price : 500
             * to : 5
             */

            @SerializedName("from")
            private String from;
            @SerializedName("price")
            private String price;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }
    }

    public static class RegularParkingBean {
        /**
         * day : {"from":"11:33 AM","price":"100","to":"04:33 AM"}
         * night : {"from":"11:34 AM","price":"200","to":"05:34 AM"}
         */

        @SerializedName("day")
        private DayBean day;
        @SerializedName("night")
        private NightBean night;

        public DayBean getDay() {
            return day;
        }

        public void setDay(DayBean day) {
            this.day = day;
        }

        public NightBean getNight() {
            return night;
        }

        public void setNight(NightBean night) {
            this.night = night;
        }

        public static class DayBean {
            /**
             * from : 11:33 AM
             * price : 100
             * to : 04:33 AM
             */

            @SerializedName("from")
            private String from;
            @SerializedName("price")
            private String price;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class NightBean {
            /**
             * from : 11:34 AM
             * price : 200
             * to : 05:34 AM
             */

            @SerializedName("from")
            private String from;
            @SerializedName("price")
            private String price;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }
    }

    public static class EventBean {
        /**
         * car : 10
         * bike : 10
         * bus : 10
         * truck : 10
         */

        @SerializedName("car")
        private int car;
        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("truck")
        private int truck;

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }

    public static class GeneralBean {
        /**
         * car : 10
         * bike : 10
         * bus : 10
         * truck : 10
         */

        @SerializedName("car")
        private int car;
        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("truck")
        private int truck;

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }

    public static class PremiumBean {
        /**
         * car : 10
         * bike : 10
         * bus : 10
         * truck : 10
         */

        @SerializedName("car")
        private int car;
        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("truck")
        private int truck;

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }

    public static class CarPriceValuesBean {
        /**
         * from : 0
         * parice_r : 20
         * price_g : 10
         * price_p : 40
         * to : 1
         */

        @SerializedName("from")
        private String from;
        @SerializedName("parice_r")
        private String pariceR;
        @SerializedName("price_g")
        private String priceG;
        @SerializedName("price_p")
        private String priceP;
        @SerializedName("to")
        private String to;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getPariceR() {
            return pariceR;
        }

        public void setPariceR(String pariceR) {
            this.pariceR = pariceR;
        }

        public String getPriceG() {
            return priceG;
        }

        public void setPriceG(String priceG) {
            this.priceG = priceG;
        }

        public String getPriceP() {
            return priceP;
        }

        public void setPriceP(String priceP) {
            this.priceP = priceP;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }

    public static class BikePriceValuesBean {
        /**
         * from : 0
         * parice_r : 20
         * price_g : 10
         * price_p : 40
         * to : 1
         */

        @SerializedName("from")
        private String from;
        @SerializedName("parice_r")
        private String pariceR;
        @SerializedName("price_g")
        private String priceG;
        @SerializedName("price_p")
        private String priceP;
        @SerializedName("to")
        private String to;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getPariceR() {
            return pariceR;
        }

        public void setPariceR(String pariceR) {
            this.pariceR = pariceR;
        }

        public String getPriceG() {
            return priceG;
        }

        public void setPriceG(String priceG) {
            this.priceG = priceG;
        }

        public String getPriceP() {
            return priceP;
        }

        public void setPriceP(String priceP) {
            this.priceP = priceP;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }

    public static class BusPriceValuesBean {
        /**
         * from : 0
         * parice_r : 20
         * price_g : 10
         * price_p : 40
         * to : 1
         */

        @SerializedName("from")
        private String from;
        @SerializedName("parice_r")
        private String pariceR;
        @SerializedName("price_g")
        private String priceG;
        @SerializedName("price_p")
        private String priceP;
        @SerializedName("to")
        private String to;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getPariceR() {
            return pariceR;
        }

        public void setPariceR(String pariceR) {
            this.pariceR = pariceR;
        }

        public String getPriceG() {
            return priceG;
        }

        public void setPriceG(String priceG) {
            this.priceG = priceG;
        }

        public String getPriceP() {
            return priceP;
        }

        public void setPriceP(String priceP) {
            this.priceP = priceP;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }

    public static class TruckPriceValuesBean {
        /**
         * from : 0
         * parice_r : 20
         * price_g : 10
         * price_p : 40
         * to : 1
         */

        @SerializedName("from")
        private String from;
        @SerializedName("parice_r")
        private String pariceR;
        @SerializedName("price_g")
        private String priceG;
        @SerializedName("price_p")
        private String priceP;
        @SerializedName("to")
        private String to;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getPariceR() {
            return pariceR;
        }

        public void setPariceR(String pariceR) {
            this.pariceR = pariceR;
        }

        public String getPriceG() {
            return priceG;
        }

        public void setPriceG(String priceG) {
            this.priceG = priceG;
        }

        public String getPriceP() {
            return priceP;
        }

        public void setPriceP(String priceP) {
            this.priceP = priceP;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }

    public static class RegularBean {
        /**
         * car : 10
         * bike : 10
         * bus : 10
         * truck : 10
         */

        @SerializedName("car")
        private int car;
        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("truck")
        private int truck;

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }

    public static class ReservedBean {
        /**
         * car : 10
         * bike : 10
         * bus : 10
         * truck : 10
         */

        @SerializedName("car")
        private int car;
        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("truck")
        private int truck;

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }
}
