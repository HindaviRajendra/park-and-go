package com.m_intellect.parkandgo1.activity;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.adapter.AdapterSearchRegularParkingList;
import com.m_intellect.parkandgo1.model.ModelParkingDetailListResponse;
import com.m_intellect.parkandgo1.model.ModelParkingDetailResponse;
import com.m_intellect.parkandgo1.model.SingletonRegularParkingDetail;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.utils.Constants;
import com.m_intellect.parkandgo1.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ActivitySearchRegularParking extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView parkingListRV;
    private Calendar calendar = Calendar.getInstance();
    private Calendar calendar1 = Calendar.getInstance();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private List<ModelParkingDetailResponse> mModelParkingDetailResponseList;
    private AdapterSearchRegularParkingList adapterSearchRegularParkingList;
    private TextView reserveNowTV;
    private TextView startDateTV;
    private TextView endDateTV;
    private TextView dailyTV;
    private TextView weeklyTV;
    private TextView monthlyTV;
    private TextView startTimeTV;
    private TextView endTimeTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_regular_parking);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        initView();

        initToolbar();

        SetData();

        getSearchRegularParkingList();

    }

    private void SetData() {

        String parkingType = SingletonRegularParkingDetail.getInstance().getParkingType();

        if (parkingType.equalsIgnoreCase("daily")) {
            dailyTV.setSelected(true);
        }

        if (parkingType.equalsIgnoreCase("weekly")) {
            weeklyTV.setSelected(true);
        }
        if (parkingType.equalsIgnoreCase("monthly")) {
            monthlyTV.setSelected(true);
        }
        if (parkingType.equalsIgnoreCase("daily_weekly")) {
            dailyTV.setSelected(true);
            weeklyTV.setSelected(true);
        }
        if (parkingType.equalsIgnoreCase("daily_monthly")) {
            dailyTV.setSelected(true);
            monthlyTV.setSelected(true);
        }
        if (parkingType.equalsIgnoreCase("weekly_monthly")) {
            weeklyTV.setSelected(true);
            monthlyTV.setSelected(true);
        }
        if (parkingType.equalsIgnoreCase("daily_weekly_monthly")) {
            dailyTV.setSelected(true);
            weeklyTV.setSelected(true);
            monthlyTV.setSelected(true);
        }

        startDateTV.setText(Utility.formatDateFromOnetoAnother(SingletonRegularParkingDetail.getInstance().getStartDate(), "yyyy-dd-MM", "dd/MM/yyyy"));
        endDateTV.setText(Utility.formatDateFromOnetoAnother(SingletonRegularParkingDetail.getInstance().getEndDate(), "yyyy-dd-MM", "dd/MM/yyyy"));
        startTimeTV.setText(SingletonRegularParkingDetail.getInstance().getStartTime());
        endTimeTV.setText(SingletonRegularParkingDetail.getInstance().getEndTime());

    }

    private void getSearchRegularParkingList() {


        String url = Constants.VIEW_PARKING_DETAIL_URL + "null/null/" + SingletonRegularParkingDetail.getInstance().getParkingType() + "/" +
                SingletonRegularParkingDetail.getInstance().getStartDate() + "/" + SingletonRegularParkingDetail.getInstance().getEndDate() + "/" +
                SingletonRegularParkingDetail.getInstance().getStartTime() + ":00/" + SingletonRegularParkingDetail.getInstance().getEndTime() + ":00/" +
                SingletonRegularParkingDetail.getInstance().getPlaceName();

        new WebServiceGet(this, new SearchRegularParkingListResponse(), url).execute();

    }

    private class SearchRegularParkingListResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {

                ModelParkingDetailListResponse modelParkingDetailListResponse = new Gson().fromJson(response, ModelParkingDetailListResponse.class);

                if (modelParkingDetailListResponse.getStatus() == 200) {

                    mModelParkingDetailResponseList = modelParkingDetailListResponse.getModelParkingDetailResponseList();
                    adapterSearchRegularParkingList = new AdapterSearchRegularParkingList(ActivitySearchRegularParking.this, mModelParkingDetailResponseList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivitySearchRegularParking.this);
                    parkingListRV.setLayoutManager(mLayoutManager);
                    parkingListRV.setItemAnimator(new DefaultItemAnimator());
                    parkingListRV.setAdapter(adapterSearchRegularParkingList);


                } else {

                    Toast.makeText(ActivitySearchRegularParking.this, modelParkingDetailListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void reservedChecker(int position, boolean isChecked) {

        for (int i = 0; i < mModelParkingDetailResponseList.size(); i++) {

            if (i == position)
                mModelParkingDetailResponseList.get(i).setRadioChecked(isChecked);
            else
                mModelParkingDetailResponseList.get(i).setRadioChecked(false);
        }

        adapterSearchRegularParkingList.notifyDataSetChanged();

        reserveNowTV.setSelected(true);
        reserveNowTV.setClickable(true);

    }

    private void initView() {
        parkingListRV = (RecyclerView) findViewById(R.id.parkingListRV);
        dailyTV = (TextView) findViewById(R.id.dailyTV);
        weeklyTV = (TextView) findViewById(R.id.weeklyTV);
        monthlyTV = (TextView) findViewById(R.id.monthlyTV);
        startDateTV = (TextView) findViewById(R.id.startDateTV);
        endDateTV = (TextView) findViewById(R.id.endDateTV);
        startTimeTV = (TextView) findViewById(R.id.startTimeTV);
        endTimeTV = (TextView) findViewById(R.id.endTimeTV);
        reserveNowTV = (TextView) findViewById(R.id.reserveNowTV);
        reserveNowTV.setOnClickListener(this);
        reserveNowTV.setClickable(false);

    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        TextView locationTV = (TextView) toolbar.findViewById(R.id.locationTV);
        locationTV.setVisibility(View.GONE);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setText(SingletonRegularParkingDetail.getInstance().getPlaceName());
        ImageView searchIV = (ImageView) toolbar.findViewById(R.id.searchIV);
        searchIV.setVisibility(View.GONE);

        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backArrowIV:
                onBackPressed();
                break;
            case R.id.reserveNowTV:
                //startActivity(new Intent(this, ActivityReserveNow.class));
               // break;
            default:
                break;
        }
    }
}
