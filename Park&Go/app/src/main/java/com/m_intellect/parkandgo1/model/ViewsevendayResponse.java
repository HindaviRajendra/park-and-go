package com.m_intellect.parkandgo1.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewsevendayResponse {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"sum":"10","created_on":"2018-04-15T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-11T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-10T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-14T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-13T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-09T00:00:00.000Z"},{"sum":"260","created_on":"2018-04-16T00:00:00.000Z"},{"sum":"10","created_on":"2018-04-12T00:00:00.000Z"}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sum : 10
         * created_on : 2018-04-15T00:00:00.000Z
         */

        @SerializedName("sum")
        private String sum;
        @SerializedName("created_on")
        private String createdOn;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
