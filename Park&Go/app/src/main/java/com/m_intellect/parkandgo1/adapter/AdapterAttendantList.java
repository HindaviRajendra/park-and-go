package com.m_intellect.parkandgo1.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.activity.ActivityAddAttendant;
import com.m_intellect.parkandgo1.activity.ActivityAttendant;
import com.m_intellect.parkandgo1.activity.AttendanceRecord;
import com.m_intellect.parkandgo1.model.DeleteAttendaneReq;
import com.m_intellect.parkandgo1.model.DeleteResponse;
import com.m_intellect.parkandgo1.model.ModelAttendant;
import com.m_intellect.parkandgo1.model.SingletonAttendantDetail;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;

import java.util.List;

/**
 * Created by sanket on 1/21/2018.
 */

public class AdapterAttendantList extends RecyclerView.Adapter<AdapterAttendantList.ViewHolder> {

    private Context context;
    public List<ModelAttendant.DataBean> modelAttendantList;

    public AdapterAttendantList(Context context, List<ModelAttendant.DataBean> modelAttendantList) {

        this.context = context;
        this.modelAttendantList = modelAttendantList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_attendant, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final ModelAttendant.DataBean modelAttendant = modelAttendantList.get(holder.getAdapterPosition());

        holder.nameTV.setText("Name :" + modelAttendant.getName());
        holder.mobileTV.setText("Mobile No :" + modelAttendant.getMobileNo());
        holder.passwordTV.setText("Password :" + modelAttendant.getPassword());

        holder.TV_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AttendanceRecord.class);
                intent.putExtra("parkID", modelAttendantList.get(position).getParkingId());
                context.startActivity(intent);
            }
        });

        holder.editTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SingletonAttendantDetail.getInstance().setModelAttendant(modelAttendant);
                Intent intent = new Intent(context, ActivityAddAttendant.class);
                intent.putExtra(Constants.FROM_EDIT, true);
                intent.putExtra("parkID", modelAttendant.getParkingId());
                Log.e("TAG","Parinng Id  :"+modelAttendant.getParkingId());
                ((ActivityAttendant) context).startActivityForResult(intent, 111);
            }
        });
        holder.delete.setTag(modelAttendantList);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Are you sure you want to delete ?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAttendance(modelAttendantList.get(position).getId());
                        modelAttendantList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, modelAttendantList.size());
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();


            }
        });
    }

    private void deleteAttendance(int id) {
        DeleteAttendaneReq deleteParkRequest = new DeleteAttendaneReq();
        deleteParkRequest.setAppId(Constants.APP_ID);
        deleteParkRequest.setId(id);

        Gson gson = new Gson();
        String payload = gson.toJson(deleteParkRequest);
        Log.e("test", "payload: " + payload);

        new WebServicePost(context, new HandlerAddAttendantResponse(), Constants.DELETE_ATTENDANCE, payload).execute();

    }

    private class HandlerAddAttendantResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                DeleteAttendance deleteAttendance = new Gson().fromJson(response, DeleteAttendance.class);
                if (deleteAttendance.getStatus() == 200) {
                   // Toast.makeText(context, deleteAttendance.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return modelAttendantList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTV, mobileTV, passwordTV, TV_record;
        private ImageView editTV, delete;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            mobileTV = (TextView) itemView.findViewById(R.id.mobileTV);
            passwordTV = (TextView) itemView.findViewById(R.id.passwordTV);
            editTV = (ImageView) itemView.findViewById(R.id.editTV);
            delete = (ImageView) itemView.findViewById(R.id.deleteTV);
            TV_record = (TextView) itemView.findViewById(R.id.record_TV);

        }
    }

    public class DeleteAttendance {

        @SerializedName("Status")
        @Expose
        private Integer status;
        @SerializedName("Success")
        @Expose
        private Boolean success;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("Data")
        @Expose
        private List<DeleteResponse> modelAttendantList = null;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<DeleteResponse> getModelAttendantList() {
            return modelAttendantList;
        }

        public void setModelAttendantList(List<DeleteResponse> modelAttendantList) {
            this.modelAttendantList = modelAttendantList;
        }
    }
}
