package com.m_intellect.parkandgo1.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.m_intellect.parkandgo1.R;
import com.m_intellect.parkandgo1.model.CancelBookClass;
import com.m_intellect.parkandgo1.model.CancelBookReq;
import com.m_intellect.parkandgo1.model.Insertvacant_Req;
import com.m_intellect.parkandgo1.model.ModelParkingAreaResponse;
import com.m_intellect.parkandgo1.model.ParkConfigClass;
import com.m_intellect.parkandgo1.network.WebServiceGet;
import com.m_intellect.parkandgo1.network.WebServicePost;
import com.m_intellect.parkandgo1.utils.Constants;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the sample app which will make use of the PG SDK. This activity will
 * show the usage of Paytm PG SDK API's.
 **/

public class MerchantActivity extends Activity {
    String mId, orderId, custID, industryID, channelID, Amount, website, callback, CheckHashsum;
    String TAG = getClass().getSimpleName();
    ParkConfigClass parkConfigClass;
    String v_type, vehicleno;
    int vacant = 0;
    int Totalsapce = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.merchantapp);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        // initOrderId();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            mId = b.getString("MID", "");
            orderId = b.getString("ORDER_ID", "");
            custID = b.getString("CUST_ID", "");
            industryID = b.getString("INDUSTRY_TYPE_ID", "");
            channelID = b.getString("CHANNEL_ID", "");
            Amount = b.getString("TXN_AMOUNT", "");
            website = b.getString("WEBSITE", "");
            callback = b.getString("CALLBACK_URL", "");
            CheckHashsum = b.getString("CHECKSUMHASH", "");
            v_type = b.getString("VehicleType", "");
            vehicleno = b.getString("vehicle_no1", "");
        }
        onStartTransaction();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //initOrderId();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void onStartTransaction() {
        PaytmPGService Service = PaytmPGService.getStagingService();
        Map<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId);
        paramMap.put("CHANNEL_ID", channelID);
        paramMap.put("CHECKSUMHASH", CheckHashsum);
        paramMap.put("CUST_ID", custID);
        paramMap.put("INDUSTRY_TYPE_ID", industryID);
        paramMap.put("MID", mId);
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("TXN_AMOUNT", Amount);
        paramMap.put("WEBSITE", website);

        PaytmOrder Order = new PaytmOrder(paramMap);
        // Log.e(TAG, "PARAMETER ::: " + paramMap);
        Toast.makeText(getApplicationContext(), "Payment Transaction in onStarting ", Toast.LENGTH_LONG).show();
        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {

                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                        new WebServiceGet(MerchantActivity.this, new HandlerparkdetailResponse(), Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null").execute();
                        // Log.e(TAG, "123455>>" + Constants.VIEW_MOBILE_CONFIG + Constants.PARKG_ID + "/" + "null");
                        updateVacant(parkConfigClass);
                        //Toast.makeText(getApplicationContext(), "Payment Transaction  onTransactionResponse ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void networkNotAvailable() { // If network is not
                        // available, then this
                        // method gets called.
                        Toast.makeText(getApplicationContext(), "Payment Transaction Network Failed ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        Toast.makeText(getApplicationContext(), "Payment Transaction clientAuthenticationFailed ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        Toast.makeText(getApplicationContext(), "Payment Transaction onError Loading WebPage ", Toast.LENGTH_LONG).show();
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        Toast.makeText(MerchantActivity.this, "Back pressed. Transaction cancelled", Toast.LENGTH_LONG).show();
                        cancelBooking();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        // Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void cancelBooking() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MerchantActivity.this);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("parkingList", null);
        Type type = new TypeToken<ArrayList<ModelParkingAreaResponse.DataBean>>() {
        }.getType();

        ArrayList<ModelParkingAreaResponse.DataBean> modelParkingDetailResponseList1 = gson.fromJson(json, type);
        List<Integer> var = new ArrayList<>();
        var.add(Constants.PARKG_ID);

        CancelBookReq cancelBookReq = new CancelBookReq();
        cancelBookReq.setAppId(Constants.APP_ID);
        cancelBookReq.setUserId(Integer.parseInt(custID));
        cancelBookReq.setBookingId(Constants.BOOK_ID);
        cancelBookReq.setParkingId(var);

        String payload = gson.toJson(cancelBookReq);
        Log.d("test", "payload: " + payload);

        new WebServicePost(this, new Handlecancelbook(), Constants.CANCEL_BOOK, payload).execute();
        Log.e(TAG, "CancelURL>>>" + Constants.CANCEL_BOOK);
        Log.e(TAG, "CancelURL>>>" + payload);

    }

    public class Handlecancelbook extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                CancelBookClass cancelBookClass = new Gson().fromJson(response, CancelBookClass.class);
                if (cancelBookClass.getStatus() == 200) {
                    Toast.makeText(MerchantActivity.this, "Your Booking is Canceled", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MerchantActivity.this, ParkingBookActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }
    }

    private class HandlerparkdetailResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d(TAG, "Parking Config::::" + response);

            if (response != null) {
                parkConfigClass = new Gson().fromJson(response, ParkConfigClass.class);
                if (parkConfigClass.getStatus() == 200) {
                    try {
                        Log.d(TAG, "Hour: " + parkConfigClass.getParkingConfigResponseList().get(0).getCarPriceValues().get(0).getFrom());
                        updateVacant(parkConfigClass);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void updateVacant(ParkConfigClass setterViewParkingResponse) {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();
        String url = Constants.INSERT_VACANT;
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(Constants.PARKG_ID);
        setterPayload.setParkingConfigurationId(Constants.Cust_CONFIGURATION_ID);
        Log.e(TAG, "i am in else");

        if (v_type.equals("Car")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1);
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bike")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1);
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);

        } else if (v_type.equals("Bus")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1);
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck());
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);


        } else if (v_type.equals("Truck")) {
            generalBean.setCar(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar());
            generalBean.setBike(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike());
            generalBean.setBus(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus());
            generalBean.setTruck(setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1);
            generalBeanList.add(generalBean);
            vacanT(setterViewParkingResponse, v_type);
        }

        setterPayload.setData(generalBeanList);
        setterPayload.setStatus(2);
        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("VAcant  ", ":::  " + vacant);


        new WebServicePost(MerchantActivity.this, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    private void vacanT(ParkConfigClass setterViewParkingResponse, String vtype) {
        if (vtype.equals("Car")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Bike")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Bus")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus() - 1)
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        } else if (vtype.equals("Truck")) {
            vacant = setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getParkingConfigResponseList().get(0).getGeneral().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getBus()
                    + (setterViewParkingResponse.getParkingConfigResponseList().get(0).getReserved().get(0).getTruck() - 1)

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getRegular().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getEvent().get(0).getTruck()

                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getCar()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBike()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getBus()
                    + setterViewParkingResponse.getParkingConfigResponseList().get(0).getPremium().get(0).getTruck();
        }

    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("VACANT RESPONSE ::", "response: " + response);
            try {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e("TAG", "SUCCESS");
                        Toast.makeText(MerchantActivity.this, "You successfully booking this Parking", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MerchantActivity.this, ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("isFromOTP", true);
                        intent.putExtra("vehicle_no1", vehicleno);
                        intent.putExtra("general1", "Reserved");
                        intent.putExtra("gvtype", Constants.VTYPE_NAME);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }
}
